<?php

use TYPO3\CMS\Core\Cache\Backend\Typo3DatabaseBackend;
use TYPO3\CMS\Core\Utility\GeneralUtility;

if (!defined ('TYPO3_MODE')) {
    die ('Access denied.');
}

$applicationContext = GeneralUtility::getApplicationContext();

if ($applicationContext->isDevelopment()) {
    $GLOBALS['TYPO3_CONF_VARS']['DB']['database'] = 'db292843_233';
    $GLOBALS['TYPO3_CONF_VARS']['DB']['username'] = 'db292843_233';
    $GLOBALS['TYPO3_CONF_VARS']['DB']['password'] = 'v8j<bcjL9:cr';
    $GLOBALS['TYPO3_CONF_VARS']['DB']['host'] = '127.0.0.3';
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['enableDeprecationLog'] = 1;
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['syslogErrorReporting'] = 1;
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['belogErrorReporting'] = 1;
}

if (PHP_SAPI === 'cli') {
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['caching']['cacheConfigurations']['extbase_object']['backend'] = Typo3DatabaseBackend::class;
}