<?php

namespace TYPO3\MbxRealestateHowoge\Helper\Import;

use TYPO3\MbxRealestate\Domain\Model\Immoarea;
use TYPO3\MbxRealestate\Domain\Model\Immoenvironment;
use TYPO3\MbxRealestate\Domain\Model\Immofeature;
use TYPO3\MbxRealestate\Domain\Model\Immoimage;
use TYPO3\MbxRealestate\Domain\Model\Immoobject;
use TYPO3\MbxRealestate\Helper\Import\ImmoDataItem;
use TYPO3\MbxRealestate\Helper\Import\ImporterImmoCsv;

class ImporterImmoHowoge extends ImporterImmoCsv {

    function __construct() {
        parent::__construct();
    }

    /**
     * Retrieves all necessary data from input immo dataset
     * @param array $immoItem
     * @return \TYPO3\MbxRealestate\Helper\Import\ImmoDataItem
     */
    public function prepareItem($immoItem) {

        $arrObjFeatures =
        $arrObjAreas =
        $arrObjImmoimages =
        $arrObjContacts = array();

        $Immoobject = $this->objectManager->get(Immoobject::class);
        $Immoobject->setUnr($unr = $this->getColVarByName('BUK', $immoItem));
        $Immoobject->setWi($wi = $this->getColVarByName('WI', $immoItem));
        $Immoobject->setMe($me = $this->getColVarByName('ME', $immoItem));
        $Immoobject->setFloors($this->getColVarByName('Etage', $immoItem));
        $Immoobject->setFloorsMax($this->getColVarByName('Geschosse', $immoItem));

        $Immoobject->setTimeOfVisit($this->getColVarByName('Besichtigung', $immoItem));

        // set prices
        $Immoobject->setCostNet($this->makeFloat($this->getColVarByName('Kaltmiete', $immoItem)));
        $Immoobject->setCostGross($this->makeFloat($this->getColVarByName('Zielmiete', $immoItem)));
        $Immoobject->setCostAttendant($this->makeFloat($this->getColVarByName('Nebenkosten', $immoItem)));

        // Flächen
        $areaTypes = 'Zimmer1,Zimmer2,Zimmer3,Zimmer4,Zimmer5,Zimmer6,'
                    . 'Raumflaeche_WK01,Raumflaeche_WB01,'
                    . 'Wohnflaeche_neben';

        foreach(explode(',', $areaTypes) as $areaType) {
            if(($area = $this->makeFloat($this->getColVarByName($areaType, $immoItem))) && $area > 0) {

                $areaObj = $this->objectManager->get(Immoarea::class);
                $areaObj->setRoomType($areaType);
                $areaObj->setRoomSize($area);

                $arrObjAreas[] = $areaObj;
            }
        }

        $Immoobject->setAreaGeneral($this->makeFloat($this->getColVarByName('Wohnflaeche_gesamt', $immoItem)));
        $Immoobject->setRooms((float)$this->getColVarByName('Raeume', $immoItem));

        $Immoobject->setYearBuild($this->getColVarByName('Baujahr', $immoItem));
        $Immoobject->setYearRestoration($this->getColVarByName('Sanierungsjahr', $immoItem));
        $Immoobject->setAvailableBy($this->getColVarByName('Verfuegbar_ab', $immoItem));
        $Immoobject->setNorthPoint($this->getColVarByName('Foto_Nordpfeil', $immoItem));
        $Immoobject->setNotice($this->getColVarByName('Besondere1', $immoItem));
        
        $wiActions = (string)$this->getConfiguratioItem('settings.import.wiActions');
        $wiActionsArr = explode(',', $wiActions);
        if(in_array($wi,$wiActionsArr)) {
            $Immoobject->setActionOffer($wi);
        } else {
            $Immoobject->setActionOffer($this->getColVarByName('Aktion', $immoItem));
        }

        foreach(explode(',', $this->getColVarByName('Ausstattung', $immoItem)) as $feature) {

            $featureVal = trim($feature);

            $Immofeature = new Immofeature();
            $Immofeature->setFeatureTitle($featureVal);
            $Immofeature->setFeatureType($Immofeature->getFeatureTypeByTitle());
            $Immofeature->setFeatureValue(1);
            $arrObjFeatures[] = $Immofeature;
        }

        // <editor-fold defaultstate="collapsed" desc="set 'WBS erforderlich' by features">
        $Immoobject->setRequiredWbs(0);

        foreach($arrObjFeatures as $immoFeature) {
            if($immoFeature->getFeatureTitle() == 'WBS erforderlich') {
                $Immoobject->setRequiredWbs(1);
                break;
            }
        }
        // </editor-fold>

        if($Immoobject->getRequiredWbs() == 0) {

            $featureVal = 'Ohne WBS';

            $Immofeature = new Immofeature();
            $Immofeature->setFeatureTitle($featureVal);
            $Immofeature->setFeatureType($Immofeature->getFeatureTypeByTitle());
            $Immofeature->setFeatureValue(1);
            $arrObjFeatures[] = $Immofeature;
        }

        $address = $this->getImmoaddressByAddress(
                preg_replace('/\s+/', ' ', $this->getColVarByName('HSTRASSE', $immoItem)),
                null,
                $this->getColVarByName('HPLZ', $immoItem),
                null    // city not defined in CSV
        );
        $Immoobject->setImmoaddress($address);

        $Immoobject->setHs($address->getHs());
        $Immoobject->setTypeOfDisposition($this->getCommandControllerReference()->immoPluginHelper->getPluginSettings('settings.search.type_of_disposition_default'));
        $Immoobject->setTypeOfUse($this->getCommandControllerReference()->immoPluginHelper->getPluginSettings('settings.search.type_of_use_default'));

        $Immoobject->setFullObjectData(json_encode($this->rowToAssociativeArray($immoItem)));

        // <editor-fold defaultstate="collapsed" desc="set environmentable features">
        $arrObjEnvironments = array();

        foreach(explode(',', $this->getColVarByName('Wohnumfeld', $immoItem)) as $environment) {

            $environmentVal = trim($environment);

            $Immoenvironment = new Immoenvironment();
            $Immoenvironment->setEnvironmentTitle($environmentVal);
            $Immoenvironment->setEnvironmentType($Immoenvironment->getEnvironmentTypeByTitle());
            $arrObjEnvironments[] = $Immoenvironment;
        }
        // </editor-fold>

        // <editor-fold defaultstate="collapsed" desc="handle images">
        $destPathRelative = $this->getPublicImagePath(false);

        $imageConfiguration = array(
            'DEFAULT'   =>  array(
                'range'     =>  '1-10',
                'dir'       =>  'Foto-Scout',
                'col'       =>  'Foto_',
                'desc'      =>  '',
                'ext'       =>  '.jpg',
                'prepend'   =>  'F'
            ),
            'GRUNDRISS' =>  array(
                'dir'       =>  'Gr-Scout',
                'col'       =>  'Foto_Grundriss',
                'desc'      =>  'Grundriss',
                'ext'       =>  '.jpg'
            ),
            'GRUNDRISS_gif' =>  array(
                'dir'       =>  'Gr-WebGross',
                'col'       =>  'Foto_Grundriss',
                'desc'      =>  'Grundriss',
                'ext'       =>  '.gif'
            )
        );

        foreach($imageConfiguration as $imageType => $imageData) {

            $filenames = array();

            if(!empty($imageData['range'])) {

                list($from, $to) = explode('-', $imageData['range']);

                for($i = $from; $i <= $to; $i++) {
                    $filenames[] = (string)$this->getColVarByName($imageData['col'] . $i, $immoItem);
                }

            }  else {

                $filenames = (array)(string)$this->getColVarByName($imageData['col'], $immoItem);
            }

            // temp var to store the state if an titelbild was found (first valid image)
            $hasTitelbild = false;

            foreach($filenames as $fieldContent) {

                $filename = $imageData['prepend'] . $fieldContent . $imageData['ext'];

                if(!empty($fieldContent) && ($copiedTargetFile = $this->copyImmoimageFile($imageData['dir'] . '/' . $filename)) !== FALSE) {

                    $useImageType = $imageType;

                    // if iterating default images and no TITELBILD set already
                    if($imageType == 'DEFAULT' && $hasTitelbild === false) {
                        $useImageType = 'TITELBILD';
                        $hasTitelbild = true;
                    }

                    $Immoimage = $this->objectManager->get(Immoimage::class);
                    $Immoimage->setDescription($imageData['desc']);
                    $Immoimage->setCategory($useImageType);
                    $Immoimage->setFilename(pathinfo($copiedTargetFile, PATHINFO_BASENAME));
                    $Immoimage->setImagePath($destPathRelative);

                    array_push($arrObjImmoimages, $Immoimage);
                }
            }

        }
        // </editor-fold>

        // <editor-fold defaultstate="collapsed" desc="set energy certificate values">
        $certificateType = $this->getColVarByName('Energieausweistyp', $immoItem);
        if (!empty($certificateType)) {
            $Immoobject->setEnergyCertificateType(($certificateType === 'BEKW') ? 1 : 2);

            $thermalCharacteristic = floatval(str_replace(',', '.', str_replace('.', '', $this->getColVarByName('Energieverbrauchkennwert', $immoItem))));
            $Immoobject->setThermalCharacteristic($thermalCharacteristic);

            if ($thermalCharacteristic > 0) {
                $eci = 0;
                $energyClass = null;
                $maxEnergyClasses = count($this->energyClassThresholds);

                foreach ($this->energyClassThresholds as $threshold => $class) {
                    $eci++;
                    if ( $thermalCharacteristic < (float)$threshold || ($thermalCharacteristic >= (float)$threshold && $eci === $maxEnergyClasses)) {
                        $energyClass = $class;
                        break;
                    }
                }
                $Immoobject->setEnergyEfficiencyClass($energyClass);
            }

//            $containsWarmWater = $this->getColVarByName('Warmwasser enhalten', $immoItem);
//            if (!empty($containsWarmWater)) {
//                $Immoobject->setEnergyContainsWarmWater(($containsWarmWater === 'ja') ? 1 : 2);
//            }

            $heatingMehod = $this->getColVarByName('Heizungsart', $immoItem);
            $Immoobject->setThermalHeatingMethod($heatingMehod);
            $energySource = $this->getColVarByName('Heizmedium', $immoItem);
            $Immoobject->setEnergySource($energySource);
        }
        // </editor-fold>

        $immoDataItem = new ImmoDataItem();
        $immoDataItem ->setImmoobject($Immoobject)
                ->setArrImmoarea($arrObjAreas)
                ->setArrImmofeature($arrObjFeatures)
                ->setArrImmoimage($arrObjImmoimages)
                ->setArrImmoenvironment($arrObjEnvironments)
                ;

        return $immoDataItem;
    }

    /**
     * Closes the CSV file.
     *
     * @return \TYPO3\MbxRealestate\Helper\Import\ImporterCsv
     */
    public function closeFile() {

        if(is_resource($this->handle)) {

            fclose($this->handle);
        }

        // delete file because of successful import
        unlink($this->getFile());

        return $this;
    }
}
