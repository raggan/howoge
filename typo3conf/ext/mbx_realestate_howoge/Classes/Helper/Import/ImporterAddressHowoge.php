<?php

namespace TYPO3\MbxRealestateHowoge\Helper\Import;

use TYPO3\MbxRealestate\Domain\Model\Immoaddress;
use TYPO3\MbxRealestate\Domain\Model\Immocontact;
use TYPO3\MbxRealestate\Helper\Import\AddressDataItem;
use TYPO3\MbxRealestate\Helper\Import\ImporterAddressCsv;
use TYPO3\MbxRealestate\Helper\Import\ImporterSave;

class ImporterAddressHowoge extends ImporterAddressCsv {

    function __construct() {
        parent::__construct();
    }

    /**
     * Retrieves all necessary data from input address dataset
     * @param array $addressItem
     * @return \TYPO3\MbxRealestate\Helper\Import\AddressDataItem
     */
    public function prepareItem($addressItem) {

        /**@var Immoaddress $ImmoAddress*/
        $ImmoAddress = $this->objectManager->get('TYPO3\MbxRealestate\Domain\Model\Immoaddress');

        $ImmoAddress->setUnr($this->getColVarByName('Unr', $addressItem));
        $ImmoAddress->setWi($this->getColVarByName('WI', $addressItem));
        $ImmoAddress->setHs($this->getColVarByName('HS', $addressItem));

        $ImmoAddress->setZip($this->getColVarByName('HPLZ', $addressItem));
        $ImmoAddress->setCity($this->getColVarByName('HORT', $addressItem));
        $ImmoAddress->setStreet(preg_replace('/\s+/', ' ', $this->getColVarByName('HSTRASSE', $addressItem)));
        $ImmoAddress->setStreetnr('');

        $geoX = $this->getColVarByName('GEOXKOOR', $addressItem);
        $geoY = $this->getColVarByName('GEOYKOOR', $addressItem);

        $ImmoAddress->setGeoX(empty($geoX) ? ImporterSave::EXPLICITLY_NULL : $geoX);
        $ImmoAddress->setGeoY(empty($geoY) ? ImporterSave::EXPLICITLY_NULL : $geoY);

        $ImmoAddress->setFullObjectData(json_encode($this->rowToAssociativeArray($addressItem)));

        $ImmoAddress->setTimeOfVisit($this->getColVarByName('Besichtigung', $addressItem));

        $districtStr = $this->getColVarByName('WEB_Stadtteil', $addressItem);

        if(empty($districtStr)) {
            $districtStr = 'Sonstige Stadtteile';
        }

        $districtId = $this->getCommandControllerReference()->immoPluginHelper->getDistrictIdByDistrictStr($districtStr);

        $ImmoAddress->setDistrict($districtId);


        // get the customer center by mapping in TS
        $customerCenterStr = (string)$this->getColVarByName('Kuz', $addressItem);
        $customerCenterUid = $this->getCustomerCenterUid($customerCenterStr);
        $customerCenter = $this->getCustomerCenterById($customerCenterUid);

        if(!empty($customerCenter)) {

            $ImmoAddress->setCustomerCenter($customerCenter);
        }

        $arrObjContacts = array();

        // <editor-fold defaultstate="collapsed" desc="general contact person">
        $Immocontact = $this->objectManager->get(Immocontact::class);
        $Immocontact->setContactType('general');
        $Immocontact->setContactName(   $this->getColVarByName('KB_Name', $addressItem));
        $Immocontact->setContactMail(   $this->getColVarByName('t_email', $addressItem));
        $Immocontact->setContactPhone(  $this->getColVarByName('KB_Telefon', $addressItem));
        $Immocontact->setContactFax(    $this->getColVarByName('Kuz_Fax', $addressItem));

        $arrObjContacts[$Immocontact->getContactMd5()] = $Immocontact;

        // </editor-fold>


        // <editor-fold defaultstate="collapsed" desc="facility manager contact person">
        $ImmocontactHm = $this->objectManager->get(Immocontact::class);
        $ImmocontactHm->setContactType('facility');
        $ImmocontactHm->setContactName(   $this->getColVarByName('HM_Name', $addressItem));
        $ImmocontactHm->setContactMobile(  $this->getColVarByName('HM_Telefon', $addressItem));

        $arrObjContacts[$ImmocontactHm->getContactMd5()] = $ImmocontactHm;

        // </editor-fold>

        // <editor-fold defaultstate="collapsed" desc="standby facility manager contact person">
        $contactStandby = $this->getColVarByName('HM_BTF', $addressItem);

        if(!empty($contactStandby)) {
            /**@var Immocontact $ImmocontactHmStandby*/
            $ImmocontactHmStandby = $this->objectManager->get(Immocontact::class);
            $ImmocontactHmStandby->setContactType('facilitystandby');
            $ImmocontactHmStandby->setContactName('Havariedienst');
            $ImmocontactHmStandby->setContactMobile($contactStandby);
            $ImmocontactHmStandby->setContactStreet($ImmoAddress->getStreet());
            $ImmocontactHmStandby->setContactZip($this->getColVarByName('HPLZ', $addressItem));     // add values for HPLZ ...
            $ImmocontactHmStandby->setContactCity($this->getColVarByName('HORT', $addressItem));    // ... and HORT to generate unique contact persons. Else their md5() is not unique by just using contact_name and contact_type as unique fields.

            $arrObjContacts[$ImmocontactHmStandby->getContactMd5()] = $ImmocontactHmStandby;
        }
                
        // </editor-fold>
        
        // <editor-fold defaultstate="collapsed" desc="hirer contact person">
        $ImmocontactHirer = $this->objectManager->get(Immocontact::class);
        $ImmocontactHirer->setContactType('hirer');
        $ImmocontactHirer->setContactName(   $this->getColVarByName('Verm_Name', $addressItem));
        $ImmocontactHirer->setContactMail(   $this->getColVarByName('Verm_Mail', $addressItem));
        $ImmocontactHirer->setContactPhone(  $this->getColVarByName('Verm_Telefon', $addressItem));
        $ImmocontactHirer->setContactStreet( $this->getColVarByName('Kontakt_Strasse', $addressItem));
        $ImmocontactHirer->setContactZip(    $this->getColVarByName('Kontakt_Plz', $addressItem));
        $ImmocontactHirer->setContactCity(   $this->getColVarByName('Kontakt_Ort', $addressItem));

        $arrObjContacts[$ImmocontactHirer->getContactMd5()] = $ImmocontactHirer;

        // </editor-fold>

        
        // <editor-fold defaultstate="collapsed" desc="mobile facility manager contact person">
        $ImmocontactHmMobile = $this->objectManager->get(Immocontact::class);
        $ImmocontactHmMobile->setContactType('facilitymobile');
        $ImmocontactHmMobile->setContactName('Mobile Hausmeister');
        $ImmocontactHmMobile->setContactMobile('030 - 54 64 13 00');

        $arrObjContacts[$ImmocontactHmMobile->getContactMd5()] = $ImmocontactHmMobile;

        // </editor-fold>

        $return = new AddressDataItem();
        $return ->setArrImmocontact($arrObjContacts)
                ->setImmoaddress($ImmoAddress)
                ;

        return $return;

    }

    /**
     * Closes the CSV file.
     *
     * @return \TYPO3\MbxRealestate\Helper\Import\ImporterCsv
     */
    public function closeFile() {

        if(is_resource($this->handle)) {

            fclose($this->handle);
        }

        // delete file because of successful import
        unlink($this->getFile());

        return $this;
    }

    /**
     * Returns the Uid of the immocontact related to the customerCenter configured in TS.
     * Customer center get sanitized by /[^a-zA-Z0-9\_]/
     *
     * @param string|int $customerCenterStr
     * @return null|int
     */
    protected function getCustomerCenterUid($customerCenterStr) {

        $customerCenterStr = preg_replace('/[^a-zA-Z0-9\_]/', '_', $customerCenterStr);

        $mapping = $this->getConfiguratioItem('settings.customerCenter.mapping.');

        foreach($mapping as $mappingKuz => $id) {

            // compare the lower cased strings of the customer center from input file and the TS configuration
            if(strtolower($mappingKuz) == strtolower($customerCenterStr)) {
                return $id;
            }
        }

        return null;
    }
}