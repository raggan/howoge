<?php

if (!defined('TYPO3_MODE')) {
    die('Access denied.');
}

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
    'TYPO3.' . $_EXTKEY, 'Pi1',
    array(
        'Immoobject' => 'list, show',
        'Immoaddress' => 'searchForm',
        'Immocontact' => 'list, show',
        'Cookie' => '',
        'Searchagent' => 'registerForm, unregisterForm',
    ),
    // non-cacheable actions
    array(
        'Immoobject' => 'searchForm, list, listNotepad',
        'Immoaddress' => 'searchForm',
        'Cookie' => 'setCookie, addCookie, removeCookie, dropCookie',
        'Searchagent' => 'registerForm, unregisterForm',
    )
);

/* Register Controller for Sheduler */
$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['extbase']['commandControllers'][] = 'TYPO3\\MbxRealestate\\Controller\\ImportImmoCommandController';
$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['extbase']['commandControllers'][] = 'TYPO3\\MbxRealestate\\Controller\\ImportAddressCommandController';
$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['extbase']['commandControllers'][] = 'TYPO3\\MbxRealestate\\Controller\\SearchagentCommandController';
$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['extbase']['commandControllers'][] = 'TYPO3\\MbxRealestate\\Controller\\ExportImmoCommandController';

$TYPO3_CONF_VARS['FE']['eID_include']['ajaxDispatcher'] = \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('mbx_realestate').'Classes/AjaxDispatcher.php';