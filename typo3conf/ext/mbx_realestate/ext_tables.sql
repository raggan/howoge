#
# Table structure for table 'tx_mbxrealestate_domain_model_immoobject'
#
CREATE TABLE tx_mbxrealestate_domain_model_immoobject (

	uid int(11) NOT NULL auto_increment,
	pid int(11) DEFAULT '0' NOT NULL,

	unr varchar(20) DEFAULT '' NOT NULL,
	wi varchar(20) DEFAULT '' NOT NULL,
	hs varchar(20) DEFAULT '' NOT NULL,
	me varchar(20) DEFAULT '' NOT NULL,
    full_object_data text NOT NULL,	
    rooms double(11,2) DEFAULT '0.00' NOT NULL,
	available_by varchar(255) DEFAULT '' NOT NULL,
	cost_gross double(11,2) DEFAULT '0.00' NOT NULL,
	cost_attendant double(11,2) DEFAULT '0.00' NOT NULL,
	cost_heat double(11,2) DEFAULT '0.00' NOT NULL,
	cost_attendant_contains_heat tinyint(1) unsigned DEFAULT '0' NOT NULL,
	cost_net double(11,2) DEFAULT '0.00' NOT NULL,
	cost_buy double(11,2) DEFAULT '0.00' NOT NULL,
	deposit double(11,2) DEFAULT '0.00' NOT NULL,
	provision tinyint(1) unsigned DEFAULT '0' NOT NULL,
	provision_net double(11,2) DEFAULT '0.00' NOT NULL,
	provision_brut double(11,2) DEFAULT '0.00' NOT NULL,
	year_build int(11) DEFAULT '0' NOT NULL,
	year_restoration int(11) DEFAULT '0' NOT NULL,
	notice_restoration varchar(255) DEFAULT '' NOT NULL,
	floors double(11,2) DEFAULT '0.00' NOT NULL,
	floors_max varchar(255) DEFAULT '' NOT NULL,
	area_general double(11,2) DEFAULT '0.00' NOT NULL,
	area_land double(11,2) DEFAULT '0.00' NOT NULL,
	area_usable double(11,2) DEFAULT '0.00' NOT NULL,
	area_office double(11,2) DEFAULT '0.00' NOT NULL,
	area_storage double(11,2) DEFAULT '0.00' NOT NULL,
	area_all double(11,2) DEFAULT '0.00' NOT NULL,
	number_parking_space int(11) DEFAULT '0' NOT NULL,
	north_point int(11) DEFAULT '0' NOT NULL,
	type_estate varchar(255) DEFAULT '' NOT NULL,
	type_estate_precise varchar(255) DEFAULT '' NOT NULL,
	type_of_use varchar(255) DEFAULT '' NOT NULL,
	type_of_disposition varchar(255) DEFAULT '' NOT NULL,
	type_building varchar(255) DEFAULT '' NOT NULL,
	required_wbs tinyint(1) unsigned DEFAULT '0' NOT NULL,
	title varchar(255) DEFAULT '' NOT NULL,
	is_investment tinyint(1) unsigned DEFAULT '0' NOT NULL,
	business_shares double(11,2) DEFAULT '0.00' NOT NULL,
	business_shares_num tinyint(2) unsigned DEFAULT '0' NOT NULL,
	business_shares_desc varchar(255) DEFAULT '' NOT NULL,
	notice text NOT NULL,
	description text NOT NULL,
	action_offer varchar(255) DEFAULT '' NOT NULL,
    energy_certificate_type tinyint(4) unsigned DEFAULT '0' NOT NULL,
    energy_efficiency_class varchar(20) DEFAULT '' NOT NULL,
    epart_valid_to varchar(20) DEFAULT '' NOT NULL,
    thermal_characteristic double(11,2) DEFAULT '0.00' NOT NULL,
    thermal_characteristic_electricity double(11,2) DEFAULT '0.00' NOT NULL,
    thermal_characteristic_heating double(11,2) DEFAULT '0.00' NOT NULL,
    thermal_heating_method varchar(255) DEFAULT '' NOT NULL,
    energy_source varchar(255) DEFAULT '' NOT NULL,
    energy_contains_warm_water tinyint(4) unsigned DEFAULT '0' NOT NULL,
	immoaddress int(11) unsigned DEFAULT '0' NOT NULL,
	immoarea int(11) unsigned DEFAULT '0' NOT NULL,
	immofeature int(11) unsigned DEFAULT '0' NOT NULL,
	immoimage int(11) unsigned DEFAULT '0' NOT NULL,
	protected_historic_building tinyint(4) unsigned DEFAULT '0' NOT NULL,
	corridor varchar(255) DEFAULT '' NOT NULL,
	splitting int(11) unsigned DEFAULT '0' NOT NULL,
	infrastructure_provision varchar(255) DEFAULT '' NOT NULL,
	build_after varchar(255) DEFAULT '' NOT NULL,
	situation_of_building varchar(255) DEFAULT '' NOT NULL,
	number_flats int(11) unsigned DEFAULT '0' NOT NULL,
	number_companies int(11) unsigned DEFAULT '0' NOT NULL,

	tstamp int(11) unsigned DEFAULT '0' NOT NULL,
	crdate int(11) unsigned DEFAULT '0' NOT NULL,
	cruser_id int(11) unsigned DEFAULT '0' NOT NULL,
	deleted tinyint(4) unsigned DEFAULT '0' NOT NULL,
	hidden tinyint(4) unsigned DEFAULT '0' NOT NULL,
	starttime int(11) unsigned DEFAULT '0' NOT NULL,
	endtime int(11) unsigned DEFAULT '0' NOT NULL,

	t3_origuid int(11) DEFAULT '0' NOT NULL,
	sys_language_uid int(11) DEFAULT '0' NOT NULL,
	l10n_parent int(11) DEFAULT '0' NOT NULL,
	l10n_diffsource mediumblob,

	PRIMARY KEY (uid),
	KEY parent (pid),

	KEY language (l10n_parent,sys_language_uid),
    UNIQUE KEY `unique_immo` (`unr`,`wi`,`hs`,`me`)

);

#
# Table structure for table 'tx_mbxrealestate_domain_model_immocontact'
#
CREATE TABLE tx_mbxrealestate_domain_model_immocontact (

	uid int(11) NOT NULL auto_increment,
	pid int(11) DEFAULT '0' NOT NULL,

	contact_md5 varchar(32) DEFAULT '' NOT NULL,
	contact_name varchar(255) DEFAULT '' NOT NULL,
	contact_street varchar(255) DEFAULT '' NOT NULL,
	contact_streetnr varchar(255) DEFAULT '' NOT NULL,
	contact_zip varchar(255) DEFAULT '' NOT NULL,
	contact_city varchar(255) DEFAULT '' NOT NULL,
	contact_company varchar(255) DEFAULT '' NOT NULL,
	contact_phone varchar(255) DEFAULT '' NOT NULL,
	contact_fax varchar(255) DEFAULT '' NOT NULL,
	contact_mail varchar(255) DEFAULT '' NOT NULL,
	contact_mobile varchar(255) DEFAULT '' NOT NULL,
	contact_url varchar(255) DEFAULT '' NOT NULL,
	contact_type varchar(255) DEFAULT '' NOT NULL,
	contact_desc text NOT NULL,

	tstamp int(11) unsigned DEFAULT '0' NOT NULL,
	crdate int(11) unsigned DEFAULT '0' NOT NULL,
	cruser_id int(11) unsigned DEFAULT '0' NOT NULL,
	deleted tinyint(4) unsigned DEFAULT '0' NOT NULL,
	hidden tinyint(4) unsigned DEFAULT '0' NOT NULL,
	starttime int(11) unsigned DEFAULT '0' NOT NULL,
	endtime int(11) unsigned DEFAULT '0' NOT NULL,

	t3_origuid int(11) DEFAULT '0' NOT NULL,
	sys_language_uid int(11) DEFAULT '0' NOT NULL,
	l10n_parent int(11) DEFAULT '0' NOT NULL,
	l10n_diffsource mediumblob,

	PRIMARY KEY (uid),
	KEY parent (pid),

	KEY language (l10n_parent,sys_language_uid),
    UNIQUE KEY `unique_contact` (`contact_md5`)

);

#
# Table structure for table 'tx_mbxrealestate_domain_model_immoaddress'
#
CREATE TABLE tx_mbxrealestate_domain_model_immoaddress (

	uid int(11) NOT NULL auto_increment,
	pid int(11) DEFAULT '0' NOT NULL,

	immoobjects int(11) unsigned DEFAULT '0' NOT NULL,

	unr varchar(20) DEFAULT '' NOT NULL,
	wi varchar(20) DEFAULT '' NOT NULL,
	hs varchar(20) DEFAULT '' NOT NULL,
	street varchar(255) DEFAULT '' NOT NULL,
	streetnr varchar(255) DEFAULT '' NOT NULL,
	zip varchar(255) DEFAULT '' NOT NULL,
	district int(11) DEFAULT '0' NOT NULL,
	city varchar(255) DEFAULT '' NOT NULL,
        location_desc text NOT NULL,
	contacts int(11) unsigned DEFAULT '0' NOT NULL,
	customer_center int(11) unsigned DEFAULT '0',
	geo_x double(13,11) DEFAULT '0.00000000000' NOT NULL,
	geo_y double(13,11) DEFAULT '0.00000000000' NOT NULL,
	time_of_visit varchar(255) DEFAULT '' NOT NULL,
	full_object_data text NOT NULL,
	tstamp int(11) unsigned DEFAULT '0' NOT NULL,
	crdate int(11) unsigned DEFAULT '0' NOT NULL,
	cruser_id int(11) unsigned DEFAULT '0' NOT NULL,
	deleted tinyint(4) unsigned DEFAULT '0' NOT NULL,
	hidden tinyint(4) unsigned DEFAULT '0' NOT NULL,
	starttime int(11) unsigned DEFAULT '0' NOT NULL,
	endtime int(11) unsigned DEFAULT '0' NOT NULL,

	t3_origuid int(11) DEFAULT '0' NOT NULL,
	sys_language_uid int(11) DEFAULT '0' NOT NULL,
	l10n_parent int(11) DEFAULT '0' NOT NULL,
	l10n_diffsource mediumblob,

	PRIMARY KEY (uid),
	KEY parent (pid),

	KEY language (l10n_parent,sys_language_uid),
    UNIQUE KEY `unique_address` (`unr`,`wi`,`hs`)

);

# Update-Script for settings md5() for old contacts
# UPDATE tx_mbxrealestate_domain_model_immocontact
# SET
#     contact_md5 = md5(CONCAT(lcase(contact_name),
#                     lcase(contact_street),
#                     lcase(contact_streetnr),
#                     lcase(contact_zip),
#                     lcase(contact_city),
#                     lcase(contact_type)))

#
# Table structure for table 'tx_mbxrealestate_domain_model_searchagent'
#
CREATE TABLE tx_mbxrealestate_domain_model_searchagent (

	uid int(11) NOT NULL auto_increment,
	pid int(11) DEFAULT '0' NOT NULL,

	contact_email varchar(255) DEFAULT '' NOT NULL,
	contact_mobile varchar(255) DEFAULT '' NOT NULL,
	search_query text NOT NULL,
	results_sent text NOT NULL,

	tstamp int(11) unsigned DEFAULT '0' NOT NULL,
	crdate int(11) unsigned DEFAULT '0' NOT NULL,
	cruser_id int(11) unsigned DEFAULT '0' NOT NULL,
	deleted tinyint(4) unsigned DEFAULT '0' NOT NULL,
	hidden tinyint(4) unsigned DEFAULT '0' NOT NULL,
	starttime int(11) unsigned DEFAULT '0' NOT NULL,
	endtime int(11) unsigned DEFAULT '0' NOT NULL,

	t3_origuid int(11) DEFAULT '0' NOT NULL,
	sys_language_uid int(11) DEFAULT '0' NOT NULL,
	l10n_parent int(11) DEFAULT '0' NOT NULL,
	l10n_diffsource mediumblob,

	PRIMARY KEY (uid),
	KEY parent (pid),

	KEY language (l10n_parent,sys_language_uid)

);

#
# Table structure for table 'tx_mbxrealestate_domain_model_immoarea'
#
CREATE TABLE tx_mbxrealestate_domain_model_immoarea (

	uid int(11) NOT NULL auto_increment,
	pid int(11) DEFAULT '0' NOT NULL,

	immoobject int(11) unsigned DEFAULT '0' NOT NULL,

	room_type varchar(30) DEFAULT '' NOT NULL,
	room_size double(11,2) DEFAULT '0.00' NOT NULL,
    area_md5 varchar(32) DEFAULT '' NOT NULL,
    immoobject_md5 varchar(32) DEFAULT '' NOT NULL,

	tstamp int(11) unsigned DEFAULT '0' NOT NULL,
	crdate int(11) unsigned DEFAULT '0' NOT NULL,
	cruser_id int(11) unsigned DEFAULT '0' NOT NULL,
	deleted tinyint(4) unsigned DEFAULT '0' NOT NULL,
	hidden tinyint(4) unsigned DEFAULT '0' NOT NULL,
	starttime int(11) unsigned DEFAULT '0' NOT NULL,
	endtime int(11) unsigned DEFAULT '0' NOT NULL,

	t3_origuid int(11) DEFAULT '0' NOT NULL,
	sys_language_uid int(11) DEFAULT '0' NOT NULL,
	l10n_parent int(11) DEFAULT '0' NOT NULL,
	l10n_diffsource mediumblob,

	PRIMARY KEY (uid),
	KEY parent (pid),

	KEY language (l10n_parent,sys_language_uid),
    UNIQUE KEY `unique_area` (area_md5)

);

#
# Table structure for table 'tx_mbxrealestate_domain_model_immofeature'
#
CREATE TABLE tx_mbxrealestate_domain_model_immofeature (

	uid int(11) NOT NULL auto_increment,
	pid int(11) DEFAULT '0' NOT NULL,

	feature_type varchar(40) DEFAULT '' NOT NULL,
	feature_value varchar(255) DEFAULT '' NOT NULL,
	feature_title varchar(255) DEFAULT '' NOT NULL,
	feature_cast varchar(255) DEFAULT '' NOT NULL,
    is_top_feature tinyint(1) unsigned DEFAULT '0' NOT NULL,
    feature_md5 varchar(32) DEFAULT '' NOT NULL,

	tstamp int(11) unsigned DEFAULT '0' NOT NULL,
	crdate int(11) unsigned DEFAULT '0' NOT NULL,
	cruser_id int(11) unsigned DEFAULT '0' NOT NULL,
	deleted tinyint(4) unsigned DEFAULT '0' NOT NULL,
	hidden tinyint(4) unsigned DEFAULT '0' NOT NULL,
	starttime int(11) unsigned DEFAULT '0' NOT NULL,
	endtime int(11) unsigned DEFAULT '0' NOT NULL,

	t3_origuid int(11) DEFAULT '0' NOT NULL,
	sys_language_uid int(11) DEFAULT '0' NOT NULL,
	l10n_parent int(11) DEFAULT '0' NOT NULL,
	l10n_diffsource mediumblob,

	PRIMARY KEY (uid),
	KEY parent (pid),

	KEY language (l10n_parent,sys_language_uid),
    UNIQUE KEY `unique_feature` (feature_md5)

);

#
# Table structure for table 'tx_mbxrealestate_domain_model_immoenvironment'
#
CREATE TABLE tx_mbxrealestate_domain_model_immoenvironment (

	uid int(11) NOT NULL auto_increment,
	pid int(11) DEFAULT '0' NOT NULL,

	immoobject int(11) unsigned DEFAULT '0' NOT NULL,

	environment_type varchar(30) DEFAULT '' NOT NULL,
	environment_title varchar(255) DEFAULT '' NOT NULL,
    environment_md5 varchar(32) DEFAULT '' NOT NULL,
    immoobject_md5 varchar(32) DEFAULT '' NOT NULL,

	tstamp int(11) unsigned DEFAULT '0' NOT NULL,
	crdate int(11) unsigned DEFAULT '0' NOT NULL,
	cruser_id int(11) unsigned DEFAULT '0' NOT NULL,
	deleted tinyint(4) unsigned DEFAULT '0' NOT NULL,
	hidden tinyint(4) unsigned DEFAULT '0' NOT NULL,
	starttime int(11) unsigned DEFAULT '0' NOT NULL,
	endtime int(11) unsigned DEFAULT '0' NOT NULL,

	t3_origuid int(11) DEFAULT '0' NOT NULL,
	sys_language_uid int(11) DEFAULT '0' NOT NULL,
	l10n_parent int(11) DEFAULT '0' NOT NULL,
	l10n_diffsource mediumblob,

	PRIMARY KEY (uid),
	KEY parent (pid),

	KEY language (l10n_parent,sys_language_uid),
    UNIQUE KEY `unique_environment` (environment_md5)

);

#
# Table structure for table 'tx_mbxrealestate_domain_model_immoimage'
#
CREATE TABLE tx_mbxrealestate_domain_model_immoimage (

	uid int(11) NOT NULL auto_increment,
	pid int(11) DEFAULT '0' NOT NULL,

	immoobject int(11) unsigned DEFAULT '0' NOT NULL,

	image_type varchar(255) DEFAULT '' NOT NULL,
	image_path varchar(255) DEFAULT '' NOT NULL,
	filename varchar(255) DEFAULT '' NOT NULL,
	ctime varchar(30) DEFAULT '' NOT NULL,
	description varchar(255) DEFAULT '' NOT NULL,
	title varchar(30) DEFAULT '' NOT NULL,
    category varchar(255) DEFAULT '' NOT NULL,
    image_md5 varchar(32) DEFAULT '' NOT NULL,
    immoobject_md5 varchar(32) DEFAULT '' NOT NULL,

	tstamp int(11) unsigned DEFAULT '0' NOT NULL,
	crdate int(11) unsigned DEFAULT '0' NOT NULL,
	cruser_id int(11) unsigned DEFAULT '0' NOT NULL,
	deleted tinyint(4) unsigned DEFAULT '0' NOT NULL,
	hidden tinyint(4) unsigned DEFAULT '0' NOT NULL,
	starttime int(11) unsigned DEFAULT '0' NOT NULL,
	endtime int(11) unsigned DEFAULT '0' NOT NULL,

	t3_origuid int(11) DEFAULT '0' NOT NULL,
	sys_language_uid int(11) DEFAULT '0' NOT NULL,
	l10n_parent int(11) DEFAULT '0' NOT NULL,
	l10n_diffsource mediumblob,

	PRIMARY KEY (uid),
	KEY parent (pid),

	KEY language (l10n_parent,sys_language_uid),
    UNIQUE KEY `unique_image` (image_md5)

);

#
# Table structure for table 'tx_mbxrealestate_domain_model_immoaddress'
#
CREATE TABLE tx_mbxrealestate_domain_model_immoaddress (

	immoobject  int(11) unsigned DEFAULT '0' NOT NULL,

);

#
# Table structure for table 'tx_mbxrealestate_domain_model_immoarea'
#
CREATE TABLE tx_mbxrealestate_domain_model_immoarea (

	immoobject  int(11) unsigned DEFAULT '0' NOT NULL,

);

#
# Table structure for table 'tx_mbxrealestate_domain_model_immofeature'
#
CREATE TABLE tx_mbxrealestate_domain_model_immofeature (

	immoobject  int(11) unsigned DEFAULT '0' NOT NULL,

);

#
# Table structure for table 'tx_mbxrealestate_immoaddress_immocontact_mm'
#
CREATE TABLE tx_mbxrealestate_immoaddress_immocontact_mm (
    pid int(11) DEFAULT '0' NOT NULL,
	uid_local int(11) unsigned DEFAULT '0' NOT NULL,
	uid_foreign int(11) unsigned DEFAULT '0' NOT NULL,
	sorting int(11) unsigned DEFAULT '0' NOT NULL,
	sorting_foreign int(11) unsigned DEFAULT '0' NOT NULL,

	KEY uid_local (uid_local),
	KEY uid_foreign (uid_foreign),
    UNIQUE KEY `unique_relation` (`uid_local`,`uid_foreign`)
);

#
# Table structure for table 'tx_mbxrealestate_immoobject_immofeature_mm'
#
CREATE TABLE tx_mbxrealestate_immoobject_immofeature_mm (
    pid int(11) DEFAULT '0' NOT NULL,
    uid_local int(11) unsigned DEFAULT '0' NOT NULL,
    uid_foreign int(11) unsigned DEFAULT '0' NOT NULL,
    sorting int(11) unsigned DEFAULT '0' NOT NULL,
    sorting_foreign int(11) unsigned DEFAULT '0' NOT NULL,

    KEY uid_local (uid_local),
    KEY uid_foreign (uid_foreign),
    UNIQUE KEY `unique_relation` (`uid_local`,`uid_foreign`)
);


DROP PROCEDURE IF EXISTS `mbx_realeaste_remove_immoobject`;

DELIMITER //
CREATE PROCEDURE `mbx_realeaste_remove_immoobject` (IN _unr INT, IN _wi INT, IN _hs INT, IN _me INT)
BEGIN

    SET @UNR = _unr;
    SET @WI = _wi;
    SET @HS = _hs;
    SET @ME = _me;

    SET @immoobjectUid = (SELECT `uid` FROM tx_mbxrealestate_domain_model_immoobject WHERE unr = @UNR AND wi = @WI AND hs = @HS AND me = @ME);
    
    IF @immoobjectUid > 0 THEN
        DELETE FROM tx_mbxrealestate_immoobject_immofeature_mm WHERE uid_local = @immoobjectUid;
        DELETE FROM tx_mbxrealestate_immoaddress_immocontact_mm WHERE uid_local = @immoobjectUid;
        DELETE FROM tx_mbxrealestate_domain_model_immoarea WHERE immoobject = @immoobjectUid;
        DELETE FROM tx_mbxrealestate_domain_model_immoenvironment WHERE immoobject = @immoobjectUid;
        DELETE FROM tx_mbxrealestate_domain_model_immoimage WHERE immoobject = @immoobjectUid;
        DELETE FROM tx_mbxrealestate_domain_model_immoobject WHERE uid = @immoobjectUid;
    END IF;

SELECT 1 as Done;

END //

DELIMITER ;

DROP FUNCTION IF EXISTS `mbx_realestate_get_immoobject`;

DELIMITER //
CREATE FUNCTION `mbx_realestate_get_immoobject` (_unr INT, _wi INT, _hs INT, _me INT) 
RETURNS INT(9)
BEGIN

    DECLARE _uid INT(9);

    SELECT uid INTO _uid FROM tx_mbxrealestate_domain_model_immoobject WHERE unr = _unr AND wi = _wi AND hs = _hs AND me = _me;

    RETURN _uid;

END //

DELIMITER ;