<?php

if (!defined('TYPO3_MODE')) {
    die('Access denied.');
}

$ll = 'LLL:EXT:mbx_realestate/Resources/Private/Language/locallang_db.xlf:';

$TCA['tx_mbxrealestate_domain_model_immoobject'] = array(
    'ctrl' => $TCA['tx_mbxrealestate_domain_model_immoobject']['ctrl'],
    'interface' => array(
        'showRecordFieldList' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, unr, wi, hs, me, full_object_data, is_investment, rooms, available_by, cost_gross, cost_attendant, cost_heat, cost_attendant_contains_heat, cost_net, cost_buy, deposit, provision, provision_net, provision_brut, year_build, year_restoration, notice_restoration, floors, floors_max, area_general, area_land, area_usable, area_office, area_storage, area_all, corridor, number_parking_space, north_point, type_estate, type_estate_precise, type_of_use, type_of_disposition, type_building, required_wbs, title, business_shares, business_shares_num, business_shares_desc, notice, description, action_offer, immoaddress, immoarea, immofeature, immoimage, energy_certificate_type, energy_efficiency_class, epart_valid_to, thermal_characteristic, thermal_characteristic_electricity, thermal_characteristic_heating, energy_source, energy_contains_warm_water, splitting, infrastructure_provision, build_after, number_flats, number_companies, situation_of_building',
    ),
    'types' => array(
        '1' => array(
            'showitem' => 'l10n_parent, l10n_diffsource,
					--palette--;' . $ll . 'palette.objectno;paletteCore, immoaddress, action_offer,
					--palette--;' . $ll . 'palette.characteristics;paletteObjectCharacteristics,
                    --palette--;;paletteAreaFeatures,
                    type_building;;paletteBuildingData, north_point,
                    --palette--;' . $ll . 'palette.characteristics;paletteEnergyCertificate,
                --div--;' . $ll . 'tabs.costs,
                    is_investment, cost_gross, cost_net, cost_buy, cost_attendant, cost_heat, deposit, provision, provision_brut, provision_net,
                    business_shares;;paletteBusinessShares,
                --div--;' . $ll . 'tabs.descriptions,
                    title, notice, description;;;richtext::rte_transform[mode=ts_css], immoimage,
				--div--;LLL:EXT:cms/locallang_ttc.xml:tabs.access,
					--palette--;LLL:EXT:cms/locallang_ttc.xml:palette.access;paletteAccess,'
        ),
    ),
    'palettes' => array(
        'paletteCore' => array(
            'showitem' => 'unr, wi, hs, me, full_object_data,
                    --linebreak--, sys_language_uid, hidden,',
            'canNotCollapse' => TRUE
        ),
        'paletteObjectCharacteristics' => array(
            'showitem' => 'rooms, number_flats, number_companies, area_general, area_land, area_usable, area_office, area_storage, area_all, number_parking_space, floors, splitting, available_by, corridor, infrastructure_provision, build_after, situation_of_building,
                    --linebreak--, type_estate, type_estate_precise, required_wbs,
                    --linebreak--, type_of_use, type_of_disposition,',
            'canNotCollapse' => TRUE
        ),
        'paletteBuildingData' => array(
            'showitem' => 'year_build, year_restoration, notice_restoration, floors_max,',
            'canNotCollapse' => TRUE
        ),
        'paletteEnergyCertificate' => array(
            'showitem' => 'energy_certificate_type, energy_efficiency_class, epart_valid_to,
                    --linebreak--, thermal_characteristic,
                    --linebreak--, thermal_characteristic_electricity, thermal_characteristic_heating,
                    --linebreak--, thermal_heating_method, energy_source,
                    --linebreak--, energy_contains_warm_water,',
            'canNotCollapse' => TRUE
        ),
        'paletteAreaFeatures' => array(
            'showitem' => 'immoarea, immofeature,',
            'canNotCollapse' => TRUE
        ),
        'paletteBusinessShares' => array(
            'showitem' => 'business_shares_num, business_shares_desc,',
            'canNotCollapse' => FALSE
        ),
        'paletteAccess' => array(
            'showitem' => 'starttime;LLL:EXT:cms/locallang_ttc.xml:starttime_formlabel,
					endtime;LLL:EXT:cms/locallang_ttc.xml:endtime_formlabel,',
            'canNotCollapse' => TRUE,
        ),
    ),
    'columns' => array(
        'sys_language_uid' => array(
            'exclude' => 1,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.language',
            'config' => array(
                'type' => 'select',
                'foreign_table' => 'sys_language',
                'foreign_table_where' => 'ORDER BY sys_language.title',
                'items' => array(
                    array('LLL:EXT:lang/locallang_general.xlf:LGL.allLanguages', -1),
                    array('LLL:EXT:lang/locallang_general.xlf:LGL.default_value', 0)
                ),
            ),
        ),
        'l10n_parent' => array(
            'displayCond' => 'FIELD:sys_language_uid:>:0',
            'exclude' => 1,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.l18n_parent',
            'config' => array(
                'type' => 'select',
                'items' => array(
                    array('', 0),
                ),
                'foreign_table' => 'tx_mbxrealestate_domain_model_immoobject',
                'foreign_table_where' => 'AND tx_mbxrealestate_domain_model_immoobject.pid=###CURRENT_PID### AND tx_mbxrealestate_domain_model_immoobject.sys_language_uid IN (-1,0)',
            ),
        ),
        'l10n_diffsource' => array(
            'config' => array(
                'type' => 'passthrough',
            ),
        ),
        't3ver_label' => array(
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.versionLabel',
            'config' => array(
                'type' => 'input',
                'size' => 30,
                'max' => 255,
            )
        ),
        'hidden' => array(
            'exclude' => 1,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.hidden',
            'config' => array(
                'type' => 'check',
            ),
        ),
        'starttime' => array(
            'exclude' => 1,
            'l10n_mode' => 'mergeIfNotBlank',
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.starttime',
            'config' => array(
                'type' => 'input',
                'size' => 13,
                'max' => 20,
                'eval' => 'datetime',
                'checkbox' => 0,
                'default' => 0,
                'range' => array(
                    'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
                ),
            ),
        ),
        'endtime' => array(
            'exclude' => 1,
            'l10n_mode' => 'mergeIfNotBlank',
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.endtime',
            'config' => array(
                'type' => 'input',
                'size' => 13,
                'max' => 20,
                'eval' => 'datetime',
                'checkbox' => 0,
                'default' => 0,
                'range' => array(
                    'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
                ),
            ),
        ),
        'unr' => array(
            'exclude' => 0,
            'label' => $ll . 'unr',
            'config' => array(
                'type' => 'input',
                'size' => 8,
                'eval' => 'int'
            ),
        ),
        'wi' => array(
            'exclude' => 0,
            'label' => $ll . 'wi',
            'config' => array(
                'type' => 'input',
                'size' => 8,
                'eval' => 'int'
            ),
        ),
        'hs' => array(
            'exclude' => 0,
            'label' => $ll . 'hs',
            'config' => array(
                'type' => 'input',
                'size' => 5,
                'eval' => 'int'
            ),
        ),
        'me' => array(
            'exclude' => 0,
            'label' => $ll . 'me',
            'config' => array(
                'type' => 'input',
                'size' => 5,
                'eval' => 'trim'
            ),
        ),
        'full_object_data' => array(
            'exclude' => 0,
            'label' => $ll . 'tx_mbxrealestate_domain_model_immoobject.full_object_data',
            'config' => array(
                'type' => 'text',
                'cols' => 40,
                'rows' => 15,
                'eval' => 'trim'
            ),
        ),
        'rooms' => array(
            'exclude' => 0,
            'label' => $ll . 'tx_mbxrealestate_domain_model_immoobject.rooms',
            'config' => array(
                'type' => 'input',
                'size' => 5,
                'eval' => 'double2'
            ),
        ),
        'number_flats' => array(
            'exclude' => 0,
            'label' => $ll . 'tx_mbxrealestate_domain_model_immoobject.number_flats',
            'config' => array(
                'type' => 'input',
                'size' => 5,
                'eval' => 'int'
            ),
        ),
        'number_companies' => array(
            'exclude' => 0,
            'label' => $ll . 'tx_mbxrealestate_domain_model_immoobject.number_companies',
            'config' => array(
                'type' => 'input',
                'size' => 5,
                'eval' => 'int'
            ),
        ),
        'available_by' => array(
            'exclude' => 0,
            'label' => $ll . 'tx_mbxrealestate_domain_model_immoobject.available_by',
            'config' => array(
                'type' => 'input',
                'size' => 7,
                'eval' => 'date',
                'checkbox' => 1,
                'default' => ''
            ),
        ),
        'cost_gross' => array(
            'exclude' => 0,
            'label' => $ll . 'tx_mbxrealestate_domain_model_immoobject.cost_gross',
            'config' => array(
                'type' => 'input',
                'size' => 15,
                'eval' => 'double2'
            ),
        ),
        'cost_attendant' => array(
            'exclude' => 0,
            'label' => $ll . 'tx_mbxrealestate_domain_model_immoobject.cost_attendant',
            'config' => array(
                'type' => 'input',
                'size' => 15,
                'eval' => 'double2'
            ),
        ),
        'cost_heat' => array(
            'exclude' => 0,
            'label' => $ll . 'tx_mbxrealestate_domain_model_immoobject.cost_heat',
            'config' => array(
                'type' => 'input',
                'size' => 15,
                'eval' => 'double2'
            ),
        ),
        'cost_attendant_contains_heat' => array(
            'exclude' => 0,
            'label' => $ll . 'tx_mbxrealestate_domain_model_immoobject.cost_attendant_contains_heat',
            'config' => array(
                'type' => 'check',
                'default' => 0
            ),
        ),
        'cost_net' => array(
            'exclude' => 0,
            'label' => $ll . 'tx_mbxrealestate_domain_model_immoobject.cost_net',
            'config' => array(
                'type' => 'input',
                'size' => 15,
                'eval' => 'double2'
            ),
        ),
        'cost_buy' => array(
            'exclude' => 0,
            'label' => $ll . 'tx_mbxrealestate_domain_model_immoobject.cost_buy',
            'config' => array(
                'type' => 'input',
                'size' => 15,
                'eval' => 'double2'
            ),
        ),
        'deposit' => array(
            'exclude' => 0,
            'label' => $ll . 'tx_mbxrealestate_domain_model_immoobject.deposit',
            'config' => array(
                'type' => 'input',
                'size' => 15,
                'eval' => 'double2'
            ),
        ),
        'provision_brut' => array(
            'exclude' => 0,
            'label' => $ll . 'tx_mbxrealestate_domain_model_immoobject.provision_brut',
            'config' => array(
                'type' => 'input',
                'size' => 15,
                'eval' => 'double2'
            ),
        ),
        'provision_net' => array(
            'exclude' => 0,
            'label' => $ll . 'tx_mbxrealestate_domain_model_immoobject.provision_net',
            'config' => array(
                'type' => 'input',
                'size' => 15,
                'eval' => 'double2'
            ),
        ),
        'protected_historic_building' => array(
            'exclude' => 1,
            'label' => $ll . 'tx_mbxrealestate_domain_model_immoobject.protected_historic_building',
            'config' => array(
                'type' => 'check',
            ),
        ),
        'year_build' => array(
            'exclude' => 0,
            'label' => $ll . 'tx_mbxrealestate_domain_model_immoobject.year_build',
            'config' => array(
                'type' => 'input',
                'size' => 4,
                'eval' => 'int'
            ),
        ),
        'splitting' => array(
            'exclude' => 0,
            'label' => $ll . 'tx_mbxrealestate_domain_model_immoobject.splitting',
            'config' => array(
                'type' => 'input',
                'size' => 6,
                'eval' => 'int'
            ),
        ),
        'year_restoration' => array(
            'exclude' => 0,
            'label' => $ll . 'tx_mbxrealestate_domain_model_immoobject.year_restoration',
            'config' => array(
                'type' => 'input',
                'size' => 4,
                'eval' => 'int'
            ),
        ),
        'notice_restoration' => array(
            'exclude' => 0,
            'label' => $ll . 'tx_mbxrealestate_domain_model_immoobject.notice_restoration',
            'config' => array(
                'type' => 'text',
                'size' => 5,
                'eval' => 'trim'
            ),
        ),
        'floors' => array(
            'exclude' => 0,
            'label' => $ll . 'tx_mbxrealestate_domain_model_immoobject.floors',
            'config' => array(
                'type' => 'input',
                'size' => 4,
                'eval' => 'double2'
            ),
        ),
        'floors_max' => array(
            'exclude' => 0,
            'label' => $ll . 'tx_mbxrealestate_domain_model_immoobject.floors_max',
            'config' => array(
                'type' => 'input',
                'size' => 4,
                'eval' => 'trim'
            ),
        ),
        'infrastructure_provision' => array(
            'exclude' => 0,
            'label' => $ll . 'tx_mbxrealestate_domain_model_immoobject.infrastructure_provision',
            'config' => array(
                'type' => 'input',
                'size' => 4,
                'eval' => 'trim'
            ),
        ),
        'build_after' => array(
            'exclude' => 0,
            'label' => $ll . 'tx_mbxrealestate_domain_model_immoobject.build_after',
            'config' => array(
                'type' => 'input',
                'size' => 5,
                'eval' => 'trim'
            ),
        ),
        'epart_valid_to' => array(
            'exclude' => 0,
            'label' => $ll . 'tx_mbxrealestate_domain_model_immoobject.epart_valid_to',
            'config' => array(
                'type' => 'input',
                'size' => 5,
                'eval' => 'trim'
            ),
        ),
        'situation_of_building' => array(
            'exclude' => 0,
            'label' => $ll . 'tx_mbxrealestate_domain_model_immoobject.situation_of_building',
            'config' => array(
                'type' => 'text',
                'size' => 5,
                'eval' => 'trim'
            ),
        ),
        'area_general' => array(
            'exclude' => 0,
            'label' => $ll . 'tx_mbxrealestate_domain_model_immoobject.area_general',
            'config' => array(
                'type' => 'input',
                'size' => 4,
                'eval' => 'double2'
            ),
        ),
        'area_land' => array(
            'exclude' => 0,
            'label' => $ll . 'tx_mbxrealestate_domain_model_immoobject.area_land',
            'config' => array(
                'type' => 'input',
                'size' => 4,
                'eval' => 'double2'
            ),
        ),
        'area_usable' => array(
            'exclude' => 0,
            'label' => $ll . 'tx_mbxrealestate_domain_model_immoobject.area_usable',
            'config' => array(
                'type' => 'input',
                'size' => 4,
                'eval' => 'double2'
            ),
        ),
        'area_office' => array(
            'exclude' => 0,
            'label' => $ll . 'tx_mbxrealestate_domain_model_immoobject.area_office',
            'config' => array(
                'type' => 'input',
                'size' => 4,
                'eval' => 'double2'
            ),
        ),
        'area_storage' => array(
            'exclude' => 0,
            'label' => $ll . 'tx_mbxrealestate_domain_model_immoobject.area_storage',
            'config' => array(
                'type' => 'input',
                'size' => 4,
                'eval' => 'double2'
            ),
        ),
        'area_all' => array(
            'exclude' => 0,
            'label' => $ll . 'tx_mbxrealestate_domain_model_immoobject.area_all',
            'config' => array(
                'type' => 'input',
                'size' => 4,
                'eval' => 'double2'
            ),
        ),
        'number_parking_space' => array(
            'exclude' => 0,
            'label' => $ll . 'tx_mbxrealestate_domain_model_immoobject.number_parking_space',
            'config' => array(
                'type' => 'input',
                'size' => 4,
                'eval' => 'int'
            ),
        ),
        'north_point' => array(
            'exclude' => 0,
            'label' => $ll . 'tx_mbxrealestate_domain_model_immoobject.north_point',
            'config' => array(
                'type' => 'input',
                'size' => 4,
                'eval' => 'int'
            ),
        ),
        'type_estate' => array(
            'exclude' => 0,
            'label' => $ll . 'tx_mbxrealestate_domain_model_immoobject.type_estate',
            'config' => array(
                'type' => 'input',
                'size' => 24,
                'eval' => 'trim'
            ),
        ),
        'type_estate_precise' => array(
            'exclude' => 0,
            'label' => $ll . 'tx_mbxrealestate_domain_model_immoobject.type_estate_precise',
            'config' => array(
                'type' => 'input',
                'size' => 24,
                'eval' => 'trim'
            ),
        ),
        'type_of_use' => array(
            'exclude' => 0,
            'label' => $ll . 'tx_mbxrealestate_domain_model_immoobject.type_of_use',
            'config' => array(
                'type' => 'input',
                'size' => 14,
                'eval' => 'trim'
            ),
        ),
        'type_of_disposition' => array(
            'exclude' => 0,
            'label' => $ll . 'tx_mbxrealestate_domain_model_immoobject.type_of_disposition',
            'config' => array(
                'type' => 'input',
                'size' => 14,
                'eval' => 'trim'
            ),
        ),
        'type_building' => array(
            'exclude' => 0,
            'label' => $ll . 'tx_mbxrealestate_domain_model_immoobject.type_building',
            'config' => array(
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ),
        ),
        'required_wbs' => array(
            'exclude' => 0,
            'label' => $ll . 'tx_mbxrealestate_domain_model_immoobject.required_wbs',
            'config' => array(
                'type' => 'check',
                'default' => 0
            ),
        ),
        'provision' => array(
            'exclude' => 0,
            'label' => $ll . 'tx_mbxrealestate_domain_model_immoobject.provision',
            'config' => array(
                'type' => 'check',
                'default' => 0
            ),
        ),
        'business_shares' => array(
            'exclude' => 0,
            'label' => $ll . 'tx_mbxrealestate_domain_model_immoobject.business_shares',
            'config' => array(
                'type' => 'input',
                'size' => 15,
                'eval' => 'double2'
            ),
        ),
        'business_shares_num' => array(
            'exclude' => 0,
            'label' => $ll . 'tx_mbxrealestate_domain_model_immoobject.business_shares_num',
            'config' => array(
                'type' => 'input',
                'size' => 8,
                'eval' => 'int'
            ),
        ),
        'business_shares_desc' => array(
            'exclude' => 0,
            'label' => $ll . 'tx_mbxrealestate_domain_model_immoobject.business_shares_desc',
            'config' => array(
                'type' => 'input',
                'size' => 17,
                'eval' => 'trim'
            ),
        ),
        'title' => array(
            'exclude' => 0,
            'label' => $ll . 'tx_mbxrealestate_domain_model_immoobject.title',
            'config' => array(
                'type' => 'input',
                'size' => 32,
                'eval' => 'trim'
            ),
        ),
        'is_investment' => array(
            'exclude' => 0,
            'label' => $ll . 'is_investment',
            'config' => array(
                'type' => 'input',
                'size' => 5,
                'eval' => 'int'
            ),
        ),        
        'notice' => array(
            'exclude' => 0,
            'label' => $ll . 'tx_mbxrealestate_domain_model_immoobject.notice',
            'config' => array(
                'type' => 'text',
                'cols' => 65,
                'rows' => 5,
                'eval' => 'trim'
            ),
        ),
        'corridor' => array(
            'exclude' => 0,
            'label' => $ll . 'tx_mbxrealestate_domain_model_immoobject.corridor',
            'config' => array(
                'type' => 'text',
                'cols' => 65,
                'rows' => 5,
                'eval' => 'trim'
            ),
        ),
        'description' => array(
            'exclude' => 0,
            'label' => $ll . 'tx_mbxrealestate_domain_model_immoobject.description',
            'config' => array(
                'type' => 'text',
                'cols' => 40,
                'rows' => 15,
                'eval' => 'trim'
            ),
        ),
        'action_offer' => array(
            'exclude' => 0,
            'label' => $ll . 'tx_mbxrealestate_domain_model_immoobject.action_offer',
            'config' => array(
                'type' => 'input',
                'size' => 32,
                'eval' => 'trim'
            ),
        ),
        'immoaddress' => array(
            'exclude' => 0,
            'label' => $ll . 'tx_mbxrealestate_domain_model_immoobject.immoaddress',
            'config' => array(
                'type' => 'group',
                'internal_type' => 'db',
                'allowed' => 'tx_mbxrealestate_domain_model_immoaddress',
                'foreign_table' => 'tx_mbxrealestate_domain_model_immoaddress',
                'foreign_table_where' => 'AND tx_mbxrealestate_domain_model_immoaddress.pid=###CURRENT_PID###',
                'minitems' => 0,
                'maxitems' => 1,
            ),
        ),
        'immoarea' => array(
            'exclude' => 0,
            'label' => $ll . 'tx_mbxrealestate_domain_model_immoobject.immoarea',
            'config' => array(
                'type' => 'inline',
                'foreign_table' => 'tx_mbxrealestate_domain_model_immoarea',
                'foreign_field' => 'immoobject',
                'foreign_sortby' => 'room_type',
                'maxitems' => 10,
                'appearance' => array(
                    'collapseAll' => 1,
                    'levelLinksPosition' => 'top',
                    'showSynchronizationLink' => 1,
                    'showPossibleLocalizationRecords' => 1,
                    'showAllLocalizationLink' => 1
                ),
            ),
        ),
        'immofeature' => array(
            'exclude' => 0,
            'label' => $ll . 'tx_mbxrealestate_domain_model_immoobject.immofeature',
            'config' => array(
                'type' => 'inline',
                'foreign_table' => 'tx_mbxrealestate_domain_model_immofeature',
                'foreign_table_where' => 'AND tx_mbxrealestate_domain_model_immofeature.pid=###CURRENT_PID###',
                'MM' => 'tx_mbxrealestate_immoobject_immofeature_mm',
                'maxitems' => 30,
                'appearance' => array(
                    'collapseAll' => 1,
                    'levelLinksPosition' => 'top',
                    'showSynchronizationLink' => 1,
                    'showPossibleLocalizationRecords' => 1,
                    'showAllLocalizationLink' => 1
                ),
            ),
        ),
        'immoimage' => array(
            'exclude' => 0,
            'label' => $ll . 'tx_mbxrealestate_domain_model_immoobject.immoimage',
            'config' => array(
                'type' => 'inline',
                'foreign_table' => 'tx_mbxrealestate_domain_model_immoimage',
                'foreign_field' => 'immoobject',
                'maxitems' => 20,
                'appearance' => array(
                    'collapseAll' => 1,
                    'levelLinksPosition' => 'top',
                    'showSynchronizationLink' => 1,
                    'showPossibleLocalizationRecords' => 1,
                    'showAllLocalizationLink' => 1
                ),
            ),
        ),
		'energy_certificate_type' => array(
			'exclude' => 0,
			'label' => $ll . 'tx_mbxrealestate_domain_model_immoobject.energy_certificate_type',
			'config' => array(
                'type' => 'select',
                'size' => 1,
                'items' => array(
                    array($ll . 'tx_mbxrealestate_domain_model_immoobject.energy_certificate_type.1', 1),
                    array($ll . 'tx_mbxrealestate_domain_model_immoobject.energy_certificate_type.2', 2),
                ),
                'default' => 1,
			),
		),
		'energy_efficiency_class' => array(
			'exclude' => 1,
			'label' => $ll . 'tx_mbxrealestate_domain_model_immoobject.energy_efficiency_class',
			'config' => array(
                'type' => 'select',
                'size' => 1,
                'items' => array(
                    array($ll . 'notSet', 0),
                    array('A+'),
                    array('A'),
                    array('B'),
                    array('C'),
                    array('D'),
                    array('E'),
                    array('F'),
                    array('G'),
                    array('H'),
                ),
                'default' => 0,
			),
		),
		'thermal_characteristic' => array(
			'exclude' => 0,
			'label' => $ll . 'tx_mbxrealestate_domain_model_immoobject.thermal_characteristic',
            'config' => array(
                'type' => 'input',
                'size' => 15,
                'eval' => 'double2'
            ),
		),
		'thermal_characteristic_electricity' => array(
			'exclude' => 1,
			'label' => $ll . 'tx_mbxrealestate_domain_model_immoobject.thermal_characteristic_electricity',
            'config' => array(
                'type' => 'input',
                'size' => 15,
                'eval' => 'double2'
            ),
		),
		'thermal_characteristic_heating' => array(
			'exclude' => 0,
			'label' => $ll . 'tx_mbxrealestate_domain_model_immoobject.thermal_characteristic_heating',
            'config' => array(
                'type' => 'input',
                'size' => 15,
                'eval' => 'double2'
            ),
		),
		'thermal_heating_method' => array(
			'exclude' => 0,
			'label' => $ll . 'tx_mbxrealestate_domain_model_immoobject.thermal_heating_method',
            'config' => array(
                'type' => 'input',
                'size' => 32,
                'eval' => 'trim'
            ),
		),
		'energy_source' => array(
			'exclude' => 0,
			'label' => $ll . 'tx_mbxrealestate_domain_model_immoobject.energy_source',
            'config' => array(
                'type' => 'input',
                'size' => 32,
                'eval' => 'trim'
            ),
		),
		'energy_contains_warm_water' => array(
			'exclude' => 0,
			'label' => $ll . 'tx_mbxrealestate_domain_model_immoobject.energy_contains_warm_water',
			'config' => array(
                'type' => 'select',
                'size' => 1,
                'items' => array(
                    array($ll . 'notSet', 0),
                    array($ll . 'yes', 1),
                    array($ll . 'no', 2),
                ),
                'default' => 0,
			),
		),
    ),
);
?>