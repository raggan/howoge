<?php
if (!defined ('TYPO3_MODE')) {
	die ('Access denied.');
}

$ll = 'LLL:EXT:mbx_realestate/Resources/Private/Language/locallang_db.xlf:';

$TCA['tx_mbxrealestate_domain_model_immocontact'] = array(
	'ctrl' => $TCA['tx_mbxrealestate_domain_model_immocontact']['ctrl'],
	'interface' => array(
		'showRecordFieldList' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, contact_name, contact_street, contact_streetnr, contact_zip, contact_city, contact_company, contact_phone, contact_fax, contact_mail, contact_mobile, contact_url, contact_type, contact_desc, contact_md5',
	),
	'types' => array(
		'1' => array(
            'showitem' => 'l10n_parent, l10n_diffsource,
					contact_name;;paletteCore,;;;;2-2-2, contact_type, contact_company,
                    --palette--;' . $ll . 'palette.address;paletteAddress,
                    contact_phone;;palettePhones,
                    contact_mail;;paletteWeb, contact_desc;;;richtext::rte_transform[mode=ts_css], contact_md5,
				--div--;LLL:EXT:cms/locallang_ttc.xml:tabs.access,
					--palette--;LLL:EXT:cms/locallang_ttc.xml:palette.access;paletteAccess,'
        ),
	),
	'palettes' => array(
		'paletteCore' => array(
			'showitem' => 'sys_language_uid, hidden,',
			'canNotCollapse' => FALSE
		),
		'paletteAddress' => array(
			'showitem' => 'contact_street, contact_streetnr,
					--linebreak--, contact_zip, contact_city,',
			'canNotCollapse' => TRUE
		),
		'palettePhones' => array(
			'showitem' => 'contact_fax, contact_mobile,',
			'canNotCollapse' => FALSE
		),
		'paletteWeb' => array(
			'showitem' => 'contact_url,',
			'canNotCollapse' => FALSE
		),
		'paletteAccess' => array(
			'showitem' => 'starttime;LLL:EXT:cms/locallang_ttc.xml:starttime_formlabel,
					endtime;LLL:EXT:cms/locallang_ttc.xml:endtime_formlabel,',
			'canNotCollapse' => TRUE,
		),
	),
	'columns' => array(
		'sys_language_uid' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.language',
			'config' => array(
				'type' => 'select',
				'foreign_table' => 'sys_language',
				'foreign_table_where' => 'ORDER BY sys_language.title',
				'items' => array(
					array('LLL:EXT:lang/locallang_general.xlf:LGL.allLanguages', -1),
					array('LLL:EXT:lang/locallang_general.xlf:LGL.default_value', 0)
				),
			),
		),
		'l10n_parent' => array(
			'displayCond' => 'FIELD:sys_language_uid:>:0',
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.l18n_parent',
			'config' => array(
				'type' => 'select',
				'items' => array(
					array('', 0),
				),
				'foreign_table' => 'tx_mbxrealestate_domain_model_immocontact',
				'foreign_table_where' => 'AND tx_mbxrealestate_domain_model_immocontact.pid=###CURRENT_PID### AND tx_mbxrealestate_domain_model_immocontact.sys_language_uid IN (-1,0)',
			),
		),
		'l10n_diffsource' => array(
			'config' => array(
				'type' => 'passthrough',
			),
		),
		't3ver_label' => array(
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.versionLabel',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'max' => 255,
			)
		),
		'hidden' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.hidden',
			'config' => array(
				'type' => 'check',
			),
		),
		'starttime' => array(
			'exclude' => 1,
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.starttime',
			'config' => array(
				'type' => 'input',
				'size' => 13,
				'max' => 20,
				'eval' => 'datetime',
				'checkbox' => 0,
				'default' => 0,
				'range' => array(
					'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
				),
			),
		),
		'endtime' => array(
			'exclude' => 1,
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.endtime',
			'config' => array(
				'type' => 'input',
				'size' => 13,
				'max' => 20,
				'eval' => 'datetime',
				'checkbox' => 0,
				'default' => 0,
				'range' => array(
					'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
				),
			),
		),
		'contact_name' => array(
			'exclude' => 0,
			'label' => $ll . 'tx_mbxrealestate_domain_model_immocontact.contact_name',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'contact_street' => array(
			'exclude' => 0,
			'label' => $ll . 'street',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'contact_streetnr' => array(
			'exclude' => 0,
			'label' => $ll . 'streetnr',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'contact_zip' => array(
			'exclude' => 0,
			'label' => $ll . 'zip',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'contact_city' => array(
			'exclude' => 0,
			'label' => $ll . 'city',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'contact_company' => array(
			'exclude' => 0,
			'label' => $ll . 'tx_mbxrealestate_domain_model_immocontact.contact_company',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'contact_phone' => array(
			'exclude' => 0,
			'label' => $ll . 'tx_mbxrealestate_domain_model_immocontact.contact_phone',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'contact_fax' => array(
			'exclude' => 0,
			'label' => $ll . 'tx_mbxrealestate_domain_model_immocontact.contact_fax',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'contact_mail' => array(
			'exclude' => 0,
			'label' => $ll . 'tx_mbxrealestate_domain_model_immocontact.contact_mail',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'contact_mobile' => array(
			'exclude' => 0,
			'label' => $ll . 'tx_mbxrealestate_domain_model_immocontact.contact_mobile',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'contact_url' => array(
			'exclude' => 0,
			'label' => $ll . 'tx_mbxrealestate_domain_model_immocontact.contact_url',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'contact_type' => array(
			'exclude' => 0,
			'label' => $ll . 'tx_mbxrealestate_domain_model_immocontact.contact_type',
			'config' => array(
                'type' => 'select',
                'size' => 1,
				'maxitems' => 1,
                'items' => array(),
                'itemsProcFunc' => 'Tx_MbxRealestate_Hooks_ItemsProcFunc->user_contactTypeOptions',
			),
		),
		'contact_desc' => array(
			'exclude' => 1,
			'l10n_mode' => 'noCopy',
			'label' => 'LLL:EXT:cms/locallang_ttc.xml:bodytext_formlabel',
			'config' => array(
				'type' => 'text',
				'cols' => 30,
				'rows' => 5,
                'eval' => 'trim',
            ),
		),
		'contact_md5' => array(
			'exclude' => 1,
			'label' => $ll . 'tx_mbxrealestate_domain_model_immocontact.contact_md5',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'md5',
                'default' => time(),
			),
		),
	),
);

?>