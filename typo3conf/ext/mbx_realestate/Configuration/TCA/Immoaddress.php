<?php

if (!defined('TYPO3_MODE')) {
    die('Access denied.');
}

$ll = 'LLL:EXT:mbx_realestate/Resources/Private/Language/locallang_db.xlf:';

$TCA['tx_mbxrealestate_domain_model_immoaddress'] = array(
    'ctrl' => $TCA['tx_mbxrealestate_domain_model_immoaddress']['ctrl'],
    'interface' => array(
        'showRecordFieldList' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, unr, wi, hs, full_object_data, street, streetnr, zip, district, city, location_desc, contacts, customer_center, immoobjects, geo_x, geo_y, time_of_visit',
    ),
    'types' => array(
        '1' => array(
            'showitem' => 'l10n_parent, l10n_diffsource,
					--palette--;' . $ll . 'palette.objectno;paletteCore,
                    --palette--;' . $ll . 'palette.address;paletteAddress,
                    --palette--;' . $ll . 'palette.geo;paletteGeo, location_desc,
				--div--;' . $ll . 'tabs.contacts, contacts, customer_center,
				--div--;' . $ll . 'tabs.immoobjects, immoobjects,
				--div--;LLL:EXT:cms/locallang_ttc.xml:tabs.access,
					--palette--;LLL:EXT:cms/locallang_ttc.xml:palette.access;paletteAccess,'
        ),
    ),
    'palettes' => array(
		'paletteCore' => array(
			'showitem' => 'unr, wi, hs,
                    --linebreak--, sys_language_uid, hidden,',
			'canNotCollapse' => TRUE
		),
		'paletteAddress' => array(
			'showitem' => 'street, streetnr,
                    --linebreak--, district,
					--linebreak--, zip, city,
					--linebreak--, time_of_visit,',
			'canNotCollapse' => TRUE
		),
		'paletteGeo' => array(
			'showitem' => 'geo_x,
                    --linebreak--, geo_y,',
			'canNotCollapse' => TRUE
		),
		'paletteAccess' => array(
			'showitem' => 'starttime;LLL:EXT:cms/locallang_ttc.xml:starttime_formlabel,
					endtime;LLL:EXT:cms/locallang_ttc.xml:endtime_formlabel,',
			'canNotCollapse' => TRUE,
		),
    ),
    'columns' => array(
        'sys_language_uid' => array(
            'exclude' => 1,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.language',
            'config' => array(
                'type' => 'select',
                'foreign_table' => 'sys_language',
                'foreign_table_where' => 'ORDER BY sys_language.title',
                'items' => array(
                    array('LLL:EXT:lang/locallang_general.xlf:LGL.allLanguages', -1),
                    array('LLL:EXT:lang/locallang_general.xlf:LGL.default_value', 0)
                ),
            ),
        ),
        'l10n_parent' => array(
            'displayCond' => 'FIELD:sys_language_uid:>:0',
            'exclude' => 1,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.l18n_parent',
            'config' => array(
                'type' => 'select',
                'items' => array(
                    array('', 0),
                ),
                'foreign_table' => 'tx_mbxrealestate_domain_model_immoaddress',
                'foreign_table_where' => 'AND tx_mbxrealestate_domain_model_immoaddress.pid=###CURRENT_PID### AND tx_mbxrealestate_domain_model_immoaddress.sys_language_uid IN (-1,0)',
            ),
        ),
        'l10n_diffsource' => array(
            'config' => array(
                'type' => 'passthrough',
            ),
        ),
        't3ver_label' => array(
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.versionLabel',
            'config' => array(
                'type' => 'input',
                'size' => 30,
                'max' => 255,
            )
        ),
        'hidden' => array(
            'exclude' => 1,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.hidden',
            'config' => array(
                'type' => 'check',
            ),
        ),
        'starttime' => array(
            'exclude' => 1,
            'l10n_mode' => 'mergeIfNotBlank',
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.starttime',
            'config' => array(
                'type' => 'input',
                'size' => 13,
                'max' => 20,
                'eval' => 'datetime',
                'checkbox' => 0,
                'default' => 0,
                'range' => array(
                    'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
                ),
            ),
        ),
        'endtime' => array(
            'exclude' => 1,
            'l10n_mode' => 'mergeIfNotBlank',
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.endtime',
            'config' => array(
                'type' => 'input',
                'size' => 13,
                'max' => 20,
                'eval' => 'datetime',
                'checkbox' => 0,
                'default' => 0,
                'range' => array(
                    'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
                ),
            ),
        ),
        'unr' => array(
            'exclude' => 0,
            'label' => $ll . 'unr',
            'config' => array(
                'type' => 'input',
                'size' => 8,
                'eval' => 'int'
            ),
        ),
        'wi' => array(
            'exclude' => 0,
            'label' => $ll . 'wi',
            'config' => array(
                'type' => 'input',
                'size' => 8,
                'eval' => 'int'
            ),
        ),
        'hs' => array(
            'exclude' => 0,
            'label' => $ll . 'hs',
            'config' => array(
                'type' => 'input',
                'size' => 5,
                'eval' => 'int'
            ),
        ),
        'full_object_data' => array(
            'exclude' => 0,
            'label' => $ll . 'tx_mbxrealestate_domain_model_immooaddress.full_object_data',
            'config' => array(
                'type' => 'text',
                'cols' => 40,
                'rows' => 15,
                'eval' => 'trim'
            ),
        ),
        'street' => array(
            'exclude' => 0,
            'label' => $ll . 'street',
            'config' => array(
                'type' => 'input',
                'size' => 20,
                'eval' => 'trim'
            ),
        ),
        'streetnr' => array(
            'exclude' => 0,
            'label' => $ll . 'streetnr',
            'config' => array(
                'type' => 'input',
                'size' => 5,
                'eval' => 'trim'
            ),
        ),
        'district' => array(
            'exclude' => 0,
            'label' => $ll . 'tx_mbxrealestate_domain_model_immoaddress.district',
            'config' => array(
                'type' => 'select',
                'size' => 1,
                'items' => array(),
                'itemsProcFunc' => 'Tx_MbxRealestate_Hooks_ItemsProcFunc->user_districtOptions',
            ),
        ),
        'zip' => array(
            'exclude' => 0,
            'label' => $ll . 'zip',
            'config' => array(
                'type' => 'input',
                'size' => 5,
                'eval' => 'trim'
            ),
        ),
        'city' => array(
            'exclude' => 0,
            'label' => $ll . 'city',
            'config' => array(
                'type' => 'input',
                'size' => 20,
                'eval' => 'trim'
            ),
        ),
        'location_desc' => array(
            'exclude' => 0,
            'label' => $ll . 'tx_mbxrealestate_domain_model_immoaddress.location_desc',
            'config' => array(
                'type' => 'text',
                'cols' => 40,
                'rows' => 15,
                'eval' => 'trim'
            ),
        ),
        'contacts' => array(
            'exclude' => 0,
            'label' => $ll . 'tx_mbxrealestate_domain_model_immoaddress.contacts',
            'config' => array(
				'type' => 'select',
                'foreign_table' => 'tx_mbxrealestate_domain_model_immocontact',
                'foreign_table_where' => 'AND contact_type NOT LIKE "customercenter" AND tx_mbxrealestate_domain_model_immocontact.pid=###CURRENT_PID###',
                'MM' => 'tx_mbxrealestate_immoaddress_immocontact_mm',
                'size' => 10,
                'maxitems' => 10,
				'wizards' => array(
                    '_PADDING' => 1,
                    '_VERTICAL' => 1,
                    'edit' => array(
                        'type' => 'popup',
                        'title' => 'Edit',
                        'script' => 'wizard_edit.php',
                        'icon' => 'edit2.gif',
                        'popup_onlyOpenIfSelected' => 1,
                        'JSopenParams' => 'height=350,width=580,status=0,menubar=0,scrollbars=1',
					),
                    'add' => Array(
                        'type' => 'script',
                        'title' => 'Create new',
                        'icon' => 'add.gif',
                        'params' => array(
                            'table' => 'tx_mbxrealestate_domain_model_immocontact',
                            'pid' => '###CURRENT_PID###',
                            'setValue' => 'prepend'
                        ),
                        'script' => 'wizard_add.php',
                    ),
                ),
            ),
        ),
        'customer_center' => array(
            'exclude' => 0,
            'label' => $ll . 'tx_mbxrealestate_domain_model_immoaddress.customer_center',
            'config' => array(
                'type' => 'select',
                'foreign_table' => 'tx_mbxrealestate_domain_model_immocontact',
                'foreign_table_where' => 'AND contact_type LIKE "customercenter"',
                'minitems' => 0,
                'maxitems' => 1,
                'items' => array(
                    array(),
                )
            ),
        ),
        'immoobjects' => array(
            'exclude' => 0,
            'label' => 'LLL:EXT:mbx_realestate_test/Resources/Private/Language/locallang_db.xlf:tx_mbxrealestatetest_domain_model_immoaddress.immoobjects',
            'config' => array(
				'type' => 'group',
				'internal_type' => 'db',
                'allowed' => 'tx_mbxrealestate_domain_model_immoobject',
                'foreign_table' => 'tx_mbxrealestate_domain_model_immoobject',
                'foreign_table_where' => 'AND tx_mbxrealestate_domain_model_immoobject.pid=###CURRENT_PID###',
                'foreign_field' => 'immoaddress',
                'autoSizeMax' => 100,
                'maxitems' => 100,
                'readOnly' => true,
            ),
        ),
        'geo_x' => array(
            'exclude' => 0,
            'label' => $ll . 'tx_mbxrealestate_domain_model_immoaddress.geo_x',
            'config' => array(
                'type' => 'input',
                'size' => 30,
                'eval' => 'double6'
            ),
        ),
        'geo_y' => array(
            'exclude' => 0,
            'label' => $ll . 'tx_mbxrealestate_domain_model_immoaddress.geo_y',
            'config' => array(
                'type' => 'input',
                'size' => 30,
                'eval' => 'double6'
            ),
        ),
        'time_of_visit' => array(
            'exclude' => 0,
            'label' => $ll . 'tx_mbxrealestate_domain_model_immoaddress.time_of_visit',
            'config' => array(
                'type' => 'input',
                'cols' => 60,
                'eval' => 'trim'
            ),
        ),
    ),
);
?>
