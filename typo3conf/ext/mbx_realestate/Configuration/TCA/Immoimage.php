<?php
if (!defined ('TYPO3_MODE')) {
	die ('Access denied.');
}

$ll = 'LLL:EXT:mbx_realestate/Resources/Private/Language/locallang_db.xlf:';

$TCA['tx_mbxrealestate_domain_model_immoimage'] = array(
	'ctrl' => $TCA['tx_mbxrealestate_domain_model_immoimage']['ctrl'],
	'interface' => array(
		'showRecordFieldList' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, image_type, image_path, filename, ctime, description, title, category, image_md5',
	),
	'types' => array(
		'1' => array(
            'showitem' => 'l10n_parent, l10n_diffsource,
					category;;paletteCore,;;;;2-2-2, filename, image_path, image_type, ctime, image_md5,
				--div--;LLL:EXT:cms/locallang_ttc.xml:tabs.access,
					--palette--;LLL:EXT:cms/locallang_ttc.xml:palette.access;paletteAccess,'
        ),
	),
	'palettes' => array(
		'paletteCore' => array(
			'showitem' => 'sys_language_uid, hidden,
					--linebreak--, title,
					--linebreak--, description,',
			'canNotCollapse' => FALSE
		),
		'paletteAccess' => array(
			'showitem' => 'starttime;LLL:EXT:cms/locallang_ttc.xml:starttime_formlabel,
					endtime;LLL:EXT:cms/locallang_ttc.xml:endtime_formlabel,',
			'canNotCollapse' => TRUE,
		),
	),
	'columns' => array(
		'sys_language_uid' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.language',
			'config' => array(
				'type' => 'select',
				'foreign_table' => 'sys_language',
				'foreign_table_where' => 'ORDER BY sys_language.title',
				'items' => array(
					array('LLL:EXT:lang/locallang_general.xlf:LGL.allLanguages', -1),
					array('LLL:EXT:lang/locallang_general.xlf:LGL.default_value', 0)
				),
			),
		),
		'l10n_parent' => array(
			'displayCond' => 'FIELD:sys_language_uid:>:0',
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.l18n_parent',
			'config' => array(
				'type' => 'select',
				'items' => array(
					array('', 0),
				),
				'foreign_table' => 'tx_mbxrealestate_domain_model_immoimage',
				'foreign_table_where' => 'AND tx_mbxrealestate_domain_model_immoimage.pid=###CURRENT_PID### AND tx_mbxrealestate_domain_model_immoimage.sys_language_uid IN (-1,0)',
			),
		),
		'l10n_diffsource' => array(
			'config' => array(
				'type' => 'passthrough',
			),
		),
		't3ver_label' => array(
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.versionLabel',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'max' => 255,
			)
		),
		'hidden' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.hidden',
			'config' => array(
				'type' => 'check',
			),
		),
		'starttime' => array(
			'exclude' => 1,
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.starttime',
			'config' => array(
				'type' => 'input',
				'size' => 13,
				'max' => 20,
				'eval' => 'datetime',
				'checkbox' => 0,
				'default' => 0,
				'range' => array(
					'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
				),
			),
		),
		'endtime' => array(
			'exclude' => 1,
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.endtime',
			'config' => array(
				'type' => 'input',
				'size' => 13,
				'max' => 20,
				'eval' => 'datetime',
				'checkbox' => 0,
				'default' => 0,
				'range' => array(
					'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
				),
			),
		),
		'image_type' => array(
			'exclude' => 0,
			'label' => $ll . 'tx_mbxrealestate_domain_model_immoimage.image_type',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim',
                'readOnly' => true,
			),
		),
		'image_path' => array(
			'exclude' => 1,
			'label' => $ll . 'tx_mbxrealestate_domain_model_immoimage.image_path',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim',
                'default' => 'uploads/tx_mbxrealestate/',
                'readOnly' => true,
			),
		),
		'filename' => array(
			'exclude' => 0,
			'label' => $ll . 'tx_mbxrealestate_domain_model_immoimage.filename',
			'config' => array(
				'type' => 'group',
				'internal_type' => 'file',
				'allowed' => $GLOBALS['TYPO3_CONF_VARS']['GFX']['imagefile_ext'],
				'max_size' => $GLOBALS['TYPO3_CONF_VARS']['BE']['maxFileSize'],
				'uploadfolder' => 'uploads/tx_mbxrealestate/',
				'show_thumbs' => 1,
				'size' => 1,
				'minitems' => 1,
				'maxitems' => 1,
			),
		),
		'ctime' => array(
			'exclude' => 1,
			'label' => $ll . 'tx_mbxrealestate_domain_model_immoimage.ctime',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'category' => array(
			'exclude' => 0,
			'label' => $ll . 'tx_mbxrealestate_domain_model_immoimage.category',
			'config' => array(
				'type' => 'select',
				'size' => 1,
				'items' => array(
					array($ll . 'tx_mbxrealestate_domain_model_immoimage.category.DEFAULT', 'DEFAULT'),
					array($ll . 'tx_mbxrealestate_domain_model_immoimage.category.TITELBILD', 'TITELBILD'),
					array($ll . 'tx_mbxrealestate_domain_model_immoimage.category.GRUNDRISS', 'GRUNDRISS'),
				)
			),
		),
		'description' => array(
			'exclude' => 0,
			'label' => $ll . 'tx_mbxrealestate_domain_model_immoimage.description',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'title' => array(
			'exclude' => 0,
			'label' => $ll . 'tx_mbxrealestate_domain_model_immoimage.title',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'immoobject' => array(
			'config' => array(
				'type' => 'passthrough',
			),
		),
		'image_md5' => array(
			'exclude' => 1,
			'label' => $ll . 'tx_mbxrealestate_domain_model_immocontact.image_md5',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'md5',
                'default' => time(),
			),
		),
	),
);

?>