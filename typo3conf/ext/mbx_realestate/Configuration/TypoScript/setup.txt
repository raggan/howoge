page.includeCSS {
    mbx_realestate_css = {$plugin.tx_mbxrealestate.view.cssPath}mbx_realestate.css
}
page.includeJSFooterlibs {
    mbx_realestate_js = {$plugin.tx_mbxrealestate.view.jsPath}mbx_realestate.js
}

plugin.tx_mbxrealestate {
    mvc.callDefaultActionIfActionCantBeResolved = 1
    view {
        templateRootPaths {
            100 = {$plugin.tx_mbxrealestate.view.templateRootPath}
            110 = fileadmin/default/templates/extensions/mbx_realestate/Templates
        }
        partialRootPaths {
            100 = {$plugin.tx_mbxrealestate.view.partialRootPath}
            110 = fileadmin/default/templates/extensions/mbx_realestate/Partials
        }
        layoutRootPaths {
            100 = {$plugin.tx_mbxrealestate.view.layoutRootPath}
            110 = fileadmin/default/templates/extensions/mbx_realestate/Layouts
        }

    }
    persistence {
            storagePid = {$plugin.tx_mbxrealestate.persistence.storagePid}
    }
    features {
            # uncomment the following line to enable the new Property Mapper.
            # rewrittenPropertyMapper = 1
    }
    settings {
        # use %field|4|-|0% to left-pad (0) the value of %field% with the character '-' up to a length of 4
        uniqeImmoIdScheme = %unr%/%wi%/%me%
        # prepare CONCAT SQL string
        uniqeImmoIdSchemeSQL = %unr%,"/",%wi%,"/",%me%
        import {
            immo {
                inputPath = fileadmin/immoUploadFiles/
                inputFile = immo.xml
                inputType = XML
                importClass = XML
                importType = full
            }
            address {

            }
            sourceImagePath = fileadmin/immoUploadFiles/images/
            publicImagePath = fileadmin/immo-images/
            backupFile = 0
            backupDir = fileadmin/immoUploadFiles/backups/
            log {
                importImmo {
                    logfile = {$plugin.tx_mbxrealestate.path.ext}log/immoobject-import.log
                }
                importAddress {
                    logfile = {$plugin.tx_mbxrealestate.path.ext}log/immoaddress-import.log
                }
            }
            types {
                xml {
                    xpathImmoNode = /openimmo/anbieter/immobilie
                }
            }
            skipContacts = customercenter
        }
        export {
            immo {
                
                outputPath = fileadmin/immoUploadFiles/export/
                outputFile = immo-export.xml
                outputType = XML
                # you can define your own exportClass to use. Define the class with namespace.
                #exportClass = XML
                transfer = useDefault
                # use this to override the default settings
                # transfer {
                #     host = 
                # }
                notification {
                # define the MBX communication to monitor the exports
                   # mbx_cnt {
                   #     task = 
                   #     secret = 
                   # }
                }
            }
            backupFile = 1
            backupDir = fileadmin/immoUploadFiles/backups/
            log {
                exportImmo {
                    logfile = {$plugin.tx_mbxrealestate.path.ext}log/immoobject-export.log
                }
            }
            transfer {
                type = FTP
                host = 
                port = 21
                user = 
                pass = 
            }
        }
        images {
            #the category of the image to display as preview image onto lists etc. for the immoobject
            previewCategory = TITELBILD
            floorplanCategory = GRUNDRISS
        }
        # configure the typeNum of the print layout
        printPage =
        # those are the default values set into the searchforms when no session value or sth. else is set
        defaultSearch {
            rooms_from = 2
            rooms_to = 3
            area_from = 50
            area_to = 70
        }
        defaultSearchStorage {
                # list your storage search here
                # myStorageName {
                #   rooms_from = 5
                # }
        }
        search {
            perPage = 10
            perPageSteps = 10,20,30,50
            listPage = # configure target page
            detailPage = # configure target page
            notelistePage = # configure target page
            field_types_of_use {
                WOHNEN = Wohnen
                GEWERBE = Gewerbe
            }
            field_types_of_disposition {
                MIETEN = Mieten
                KAUFEN = Kaufen
            }
            type_of_use_default = WOHNEN
            type_of_disposition_default = MIETEN
            type_of_disposition_rent = MIETEN
            type_of_disposition_buy = KAUFEN
            fieldsConfiguration {
                rooms {
                    min = 1
                    max = 6
                }
                area {
                    min = 0
                    max = 200
                }
                cost_gross {
                    min = 0
                    max = 2000
                    steps = 10
                }
                cost_buy {
                    min = 20000
                    max = 60000
                    steps = 250
                }
            }
            yearBuildList {
                0 {
                    val = -1950
                    title = bis 1950
                }
                1 {
                    val = 1951-1989
                    title = 1951-1989
                }
                2 {
                    val = 1990-
                    title = ab 1990
                }
            }
            listSort {
                costNetAsc {
                    label = Kaltmiete aufsteigend
                    sort = cost_net ASC
                }
                costNetDesc {
                    label = Kaltmiete absteigend
                    sort = cost_net DESC
                }
                costGrossAsc {
                    label = Warmmiete aufsteigend
                    sort = cost_gross ASC
                }
                costGrossDesc {
                    label = Warmmiete absteigend
                    sort = cost_gross DESC
                }
                areaAsc {
                    label = Fläche aufsteigend
                    sort = area_general ASC
                }
                areaDesc {
                    label = Fläche absteigend
                    sort = area_general DESC
                }
            }
        }
        searchAgent {
            #logfile
            logfile = {$plugin.tx_mbxrealestate.path.ext}log/searchagent.log
            #must be set for creating urls for searchagent in BE/commandController
            targetPage =
            notification {
                email {
                    templateFilePath = {$plugin.tx_mbxrealestate.view.extensionsRootPath}
                    templateFileNameHtml = /formhandler/searchagent-notification-html.html
                    templateFileNamePlain = /formhandler/searchagent-notification-plain.html
                    subject = Neue Inserate für Ihren Suchauftrag
                }
            }
            subscribe {
                email {
                    templateFilePath = {$plugin.tx_mbxrealestate.view.extensionsRootPath}
                    templateFileNameSubscribeDataHtml = /formhandler/searchagent-subscribe-data-html.html
                    templateFileNameSubscribeDataPlain = /formhandler/searchagent-subscribe-data-plain.html
                }
            }
            # configure the sender email address
            email = {$emails.admin}
        }
        notepad {
            send {
                email {
                    templateFilePath = {$plugin.tx_mbxrealestate.view.extensionsRootPath}
                    templateFileNameSendDataHtml = /formhandler/notepad-send-html.html
                    templateFileNameSendDataPlain = /formhandler/notepad-send-plain.html
                }
            }
        }
        contactTypes {
        }
        offers {
        }
        districts {
        }
        districtZips {
        }
        districtMappingType = ts
        customerCenter {
            # the value of the DB field representing the immocontact as customer center
            type = customercenter
            # the mapping (districts|numeric)
            mappingType = districts
            mapping {
                # mappingType=districts -> district id to customer center id (uid immocontact)
                # mappingType=numeric -> a conversion of incoming IDs to the customer center without using district
            }
        }
        customSearchOptions {
            # configure predefined immolists
            # e.g.

            #topoffers {
            #    title = Our Topoffers (displayed in BE)
            #    options {
            #        rooms_from = 2
            #        rooms_to = 3
            #        area_from = 60
            #        area_to = 80
            #        limit = 3
            #    }
            #}
        }

        # Predefined search options for immoaddresses
        customAddressSearchOptions {
            #default {
            #    title = alle Kontakte
            #}
        }
        apis {
            google {
                maps {
                    #key = your-google-maps-api-key-here
                }
            }
        }
    }
}

# Define your custom paginate view template here (else the widget will fall back to the default path and searches in the current extension for the template file)
#plugin.tx_mbxrealestate {
#    view {
#        widget.TYPO3\MbxRealestate\ViewHelpers\Widget\PaginateViewHelper.templateRootPath =
#    }
#}

plugin.tx_mbxcleaner {
    
    tasks {
        mbxRealEstate_cleanSearchAgents {
            type = DB
            enabled = 1
            settings {
                table = tx_mbxrealestate_domain_model_searchagent
                where = FROM_UNIXTIME(crdate) < DATE_SUB(NOW(), INTERVAL 12 MONTH)
            }            
        }
    }
}

lib.searchagentRegisterForm < plugin.tx_formhandler_pi1
lib.searchagentRegisterForm.settings {
    langFile.1 = fileadmin/default/templates/extensions/mbx_realestate/Extensions/formhandler/lang.xml

    # The master template is a file containing the markup for specific field types or other sub templates (e.g. for emails). You can use these predefined markups in your HTML template for a specific form.
    masterTemplateFile.1 = {$filepaths.extTemplates}/formhandler/mastertemplate.html

    # This is the title of the predefined form shown in the dropdown box in the plugin options.
    name = Suchagent beauftragen

    # All form fields are prefixed with this values (e.g. contact[name])
    formValuesPrefix = searchagent_register

    templateFile = TEXT
    templateFile.value = fileadmin/default/templates/extensions/mbx_realestate/Extensions/formhandler/searchagent-form.html

    singleErrorTemplate {
        totalWrap = <div class="statusbox err">|</div>
    }

    # This block defines the error checks performed when the user hits submit.
    validators >
    validators {
            1.class = Validator_Default
            1.config.fieldConf {
                    email.errorCheck.1 = required
                    email.errorCheck.2 = email
            }
            2.class = TYPO3\MbxRealestate\Validator\SearchAgentQuery
    }
    finishers >
    finishers {
        1.class = Finisher_DB
        1.config {
            table = tx_mbxrealestate_domain_model_searchagent
            key = uid

            fields {
                contact_email.mapping = email
                search_query.postProcessing = USER
                search_query.postProcessing.userFunc = TYPO3\MbxRealestate\Helper\Search\ImmoSearchHelper->getEncodedSearchFromSession
                tstamp.special = sub_tstamp
                crdate.special = sub_tstamp
            }
        }

        10.class = TYPO3\MbxRealestate\Finisher\SearchagentSubscribe
        10.config {
            user {
                subject = Ihr Suchagent-Auftrag
                templateFile = TEXT
                templateFile.value = {$plugin.tx_mbxrealestate.view.extensionsRootPath}/formhandler/searchagent-subscribe.html
                sender_email = TEXT
                sender_email.value = {$emails.admin}
                to_email = email
            }
        }

        # Finisher_Mail sends emails to an admin and/or the user.

        20.class = Finisher_SubmittedOK
        20.config.returns = 1
    }
}

lib.notepadMailForm < plugin.tx_formhandler_pi1
lib.notepadMailForm.settings {

    langFile.1 = {$plugin.tx_mbxrealestate.view.extensionsRootPath}/formhandler/lang.xml
    debug = 0
    # The master template is a file containing the markup for specific field types or other sub templates (e.g. for emails). You can use these predefined markups in your HTML template for a specific form.
    masterTemplateFile.1 = TEXT
    masterTemplateFile.1.value = {$plugin.tx_mbxrealestate.view.extensionsRootPath}/formhandler/mastertemplate.html

    # This is the title of the predefined form shown in the dropdown box in the plugin options.
    name = Merkzettel versenden

    # All form fields are prefixed with this values (e.g. contact[name])
    formValuesPrefix = notepad_send

    templateFile = TEXT
    templateFile.value = {$plugin.tx_mbxrealestate.view.extensionsRootPath}/formhandler/notepad_send-form.html

    # This block defines the error checks performed when the user hits submit.
    validators >
    validators {
            1.class = Validator_Default
            1.config.fieldConf {
                    sender-name.errorCheck.1 = required
                    email-recipient.errorCheck.1 = required
                    email-recipient.errorCheck.2 = email
            }

    }
    finishers >
    finishers {

        10.class = TYPO3\MbxRealestate\Finisher\NotepadSend
        10.config {
            user {
                subject = Mein Merkzettel
                templateFile = TEXT
                templateFile.value = {$plugin.tx_mbxrealestate.view.extensionsRootPath}/formhandler/notepad_send-email.html
                sender_email = {$emails.admin}
                to_email = email-recipient
            }
        }

        # Finisher_Mail sends emails to an admin and/or the user.

        20.class = Finisher_SubmittedOK
        20.config.returns = 1
    }
}