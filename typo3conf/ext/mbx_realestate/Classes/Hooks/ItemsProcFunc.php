<?php
use TYPO3\MbxRealestate\Domain\Repository\ImmocontactRepository;
use TYPO3\MbxRealestate\Helper\ImmoPluginHelper;
use TYPO3\CMS\Backend\Form\FormDataProvider\TcaSelectItems;
use TYPO3\CMS\Extbase\Object\ObjectManager;

/**
 * Userfunc to render alternative label for media elements
 *
 * @package TYPO3
 * @subpackage mbx_realestate
 */
class Tx_MbxRealestate_Hooks_ItemsProcFunc {

    CONST TS_TYPE_GLOBALS = 'globals';
    CONST TS_TYPE_PAGESTS = 'pagesTSconfig';
    CONST TS_TYPE_TS = 'ts';

    /**
     * Itemsproc function to extend the selection of templateLayouts in the plugin
     *
     * @param array &$config configuration array
     * @param TcaSelectItems $parentObject parent object
     * @return void
     */
    public function user_templateLayout_Immoobject_SearchForm(array &$config, TcaSelectItems $parentObject) {

        $this->user_templateLayout(__FUNCTION__, $config, $parentObject);
    }

    /**
     * Itemsproc function to extend the selection of templateLayouts in the plugin
     *
     * @param array &$config configuration array
     * @param TcaSelectItems $parentObject parent object
     * @return void
     */
    public function user_templateLayout_Immoobject_List(array &$config, TcaSelectItems $parentObject) {

        $this->user_templateLayout(__FUNCTION__, $config, $parentObject);
    }

    /**
     * Itemsproc function to extend the selection of templateLayouts in the plugin
     *
     * @param array &$config configuration array
     * @param TcaSelectItems $parentObject parent object
     * @return void
     */
    public function user_templateLayout_Immoobject_ListNotepad(array &$config, TcaSelectItems $parentObject) {

        $this->user_templateLayout(__FUNCTION__, $config, $parentObject);
    }

    /**
     * Itemsproc function to extend the selection of templateLayouts in the plugin
     *
     * @param array &$config configuration array
     * @param TcaSelectItems $parentObject parent object
     * @return void
     */
    public function user_templateLayout_Immoobject_MailtoNotepad(array &$config, TcaSelectItems $parentObject) {

        $this->user_templateLayout(__FUNCTION__, $config, $parentObject);
    }

    /**
     * Itemsproc function to extend the selection of templateLayouts in the plugin
     *
     * @param array &$config configuration array
     * @param TcaSelectItems $parentObject parent object
     * @return void
     */
    public function user_templateLayout_Immoobject_Show(array &$config, TcaSelectItems $parentObject) {

        $this->user_templateLayout(__FUNCTION__, $config, $parentObject);
    }

    /**
     * Itemsproc function to extend the possible predefined searchresult lists in the plugin
     *
     * @param array &$config configuration array
     * @param TcaSelectItems $parentObject parent object
     * @return void
     */
    public function user_customSearchOptions(array &$config, TcaSelectItems $parentObject) {

        array_push($config['items'], array());

        $immoPluginHelper = ImmoPluginHelper::getInstance();
        $customSearchOptions = $immoPluginHelper->getPluginSettings('settings.customSearchOptions.');

        if(!empty($customSearchOptions)) {

            $customSearchOptionsParsed = $immoPluginHelper->parseTS($customSearchOptions);

            foreach($customSearchOptionsParsed as $customSearchKey => $customSearchData) {

                $additionalLayout = array(
                    $GLOBALS['LANG']->sL($customSearchData['title'], TRUE),
                    $customSearchKey
                );
                array_push($config['items'], $additionalLayout);
            }
        }

    }

    /**
     * Retrieve pid from TypoScript configuration
     *
     * @param array &$config configuration array
     * @param TcaSelectItems $parentObject parent object
     * @return void
     */
    public function user_storagePids(array &$config, TcaSelectItems $parentObject) {

        array_push($config['items'], array());

        $immoPluginHelper = ImmoPluginHelper::getInstance();
        $storages = $immoPluginHelper->getStorages();

        foreach($storages as $storageName => $storageData) {

            $additionalLayout = array(
                $GLOBALS['LANG']->sL($storageData['title'], TRUE),
                $storageName
            );
            array_push($config['items'], $additionalLayout);
        }

    }


    /**
     * Filter selectable contacts by skipContacts defined in TypoScript configuration
     *
     * @param array &$config configuration array
     * @param TcaSelectItems $parentObject parent object
     * @return void
     */
    public function user_singleImmocontact(array &$config, TcaSelectItems $parentObject) {
        array_push($config['items'], array());

        $immoPluginHelper = ImmoPluginHelper::getInstance();
        $skipContacts = $immoPluginHelper->getPluginSettings('settings.import.skipContacts');

        $objectManager = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(ObjectManager::class);
        $immocontactRepository = $objectManager->get(ImmocontactRepository::class);

        $contacts = $immocontactRepository->findByTypes(explode(',', $skipContacts));

        foreach ($contacts as $contactItem) {
            if ($contactItem->getContactName() !== '') {
                $contactOption = array(
                    $contactItem->getContactName(),
                    $contactItem->getUid()
                );
                array_push($config['items'], $contactOption);
            }
        }
    }

    /**
     * Itemsproc function to extend the possible predefined addressresult lists in the plugin
     *
     * @param array &$config configuration array
     * @param t3lib_TCEforms $parentObject parent object
     * @return void
     */
    public function user_customAddressSearchOptions(array &$config, TcaSelectItems $parentObject) {

        array_push($config['items'], array());

        $immoPluginHelper = ImmoPluginHelper::getInstance();
        $customAddressSearchOptions = $immoPluginHelper->getPluginSettings('settings.customAddressSearchOptions.');

        if(!empty($customAddressSearchOptions)) {

            $customAddressSearchOptionsParsed = $immoPluginHelper->parseTS($customAddressSearchOptions);

            foreach($customAddressSearchOptionsParsed as $customSearchKey => $customSearchData) {

                $additionalLayout = array(
                    $GLOBALS['LANG']->sL($customSearchData['title'], TRUE),
                    $customSearchKey
                );
                array_push($config['items'], $additionalLayout);
            }
        }

    }

    public function user_districtOptions(array &$config, TcaSelectItems $parentObject) {
        array_push($config['items'], array());

        $immoPluginHelper = ImmoPluginHelper::getInstance();
        $districtsMapping = $immoPluginHelper->getDistrictsMapping();

        if ($districtsMapping !== NULL) {
            foreach ($districtsMapping as $districtKey => $districtName) {
                $districtOption = array(
                    $districtName,
                    $districtKey
                );
                array_push($config['items'], $districtOption);
            }
        }
    }

    /**
     * Get all possible contact types defined for current project
     *
     * @param array $config
     * @param t3lib_TCEforms $parentObject
     */
    public function user_contactTypeOptions(array &$config, TYPO3\CMS\Backend\Form\FormDataProvider\TcaSelectItems  $parentObject) {
        array_push($config['items'], array());

        $immoPluginHelper = ImmoPluginHelper::getInstance();
        $contactTypesMapping = $immoPluginHelper->getPluginSettings('settings.contactTypes.');

        if ($contactTypesMapping !== NULL) {
            foreach ($contactTypesMapping as $key => $name) {
                $contactTypeOption = array(
                    $name,
                    $key
                );
                array_push($config['items'], $contactTypeOption);
            }
        }
    }


    /**
     * Extracts the controller and action from function data
     * @param string $func
     * @return array
     */
    private function extractTemplateType($func) {

        list($foo, $bar, $Controller, $Action) = explode('_', $func);
        unset($foo, $bar);

        return array(
            $Controller,
            $Action
        );
    }


    /**
     * @param string $Controller
     * @param string $Action
     * @param array &$config configuration array
     * @param string $type
     * @return boolean|array
     */
    private function layoutsConfigured($Controller, $Action, $config, $type = self::TS_TYPE_GLOBALS) {

        switch($type) {
            case self::TS_TYPE_GLOBALS :

                if(isset($GLOBALS['TYPO3_CONF_VARS']['EXT']['mbx_realestate']['templateLayouts'])
                && ($ref = $GLOBALS['TYPO3_CONF_VARS']['EXT']['mbx_realestate']['templateLayouts'])
                && is_array($ref)
                && array_key_exists($Controller, $ref)
                && is_array($ref[$Controller])
                && array_key_exists($Action, $ref[$Controller])
                ) {
                    return $ref[$Controller][$Action];
                }

                break;
            case self::TS_TYPE_PAGESTS :

                if (is_numeric($config['row']['pid'])) {
                    $pagesTsConfig = \TYPO3\CMS\Backend\Utility\BackendUtility::getPagesTSconfig($config['row']['pid']);
                    if (isset($pagesTsConfig['tx_mbxrealestate.']['templateLayouts.'])
                    && ($ref = $pagesTsConfig['tx_mbxrealestate.']['templateLayouts.'])
                    && is_array($ref)
                    && array_key_exists($Controller . '.', $ref)
                    && is_array($ref[$Controller . '.'])
                    && array_key_exists($Action . '.', $ref[$Controller . '.'])
                    ) {

                        return $ref[$Controller . '.'][$Action . '.'];
                    }
                }

                break;
            case self::TS_TYPE_TS :

                $immoPluginHelper = ImmoPluginHelper::getInstance();
                $tsLayouts = $immoPluginHelper->getPluginSettings('settings.templateLayouts.');

                if(!is_null($tsLayouts)) {

                    $layouts = $immoPluginHelper->parseTS($tsLayouts);

                    if(is_array($layouts)
                    && array_key_exists($Controller, $layouts)
                    && is_array($layouts[$Controller])
                    && array_key_exists($Action, $layouts[$Controller])
                    ) {

                        return $layouts[$Controller][$Action];
                    }
                }

                break;
        }

        return false;
    }

    /**
     * @param string $type the called function containing the controller and action templates should be defined in TS or ext_tables
     * @param array $config
     * @param t3lib_TCEforms $parentObject
     */
    private function user_templateLayout($type, array &$config, TYPO3\CMS\Backend\Form\FormDataProvider\TcaSelectItems  $parentObject) {

        list($Controller, $Action) = $this->extractTemplateType($type);

        // Check if the layouts are extended by ext_tables
        if (($ref = $this->layoutsConfigured($Controller, $Action, $config, self::TS_TYPE_GLOBALS)) !== FALSE) {

            // Add every item
            foreach ($ref as $layoutKey => $layoutData) {
                $additionalLayout = array(
                    $GLOBALS['LANG']->sL($layoutData['title'], TRUE),
                    $layoutKey
                );
                array_push($config['items'], $additionalLayout);
            }
        }

        // Add tsconfig values
        if (($ref = $this->layoutsConfigured($Controller, $Action, $config, self::TS_TYPE_PAGESTS)) !== FALSE) {

            // Add every item
            foreach ($ref as $key => $label) {
                $additionalLayout = array(
                    $GLOBALS['LANG']->sL($label, TRUE),
                    $key
                );
                array_push($config['items'], $additionalLayout);
            }
        }

        // Add tsconfig values
        if (($ref = $this->layoutsConfigured($Controller, $Action, $config, self::TS_TYPE_TS)) !== FALSE) {

            // Add every item
            foreach ($ref as $layoutKey => $layoutData) {
                $additionalLayout = array(
                    $GLOBALS['LANG']->sL($layoutData['title'], TRUE),
                    $layoutKey
                );
                array_push($config['items'], $additionalLayout);
            }
        }
    }
}