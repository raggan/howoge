<?php

namespace TYPO3\MbxRealestate\Service;

use TYPO3\CMS\Backend\Utility\BackendUtility,
    TYPO3\CMS\Core\TimeTracker\NullTimeTracker,
    TYPO3\CMS\Core\Utility\ExtensionManagementUtility,
    TYPO3\CMS\Core\Utility\GeneralUtility,
    TYPO3\CMS\Core\SingletonInterface;

/**
 * Class BackendService
 */
class BackendService implements SingletonInterface
{
    /**
     * Initializes TSFE for BE operations
     *
     * @param int $id       root page id
     * @param int $typeNum  page typeNum
     */
    public function initTSFE($id = 1, $typeNum = 0)
    {
        if (!is_object($GLOBALS['TT'])) {
            $GLOBALS['TT'] = new NullTimeTracker();
            $GLOBALS['TT']->start();
        }
        $GLOBALS['TSFE'] = GeneralUtility::makeInstance('TYPO3\\CMS\\Frontend\\Controller\\TypoScriptFrontendController',  $GLOBALS['TYPO3_CONF_VARS'], $id, $typeNum);
        $GLOBALS['TSFE']->connectToDB();
        $GLOBALS['TSFE']->initFEuser();
        $GLOBALS['TSFE']->determineId();
        $GLOBALS['TSFE']->initTemplate();
        $GLOBALS['TSFE']->getConfigArray();

        if (ExtensionManagementUtility::isLoaded('realurl')) {
            $rootline = BackendUtility::BEgetRootLine($id);
            $host = BackendUtility::firstDomainRecord($rootline);
            $_SERVER['HTTP_HOST'] = $host;
        }
    }
}