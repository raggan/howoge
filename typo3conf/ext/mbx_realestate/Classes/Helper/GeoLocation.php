<?php

namespace TYPO3\MbxRealestate\Helper;

/**
 * Description of GeoLocation
 *
 * @author tobias.jueschke
 */
class GeoLocation {

    /**
     *
     * @param string $address
     * @return array
     */
    public static function getGeoLocation($address){
        if(!empty($address)){

            $url = 'http://maps.google.com/maps/api/geocode/json?address=' . urlencode($address . ' de') . '&region=de&output=json&sensor=false';
            
            $json = file_get_contents($url);
            $locationData = json_decode($json);

            if($locationData->status == 'OK'){
                return $locationData->results[0]->geometry->location;
            }
        }

        return false;
    }
}

?>
