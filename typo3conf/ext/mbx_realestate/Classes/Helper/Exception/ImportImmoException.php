<?php

namespace TYPO3\MbxRealestate\Helper\Exception;

class ImportImmoException extends \TYPO3\CMS\Core\Exception {

    function __construct() {
        call_user_func_array(array(parent, '__construct'), func_get_args());
    }

    /**
     * 
     */
    public function __toString() {
        
        return $this->getMessage();
    }
}