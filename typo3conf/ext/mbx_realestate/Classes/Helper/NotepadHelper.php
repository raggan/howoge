<?php

namespace TYPO3\MbxRealestate\Helper;

use TYPO3\MbxRealestate\Domain\Repository\ImmoaddressRepository;
use TYPO3\MbxRealestate\Domain\Repository\ImmoobjectRepository;

class NotepadHelper {
    
    /**
     * @var \TYPO3\CMS\Extbase\Object\ObjectManager
     */
    private $objectManager = null;
    
    /**
     * @var \TYPO3\MbxRealestate\Helper\NotepadHelper 
     */
    private static $instance = null;
    
    
    /**
     * @return \TYPO3\MbxRealestate\Helper\NotepadHelper
     */
    public static function getInstance() {
        
        if(is_null(self::$instance) || !(self::$instance instanceof NotepadHelper)) {
            self::$instance = new NotepadHelper();
        }
        
        return self::$instance;
    }
    
    public function __construct() {
        
        $this->objectManager = new \TYPO3\CMS\Extbase\Object\ObjectManager();
    }
    
    /**
     * 
     * @param string $mime - plain|html
     * @return string
     */
    public function getMailSendData($mime) {

        $immoPluginHelper = \TYPO3\MbxRealestate\Helper\ImmoPluginHelper::getInstance();
        
        $emailView = $this->objectManager->get('TYPO3\\CMS\\Fluid\\View\\StandaloneView');
        $immoobjectRepository = $this->objectManager->get(ImmoobjectRepository::class);

        $templatePath = $immoPluginHelper->getPluginSettings('settings.notepad.send.email.templateFilePath');
        $templateFile = $immoPluginHelper->getPluginSettings('settings.notepad.send.email.templateFileNameSendData' . ucfirst($mime));
        
        $file = \TYPO3\CMS\Core\Utility\GeneralUtility::getFileAbsFileName($templatePath) . $templateFile;
        
        if(file_exists($file)) {
            
            $emailView->setTemplatePathAndFilename($file);
            $emailView->assignMultiple(array(
                'immoobjects'           => $immoobjectRepository->findByNotepad(),
                'immoaddressRepository' => $this->objectManager->get(ImmoaddressRepository::class)
            ));

            return $emailView->render();
        }

        return null;
        
    }
}