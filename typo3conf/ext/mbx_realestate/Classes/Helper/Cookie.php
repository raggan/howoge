<?php

namespace TYPO3\MbxRealestate\Helper;

/**
 * Description of Cookie
 *
 * @author tobias.jueschke
 * @author denis.krueger
 */
class Cookie {

    const oneDay = 86400;
    const sevenDays = 604800;
    const thirtyDays = 2592000;
    const sixMonths = 15811200;
    const oneYear = 31536000;
    const lifetime = -1; // 2030-01-01 00:00:00
    const session = null;
    

    /**
     * Returns true if there is a cookie with this name.
     *
     * @param string $name
     * @return bool
     */

    static public function exists($name) {
        return isset($_COOKIE[$name]);
    }

    /**
     * Returns true if there no cookie with this name or it's empty, or 0,
     * or a few other things. Check http://php.net/empty for a full list.
     *
     * @param string $name
     * @return bool
     */
    static public function isEmpty($name) {
        return empty($_COOKIE[$name]);
    }

    /**
     * Get the value of the given cookie. If the cookie does not exist the value
     * of $default will be returned.
     *
     * @param string $name
     * @param string $default
     * @return mixed
     */
    static public function get($name, $default = '') {
        return (isset($_COOKIE[$name]) ? $_COOKIE[$name] : $default);
    }

    /**
     * Adds a cookie value into a stack.
     * @param string $name
     * @param mixed $value
     * @param int $expiry
     * @param string $path
     * @param string $domain
     */
    static public function add($name, $value, $expiry = self::oneYear, $path = '/', $domain = false) {

        if(!(self::isEmpty($name))) {

            $currentValue = (array)json_decode(self::get($name));

            // remove quotes from numeric array keys
            self::healNumericKeys($currentValue);

        }  else {

            $currentValue = array();
        }

        // only add item if value not present in current cookie value
        if(!in_array($value, $currentValue)) {

            array_push($currentValue, $value);
        }

        self::set($name, json_encode($currentValue), $expiry, $path, $domain);
    }

    /**
     * replaces all numeric array keys quoted with their numeric representations
     * @param array $arr
     */
    private static function healNumericKeys(&$arr) {
        
        $new = array();
        
        foreach($arr as $key => $val) {
            if(is_numeric($key)) {
                $new[(int)$key] = $val;
            }  else {
                $new[$key] = $val;
            }
        }
        
        $arr = $new;
    }

    /**
     * Set a cookie. Silently does nothing if headers have already been sent.
     *
     * @param string $name
     * @param string $value
     * @param mixed $expiry
     * @param string $path
     * @param string $domain
     * @return bool
     */
    static public function set($name, $value, $expiry = self::oneYear, $path = '/', $domain = false) {
        $retval = false;
        if (!headers_sent()) {
            if ($domain === false) {
                
                $domainStr = explode('.', $_SERVER['HTTP_HOST']);
            }
                
            $domain = implode('.',array_slice($domainStr,-2));

            if ($expiry === -1) {
                $expiry = 1893456000; // Lifetime = 2030-01-01 00:00:00
            } elseif (is_numeric($expiry)) {
                $expiry += time();
            } else {
                $expiry = strtotime($expiry);
            }
            
            $retval = @setcookie($name, $value, $expiry, $path, $domain);
            if ($retval) {
                $_COOKIE[$name] = $value;
            }
        }
        return $retval;
    }

    /**
     * Removes a single value from a cookie list (undo for add() ).
     *
     * @param string $name
     * @param string $value
     * @param mixed $expiry
     * @param string $path
     * @param string $domain
     * @return bool
     */
    public static function drop($name, $value, $expiry = self::oneYear, $path = '/', $domain = false) {
        
        if(!(self::isEmpty($name))) {
            
            $currentValue = (array)json_decode(self::get($name));
            
            // remove quotes from numeric array keys
            self::healNumericKeys($currentValue);
            
        }  else {
            
            $currentValue = array();
        }
        
        // only drop item if value not present in current cookie value
        if(($key = array_search($value, $currentValue)) !== FALSE) {
            if(count($currentValue) === 1) {
                $currentValue = array();
            } else {         
                unset($currentValue[$key]);
            }
        }

        self::set($name, json_encode($currentValue), $expiry, $path, $domain);
    }
    
    /**
     * Delete a cookie.
     *
     * @param string $name
     * @param string $path
     * @param string $domain
     * @param bool $remove_from_global Set to true to remove this cookie from this request.
     * @return bool
     */
    static public function delete($name, $path = '/', $domain = false, $remove_from_global = false) {
        $retval = false;
        if (!headers_sent()) {
            if ($domain === false) {
                
                $domain = $_SERVER['HTTP_HOST'];
            }
            
            $retval = setcookie($name, '', time() - 3600, $path, $domain);

            if ($remove_from_global) {
                
                unset($_COOKIE[$name]);
            }
        }
        return $retval;
    }

}

?>