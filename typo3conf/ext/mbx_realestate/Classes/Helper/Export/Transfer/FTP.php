<?php

namespace TYPO3\MbxRealestate\Helper\Export\Transfer;

/**
 * @author denis.krueger
 */
class FTP implements TransferInterface {

    /**
     * @var string
     */
    private $host;

    /**
     * @var string
     */
    private $port;

    /**
     * @var string
     */
    private $user;

    /**
     * @var string
     */
    private $pass;

    /**
     * @var resource
     */
    private $cust_ftp_conn = null;

    /**
     * 
     * @param string $host
     * @param int $port
     * @param string $user
     * @param string $pass
     */
    public function __construct($host = null, $port = null, $user = null, $pass = null) {
        
        $this->host = $host;
        $this->port = $port;
        $this->user = $user;
        $this->pass = $pass;
    }    
    
    public function __destruct() {
        
        if(!empty($this->cust_ftp_conn)) {
            ftp_close($this->cust_ftp_conn);
        }
    }
    
    /**
     * @return string
     */
    public function getHost() {
        return $this->host;
    }

    /**
     * @return int
     */
    public function getPort() {
        return $this->port;
    }

    /**
     * @return string
     */
    public function getUser() {
        return $this->user;
    }

    /**
     * @return string
     */
    public function getPass() {
        return $this->pass;
    }

    /**
     * @param string $host
     * @return FTP
     */
    public function setHost($host) {
        $this->host = $host;
        return $this;
    }

    /**
     * @param int $port
     * @return FTP
     */
    public function setPort($port) {
        $this->port = $port;
        return $this;
    }

    /**
     * @param string $user
     * @return FTP
     */
    public function setUser($user) {
        $this->user = $user;
        return $this;
    }

    /**
     * @param string $pass
     * @return FTP
     */
    public function setPass($pass) {
        $this->pass = $pass;
        return $this;
    }

        
    /**
     * this method tries to connect via FTP
     * @return \Exception|boolean
     */
    public function connect() {
        
        if(!($this->cust_ftp_conn = ftp_ssl_connect($this->host, $this->port))) {
                    
            throw new \Exception('Unable to connect to FTP ' . $this->host . ':' . $this->port);
            
        } elseif(!($resLogin = ftp_login($this->cust_ftp_conn, $this->user, $this->pass))) {

            throw new \Exception('Unable to login to FTP as ' . $this->user);
        }
	
        return true;
    }

    /**
     * this method puts the file to the remote FTP directory
     *
     * @param $localFile
     * @param null $targetFilename
     * @return bool
     * @throws \Exception
     */
    public function transferFile($localFile, $targetFilename = null) {
        
        if(empty($targetFilename)) {
            
            $targetFilename = pathinfo($localFile, PATHINFO_BASENAME);
        }
        
        $remoteFile = ftp_pwd($this->cust_ftp_conn) . '/' . $targetFilename;

        // change into this directory or error
        if (@ftp_chdir($this->cust_ftp_conn, '/')) {
	
            if (@ftp_put($this->cust_ftp_conn, $remoteFile, $localFile, FTP_BINARY)) {
                
                return true;
            }

            // ftp_get() failed
            throw new \Exception('Unable to ftp_get() file in FTP');
        }
        
        throw new \Exception('Unable to chdir() in FTP');
    }
	
}