<?php

namespace TYPO3\MbxRealestate\Helper\Export\Transfer;

interface TransferInterface {
    
    public function connect();
    public function transferFile($localFile, $targetFilename);
}