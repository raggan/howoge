<?php
namespace TYPO3\MbxRealestate\Helper\Export;

class ImmoXmlWriter {

    /**
     *
     * @var array
     */
    private $data = null;

    /**
     *
     * @var \XMLWriter
     */
    protected $xmlWriter = null;
    
    public function __construct($data = null) {
            
        if(!is_null($data)) {
            $this->setData($data);
        }
    }

    /**
     * set the data for the rendering xml
     * @param array $data
     */
    public function setData($data) { $this->data = $data; }
    /**
     * the to rendering data
     * @return array
     */
    public function getData() { return $this->data; }

    /**
     * this method renders a full XML-structure for the setted $data
     * @param mixed $data
     * @param string $rootNodeName
     * @param string $charset
     * @param string $version
     * @param array $attributes
     * @return string
     */
    public function renderXml($data = null, $rootNodeName = 'root', $charset = 'UTF-8', $version = '1.0', $attributes = null) {

        if(is_null($data)) {
            $data = $this->getData();
        }

        $this->startXml($rootNodeName, $charset, $version, $attributes);
        $this->write($data);

        return $this->endXml();
    }

    /**
     * this method just starts an xml-structure and returns the created XmlWriter-Object
     * @param string $rootNodeName the name for the root-element
     * @param string $charset the xml-charset
     * @param string $version the xml-version
     * @param array $attributes
     */
    public function startXml($rootNodeName = 'root', $charset = 'UTF-8', $version = '1.0', $attributes = null) {

        $this->xmlWriter = new \XmlWriter();
        $this->xmlWriter->openMemory();
        $this->xmlWriter->startDocument($version, $charset);
        $this->xmlWriter->startElement($rootNodeName);
        
        if(is_array($attributes)) {
            foreach($attributes as $attribute => $value) {
                
                $this->xmlWriter->writeAttribute($attribute, $value);
            }
        }
    }

    /**
     * this method adds a single node to the XML
     * @param string $node the node to append to the xml
     */
    public function appendXml($node) {
        $this->xmlWriter->writeRaw($node);
    }

    /**
     * creating new node and appending directly to xml
     * 
     * @param array $data
     * @param string $nodeName
     * @return \TYPO3\MbxRealestate\Helper\Export\ImmoXmlWriter
     */
    public function appendNode(array $data, $nodeName) {
        
        $this->appendXml($this->renderNode($data, $nodeName));
        
        return $this;
    }

    /**
     * this method finished the XMLWriter-object and returns the resulting xml-string
     * @return string 
     */
    public function endXml() {

        $this->xmlWriter->endElement();
        return $this->xmlWriter->outputMemory(true);
    }

    /**
     * this /recursion/ method writes $data to the $xml-element
     * @param mixed $data
     * @param \XMLWriter $xmlWriter
     */
    private function write(array $data, \XMLWriter &$xmlWriter = null){
        
        if(empty($xmlWriter)) {
            $xmlWriter =& $this->xmlWriter;
        }
        
        // ensure to have the attributes first because writeAttribute() only succeed if no other element was written before
        uksort($data, array($this, '_sortDataByAttributes'));
        
        foreach($data as $key => $value){
            if(is_array($value)){

                $elementName = $this->getTagNameByKey($key);

                $xmlWriter->startElement($elementName);
                $this->write($value, $xmlWriter);
                $xmlWriter->endElement();
                continue;
            }
            $key = $this->getTagNameByKey($key);
            
            $isAttribute = null;
            
            if(preg_match('/^\@(?<ATTRIBUTE>.*)$/', $key, $isAttribute)) {
                
                $xmlWriter->writeAttribute($isAttribute['ATTRIBUTE'], $value);
                
            } elseif(preg_match('/\\s/', $value)){
                
                $xmlWriter->startElement($key);
                $xmlWriter->writeCdata(trim($value));
                $xmlWriter->endElement();
            }else {
                $xmlWriter->writeElement($key, $value);
            }
        }
    }
    
    /**
     * 
     * @param string|int $key
     * @return string
     */
    private function getTagNameByKey($key) {
        
        return (is_numeric($key) 
                ? 'num_' . $key 
                : (substr($key,0,2) == '##') 
                    ? substr($key, 2, strrpos($key, '#')-2) 
                    : $key);
    }

    /**
     * Sorting the incoming array by items which keys starts with '@' ('@attribute')
     * 
     * @param string $a
     * @param string $b
     * @return int
     */
    protected function _sortDataByAttributes($a, $b) {

        $pattern = '/^\@/';
        $aIsAttr = preg_match($pattern, $a);
        $bIsAttr = preg_match($pattern, $b);
        
        if($aIsAttr && $bIsAttr) {
            return 0;
        }
        
        if($aIsAttr && !$bIsAttr) {            
            return -1;
        } else {
            return 1;
        }
    }
    
    /**
     * this method creates a node with childs
     * @param mixed $data
     * @param string $nodeName
     * @return string the xml-string for the created node
     */
    public function renderNode(array $data, $nodeName) {

        $xml = new \XmlWriter();
        $xml->openMemory();
        $xml->startElement($nodeName);

        $this->write($data, $xml);

        $xml->endElement();
        return $xml->outputMemory(true);
    }
}