<?php

namespace TYPO3\MbxRealestate\Helper\Export\Interfaces;

interface ExporterInterface {
    
    public function createFile();
    public function addItem(array $data);
    public function saveFile();
    public function closeFile();
}