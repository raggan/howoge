<?php

namespace TYPO3\MbxRealestate\Helper\Export\Interfaces;

interface ExporterCommandControllerInterface {

    public function exportCommand();
}