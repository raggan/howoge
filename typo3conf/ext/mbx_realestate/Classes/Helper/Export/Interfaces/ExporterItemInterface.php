<?php

namespace TYPO3\MbxRealestate\Helper\Export\Interfaces;

interface ExporterItemInterface {

    public function prepareItem(\TYPO3\MbxRealestate\Domain\Model\Immoobject $immoItem);
}