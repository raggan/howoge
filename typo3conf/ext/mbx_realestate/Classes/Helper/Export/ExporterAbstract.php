<?php

namespace TYPO3\MbxRealestate\Helper\Export;

use TYPO3\CMS\Extbase\DomainObject\AbstractEntity;
use TYPO3\CMS\Extbase\Object\ObjectManager;
use TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer;
use TYPO3\MbxRealestate\Controller\ExportAbstractCommandController;
use TYPO3\MbxRealestate\Domain\Repository\ImmoaddressRepository;
use TYPO3\MbxRealestate\Domain\Repository\ImmocontactRepository;
use TYPO3\MbxRealestate\Domain\Repository\ImmofeatureRepository;
use TYPO3\MbxRealestate\Domain\Repository\ImmoimageRepository;
use TYPO3\MbxRealestate\Domain\Repository\ImmoobjectRepository;
use TYPO3\MbxRealestate\Service\BackendService;

abstract class ExporterAbstract {

    /**
     * ImmoobjectRepository
     *
     * @var ImmoobjectRepository
     */
    protected $immoobjectRepository;

    /**
     * ImmofeatureRepository
     *
     * @var ImmofeatureRepository
     */
    protected $immofeatureRepository;

    /**
     * ImmoaddressRepository
     *
     * @var ImmoaddressRepository
     */
    protected $immoaddressRepository;

    /**
     * ImmocontactRepository
     *
     * @var ImmocontactRepository
     */
    protected $immocontactRepository;

    /**
     * ImmoimageRepository
     *
     * @var ImmoimageRepository
     */
    protected $immoimageRepository;

    /**
     * @var array
     */
    protected $configuration = null;

    /**
     *
     * @var \TYPO3\CMS\Extbase\Object\ObjectManager
     */
    protected $objectManager = null;

    /**
     * @var \Resource
     */
    protected $handle = null;

    /**
     * @var string
     */
    private $filename;

    /**
     * @var string
     */
    private $file;

    /**
     * @var string|null
     */
    private $scope;

    /**
     * @var int|null
     */
    private $storagePid;

    /**
     *
     * @var \TYPO3\MbxRealestate\Service\BackendService
     */
    private $backendService;

    /**
     * @var \TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer
     */
    private $cObj;
    
    /**
     * 
     */
    public function __construct() {

        $this->backendService = new BackendService();
        $this->backendService->initTSFE();
        $this->cObj = new ContentObjectRenderer();
        $this->objectManager = new ObjectManager();
    }
    
    /**
     * @return \TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer
     */
    public function getCObj() {
        return $this->cObj;
    }

    /**
     * @param  ContentObjectRenderer $cObj
     * @return ExporterAbstract
     */
    public function setCObj(ContentObjectRenderer $cObj) {
        $this->cObj = $cObj;
        return $this;
    }

        
    /**
     * @param string $scope
     * @return \TYPO3\MbxRealestate\Helper\Export\ExporterAbstract
     */
    public function setScope($scope) {
        $this->scope = $scope;
        return $this;
    }

    /**
     *
     * @return scope
     */
    public function getScope() {
        return $this->scope;
    }

    /**
     * @param  ImmoobjectRepository $immoobjectRepository
     * @return ExporterAbstract
     */
    public function setImmoobjectRepository(ImmoobjectRepository $immoobjectRepository) {

        $this->immoobjectRepository = $immoobjectRepository;
        return $this;
    }

    /**
     * @param  ImmofeatureRepository $immofeatureRepository
     * @return ExporterAbstract
     */
    public function setImmofeatureRepository(ImmofeatureRepository $immofeatureRepository) {

        $this->immofeatureRepository = $immofeatureRepository;
        return $this;
    }

    /**
     * @param  ImmoaddressRepository $immoaddressRepository
     * @return ExporterAbstract
     */
    public function setImmoaddressRepository(ImmoaddressRepository $immoaddressRepository) {

        $this->immoaddressRepository = $immoaddressRepository;
        return $this;
    }

    /**
     * @param  ImmocontactRepository $immocontactRepository
     * @return ExporterAbstract
     */
    public function setImmocontactRepository(ImmocontactRepository $immocontactRepository) {

        $this->immocontactRepository = $immocontactRepository;
        return $this;
    }

    /**
     * @param  ImmoimageRepository $immoimageRepository
     * @return ExporterAbstract
     */
    public function setImmoimageRepository(ImmoimageRepository $immoimageRepository) {

        $this->immoimageRepository = $immoimageRepository;
        return $this;
    }

    /**
     *
     * @param  ExportAbstractCommandController $commandController
     * @return ExporterAbstract
     */
    public function setCommandControllerReference(ExportAbstractCommandController $commandController) {

        $this->commandControllerReference = $commandController;
        return $this;
    }

    /**
     * @return ExportAbstractCommandController
     */
    protected function getCommandControllerReference() {
        return $this->commandControllerReference;
    }

    /**
     *
     * @param  array $options
     * @return ExporterAbstract
     */
    public function setConfiguration(array $options) {

        $this->configuration = $options;
        return $this;
    }

    /**
     * Retrieves a TS configuration value for a required path.
     *
     * @param string $configurationPath - a configuration path of the TS configured value e.g. export.types.xml.xpathImmoNode
     * @return string - the configured value
     */
    public function getConfigurationItem($configurationPath) {

        $configNode = $this->configuration;
        $path = explode('.', $configurationPath);

        while($pathItem = array_shift($path)) {

            // if no end of path reached we add the '.' to access next deeper configuration level
            if(count($path)) {
                $pathItem .= '.';
            }

            $configNode = $configNode[$pathItem];
        }

        return $configNode;
    }
    
    /**
     *
     * @return int
     */
    public function getStoragePid() {

        // use the custom defined storagePid (can be set as execute option in BE)
        if(!empty($this->storagePid)) {
            return $this->storagePid;
        }

        $scope = $this->getScope();
        $customScope = $this->getConfigurationItem('settings.export.' . $scope . '.storagePid');

        if(!empty($customScope)) {
            return $customScope;
        } else {
            return $this->getConfigurationItem('persistence.storagePid');
        }
    }
    
    /**
     * Returns the file to export from.
     * @return string
     */
    public function getFile() {

        if(empty($this->file)) {

            $path = $this->getConfigurationItem('settings.export.' . $this->getScope() . '.outputPath');
            $file = $this->getFilename();

            $this->file = PATH_site . $path . $file;
        }

        return $this->file;
    }

    /**
     * @return string
     */
    public function getFilename() {

        if(empty($this->filename)) {

            $this->filename = $this->getConfigurationItem('settings.export.' . $this->getScope() . '.outputFile');
        }

        return $this->filename;
    }

    // <editor-fold defaultstate="collapsed" desc="file handling">

    /**
     * Closes the CSV file.
     *
     * @return \TYPO3\MbxRealestate\Helper\Export\ExporterAbstract
     */
    public function closeFile() {

        if(is_resource($this->handle)) {

            @fclose($this->handle);
        }

        return $this;
    }

    /**
     * Checks if the backup option is set in the extension configuration and copies the export file
     * into the backup directory (TS: settings.export.backupFile).
     *
     * @return null|boolean
     */
    public function backupFile() {

        $useBackup = (bool)$this->configuration['settings.']['export.']['backupFile'];

        if($useBackup) {

            $sourceFile = $this->getFile();
            $targetFile = PATH_site . $this->configuration['settings.']['export.']['backupDir'] . date('Y-m-d_H-i-s') . '_' . $this->getFilename();

            $cmdRef = $this->getCommandControllerReference();
            $cmdRef->log('Backup export file...');

            $res = (@copy($sourceFile, $targetFile));
            
            if($res) {
                $cmdRef->log('Backup file: OK');
            } else {
                $cmdRef->log('Backup file: FAILURE');
            }
            
            return $res;
        }

        return null;
    }

    // </editor-fold>

    /**
     * 
     * @param  AbstractEntity $entity
     * @param  array $data - array to append the new/extracted data
     * @param  array $allowedProperties - whitelist for allowed properties to export
     * @return array
     */
    protected function exportModelPropertiesToArray(AbstractEntity $entity, &$data = array(), $allowedProperties = null) {
        
        $refl = new \ReflectionClass($entity);
        $properties = $refl->getProperties();
        
        foreach($properties as $property) {
            
            $property instanceof \ReflectionProperty;

            // skip the current property if not no occurence in whitelist.
            if(!empty($allowedProperties) && !in_array($property->getName(), $allowedProperties)) {
                continue;
            }
            
            $camelCasedArg = str_replace(' ', '', ucwords(str_replace('_', ' ', $property->getName())));
            $getterFunction = 'get' . ucfirst($camelCasedArg);

            if($refl->hasMethod($getterFunction)) {
                $value = $entity->$getterFunction();
                   
                if(($value instanceof \TYPO3\CMS\Extbase\Persistence\Generic\LazyObjectStorage)
                || ($value instanceof \TYPO3\CMS\Extbase\Persistence\Generic\LazyLoadingProxy)
                ) {
                        continue;
                }
                        
                $data[$property->getName()] = $value;
            }
        }
        
        return $data;
    }
    
    /**
     * 
     * @param \TYPO3\CMS\Extbase\DomainObject\AbstractEntity $entity
     * @param string $lazyObjectStorageName
     * @param string $lazyObjectStorageMethod
     * @param array $data - array to append the new/extracted data
     * @param array $allowedProperties - whitelist for allowed properties to export
     * @return array
     */
    protected function exportLazyObjectStorageToArray(AbstractEntity $entity, $lazyObjectStorageName, $lazyObjectStorageMethod, &$data = array(), $allowedProperties = null) {
        
        $lazyObjectStorageSingleName = substr($lazyObjectStorageName, 0, strlen($lazyObjectStorageName) - 1);       // transform 'anyitems' to 'anyitem' (cut of last character)
        $lazyObjectsGetter = 'get' . ucfirst($lazyObjectStorageMethod);                                             // create getter method from method name
        $lazyObjects = $entity->{$lazyObjectsGetter}();                                                             // call getter and retrieve lazy objects

        $data[$lazyObjectStorageName] = array();
        $_unsedRef = array();
        
        if($lazyObjects  instanceof \TYPO3\CMS\Extbase\Persistence\Generic\LazyObjectStorage) {

            foreach($lazyObjects as $_ => $lazyObject) {

                $key = sprintf('##%s#%s', $lazyObjectStorageSingleName, $_);
                $data[$lazyObjectStorageName][$key] = $this->exportModelPropertiesToArray($lazyObject, $_unsedRef, $allowedProperties);
                $data[$lazyObjectStorageName][$key]['@uid'] = $lazyObject->getUid();
            }
        }
        
        return $data;
    }
    
    /**
     * Copies the exported file via FTP to target server. (SFTP not currently implemented)
     * 
     * @return \Exception|boolean
     * @throws \Exception
     */
    public function transferFile() {
        
        try {
            
            $cmdRef = $this->getCommandControllerReference();
            
            // <editor-fold defaultstate="collapsed" desc="get transfer settings from TS">

            $scope = $this->getScope();
            $transferState = $this->getConfigurationItem('settings.export.' . $scope . '.transfer');
            $transferSettingsDefault = null;
            
            if($transferState === 'useDefault') {
            
                $transferSettingsDefault = $this->getConfigurationItem('settings.export.transfer.');
                $transferSettingsCustom = $this->getConfigurationItem('settings.export.' . $scope . '.transfer.');
                
            } elseif($transferState === 'custom') {
                
                $transferSettingsCustom = $this->getConfigurationItem('settings.export.' . $scope . '.transfer.');
            }
                
            $transferSettings = array_merge((array)$transferSettingsDefault, (array)$transferSettingsCustom);
            // </editor-fold>
            
            if(empty($transferSettings)) {
                return null;
            }
            
            $transferClassName = '\TYPO3\MbxRealestate\Helper\Export\Transfer\\' . $transferSettings['type'];
            
            if(!class_exists($transferClassName)) {
                throw new \Exception(sprintf('Unable to transfer file via "%s" because class %s does not exists or could not be loaded.', $transferSettings['type'], $transferClassName));
            }
            
            $cmdRef->log('Create transfer Class ' . $transferClassName);
            
            $transferObj = new $transferClassName();
            $transferObj instanceof Transfer\TransferInterface;
            
            // <editor-fold defaultstate="collapsed" desc="set the TS properties as object properties by checking if corresponding getters exists.">
            
            $cmdRef->log('Set transfer properties/credentials.');
            
            $reflection = new \ReflectionClass($transferObj);
            foreach($transferSettings as $prop => $value) {
                
                $camelCasedArg = str_replace(' ', '', ucwords(str_replace('_', ' ', $prop)));
                $setterFunction = 'set' . ucfirst($camelCasedArg);

                if($reflection->hasMethod($setterFunction)) {
                    $cmdRef->log(sprintf('Set %s...', $prop));
                    $value = $transferObj->{$setterFunction}($value);
                }
            }
            // </editor-fold>
            
            $cmdRef->log('Connect FTP...');
            
            $transferObj->connect();
            $cmdRef->log('Transfer File...');
            $transferObj->transferFile($this->getFile());
            $cmdRef->log('File transfered.');
            
            return true;
            
        } catch (\Exception $ex) {

            return $ex;
        }
    }
}