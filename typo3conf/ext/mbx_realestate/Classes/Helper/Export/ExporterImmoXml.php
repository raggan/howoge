<?php

namespace TYPO3\MbxRealestate\Helper\Export;

use TYPO3\MbxRealestate\Domain\Model\Immoobject;
use TYPO3\MbxRealestate\Helper\Export\Interfaces\ExporterXmlInterface;

class ExporterImmoXml extends ExporterXml implements ExporterXmlInterface {

    /**
     * @var \TYPO3\MbxRealestate\Domain\Model\Immoobject[]
     */
    protected $items = null;
    
    public function __construct() {
        parent::__construct();
        
        $this->xmlWriter->startXml('objects', 'UTF-8', '1.0', array(
            'date'  =>  date('Y-m-d'),
            'time'  =>  date('H:i:s')
        ));
    }

    /**
     * @return \TYPO3\MbxRealestate\Helper\Export\ExporterImmoXml
     */
    protected function storeItems() {
        
        $this->items = array();
        
        $objects = $this->immoobjectRepository->findAll();
        
        foreach($objects as $immoObject) {
            
            $this->items[] = $immoObject;
        }
        
        return $this;
    }
    
    /**
     * @return \TYPO3\MbxRealestate\Domain\Model\Immoobject[]
     */
    public function getEnabledItems() {
        
        if(is_null($this->items)) {
            $this->storeItems();
        }
        
        return $this->items;
    }
    
    /**
     * 
     * @return \TYPO3\MbxRealestate\Domain\Model\Immoobject|boolean
     */
    public function iterateItem() {
        
        $this->getEnabledItems();
        $items =& $this->items;
        
        if(($item = array_shift($items)) !== null) {
            return $item;
        }  else {
            return false;
        }
    }
    
    /**
     * 
     * @param \TYPO3\MbxRealestate\Domain\Model\Immoobject $immoObject
     * @return \stdClass
     */
    public function prepareItem(Immoobject $immoObject) {
        
        $data = array();
        $data['@uid'] = $immoObject->getUid();

        $this->exportModelPropertiesToArray($immoObject, $data);
        
        // <editor-fold defaultstate="collapsed" desc="append address">

        $address = $immoObject->getImmoaddress();
        
        if($address instanceof \TYPO3\MbxRealestate\Domain\Model\Immoaddress) {
            $addressData = $this->exportModelPropertiesToArray($address);
            
            $data['address'] = $addressData;
            $data['address']['@uid'] = $address->getUid();

            // append contacts lazy objects to address item on node contacts
            $this->exportLazyObjectStorageToArray($address, 'contacts', 'contacts', $data['address']);
        }
        // </editor-fold>
        
        $lazyObjectStorages = array(
            'images'    =>  'Immoimage',
            'features'  =>  'Immofeature',
            'areas'     =>  'Immoarea',
        );
        
        foreach($lazyObjectStorages as $lazyObjectStorageName => $lazyObjectStorageMethod) {
            
            $this->exportLazyObjectStorageToArray($immoObject, $lazyObjectStorageName, $lazyObjectStorageMethod, $data);
        }
        
        return $data;
    }

}