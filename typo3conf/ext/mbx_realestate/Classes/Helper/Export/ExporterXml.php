<?php

namespace TYPO3\MbxRealestate\Helper\Export;

use TYPO3\MbxRealestate\Controller\ExportAbstractCommandController;
use TYPO3\MbxRealestate\Helper\Export\Interfaces\ExporterInterface;

class ExporterXml extends ExporterAbstract implements ExporterInterface
{

    /**
     * Contains the whole xml file string as SimpleXMLElement object
     * @var ImmoXmlWriter
     */
    protected $xmlWriter;
        
    public function __construct() {
        parent::__construct();
        
        $this->xmlWriter = new ImmoXmlWriter();
    }
    
    /**
     * Openes the XML file for storing the export data to it
     * @return boolean|\TYPO3\MbxRealestate\Helper\Exception\ImportImmoException
     */
    public function createFile() {
        
        $file = $this->getFile();
            
        if(!($this->handle = @fopen($file, 'w+'))) {
            
            return ExportAbstractCommandController::throwException('Unable to create file "' . $file . '" in ' . __FILE__ . '::' . __FUNCTION__ . '()');
        }
            
        return true;
    }

    /**
     * 
     * @param array $data
     */
    public function addItem(array $data) {
        
        $this->xmlWriter->appendNode($data, 'immoobject');
    }
    

    /**
     * Save entire XML data to output file
     * @return boolean
     */
    public function saveFile() {
        
        $content = $this->xmlWriter->endXml();
        
        return fwrite($this->handle, $content);
    }

}