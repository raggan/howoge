<?php

namespace TYPO3\MbxRealestate\Helper\Import;

class ImporterImmoWgs extends \TYPO3\MbxRealestate\Helper\Import\ImporterImmoXml {

    function __construct() {
        parent::__construct();
    }

    /**
     * Searchs for the correct input file
     * @return string
     */
    public final function getFile() {

        $path = PATH_site . $this->getConfiguratioItem('settings.import.' . $this->getScope() . '.inputPath');

        $sort = array();
        $filenameXML = false;

        if(($directory = opendir($path))) {

            while ($file = readdir($directory)) {
                if (filetype($path . $file) != 'dir') {

                    $test = null;
                    if (preg_match("/([0-9]{14})/", $file, $test)){
                        $sort[] = $test[0];
                    }
                }
            }

            closedir($directory);

            // got filelist ... check if not empty and files are valid
            if (count($sort)) {

                // get latest file
                rsort($sort, SORT_NUMERIC);

                $filenameXML = 'Homepage_der_WGS-12321-' . $sort[0] . '.xml';
            }
        }

        return $path . $filenameXML;
    }

    /**
     * Retrieves all necessary data from input immo dataset
     * @param \SimpleXMLElement $immoItem
     * @return \TYPO3\MbxRealestate\Helper\Import\ImmoDataItem
     */
    public final function prepareItem($immoItem) {

        $immoDataItem = parent::prepareItem($immoItem);

        // remove 'PACHT'
        $immoObject = $immoDataItem->getImmoobject();
        $immoObject->setTypeOfDisposition(preg_replace('/(,PACHT|PACHT,)/', '', $immoObject->getTypeOfDisposition()));

        // handle images
        $destPathRelative = $this->getPublicImagePath(false);

        $modeNode = $immoItem->xpath('verwaltung_techn/aktion');
        $modeAttributes = $modeNode[0]->attributes();
        $mode = $this->castValue($modeAttributes['aktionart']);

        $immoDataItem->setMode($mode == 'CHANGE'
                ? \TYPO3\MbxRealestate\Helper\Import\ImmoDataItem::MODE_SAVE
                : \TYPO3\MbxRealestate\Helper\Import\ImmoDataItem::MODE_DROP
        );

        $arrObjImmoimages = array();

        foreach($immoItem->xpath('anhaenge/anhang') as $xmlAnhang) {

            $attributes = $xmlAnhang->attributes();
            $category = (string)$attributes['gruppe'];
            $filenames = array(
                'big_' . $this->extractXmlValue('daten/pfad', $xmlAnhang),
                $this->extractXmlValue('daten/pfad', $xmlAnhang)
            );

            if($category == 'GRUNDRISS') {
                foreach($filenames as $filename) {
                    $filenames[] = str_replace('.jpg', '.gif', $filename);
                }
            }

            foreach($filenames as $filename) {

                if($this->copyImmoimageFile($filename)) {

                    $Immoimage = $this->objectManager->get('TYPO3\MbxRealestate\Domain\Model\Immoimage');
                    $Immoimage->setDescription($this->extractXmlValue('anhangtitel', $xmlAnhang));
                    $Immoimage->setCategory($category);
                    $Immoimage->setFilename($filename);
                    $Immoimage->setImagePath($destPathRelative);

                    array_push($arrObjImmoimages, $Immoimage);

                    break;
                }
            }

        }

        $immoDataItem->setArrImmoimage($arrObjImmoimages);

        return $immoDataItem;


        /**
         OLD structure from WGS:
         - Asteriks (*) means we've mapped the old value to a new value
         - Route (#) means the old value/field was saved in WGS DB but nowhere used in FE or for calculating/querying

       * ['OBJ'] = Unr		= (integer) $objekt->verwaltung_objekt->user_defined_anyfield->GA_ObjektId;															//Objektnummer
       * ['WOH'] = Wi		= (integer) $objekt->verwaltung_objekt->user_defined_anyfield->GA_WohnungsId;														//WohnungsID
       * ['HNR'] = Me 		= (integer) $objekt->verwaltung_objekt->user_defined_anyfield->GA_HausId;																//Hausnummer
       # $mappingArray[$i]['LAGE'] 		= (string) delBadChar($objekt->verwaltung_objekt->user_defined_anyfield->GA_Wohnungsnummer);						//Wohnungsnummer
       # $mappingArray[$i]['ANGEB_AKT'] 		= (integer) $objekt->verwaltung_objekt->user_defined_anyfield->GA_offeneAngebote;
       * ['VERFUEGBAR'] = TypeOfDisposition[] 	= (integer) getTrueOrFalse($objekt->objektkategorie->vermarktungsart[MIETE_PACHT]);									//Zur Miete?
       * ['VERF_VERK'] = TypeOfDisposition[] 		= (integer) getTrueOrFalse($objekt->objektkategorie->vermarktungsart[KAUF]);												//Zum Kauf
       * ['ANTEILE'] = businessShares 		= (string) delBadChar($objekt->verwaltung_objekt->user_defined_anyfield->GA_Genossenschaftsanteile);	//Genossenschaftsanteile
       * ['WG_MIET'] = District 		= (integer) $objekt->verwaltung_objekt->user_defined_anyfield->GA_Mietgebiet;													//Mietpreis
       * ['WB'] 		= (integer) $objekt->verwaltung_objekt->user_defined_anyfield->GA_Geschaeftsstelle;											//Gesch�ftsstelle
       * ['RAEUME'] = Rooms             = (string) delBadChar($objekt->flaechen->anzahl_zimmer);																								//Anzahl der R�ume
       * ['WOF'] = AreaGeneral       = (float) str_replace(',', '.', delBadChar($objekt->verwaltung_objekt->user_defined_anyfield->GA_WohflaecheMiete));					//Miete
       # $mappingArray[$i]['WOFL2'] 		= (float) str_replace(',', '.', delBadChar($objekt->verwaltung_objekt->user_defined_anyfield->GA_WohnflaecheVerkauf));				//Kaufpreis
       * ['STR'] = Street . Streetnr 		= (string) delBadChar($objekt->geo->strasse).' '.delBadChar($objekt->geo->hausnummer);									//Strasse mit Hausnummer
       * ['ORT'] = City 		= (string) delBadChar($objekt->geo->ort);																															//Ort
       * ['PLZ'] = Zip 		= (string) delBadChar($objekt->geo->plz);																															//plz
       * ['ETAGE'] = Floors 		= (string) delBadChar($objekt->geo->etage);																													//Etage
       * ['BALKON'] = $ImmofeatureBalcony 		= (integer) getTrueOrFalse($objekt->verwaltung_objekt->user_defined_anyfield->GA_isBalkon);
       * ['LOGGIA'] = $ImmofeatureLoggia 		= (integer) getTrueOrFalse($objekt->verwaltung_objekt->user_defined_anyfield->GA_isLoggia);
       # $mappingArray[$i]['BAD'] 		= (integer) getTrueOrFalse($objekt->verwaltung_objekt->user_defined_anyfield->GA_isBad);
       * ['DUSCHE'] = $ImmofeatureShower 		= (integer) getTrueOrFalse($objekt->verwaltung_objekt->user_defined_anyfield->GA_isDusche);
       * ['KUECHE'] = $ImmofeatureKitchen 		= (integer) getTrueOrFalse($objekt->verwaltung_objekt->user_defined_anyfield->GA_isKueche);
       * ['KELLER'] = $ImmofeatureCellar 		= (integer) getTrueOrFalse($objekt->verwaltung_objekt->user_defined_anyfield->GA_isKeller);
       * ['AUFZUG'] = $ImmofeatureLift 		= (integer) getTrueOrFalse($objekt->ausstattung->fahrstuhl[PERSONEN]);
       * ['EINBAUKU'] = $ImmofeatureEbk 		= (integer) getTrueOrFalse($objekt->ausstattung->kueche[EBK]);
       # $mappingArray[$i]['KATEGORIE'] 		= (integer) $objekt->verwaltung_objekt->user_defined_anyfield->GA_Zustandskategorienummer;
       # $mappingArray[$i]['MODSTUFE'] 		= (integer) $objekt->verwaltung_objekt->user_defined_anyfield->GA_Modernisierungsstufe;
       * ['BAUJAHR'] = YearBuild 		= (integer) $objekt->zustand_angaben->baujahr;
       # $mappingArray[$i]['KUEND_ZUM'] 		= (string) delBadChar(dateGerman2Mysql($objekt->verwaltung_objekt->user_defined_anyfield->GA_KuendigungZum));
       * ['UEBERG_AB'] = AvailableBy		= (string) delBadChar(dateGerman2Mysql($objekt->verwaltung_objekt->verfuegbar_ab));
       # $mappingArray[$i]['VERTRAG_AB'] 	= (string) delBadChar(dateGerman2Mysql($objekt->verwaltung_objekt->verfuegbar_ab));
       * ['NG_GRUND'] = CostNet 		= (string) delBadChar($objekt->preise->nettokaltmiete);
       * ['NG_NK'] = CostAttendant 		= (string) delBadChar($objekt->preise->nebenkosten);
       # $mappingArray[$i]['NG_STPL'] 		= (string) delBadChar($objekt->verwaltung_objekt->user_defined_anyfield->GA_Stellplatzmiete);
       * ['NG_GESAMT'] = CostGross 		= (string) delBadChar($objekt->preise->warmmiete);
       * ['VKP'] = CostBuy 		= (string) delBadChar($objekt->preise->kaufpreis);
       * ['INET_BEMT'] = Notice 		= (string) delBadChar($objekt->freitexte->objektbeschreibung);
       * ['ANSPRECHP1'] = $ImmocontactAnsprechpartner 	= (string) delBadChar($objekt->verwaltung_objekt->user_defined_anyfield->GA_Ansprechpartner);
       * ['ANSPRECHP2'] = $ImmocontactAnsprechpartner 	= (string) delBadChar($objekt->verwaltung_objekt->user_defined_anyfield->GA_AnsprechpartnerTelefon);
       * ['AKTION_1'] = actionOffer 		= (string) delBadChar($objekt->verwaltung_objekt->user_defined_anyfield->GA_Aktion1);
       # $mappingArray[$i]['AKTION_2'] 		= (string) delBadChar($objekt->verwaltung_objekt->user_defined_anyfield->GA_Aktion2);
       # $mappingArray[$i]['show']		= (string) delBadChar($objekt->verwaltung_techn->aktion[aktionart]);
     */

    }

}