<?php

namespace TYPO3\MbxRealestate\Helper\Import;

class AddressDataItem {

    CONST MODE_SAVE = 'save';
    CONST MODE_DROP = 'drop';
    
    /**
     * @var string
     */
    private $mode = self::MODE_SAVE;
    
    /**
     * @var \TYPO3\MbxRealestate\Domain\Model\Immoaddress|null  
     */
    private $immoaddress = null;
    
    /**
     * @var \TYPO3\MbxRealestate\Domain\Model\Immocontact[]|null  
     */
    private $arrImmocontact = null;
    
    public function __construct() { }
    
    /**
     * @param string $mode
     * @return \TYPO3\MbxRealestate\Helper\Import\ImmoDataItem
     */
    public function setMode($mode) {
        $this->mode = $mode;
        return $this;
    }

    /**
     * @return string
     */
    public function getMode() {
        return $this->mode;
    }
    
     /**
      * @return \TYPO3\MbxRealestate\Domain\Model\Immoaddress
      */
     public function getImmoaddress() {
         return $this->immoaddress;
     }

     /**
      * @return \TYPO3\MbxRealestate\Domain\Model\Immocontact[]
      */
     public function getArrImmocontact() {
         return $this->arrImmocontact;
     }


     /**
      * @param \TYPO3\MbxRealestate\Domain\Model\Immoaddress $immoaddress
      * @return \TYPO3\MbxRealestate\Helper\Import\ImmoDataItem
      */
     public function setImmoaddress($immoaddress) {
         $this->immoaddress = $immoaddress;
         return $this;
     }

     /**
      * Sets a collection of Immocontact objects
      * @param \TYPO3\MbxRealestate\Domain\Model\Immocontact $arrImmocontact
      * @return \TYPO3\MbxRealestate\Helper\Import\ImmoDataItem
      */
     public function setArrImmocontact($arrImmocontact) {
         $this->arrImmocontact = $arrImmocontact;
         return $this;
     }

}