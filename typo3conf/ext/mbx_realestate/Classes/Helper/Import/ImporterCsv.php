<?php

namespace TYPO3\MbxRealestate\Helper\Import;

use TYPO3\MbxRealestate\Controller\ImportAbstractCommandController;
use TYPO3\MbxRealestate\Helper\Exception\ImportImmoException;
use TYPO3\MbxRealestate\Helper\Import\Interfaces\ImporterInterface;

/**
 * Accepts following data for configuration: 
 * {
 *      file : string,
 *      csv : {
 *          delimiter : string,
 *          enclosure : string,
 *          escape : string
 *      } 
 * }
 */
class ImporterCsv extends ImporterAbstract implements ImporterInterface {

    CONST DELIMITER_COMMA = ',';
    CONST DELIMITER_SEMICOLON = ';';
    CONST DELIMITER_TAB = "\t";
    
    CONST ENCLOSURE_DOUBLE = '"';
    CONST ENCLOSURE_SINGLE = "'";
    
    CONST ESCAPE_BACKSLASH = '\\';
    
    CONST REGEX_COL_HEADER_CLEAN = '/[^a-zA-Z0-9\_\-]/';
    
    /**
     * @var array
     */
    protected $colHeaders = array();
    
    function __construct() { 
        parent::__construct();
    }
    
    /**
     * Openes the CSV file and reads first line for setting column header.
     * 
     * @return boolean|ImportImmoException
     */
    public function openFile() {
        if(!file_exists($file = $this->getFile()) || !is_file($file)) {
            
            return ImportAbstractCommandController::throwException('Import file "' . $file . '" does not exists in ' . __FILE__ . '::' . __FUNCTION__ . '()');
            
        } elseif(!($this->handle = @fopen($file, 'r'))) {
            
            return ImportAbstractCommandController::throwException('Unable to open file "' . $file . '" in ' . __FILE__ . '::' . __FUNCTION__ . '()');
        }
        
        $this->correlateColHeaders();
        
        return true;
    }
    
    /**
     * Reads the first row and push the column headers into an array for fast access via column names.
     * 
     * @return ImporterCsv
     */
    private function correlateColHeaders() {
        
        if(($data = fgetcsv($this->handle, 0, $this->getCsvDelimiter(), $this->getCsvEnclosure(), $this->getCsvEscape()))) {
            
            foreach($data as $key => $value) {
                
                $this->colHeaders[$key] = strtoupper(preg_replace(self::REGEX_COL_HEADER_CLEAN, '', $value));
            }
        }

        return $this;
    }
    
    /**
     * Searches the value of a field of $row by its column name $colName.
     * $colName is the cleaned name of the original column:
     * 
     *  such as "this is my Column-title!" will be found by "thisismyColumntitle"
     * 
     * 
     * @param string $colName
     * @param array $row
     * @return string
     */
    protected function getColVarByName($colName, $row) {
        $colNum = array_search(strtoupper($colName), $this->colHeaders);

        if($colNum !== false) {
            return utf8_encode(trim($row[$colNum]));
        }  else {
            return null;
        }
    }

    /**
     * Adds string keys (column names) to the $row item
     *
     * @param array $row
     * @return array
     */
    protected function rowToAssociativeArray(array $row = array()) {

        $result = array();

        foreach($this->colHeaders as $num => $colName) {
            $result[$colName] = $row[$num];
        }

        return $result;
    }
    
    /**
     * Reads all immo nodes from file and stores the into the $items var related to the configuration value 'import.types.xml.xpathImmoNode'.
     * @return ImporterCsv
     */
    public function storeItems() {
        
        while(($data = fgetcsv($this->handle, 0, $this->getCsvDelimiter(), $this->getCsvEnclosure(), $this->getCsvEscape()))) {
            
            $this->items[] = $data;
        }
        
        return $this;
    }
    
    /**
     * Returns the items of the XML immo file. (Attention: the items gets shifted each time iterateItem() is called!)
     * @return \SimpleXMLElement[]
     */
    public function &getItems() {
        
        if(is_null($this->items)) {
            $this->storeItems();
        }
        
        return $this->items;
    }
    
    /**
     * iterates to the next item of the immo file.
     * 
     * @return boolean|\SimpleXMLElement
     */
    public function iterateItem() {

        $items = &$this->getItems();
        
        if(($item = array_shift($items)) !== null) {
            return $item;
        }  else {
            return false;
        }                
    }
    
    /**
     * Returns the delimiter to use for reading CSV file
     * @return string
     */
    protected function getCsvDelimiter() {
        
        if(!empty($this->configuration['settings.']['import.'][$this->getScope().'.']['csv.']['delimiter'])) {
            return trim($this->configuration['settings.']['import.'][$this->getScope().'.']['csv.']['delimiter']);
        }  else {
            return self::DELIMITER_COMMA;
        }
    }
    
    /**
     * Returns the enclosure to use for reading CSV file
     * @return string
     */
    protected function getCsvEnclosure() {
        
        if(!empty($this->configuration['settings.']['import.'][$this->getScope().'.']['csv.']['enclosure'])) {
            return trim($this->configuration['settings.']['import.'][$this->getScope().'.']['csv.']['enclosure']);
        }  else {
            return self::ENCLOSURE_DOUBLE;
        }
    }
    
    /**
     * Returns the escape to use for reading CSV file
     * @return string
     */
    protected function getCsvEscape() {
        
        if(!empty($this->configuration['settings.']['import.'][$this->getScope().'.']['csv.']['escape'])) {
            return trim($this->configuration['settings.']['import.'][$this->getScope().'.']['csv.']['escape']);
        }  else {
            return self::ESCAPE_BACKSLASH;
        }
    }
    
    /**
     * Closes the CSV file.
     * 
     * @return ImporterCsv
     */
    public function closeFile() {
    
        if(is_resource($this->handle)) {
            
            fclose($this->handle);
        }
        
        return $this;
    }
}