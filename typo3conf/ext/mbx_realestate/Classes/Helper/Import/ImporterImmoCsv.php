<?php

namespace TYPO3\MbxRealestate\Helper\Import;

class ImporterImmoCsv extends \TYPO3\MbxRealestate\Helper\Import\ImporterCsv implements \TYPO3\MbxRealestate\Helper\Import\Interfaces\ImporterCsvInterface {

    function __construct() {
        parent::__construct();
    }

    /**
     * Retrieves all necessary data from input immo dataset
     * @param array $immoItem
     * @return \TYPO3\MbxRealestate\Helper\Import\ImmoDataItem
     */
    public function prepareItem($immoItem) {

        $arrObjFeatures =
        $arrObjAreas =
        $arrObjContacts = array();

        $Immoobject = $this->objectManager->get('TYPO3\MbxRealestate\Domain\Model\Immoobject');
        $Immoobject->setUnr($unr = $this->getColVarByName('UNR', $immoItem));
        $Immoobject->setWi($wi = $this->getColVarByName('WI', $immoItem));
        $Immoobject->setHs($hs = $this->getColVarByName('HS', $immoItem));
        $Immoobject->setMe($me = $this->getColVarByName('ME', $immoItem));
        $Immoobject->setFloors($this->getColVarByName('Geschoss', $immoItem));
        $Immoobject->setFloorsMax($this->getColVarByName('Geschosse', $immoItem));

        // set prices
        $Immoobject->setCostNet($this->makeFloat($this->getColVarByName('Kaltmiete', $immoItem)));
        $Immoobject->setCostGross($this->makeFloat($this->getColVarByName('Zielmiete', $immoItem)));
        $Immoobject->setCostAttendant($this->makeFloat($this->getColVarByName('Nebenkosten', $immoItem)));

        // Flächen

        $areaTypes = 'Raumflaeche_WR01,Raumflaeche_WR02,Raumflaeche_WR03,Raumflaeche_WR04,Raumflaeche_WR05,Raumflaeche_WR06,'
                    . 'Raumflaeche_WK01,Raumflaeche_WB01,'
                    . 'Raumflaeche_Neben';

        $areaGeneral = 0;
        $arrObjAreas = array();

        foreach(explode(',', $areaTypes) as $areaType) {
            if(($area = $this->makeFloat($this->getColVarByName($areaType, $immoItem))) && $area > 0) {

                $areaGeneral += $area;
                $areaObj = $this->objectManager->get('TYPO3\MbxRealestate\Domain\Model\Immoarea');
                $areaObj->setRoomType($areaType);
                $areaObj->setRoomSize($area);

                $arrObjAreas[] = $areaObj;
            }
        }

        $Immoobject->setAreaGeneral($areaGeneral);
        $Immoobject->setRooms((float)$this->getColVarByName('Raeume', $immoItem));

        $Immoobject->setYearBuild($this->getColVarByName('Baujahr', $immoItem));
        $Immoobject->setYearRestoration($this->getColVarByName('Sanierungsjahr', $immoItem));
        $Immoobject->setAvailableBy($this->getColVarByName('Verfuegbar_ab', $immoItem));
        $Immoobject->setNorthPoint($this->getColVarByName('Foto_Nordpfeil', $immoItem));

        foreach(explode(',', $this->getColVarByName('Ausstattung', $immoItem)) as $feature) {

            $featureVal = trim($feature);

            $Immofeature = new \TYPO3\MbxRealestate\Domain\Model\Immofeature();
            $Immofeature->setFeatureTitle($featureVal);
            $Immofeature->setFeatureType($Immofeature->getFeatureTypeByTitle());
            $Immofeature->setFeatureValue(1);
            $arrObjFeatures[] = $Immofeature;
        }

        $address = $this->getImmoaddressByAddress(
                $this->getColVarByName('HSTRASSE', $immoItem),
                null,
                $this->getColVarByName('HPLZ', $immoItem),
                null    // city not defined in CSV
        );
        $Immoobject->setImmoaddress($address);

        $return = new \TYPO3\MbxRealestate\Helper\Import\ImmoDataItem();
        $return ->setImmoobject($Immoobject)
                ->setArrImmoarea($arrObjAreas)
                ->setArrImmofeature($arrObjFeatures)
                ;

        return $return;

    }
}