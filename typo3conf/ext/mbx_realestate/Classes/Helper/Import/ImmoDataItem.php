<?php

namespace TYPO3\MbxRealestate\Helper\Import;

class ImmoDataItem {

    CONST MODE_SAVE = 'save';
    CONST MODE_DROP = 'drop';

    /**
     * @var string
     */
    private $mode = self::MODE_SAVE;

    /**
     * @var \TYPO3\MbxRealestate\Domain\Model\Immoobject|null
     */
    private $immoobject = null;

    /**
     * @var \TYPO3\MbxRealestate\Domain\Model\Immoarea[]|null
     */
    private $arrImmoarea = null;

    /**
     * @var \TYPO3\MbxRealestate\Domain\Model\Immoaddress|null
     */
//    private $immoaddress = null;

    /**
     * @var \TYPO3\MbxRealestate\Domain\Model\Immofeature[]|null
     */
    private $arrImmofeature = null;

    /**
     * @var \TYPO3\MbxRealestate\Domain\Model\Immoenvironment[]|null
     */
    private $arrImmoenvironment = null;

    /**
     * @var \TYPO3\MbxRealestate\Domain\Model\Immocontact[]|null
     */
//    private $arrImmocontact = null;

    /**
     * @var \TYPO3\MbxRealestate\Domain\Model\Immoimage[]|null
     */
    private $arrImmoimage = null;

    /**
     * @var int|null
     */
//    private $customerCenter = null;

    public function __construct() { }

    /**
     * @param string $mode
     * @return \TYPO3\MbxRealestate\Helper\Import\ImmoDataItem
     */
    public function setMode($mode) {
        $this->mode = $mode;
        return $this;
    }

    /**
     * @return string
     */
    public function getMode() {
        return $this->mode;
    }

    /**
     * @return \TYPO3\MbxRealestate\Domain\Model\Immoobject
     */
     public function getImmoobject() {
         return $this->immoobject;
     }

     /**
      * @return \TYPO3\MbxRealestate\Domain\Model\Immoarea[]
      */
     public function getArrImmoarea() {
         return $this->arrImmoarea;
     }

     /**
      * @return \TYPO3\MbxRealestate\Domain\Model\Immoaddress
      */
//     public function getImmoaddress() {
//         return $this->immoaddress;
//     }

     /**
      * @return \TYPO3\MbxRealestate\Domain\Model\Immofeature[]
      */
     public function getArrImmofeature() {
         return $this->arrImmofeature;
     }

     /**
      * @return \TYPO3\MbxRealestate\Domain\Model\Immoenvironment[]
      */
     public function getArrImmoenvironment() {
         return $this->arrImmoenvironment;
     }

     /**
      * @return \TYPO3\MbxRealestate\Domain\Model\Immocontact[]
      */
//     public function getArrImmocontact() {
//         return $this->arrImmocontact;
//     }

     /**
      * @return \TYPO3\MbxRealestate\Domain\Model\Immoimage[]
      */
     public function getArrImmoimage() {
         return $this->arrImmoimage;
     }

     /**
      * @return int|null
      */
//     public function getCustomerCenter() {
//         return $this->customerCenter;
//     }

     /**
      * @param \TYPO3\MbxRealestate\Domain\Model\Immoobject $immoobject
      * @return \TYPO3\MbxRealestate\Helper\Import\ImmoDataItem
      */
     public function setImmoobject(\TYPO3\MbxRealestate\Domain\Model\Immoobject $immoobject) {
         $this->immoobject = $immoobject;
         return $this;
     }

     /**
      * Sets a collection of Immoarea objects
      * @param \TYPO3\MbxRealestate\Domain\Model\Immoarea $arrImmoarea
      * @return \TYPO3\MbxRealestate\Helper\Import\ImmoDataItem
      */
     public function setArrImmoarea($arrImmoarea) {
         $this->arrImmoarea = $arrImmoarea;
         return $this;
     }

     /**
      * @param \TYPO3\MbxRealestate\Domain\Model\Immoaddress $immoaddress
      * @return \TYPO3\MbxRealestate\Helper\Import\ImmoDataItem
      */
//     public function setImmoaddress($immoaddress) {
//         $this->immoaddress = $immoaddress;
//         return $this;
//     }

     /**
      * Sets a collection of Immofeature objects
      * @param \TYPO3\MbxRealestate\Domain\Model\Immofeature $arrImmofeature
      * @return \TYPO3\MbxRealestate\Helper\Import\ImmoDataItem
      */
     public function setArrImmofeature($arrImmofeature) {
         $this->arrImmofeature = $arrImmofeature;
         return $this;
     }

     /**
      * Sets a collection of Immoenvironment objects
      * @param \TYPO3\MbxRealestate\Domain\Model\Immoenvironment $arrImmoenvironment
      * @return \TYPO3\MbxRealestate\Helper\Import\ImmoDataItem
      */
     public function setArrImmoenvironment($arrImmoenvironment) {
         $this->arrImmoenvironment = $arrImmoenvironment;
         return $this;
     }

     /**
      * Sets a collection of Immocontact objects
      * @param \TYPO3\MbxRealestate\Domain\Model\Immocontact $arrImmocontact
      * @return \TYPO3\MbxRealestate\Helper\Import\ImmoDataItem
      */
//     public function setArrImmocontact($arrImmocontact) {
//         $this->arrImmocontact = $arrImmocontact;
//         return $this;
//     }

     /**
      * Sets a collection of Immoimage objects
      * @param \TYPO3\MbxRealestate\Domain\Model\Immoimage $arrImmoimage
      * @return \TYPO3\MbxRealestate\Helper\Import\ImmoDataItem
      */
     public function setArrImmoimage($arrImmoimage) {
         $this->arrImmoimage = $arrImmoimage;
         return $this;
     }

     /**
      * Sets the customer center ID
      * @param int|null $customerCenter
      * @return \TYPO3\MbxRealestate\Helper\Import\ImmoDataItem
      */
//     public function setCustomerCenter($customerCenter) {
//         $this->customerCenter = $customerCenter;
//         return $this;
//     }


}