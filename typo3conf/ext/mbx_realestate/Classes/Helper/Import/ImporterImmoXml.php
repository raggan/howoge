<?php

namespace TYPO3\MbxRealestate\Helper\Import;

class ImporterImmoXml extends \TYPO3\MbxRealestate\Helper\Import\ImporterXml implements \TYPO3\MbxRealestate\Helper\Import\Interfaces\ImporterXmlInterface {

    function __construct() {
        parent::__construct();
    }

    /**
     * Reads the XML content from file and returns it or false if read failed.
     * @return boolean|\SimpleXMLElement
     */
    public function readXmlFromFile() {

        $xmlStr = file_get_contents($this->getFile());
        $xmlStrCorrected = preg_replace('/\<openimmo\s+([^\>]*)\>/Uis', '<openimmo>', $xmlStr);    // remove xmlns from root tag so we can parse the file

        $this->setXml(simplexml_load_string($xmlStrCorrected));

        if(!($this->getXml() instanceof \SimpleXMLElement)) {

            throw \TYPO3\MbxRealestate\Controller\ImportAbstractCommandController::throwException(__FUNCTION__ . '() failed in ' . __FILE__);
        }

        return $this->getXml();
    }

    /**
     * Reads all immo nodes from file and stores the into the $items var related to the configuration value 'import.types.xml.xpathImmoNode'.
     * @return \TYPO3\MbxRealestate\Helper\Import\ImporterXml
     */
    public function storeItems() {

        $immoNode = $this->getConfiguratioItem('settings.import.types.xml.xpathImmoNode');
        $this->items = $this->getXml()->xpath($immoNode);

        return $this;
    }

    /**
     * Retrieves all necessary data from input immo dataset
     * @param \SimpleXMLElement $immoItem
     * @return \TYPO3\MbxRealestate\Helper\Import\ImmoDataItem
     */
    public function prepareItem($immoItem) {

        $this->currentItem = $immoItem;

        $arrTypesOfDisposition =
        $arrTypesOfUse =
        $arrTypesOfEstate =
        $arrObjFeatures = array();

        $unr = $this->extractXmlValue('verwaltung_objekt/user_defined_anyfield/GA_ObjektId');
        $wi = $this->extractXmlValue('verwaltung_objekt/user_defined_anyfield/GA_WohnungsId');
        $hs = $this->extractXmlValue('verwaltung_objekt/user_defined_anyfield/GA_HausId');

        $Immoobject = $this->objectManager->get('TYPO3\MbxRealestate\Domain\Model\Immoobject');
        $Immoobject->setUnr($unr);
        $Immoobject->setWi($wi);
        $Immoobject->setHs($hs);
        $Immoobject->setMe($wi);
        $Immoobject->setFloors($this->extractXmlValue('geo/etage'));

        foreach($immoItem->xpath('objektkategorie/vermarktungsart') as $xmlDispositionType) {

            foreach($xmlDispositionType->attributes() as $vermarktungsArt => $vermarktungsStatus) {

                // check if TRUE|JA|YES|1 is set for the value
                if($this->castIsTrue($this->castValue($vermarktungsStatus))) {
                    $arrTypesOfDisposition = array_merge($arrTypesOfDisposition, explode('_', $vermarktungsArt));
                }
            }
        }

        $Immoobject->setTypeOfDisposition(implode(',', $arrTypesOfDisposition));

        foreach($immoItem->xpath('objektkategorie/nutzungsart') as $xmlTypeOfUse) {

            foreach($xmlTypeOfUse->attributes() as $nutzungsArt => $nutzungsStatus) {

                // check if TRUE|JA|YES|1 is set for the value
                if($this->castIsTrue($this->castValue($nutzungsStatus))) {
                    $arrTypesOfUse = array_merge($arrTypesOfUse, explode('_', $nutzungsArt));
                }
            }
        }

        $Immoobject->setTypeOfUse(implode(',', $arrTypesOfUse));

        foreach($immoItem->xpath('objektkategorie/objektart/*') as $xmlObjectType) {
            $arrTypesOfEstate[] = (string)$xmlObjectType->getName();
        }

        $Immoobject->setTypeEstate(implode(',', $arrTypesOfEstate));

        $address = $this->getImmoaddressByAddress(
                $this->extractXmlValue('geo/strasse') . ' ' . $this->extractXmlValue('geo/hausnummer'),
                null,
                $this->extractXmlValue('geo/plz'),
                $this->extractXmlValue('geo/ort')
        );

        $Immoobject->setImmoaddress($address);

        // set prices
        $Immoobject->setCostBuy((float)$this->extractXmlValue('preise/kaufpreis'));
        $Immoobject->setCostNet((float)$this->extractXmlValue('preise/nettokaltmiete'));
        $Immoobject->setCostGross((float)$this->extractXmlValue('preise/warmmiete'));
        $Immoobject->setCostAttendant((float)$this->extractXmlValue('preise/nebenkosten'));
        $Immoobject->setDeposit((float)$this->extractXmlValue('preise/kaution'));
        $Immoobject->setBusinessShares((float)$this->extractXmlValue('verwaltung_objekt/user_defined_anyfield/GA_Genossenschaftsanteile'));

        // Flächen

        $Immoobject->setAreaGeneral((float)$this->extractXmlValue('verwaltung_objekt/user_defined_anyfield/GA_WohflaecheMiete'));
        $Immoobject->setRooms((float)$this->extractXmlValue('flaechen/anzahl_zimmer'));

        $Immoobject->setYearBuild($this->extractXmlValue('zustand_angaben/baujahr'));
        $Immoobject->setTitle($this->extractXmlValue('freitexte/objekttitel'));
        $Immoobject->setNotice($this->extractXmlValue('freitexte/objektbeschreibung'));
        $Immoobject->setAvailableBy($this->extractXmlValue('verwaltung_objekt/verfuegbar_ab'));

        $aktion1 = $this->extractXmlValue('verwaltung_objekt/user_defined_anyfield/GA_Aktion1');
        $aktion2 = $this->extractXmlValue('verwaltung_objekt/user_defined_anyfield/GA_Aktion2');
        $offers = array();

        if(!empty($aktion1)) { $offers[] = $aktion1; }
        if(!empty($aktion2)) { $offers[] = $aktion2; }

        $Immoobject->setActionOffer(implode(',', $offers));


        $balcony = $this->castValue($this->extractXmlValue('verwaltung_objekt/user_defined_anyfield/GA_isBalkon'));
        if($this->castIsTrue($balcony)) {

            $ImmofeatureBalcony = new \TYPO3\MbxRealestate\Domain\Model\Immofeature();
            $ImmofeatureBalcony->setFeatureType('balcony');
            $ImmofeatureBalcony->setFeatureValue((int)$balcony);
            $arrObjFeatures[] = $ImmofeatureBalcony;
        }

        $loggia = $this->castValue($this->extractXmlValue('verwaltung_objekt/user_defined_anyfield/GA_isLoggia'));
        if($this->castIsTrue($loggia)) {

            $ImmofeatureLoggia = new \TYPO3\MbxRealestate\Domain\Model\Immofeature();
            $ImmofeatureLoggia->setFeatureType('loggia');
            $ImmofeatureLoggia->setFeatureValue((int)$loggia);
            $arrObjFeatures[] = $ImmofeatureLoggia;
        }

        $shower = $this->castValue($this->extractXmlValue('verwaltung_objekt/user_defined_anyfield/GA_isDusche'));
        if($this->castIsTrue($shower)) {

            $ImmofeatureShower = new \TYPO3\MbxRealestate\Domain\Model\Immofeature();
            $ImmofeatureShower->setFeatureType('shower');
            $ImmofeatureShower->setFeatureValue((int)$shower);
            $arrObjFeatures[] = $ImmofeatureShower;
        }

        $bath = $this->castValue($this->extractXmlValue('verwaltung_objekt/user_defined_anyfield/GA_isBad'));
        if($this->castIsTrue($bath)) {

            $ImmofeatureBath = new \TYPO3\MbxRealestate\Domain\Model\Immofeature();
            $ImmofeatureBath->setFeatureType('bath');
            $ImmofeatureBath->setFeatureValue((int)$bath);
            $arrObjFeatures[] = $ImmofeatureBath;
        }

        $cellar = $this->castValue($this->extractXmlValue('verwaltung_objekt/user_defined_anyfield/GA_isKeller'));
        if($this->castIsTrue($cellar)) {

            $ImmofeatureCellar = new \TYPO3\MbxRealestate\Domain\Model\Immofeature();
            $ImmofeatureCellar->setFeatureType('cellar');
            $ImmofeatureCellar->setFeatureValue((int)$cellar);
            $arrObjFeatures[] = $ImmofeatureCellar;
        }

        $liftXml = $immoItem->xpath('ausstattung/fahrstuhl');
        $liftAttributes = $liftXml[0]->attributes();
        $lift = $this->castValue($liftAttributes['PERSONEN']);
        if($this->castIsTrue($lift)) {

            $ImmofeatureLift = new \TYPO3\MbxRealestate\Domain\Model\Immofeature();
            $ImmofeatureLift->setFeatureType('lift');
            $ImmofeatureLift->setFeatureValue((int)$lift);
            $arrObjFeatures[] = $ImmofeatureLift;
        }

        $ebkXml = $immoItem->xpath('ausstattung/kueche');
        $ebkAttributes = $ebkXml[0]->attributes();
        $ebk = $this->castValue($ebkAttributes['EBK']);
        if($this->castIsTrue($ebk)) {

            $ImmofeatureEbk = new \TYPO3\MbxRealestate\Domain\Model\Immofeature();
            $ImmofeatureEbk->setFeatureType('ebk');
            $ImmofeatureEbk->setFeatureValue((int)$ebk);
            $arrObjFeatures[] = $ImmofeatureEbk;
        }

        $return = new \TYPO3\MbxRealestate\Helper\Import\ImmoDataItem();
        $return ->setImmoobject($Immoobject)
                ->setArrImmoarea(array())
                ->setArrImmofeature($arrObjFeatures)
                ;

        return $return;

    }
}