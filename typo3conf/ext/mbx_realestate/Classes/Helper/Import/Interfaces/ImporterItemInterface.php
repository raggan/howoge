<?php

namespace TYPO3\MbxRealestate\Helper\Import\Interfaces;

interface ImporterItemInterface {

    public function prepareItem($immoItem);
}