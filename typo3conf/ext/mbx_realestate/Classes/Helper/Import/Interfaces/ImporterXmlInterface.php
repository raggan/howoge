<?php

namespace TYPO3\MbxRealestate\Helper\Import\Interfaces;

interface ImporterXmlInterface extends \TYPO3\MbxRealestate\Helper\Import\Interfaces\ImporterItemInterface {

    /**
     * Reads all immo nodes from file and stores the into the $items var related to the configuration value 'import.types.xml.xpathImmoNode'.
     * @return \TYPO3\MbxRealestate\Helper\Import\ImporterXml
     */
    public function storeItems();
}