<?php

namespace TYPO3\MbxRealestate\Helper\Import\Interfaces;

interface ImporterCommandControllerInterface {

    public function importCommand();
}