<?php

namespace TYPO3\MbxRealestate\Helper\Import\Interfaces;

interface ImporterInterface {
    
    public function openFile();
    public function closeFile();
    public function iterateItem();
    public function &getItems();
}