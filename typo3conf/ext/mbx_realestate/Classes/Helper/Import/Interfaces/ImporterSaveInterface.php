<?php

namespace TYPO3\MbxRealestate\Helper\Import\Interfaces;

interface ImporterSaveInterface {
    
    public function saveObject($saveDataItem);
} 
