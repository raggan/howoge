<?php

namespace TYPO3\MbxRealestate\Helper\Import;

class ImporterAddressCsv extends \TYPO3\MbxRealestate\Helper\Import\ImporterCsv implements \TYPO3\MbxRealestate\Helper\Import\Interfaces\ImporterCsvInterface {

    function __construct() {
        parent::__construct();
    }

    /**
     * Retrieves all necessary data from input address dataset
     * @param array $addressItem
     * @return \TYPO3\MbxRealestate\Helper\Import\AddressDataItem
     */
    public function prepareItem($addressItem) {

        $ImmoAddress = $this->objectManager->get('TYPO3\MbxRealestate\Domain\Model\Immoaddress');

        $ImmoAddress->setUnr($this->getColVarByName('UNR', $addressItem));
        $ImmoAddress->setWi($this->getColVarByName('WI', $addressItem));
        $ImmoAddress->setHs($this->getColVarByName('HS', $addressItem));

        $ImmoAddress->setZip($this->getColVarByName('HPLZ', $addressItem));
        $ImmoAddress->setCity($this->getColVarByName('HORT', $addressItem));
        $ImmoAddress->setStreet(preg_replace('/\s+/', ' ', $this->getColVarByName('HSTRASSE', $addressItem)));
        $ImmoAddress->setStreetnr('');

        $ImmoAddress->setTimeOfVisit($this->getColVarByName('Besichtigung', $addressItem));
        $ImmoAddress->setGeoX($this->getColVarByName('GEOXKOOR', $addressItem));
        $ImmoAddress->setGeoY($this->getColVarByName('GEOYKOOR', $addressItem));

        $districtStr = $this->getColVarByName('WEB_Stadtteil', $addressItem);

        if(empty($districtStr)) {
            $districtStr = 'Sonstige Stadtteile';
        }

        $districtId = $this->getCommandControllerReference()->immoPluginHelper->getDistrictIdByDistrictStr($districtStr);

        $ImmoAddress->setDistrict($districtId);


        // get the customer center by mapping in TS
        $customerCenterStr = (string)$this->getColVarByName('Kuz', $addressItem);
        $customerCenterUid = $this->getCustomerCenterUid($customerCenterStr);
        $customerCenter = $this->getCustomerCenterById($customerCenterUid);

        if(!empty($customerCenter)) {

            $ImmoAddress->setCustomerCenter($customerCenter);
        }

        $arrObjContacts = array();

        // <editor-fold defaultstate="collapsed" desc="general contact person">
        $Immocontact = $this->objectManager->get('TYPO3\MbxRealestate\Domain\Model\Immocontact');
        $Immocontact->setContactType('general');
        $Immocontact->setContactName(   $this->getColVarByName('KB_Name', $addressItem));
        $Immocontact->setContactStreet( $this->getColVarByName('Kontakt_Strasse', $addressItem));
        $Immocontact->setContactZip(    $this->getColVarByName('Kontakt_Plz', $addressItem));
        $Immocontact->setContactCity(   $this->getColVarByName('Kontakt_Ort', $addressItem));
        $Immocontact->setContactMail(   $this->getColVarByName('t_email', $addressItem));
        $Immocontact->setContactPhone(  $this->getColVarByName('KB_Telefon', $addressItem));
        $Immocontact->setContactFax(    $this->getColVarByName('Kuz_Fax', $addressItem));

        $arrObjContacts[$Immocontact->getContactMd5()] = $Immocontact;

        // </editor-fold>


        // <editor-fold defaultstate="collapsed" desc="facility manager contact person">
        $ImmocontactHm = $this->objectManager->get('TYPO3\MbxRealestate\Domain\Model\Immocontact');
        $ImmocontactHm->setContactType('facility');
        $ImmocontactHm->setContactName(   $this->getColVarByName('HM_Name', $addressItem));
        $ImmocontactHm->setContactMobile(  $this->getColVarByName('HM_Telefon', $addressItem));

        $arrObjContacts[$ImmocontactHm->getContactMd5()] = $ImmocontactHm;

        // </editor-fold>


        // <editor-fold defaultstate="collapsed" desc="hirer contact person">
        $ImmocontactHirer = $this->objectManager->get('TYPO3\MbxRealestate\Domain\Model\Immocontact');
        $ImmocontactHirer->setContactType('hirer');
        $ImmocontactHirer->setContactName(   $this->getColVarByName('Verm_Name', $addressItem));
        $ImmocontactHirer->setContactMail(   $this->getColVarByName('Verm_Mail', $addressItem));
        $ImmocontactHirer->setContactPhone(  $this->getColVarByName('Verm_Telefon', $addressItem));

        $arrObjContacts[$ImmocontactHirer->getContactMd5()] = $ImmocontactHirer;

        // </editor-fold>



        $return = new \TYPO3\MbxRealestate\Helper\Import\AddressDataItem();
        $return ->setArrImmocontact($arrObjContacts)
                ->setImmoaddress($ImmoAddress)
                ;

        return $return;

    }
}