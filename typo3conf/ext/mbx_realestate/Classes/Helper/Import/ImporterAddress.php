<?php

namespace TYPO3\MbxRealestate\Helper\Import;

class ImporterAddress extends \TYPO3\MbxRealestate\Helper\Import\ImporterSave implements \TYPO3\MbxRealestate\Helper\Import\Interfaces\ImporterSaveInterface {
    
    /**
     * @var array
     */
    protected $requiredTables = array(
        self::TABLE_ADDRESS,
        self::TABLE_CONTACTS,
        self::TABLE_ADRESS_CONTACTS_nm
    );
    
    /**
     * Contains the unr-wi-hs as key and the related md5-hashes of the immocontacts as array values
     * @var array
     */
    private $nm = array();
    
    /**
     * 
     * @param type $importer
     */
    function __construct($importer) {
        
        parent::__construct($importer);
        
    }
    
    
    /**
     * 
     * @param \TYPO3\MbxRealestate\Helper\Import\AddressDataItem $addressDataItem
     */
    public function saveObject($addressDataItem) {

        $customerCenter = $addressDataItem->getImmoaddress()->getCustomerCenter();

        if($customerCenter instanceof \TYPO3\MbxRealestate\Domain\Model\Immocontact) {
            $customerCenterUid = $customerCenter->getUid();
        }  else {
            $customerCenterUid = null;
        }
        
        $this->pushInsert(array(
            $this->importer->getStoragePid(),
            $unr = $addressDataItem->getImmoaddress()->getUnr(),
            $wi = $addressDataItem->getImmoaddress()->getWi(),
            $hs = $addressDataItem->getImmoaddress()->getHs(),
            $addressDataItem->getImmoaddress()->getStreet(),
            $addressDataItem->getImmoaddress()->getStreetnr(),
            $addressDataItem->getImmoaddress()->getZip(),
            $addressDataItem->getImmoaddress()->getDistrict(),
            $addressDataItem->getImmoaddress()->getCity(),
            $addressDataItem->getImmoaddress()->getLocationDesc(),
            $customerCenterUid,
            $addressDataItem->getImmoaddress()->getGeoX(),
            $addressDataItem->getImmoaddress()->getGeoY(),
            $addressDataItem->getImmoaddress()->getTimeOfVisit(),
            $addressDataItem->getImmoaddress()->getFullObjectData()
        ), self::TABLE_ADDRESS);
        $contacts = array();
        
        foreach($addressDataItem->getArrImmocontact() as $immocontact) {
            
            $contactName = $immocontact->getContactName();
            
            // dont import empty contacts
            if(empty($contactName)) {
                continue;
            }
            
            $contacts[] = $immocontact->getContactMd5();
            
            $this->pushInsert(array(
                $this->importer->getStoragePid(),
                $immocontact->getContactMd5(),
                $immocontact->getContactName(),
                $immocontact->getContactStreet(),
                $immocontact->getContactStreetnr(),
                $immocontact->getContactZip(),
                $immocontact->getContactCity(),
                $immocontact->getContactCompany(),
                $immocontact->getContactPhone(),
                $immocontact->getContactFax(),
                $immocontact->getContactMail(),
                $immocontact->getContactMobile(),
                $immocontact->getContactUrl(),
                $immocontact->getContactType()
            ), self::TABLE_CONTACTS);
        }
        
        $this->nm[implode('-', array($unr, $wi, $hs))] = $contacts;
    }
    
    /**
     * This method creates a temporary table containing the relations to build between contacts and address items.
     */
    public function createNM() {
        
        $liveImmocontacts = $this->importer->immocontactRepository->findAll();
        $liveImmoaddress = $this->importer->immoaddressRepository->findAll();
        
        $md5mapping = array();
        
        /**
         * make array such as:
         * array(
         *  jlads97o23dasi7dasldh => 89,
         *  pa9sdf89034rdaslp230d => 90,
         *  ....
         * )
         */
        foreach($liveImmocontacts as $Immocontact) {
            $md5mapping[$Immocontact->getContactMd5()] = $Immocontact->getUid();
        }
        
        foreach($liveImmoaddress as $Immoaddress) {
            
            $uniqueAddressKey = $Immoaddress->getUnr().'-'.$Immoaddress->getWi().'-'.$Immoaddress->getHs();
            
            // check if address from DB was found in previous address import
            if(array_key_exists($uniqueAddressKey, $this->nm)) {
                
                $contactsForAddress =& $this->nm[$uniqueAddressKey];
                $sorting = 0;
                
                // iterate each contact items for the address dataset
                foreach($contactsForAddress as $contactMd5) {
                    
                    if(array_key_exists($contactMd5, $md5mapping)) {
                        
                        $this->pushInsert(array(
                            $this->importer->getStoragePid(),
                            $Immoaddress->getUid(),
                            $md5mapping[$contactMd5],
                            ++$sorting
                        ), self::TABLE_ADRESS_CONTACTS_nm);
                    }
                }
                
                unset($contactsForAddress);
            }
            
        }
        
        $this->sqlTempDataPush();
        $this->sqlTempDataTransfer(array(self::TABLE_ADRESS_CONTACTS_nm));
    }
    
}