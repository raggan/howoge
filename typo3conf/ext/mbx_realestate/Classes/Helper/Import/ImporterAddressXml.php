<?php

namespace TYPO3\MbxRealestate\Helper\Import;

class ImporterAddressXml extends \TYPO3\MbxRealestate\Helper\Import\ImporterXml implements \TYPO3\MbxRealestate\Helper\Import\Interfaces\ImporterXmlInterface {

    function __construct() {
        parent::__construct();
    }

    /**
     * Reads the XML content from file and returns it or false if read failed.
     * @return boolean|\SimpleXMLElement
     */
    public function readXmlFromFile() {
        
        $xmlStr = file_get_contents($this->getFile());
        $xmlStrCorrected = preg_replace('/\<openimmo\s+([^\>]*)\>/Uis', '<openimmo>', $xmlStr);    // remove xmlns from root tag so we can parse the file
        
        $this->setXml(simplexml_load_string($xmlStrCorrected));
        
        if(!($this->getXml() instanceof \SimpleXMLElement)) {
            
            throw \TYPO3\MbxRealestate\Controller\ImportAbstractCommandController::throwException(__FUNCTION__ . '() failed in ' . __FILE__);
        }

        return $this->getXml();
    }
    
    /**
     * Reads all immo nodes from file and stores the into the $items var related to the configuration value 'import.types.xml.xpathImmoNode'.
     * @return \TYPO3\MbxRealestate\Helper\Import\ImporterXml
     */
    public function storeItems() {
        
        $immoNode = $this->getConfiguratioItem('settings.import.types.xml.xpathImmoNode');    
        $this->items = $this->getXml()->xpath($immoNode);
        
        return $this;
    }
    /**
     * Retrieves all necessary data from input address dataset
     * @param \SimpleXMLElement $addressItem
     * @return \TYPO3\MbxRealestate\Helper\Import\AddressDataItem
     */
    public function prepareItem($addressItem) {

        $this->currentItem = $addressItem;
        
        $ImmoAddress = $this->objectManager->get('TYPO3\MbxRealestate\Domain\Model\Immoaddress');
        
        $ImmoAddress->setUnr($this->extractXmlValue('verwaltung_objekt/user_defined_anyfield/GA_ObjektId'));
        $ImmoAddress->setWi($this->extractXmlValue('verwaltung_objekt/user_defined_anyfield/GA_WohnungsId'));
        $ImmoAddress->setHs($this->extractXmlValue('verwaltung_objekt/user_defined_anyfield/GA_HausId'));
        $ImmoAddress->setStreet($this->extractXmlValue('geo/strasse') . ' ' . $this->extractXmlValue('geo/hausnummer'));
        $ImmoAddress->setZip($this->extractXmlValue('geo/plz'));
        $ImmoAddress->setDistrict($this->extractXmlValue('verwaltung_objekt/user_defined_anyfield/GA_Mietgebiet'));
        $ImmoAddress->setCity($this->extractXmlValue('geo/ort'));
        
        // get the customer center by mapping in TS
        $customerCenterStr = (string)$this->extractXmlValue('verwaltung_objekt/user_defined_anyfield/GA_Geschaeftsstelle');
        $customerCenterUid = $this->getCustomerCenterUid($customerCenterStr);        
        $customerCenter = $this->getCustomerCenterById($customerCenterUid);

        if(!empty($customerCenter)) {
            
            $ImmoAddress->setCustomerCenter($customerCenter);
        }
        
        $arrObjContacts = array();
        
        // <editor-fold defaultstate="collapsed" desc="general contact person">
        $Immocontact = $this->objectManager->get('TYPO3\MbxRealestate\Domain\Model\Immocontact');
        $Immocontact->setContactType('general');
        $Immocontact->setContactName(   $this->extractXmlValue('kontaktperson/name'));
        $Immocontact->setContactStreet( $this->extractXmlValue('kontaktperson/strasse'));
        $Immocontact->setContactStreetnr( $this->extractXmlValue('kontaktperson/hausnummer'));
        $Immocontact->setContactZip(    $this->extractXmlValue('kontaktperson/plz'));
        $Immocontact->setContactCity(   $this->extractXmlValue('kontaktperson/ort'));
        $Immocontact->setContactMail(   $this->extractXmlValue('kontaktperson/email_zentrale'));
        $Immocontact->setContactPhone(  $this->extractXmlValue('kontaktperson/tel_zentrale'));
        $Immocontact->setContactFax(    $this->extractXmlValue('kontaktperson/tel_fax'));
        $Immocontact->setContactCompany($this->extractXmlValue('kontaktperson/firma'));
        $Immocontact->setContactUrl(    $this->extractXmlValue('kontaktperson/url'));
                
        $arrObjContacts[$Immocontact->getContactMd5()] = $Immocontact;

        // </editor-fold>

        
        // <editor-fold defaultstate="collapsed" desc="interestcontact person">
        $ImmocontactHm = $this->objectManager->get('TYPO3\MbxRealestate\Domain\Model\Immocontact');
        $ImmocontactHm->setContactType('interestcontact');
        $ImmocontactHm->setContactName(   $this->extractXmlValue('verwaltung_objekt/user_defined_anyfield/GA_Ansprechpartner'));
        $ImmocontactHm->setContactPhone(  $this->extractXmlValue('verwaltung_objekt/user_defined_anyfield/GA_AnsprechpartnerTelefon'));
                
        $arrObjContacts[$ImmocontactHm->getContactMd5()] = $ImmocontactHm;

        // </editor-fold>

        $return = new \TYPO3\MbxRealestate\Helper\Import\AddressDataItem();
        $return ->setArrImmocontact($arrObjContacts)
                ->setImmoaddress($ImmoAddress)
                ;

        return $return;
        
    }
}