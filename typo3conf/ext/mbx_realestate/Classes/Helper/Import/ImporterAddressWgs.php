<?php

namespace TYPO3\MbxRealestate\Helper\Import;

class ImporterAddressWgs extends \TYPO3\MbxRealestate\Helper\Import\ImporterAddressXml {

    function __construct() {
        parent::__construct();
    }

    /**
     * Searchs for the correct input file
     * @return string
     */
    public final function getFile() {

        $path = PATH_site . $this->getConfiguratioItem('settings.import.' . $this->getScope() . '.inputPath');
        
        $sort = array();
        $filenameXML = false;
        
        if(($directory = opendir($path))) {

            while ($file = readdir($directory)) {
                if (filetype($path . $file) != 'dir') {    
                    
                    $test = null;
                    if (preg_match("/([0-9]{14})/", $file, $test)){                        
                        $sort[] = $test[0];
                    }
                }
            }

            closedir($directory); 
            
            // got filelist ... check if not empty and files are valid
            if (count($sort)) {

                // get latest file
                rsort($sort, SORT_NUMERIC);

                $filenameXML = 'Homepage_der_WGS-12321-' . $sort[0] . '.xml';
            }
        }
        
        return $path . $filenameXML;
    }
    
    /**
     * Retrieves all necessary data from input address dataset
     * @param \SimpleXMLElement $addressItem
     * @return \TYPO3\MbxRealestate\Helper\Import\AddressDataItem
     */
    public function prepareItem($addressItem) {

        $addressDataItem = parent::prepareItem($addressItem);

        /**
         * @author Denis Krueger
         * @info Bugfix 2014-07-01 
         * 
         * Because old districts where mapped by a fixed table 'immo_objecturbandistrict' we select the corresponding 
         * urbandistrictid and use those as district id when importing data. So dont drop Table 'immo_objecturbandistrict' !!!
         */
        
        $ImmoAddress = $addressDataItem->getImmoaddress();
        $unr = $ImmoAddress->getUnr();
        $districtId = 0;
        
        $result = $GLOBALS['TYPO3_DB']->exec_SELECTquery('urbandirstrictid', 'immo_objecturbandistrict', 'objectid=' . $unr);
        
        if ($GLOBALS['TYPO3_DB']->sql_num_rows($result)) {                            
            if(($row = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($result))) {
                $districtId = $row['urbandirstrictid'];
            }
        }

        $ImmoAddress->setDistrict($districtId);
        
        $addressDataItem->setImmoaddress($ImmoAddress);
        
        return $addressDataItem;
    }
}