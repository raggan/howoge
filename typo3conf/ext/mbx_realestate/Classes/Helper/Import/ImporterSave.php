<?php

namespace TYPO3\MbxRealestate\Helper\Import;

abstract class ImporterSave extends \TYPO3\MbxRealestate\Helper\Import\ImporterAbstract {

    CONST EXPLICITLY_NULL = 'noData';
    CONST SQL_TRANSFER_STEPS = 1000;

    CONST TABLE_ADDRESS = 'address';
    CONST TABLE_IMMOOBJECTS = 'immoobjects';
    CONST TABLE_AREAS = 'immoareas';
    CONST TABLE_CONTACTS = 'contacts';
    CONST TABLE_FEATURES = 'features';
    CONST TABLE_ENVIRONMENTS = 'environments';
    CONST TABLE_IMAGES = 'images';
    CONST TABLE_ADRESS_CONTACTS_nm = 'address_to_contacts_nm';
    CONST TABLE_IMMOOBJECT_FEATURES_nm = 'immoobject_to_features_nm';

    /**
     * @var array
     */
    protected static $columsTables = array(

        self::TABLE_ADDRESS => array(
            'pid'               => "int(11) DEFAULT '0' NOT NULL",
            'unr'               => "varchar(20) DEFAULT '' NOT NULL",
            'wi'                => " varchar(20) DEFAULT '' NOT NULL",
            'hs'                => " varchar(20) DEFAULT '' NOT NULL",
            'street'            => " varchar(255) DEFAULT '' NOT NULL",
            'streetnr'          => " varchar(255) DEFAULT '' NOT NULL",
            'zip'               => " varchar(255) DEFAULT '' NOT NULL",
            'district'          => " varchar(255) DEFAULT '' NOT NULL",
            'city'              => " varchar(255) DEFAULT '' NOT NULL",
            'location_desc'     => " text NOT NULL",
            'customer_center'   => " int(11) unsigned DEFAULT '0'",
            'geo_x'             => " double(13,11) DEFAULT NULL",
            'geo_y'             => " double(13,11) DEFAULT NULL",
            'time_of_visit'     => " varchar(255) DEFAULT '' NOT NULL",
            'full_object_data'  => " text NOT NULL",
        ),

        self::TABLE_CONTACTS => array(
            'pid'               => "int(11) DEFAULT '0' NOT NULL",
            'contact_md5'       => " varchar(32) DEFAULT '' NOT NULL",
            'contact_name'      => " varchar(255) DEFAULT '' NOT NULL",
            'contact_street'    => " varchar(255) DEFAULT '' NOT NULL",
            'contact_streetnr'  => " varchar(255) DEFAULT '' NOT NULL",
            'contact_zip'       => " varchar(255) DEFAULT '' NOT NULL",
            'contact_city'      => " varchar(255) DEFAULT '' NOT NULL",
            'contact_company'   => " varchar(255) DEFAULT '' NOT NULL",
            'contact_phone'     => " varchar(255) DEFAULT '' NOT NULL",
            'contact_fax'       => " varchar(255) DEFAULT '' NOT NULL",
            'contact_mail'      => " varchar(255) DEFAULT '' NOT NULL",
            'contact_mobile'    => " varchar(255) DEFAULT '' NOT NULL",
            'contact_url'       => " varchar(255) DEFAULT '' NOT NULL",
            'contact_type'      => " varchar(255) DEFAULT '' NOT NULL"
        ),

        self::TABLE_ADRESS_CONTACTS_nm => array(
            'pid'               => "int(11) DEFAULT '0' NOT NULL",
            'uid_local'         => " int(11) unsigned DEFAULT '0' NOT NULL",
            'uid_foreign'       => " int(11) unsigned DEFAULT '0' NOT NULL",
            'sorting'           => " int(11) unsigned DEFAULT '0' NOT NULL"
        ),

        self::TABLE_IMMOOBJECTS     => array(
            'pid'                   => "int(11) DEFAULT '0' NOT NULL",
            'unr'                   => " varchar(20) DEFAULT '' NOT NULL",
            'wi'                    => " varchar(20) DEFAULT '' NOT NULL",
            'hs'                    => " varchar(20) DEFAULT '' NOT NULL",
            'me'                    => " varchar(20) DEFAULT '' NOT NULL",
            'full_object_data'      => " text NOT NULL",
            'rooms'                 => " double(11,2) DEFAULT '0.00' NOT NULL",
            'available_by'          => " varchar(255) DEFAULT '' NOT NULL",
            'cost_gross'            => " double(11,2) DEFAULT '0.00' NOT NULL",
            'cost_attendant'        => " double(11,2) DEFAULT '0.00' NOT NULL",
            'cost_heat'        => " double(11,2) DEFAULT '0.00' NOT NULL",
            'cost_net'              => " double(11,2) DEFAULT '0.00' NOT NULL",
            'cost_buy'              => " double(11,2) DEFAULT '0.00' NOT NULL",
            'deposit'               => " double(11,2) DEFAULT '0.00' NOT NULL",
            'provision'               => " tinyint(1) unsigned DEFAULT '0' NOT NULL",
            'provision_net'         => " double(11,2) DEFAULT '0.00' NOT NULL",
            'provision_brut'        => " double(11,2) DEFAULT '0.00' NOT NULL",
            'year_build'            => " int(11) DEFAULT '0' NOT NULL",
            'year_restoration'      => " int(11) DEFAULT '0' NOT NULL",
            'notice_restoration'    => "varchar(255) DEFAULT '' NOT NULL",
            'floors'                => " double(11,2) DEFAULT '0.00' NOT NULL",
            'floors_max'            => " varchar(255) DEFAULT '' NOT NULL",
            'area_general'          => " double(11,2) DEFAULT '0.00' NOT NULL",
            'area_land'             => " double(11,2) DEFAULT '0.00' NOT NULL",
            'area_usable'           => " double(11,2) DEFAULT '0.00' NOT NULL",
            'area_office'           => " double(11,2) DEFAULT '0.00' NOT NULL",
            'area_storage'          => " double(11,2) DEFAULT '0.00' NOT NULL",
            'area_all'              => " double(11,2) DEFAULT '0.00' NOT NULL",
            'number_parking_space'  => " int(11) DEFAULT '0' NOT NULL",
            'north_point'           => " int(11) DEFAULT '0' NOT NULL",
            'type_estate'           => " varchar(255) DEFAULT '' NOT NULL",
            'type_estate_precise'   => " varchar(255) DEFAULT '' NOT NULL",
            'type_of_use'           => " varchar(255) DEFAULT '' NOT NULL",
            'type_of_disposition'   => " varchar(255) DEFAULT '' NOT NULL",
            'type_building'         => " varchar(255) DEFAULT '' NOT NULL",
            'required_wbs'          => " tinyint(1) unsigned DEFAULT '0' NOT NULL",
            'title'                 => " varchar(255) DEFAULT '' NOT NULL",
            'is_investment'         => " tinyint(1) unsigned DEFAULT '0' NOT NULL",
            'business_shares'       => " double(11,2) DEFAULT '0.00' NOT NULL",
            'business_shares_num'   => " tinyint(2) unsigned DEFAULT '0' NOT NULL",
            'business_shares_desc'  => " varchar(255) DEFAULT '' NOT NULL",
            'notice'                => " text NOT NULL",
            'description'           => " text NOT NULL",
            'action_offer'          => " varchar(255) DEFAULT '' NOT NULL",
            'energy_certificate_type'               => " tinyint(4) unsigned DEFAULT '0' NOT NULL",
            'energy_efficiency_class'               => " varchar(20) DEFAULT '' NOT NULL",
            'epart_valid_to'                        => " varchar(20) DEFAULT '' NOT NULL",
            'thermal_characteristic'                => " double(11,2) DEFAULT '0.00' NOT NULL",
            'thermal_characteristic_electricity'    => " double(11,2) DEFAULT '0.00' NOT NULL",
            'thermal_characteristic_heating'        => " double(11,2) DEFAULT '0.00' NOT NULL",
            'thermal_heating_method'                => " varchar(255) DEFAULT '' NOT NULL",
            'energy_source'                         => " varchar(255) DEFAULT '' NOT NULL",
            'energy_contains_warm_water'            => " tinyint(4) unsigned DEFAULT '0' NOT NULL",
            'protected_historic_building'           => " tinyint(4) unsigned DEFAULT '0' NOT NULL",
            'corridor'                              => " varchar(255) DEFAULT '' NOT NULL",
            'splitting'                             => " int(11) DEFAULT '0' NOT NULL",
            'infrastructure_provision'              => " varchar(255) DEFAULT '' NOT NULL",
            'build_after'                           => " varchar(255) DEFAULT '' NOT NULL",
            'situation_of_building'                 => " varchar(255) DEFAULT '' NOT NULL",
            'number_flats'                          => " int(11) DEFAULT '0' NOT NULL",
            'number_companies'                      => " int(11) DEFAULT '0' NOT NULL",
            'immoaddress'           => " int(11) unsigned DEFAULT '0' NOT NULL"
        ),

        self::TABLE_AREAS => array(
            'pid'               => " int(11) DEFAULT '0' NOT NULL",
            'room_type'         => " varchar(30) DEFAULT '' NOT NULL",
            'room_size'         => " double(11,2) DEFAULT '0.00' NOT NULL",
            'area_md5'          => " varchar(32) DEFAULT '' NOT NULL",          // contains unr,wi,me,room_type
            'immoobject_md5'    => " varchar(32) DEFAULT '' NOT NULL"           // contains unr,wi,me
        ),

        self::TABLE_FEATURES => array(
            'pid'               => " int(11) DEFAULT '0' NOT NULL",
            'feature_type'      => " varchar(40) DEFAULT '' NOT NULL",
            'feature_title'     => " varchar(255) DEFAULT '' NOT NULL",
            'is_top_feature'     => " tinyint(1) unsigned DEFAULT '0' NOT NULL",
            'feature_value'     => " varchar(255) DEFAULT '' NOT NULL",
            'feature_cast'      => " varchar(255) DEFAULT '' NOT NULL",
            'feature_md5'       => " varchar(32) DEFAULT '' NOT NULL"
        ),

        self::TABLE_ENVIRONMENTS => array(
            'pid'               => " int(11) DEFAULT '0' NOT NULL",
            'environment_type'  => " varchar(30) DEFAULT '' NOT NULL",
            'environment_title'  => " varchar(255) DEFAULT '' NOT NULL",
            'environment_md5'   => " varchar(32) DEFAULT '' NOT NULL",
            'immoobject_md5'    => " varchar(32) DEFAULT '' NOT NULL"
        ),

        self::TABLE_IMAGES => array(
            'pid'               => " int(11) DEFAULT '0' NOT NULL",
            'image_type'        => " varchar(255) DEFAULT '' NOT NULL",
            'image_path'        => " varchar(255) DEFAULT '' NOT NULL",
            'filename'          => " varchar(255) DEFAULT '' NOT NULL",
            'description'       => " varchar(255) DEFAULT '' NOT NULL",
            'title'             => " varchar(30) DEFAULT '' NOT NULL",
            'category'          => " varchar(255) DEFAULT '' NOT NULL",
            'image_md5'         => " varchar(32) DEFAULT '' NOT NULL",          // contains unr,wi,me,filename
            'immoobject_md5'    => " varchar(32) DEFAULT '' NOT NULL"           // contains unr,wi,me
        ),

        self::TABLE_IMMOOBJECT_FEATURES_nm => array(
            'pid'               => "int(11) DEFAULT '0' NOT NULL",
            'uid_local'         => " int(11) unsigned DEFAULT '0' NOT NULL",
            'uid_foreign'       => " int(11) unsigned DEFAULT '0' NOT NULL",
            'sorting'           => " int(11) unsigned DEFAULT '0' NOT NULL"
        )
    );

    /**
     * @var array
     */
    protected static $uniques = array(

        self::TABLE_ADDRESS => array(
            'UNIQUE KEY `unique_address`' => array('unr', 'wi', 'hs')
        ),

        self::TABLE_CONTACTS => array(
            'UNIQUE KEY `unique_contact`' => array('contact_md5')
        ),

        self::TABLE_ADRESS_CONTACTS_nm => array(
            'UNIQUE KEY `unique_relation`' => array('uid_local', 'uid_foreign')
        ),

        self::TABLE_IMMOOBJECTS => array(
            'UNIQUE KEY `unique_immo`' => array('unr', 'wi', 'hs', 'me')
        ),

        self::TABLE_AREAS => array(
            'UNIQUE KEY `unique_area`' => array('area_md5')
        ),

        self::TABLE_FEATURES => array(
            'UNIQUE KEY `unique_feature`' => array('feature_md5')
        ),

        self::TABLE_ENVIRONMENTS => array(
            'UNIQUE KEY `unique_environment`' => array('environment_md5')
        ),

        self::TABLE_IMAGES => array(
            'UNIQUE KEY `unique_image`' => array('image_md5')
        ),

        self::TABLE_IMMOOBJECT_FEATURES_nm => array(
            'UNIQUE KEY `unique_relation`' => array('uid_local', 'uid_foreign')
        )
    );

    /**
     * @var array
     */
    protected $requiredTables = array();

    /**
     * Keeps temporary insert statements
     * @var array
     */
    protected $sqlInserts = array();

    /**
     * Containing the amount of inserted entries in the temporary tables
     * @var array
     */
    protected $countInserts = array();

    /**
     * A reference to the importer class to call functions fromin the save handler
     * @var \TYPO3\MbxRealestate\Helper\Import\ImporterAbstract
     */
    public $importer;

    function __construct($importer) {

        $this->importer = $importer;

        parent::__construct();

        $this->createTemporaryTables($this->requiredTables);
    }

    function __destruct() {
        $this->dropTemporaryTables();
    }

    /**
     * Creates the temporary tables where we add the imported data.
     *
     * @return boolean
     * @throws Exception
     */
    protected function createTemporaryTables() {

        $tables = $this->requiredTables;

        $this->dropTemporaryTables();

        $this->log(__FUNCTION__);

        foreach($tables as $table) {

            // <editor-fold defaultstate="collapsed" desc="create fields and append uniques if defined">
            $fields = array($this->getCreateTableColumns($table));

            if(array_key_exists($table, self::$uniques)) {
                foreach(self::$uniques[$table] as $unique => $uniqueFields) {
                    $fields[] = " {$unique} (" . implode(',', $uniqueFields) . ")";
                }
            }
            // </editor-fold>

            $sql = "CREATE TABLE IF NOT EXISTS " . $this->getTableName($table) . " (" . implode(', ', $fields) . " ) ENGINE=MyISAM DEFAULT CHARSET=utf8";

            if(($created = $GLOBALS['TYPO3_DB']->sql_query($sql)) === false) {

                \TYPO3\MbxRealestate\Controller\ImportAbstractCommandController::throwException('Failed to CREATE "' . $table . '" table! ' . $sql);

            }  else {

                $this->log('Created temporary "' . $table . '" table');
            }
        }

        $this->log('Created temporary tables');

        return true;
    }

    /**
     *
     * @param array $data
     * @param string $type
     */
    protected function pushInsert($data, $type) {

        if(!array_key_exists($type, $this->sqlInserts)) {
            $this->sqlInserts[$type] = array();
        }

        $this->sqlInserts[$type][] = $this->createUpdateListStr($data);
    }

    /**
     * Creates a string that can be used for MySQL INSERT Statements e.g. '("val1", "2.99", "val3", "some text here")'
     *
     * @param array $data
     * @return string
     */
    private function createUpdateListStr($data) {

        $items = array();
        foreach($data as $val) {
            $items[] = (($val === self::EXPLICITLY_NULL ) ? 'NULL' : '"' . $GLOBALS['TYPO3_DB']->quoteStr($val, null) . '"');
        }

        return '(' . implode(', ', $items) . ')';
    }

    /**
     *
     * @param string $table
     * @return boolean|string
     * @throws Exception
     */
    private function getTableName($table) {

        switch($table) {
            case self::TABLE_ADDRESS :
                return 'tmp_import_mbx_realestate_address';
            case self::TABLE_CONTACTS :
                return 'tmp_import_mbx_realestate_contacts';
            case self::TABLE_ADRESS_CONTACTS_nm :
                return 'tmp_import_mbx_realestate_address_contacts_mm';
            case self::TABLE_IMMOOBJECTS :
                return 'tmp_import_mbx_realestate_immoobjects';
            case self::TABLE_AREAS :
                return 'tmp_import_mbx_realestate_immoareas';
            case self::TABLE_FEATURES :
                return 'tmp_import_mbx_realestate_immofeatures';
            case self::TABLE_IMAGES :
                return 'tmp_import_mbx_realestate_immoimages';
            case self::TABLE_ENVIRONMENTS :
                return 'tmp_import_mbx_realestate_immoenvironments';
            case self::TABLE_IMMOOBJECT_FEATURES_nm :
                return 'tmp_import_mbx_realestate_immoobject_immofeature_mm';
            default :
                \TYPO3\MbxRealestate\Controller\ImportAbstractCommandController::throwException('Unable to ' . __FUNCTION__ . '() because table "' . $table . '" not found.');
        }
    }

    /**
     *
     * @param string $table
     * @return string
     * @throws Exception
     */
    private function getTargetTable($table) {

        switch($table) {
            case self::TABLE_ADDRESS :
                return 'tx_mbxrealestate_domain_model_immoaddress';
            case self::TABLE_CONTACTS :
                return 'tx_mbxrealestate_domain_model_immocontact';
            case self::TABLE_ADRESS_CONTACTS_nm :
                return 'tx_mbxrealestate_immoaddress_immocontact_mm';
            case self::TABLE_IMMOOBJECTS :
                return 'tx_mbxrealestate_domain_model_immoobject';
            case self::TABLE_AREAS :
                return 'tx_mbxrealestate_domain_model_immoarea';
            case self::TABLE_FEATURES :
                return 'tx_mbxrealestate_domain_model_immofeature';
            case self::TABLE_IMAGES :
                return 'tx_mbxrealestate_domain_model_immoimage';
            case self::TABLE_ENVIRONMENTS :
                return 'tx_mbxrealestate_domain_model_immoenvironment';
            case self::TABLE_IMMOOBJECT_FEATURES_nm :
                return 'tx_mbxrealestate_immoobject_immofeature_mm';
            default :
                \TYPO3\MbxRealestate\Controller\ImportAbstractCommandController::throwException('Unable to ' . __FUNCTION__ . '() because table "' . $table . '" not found.');
        }
    }


    /**
     * Returns the CREATE fields statement
     *
     * @param string $table
     * @param boolean $implode
     * @return string
     */
    private function getCreateTableColumns($table, $implode = true) {

        $data = self::$columsTables[$table];
        $strings = array();

        foreach($data as $colName => $colData) {
            $strings[] = $colName . ' ' . $colData;
        }

        if($implode) {
            return implode(', ', $strings);
        }  else {
            return $strings;
        }
    }


    /**
     * This method inserts products, features and prices into the temporary tables
     *
     * @return boolean
     * @throws Exception
     */
    public function sqlTempDataPush() {

        foreach($this->requiredTables as $table) {

            switch($table) {

                // customize your inserts here comparing $table with self::TABLE_*

                default :
                    if(count($this->sqlInserts[$table])) {

                        $t = $this->getTableName($table);

                        $sql = "INSERT INTO " . $t . "
                                    (" . implode(', ', $this->getTransferFields($table, $t)) . ")
                                    VALUES " . implode(",\n", $this->sqlInserts[$table]) . "
                                    ON DUPLICATE KEY UPDATE " . implode(",\n", $this->getDuplicateUpdateStatement($table, $t));

                        if(($inserted = $GLOBALS['TYPO3_DB']->sql_query($sql)) === false) {

                            \TYPO3\MbxRealestate\Controller\ImportAbstractCommandController::throwException('Failed to ' . __CLASS__ .'::'. __FUNCTION__
                                    . ' (INSERT ' . $table . ')! '
                                    . ' SQL: ' . $sql . ' /'
                                    . ' Error: ' . $GLOBALS['TYPO3_DB']->sql_error());

                        }  else {

                            if(!array_key_exists($table, $this->countInserts)) {
                                $this->countInserts[$table] = 0;
                            }

                            $this->countInserts[$table] += count($this->sqlInserts[$table]);

                            $this->sqlInserts[$table] = array();
                            $this->log('Inserted temp ' . $table);
                        }
                    }

                    break;
            }
        }

        return true;
    }

    /**
     * This method transfers the temporary data from the temp tables to the live tables.
     *
     * @param array|null $tables
     * @return boolean
     * @throws Exception
     */
    public function sqlTempDataTransfer($tables = null) {

        /**
         * INSERT INTO ks_web_products (
         *      ks_web_products.ean,
         *      ks_web_products.products_name,
         *      ks_web_products.segment,
         *      [...]
         *      ks_web_products.entry_updated,
         *      ks_web_products.deleted
         * )
         * (SELECT
         *      temp.ean,
         *      temp.products_name,
         *      temp.segment,
         *      [...]
         *      temp.entry_updated,
         *      temp.deleted
         * FROM tmp_import_products_beauty temp
         * )
         * ON DUPLICATE KEY UPDATE
         *      ks_web_products.ean = VALUES(ks_web_products.ean),
         *      ks_web_products.products_name = VALUES(ks_web_products.products_name),
         *      ks_web_products.segment = VALUES(ks_web_products.segment),
         *      [...]
         *      ks_web_products.entry_updated = VALUES(ks_web_products.entry_updated),
         *      ks_web_products.deleted = VALUES(ks_web_products.deleted)
         */

        $sqlStatements = array();

        if(empty($tables)) {
            $useTables = $this->requiredTables;
        }  else {
            $useTables = $tables;
        }

        foreach($useTables as $table) {

            $t = $this->getTargetTable($table);
            $sqlStatements[$table] = "INSERT INTO " . $t . "
                                    (" . implode(', ', $this->getTransferFields($table, $t)) . ")
                                    (SELECT " . implode(', ', $this->getTransferFields($table, 'temp')) . " FROM " . $this->getTableName($table) . " temp {LIMIT})
                                    ON DUPLICATE KEY UPDATE " . implode(",\n", $this->getDuplicateUpdateStatement($table, $t));
        }

        $GLOBALS['TYPO3_DB']->sql_query('START TRANSACTION');

        try {

            foreach($sqlStatements as $tableName => $sql) {

                $this->performTransfer($sql, $tableName);
            }

            $GLOBALS['TYPO3_DB']->sql_query('COMMIT');

        } catch (\Exception $ex) {

            $GLOBALS['TYPO3_DB']->sql_query('ROLLBACK');
            throw $ex;
        }

        return true;
    }

    /**
     * Drops the temporary created tables
     *
     * @return boolean
     * @throws Exception
     */
    protected function dropTemporaryTables() {

        $this->log(__FUNCTION__);

        foreach($this->requiredTables as $table) {

            $tableName = $this->getTableName($table);

            // Truncate table first
            $this->log(__FUNCTION__ . ': ' . ($sql = "TRUNCATE " . $tableName) . ' ... ', false);
            $GLOBALS['TYPO3_DB']->sql_query($sql);
            $this->log('OK');

            // ... then drop the table
            $this->log(__FUNCTION__ . ': ' . ($sql = "DROP TABLE IF EXISTS " . $tableName) . ' ... ', false);
            if(($dropped = $GLOBALS['TYPO3_DB']->sql_query($sql)) === false) {

                \TYPO3\MbxRealestate\Controller\ImportAbstractCommandController::throwException('Failed to DROP table "' . $table . '"');

            }  else {

                $this->log('OK');
            }
        }

        return true;
    }

    /**
     * returns sth like
     *
     * array(
     *      'ks_web_products.ean',
     *      'ks_web_products.products_name',
     *      'ks_web_products.segment',
     *      'ks_web_products.description'
     * )
     *
     * @param string $table
     * @param null|string $prefix
     * @return array
     */
    private function getTransferFields($table, $prefix = null) {

        $fieldsConfigured = self::$columsTables[$table];
        $fields = array();

        foreach(array_keys($fieldsConfigured) as $fieldName) {

            if(!empty($prefix)) {
                $fields[] = $prefix . '.' . $fieldName;
            } else {
                $fields[] = $fieldName;
            }

        }

        return $fields;
    }


    /**
     * returns sth like
     *
     * array(
     *      'ks_web_products.products_name = VALUES(ks_web_products.products_name)',
     *      'ks_web_products.segment = VALUES(ks_web_products.segment)',
     *      'ks_web_products.description = VALUES(ks_web_products.description)',
     *      'ks_web_products.supplier_aid = VALUES(ks_web_products.supplier_aid)'
     * )
     *
     * @param string $table
     * @param string $prefix
     * @return array
     */
    private function getDuplicateUpdateStatement($table, $prefix) {

        $fieldsConfigured = self::$columsTables[$table];
        $fields = array();

        foreach(array_keys($fieldsConfigured) as $fieldName) {

            if(!empty($prefix)) {
                $fieldName = $prefix . '.' . $fieldName;
            }

            switch($fieldName) {
                case $prefix . '.' . 'geo_x':
                case $prefix . '.' . 'geo_y':
                    $update = 'COALESCE(VALUES(' . $fieldName . '), ' . $fieldName . ')';
                    break;
                default :
                    $update = 'VALUES(' . $fieldName . ')';
            }

            $fields[] = $fieldName . ' = ' . $update;
        }

        return $fields;
    }

    /**
     *
     * @param string $baseSQL
     * @param string $type
     * @return \Exception|boolean
     */
    private function performTransfer($baseSQL, $type) {

        $itemsCount = $this->countInserts[$type];

        $offset = 0;
        $this->log('Items count for ' . $type . ': ' . $itemsCount);

        if($itemsCount > 0) {

            do {
                $loopSQL = str_replace('{LIMIT}', ' LIMIT ' . ($offset * self::SQL_TRANSFER_STEPS) . ', ' . self::SQL_TRANSFER_STEPS, $baseSQL);

                $this->log('Perform transaction ' . $type);
//                $this->log('SQL: ' . $loopSQL);

                if(($updated = $GLOBALS['TYPO3_DB']->sql_query($loopSQL)) === false) {

                    \TYPO3\MbxRealestate\Controller\ImportAbstractCommandController::throwException('Failed to TRANSFER! ' . $loopSQL);
                }

                $this->log('Handled transaction');

                ++$offset;

            } while(($offset * self::SQL_TRANSFER_STEPS) < $itemsCount);

            $this->log('Finished transaction');

        } else {

            $this->log('No items for ' . $type . ' to transact');
        }

        return true;
    }


    /**
     * Removes all entries from the live tables which arent present in the temporary tables
     *
     * @param string $table
     * @return int affected rows
     */
    protected function cleanByTemporary($table) {
        $join = array();

        $targetTable = $this->getTargetTable($table);

        switch ($table) {
            case self::TABLE_ADRESS_CONTACTS_nm :
                $fields = array('uid_local', 'uid_foreign');
                $join[] = "LEFT JOIN " . $this->getTargetTable(self::TABLE_CONTACTS) . " contacts ON "
                        . " {$targetTable}.uid_foreign = contacts.uid";
                break;

            case self::TABLE_ADDRESS :
                $fields = array('unr', 'wi', 'hs');
                break;

            case self::TABLE_CONTACTS :
                $fields = array('contact_md5');
                break;

            case self::TABLE_AREAS :
                $fields = array('area_md5');
                break;

            case self::TABLE_FEATURES :
                $fields = array('feature_md5');
                break;

            case self::TABLE_ENVIRONMENTS :
                $fields = array('environment_md5');
                break;

            case self::TABLE_IMAGES :
                $fields = array('image_md5');
                break;

            case self::TABLE_IMMOOBJECTS :
                $fields = array('unr', 'wi', 'hs', 'me');
                break;

            case self::TABLE_IMMOOBJECT_FEATURES_nm :
                $fields = array('uid_local', 'uid_foreign');
                $join[] = "LEFT JOIN " . $this->getTargetTable(self::TABLE_FEATURES) . " features ON "
                        . " {$targetTable}.uid_foreign = features.uid";
                break;

            default :
                \TYPO3\MbxRealestate\Controller\ImportAbstractCommandController::throwException('Failed to ' . __FUNCTION__ . '! because no fields declared for table ' . $table);
                break;
        }

        // <editor-fold defaultstate="collapsed" desc="create ON statement items">
        $on = array();

        foreach($fields as $field) {
            $on[] = " temp.{$field} = {$targetTable}.{$field} ";
        }
        // </editor-fold>

        $join[] = "LEFT JOIN " . $this->getTableName($table) . " temp "
                        . " ON " . implode(' AND ', $on);

        $sql = "DELETE {$targetTable}
                FROM {$targetTable} AS {$targetTable}
                " . implode(' ', $join) . "
                WHERE temp.{$fields[0]} IS NULL ";

        $sql .= " AND {$targetTable}.pid = " . $this->importer->getStoragePid();

        $tmp_skipContacts = array_unique( array_merge(array($this->importer->getConfiguratioItem('settings.customerCenter.type')), explode(',', $this->importer->getConfiguratioItem('settings.import.skipContacts'))) );
        $skipContacts = implode("','", $tmp_skipContacts);

        // dont drop the contacts of type customercenter
        if($table == self::TABLE_CONTACTS && !empty($skipContacts)) {
            $sql .= " AND {$targetTable}.contact_type NOT IN ('{$skipContacts}')";
        }

        if($table == self::TABLE_ADRESS_CONTACTS_nm && !empty($skipContacts)) {
            $sql .= " AND contacts.contact_type NOT IN ('{$skipContacts}')";
        }

        if(($updated = $GLOBALS['TYPO3_DB']->sql_query($sql)) === false) {

            \TYPO3\MbxRealestate\Controller\ImportAbstractCommandController::throwException('Failed to TRANSFER! ' . $sql);
        }  else {

            $removed = $GLOBALS['TYPO3_DB']->sql_affected_rows();

            $this->log('Dropped ' . $removed . ' links in ' . $targetTable);

            return $removed;
        }

    }

    /**
     * Generates the correct foreign keys based on the md5() values fo the UNIQUE fields
     *
     * @param string $table
     * @return int
     * @throws \TYPO3\MbxRealestate\Helper\Exception\ImportImmoException
     */
    protected function generateForeign($table) {

        switch ($table) {
            case self::TABLE_ENVIRONMENTS :
//            case self::TABLE_FEATURES :
            case self::TABLE_AREAS :
            case self::TABLE_IMAGES :
                $sql = "UPDATE " . $this->getTargetTable($table) . " f
                        LEFT JOIN " . $this->getTargetTable(self::TABLE_IMMOOBJECTS) . " i
                        ON md5(CONCAT(LCASE(i.unr),LCASE(i.wi),LCASE(i.me))) = f.immoobject_md5
                        SET f.immoobject = i.uid
                        WHERE f.immoobject_md5 != ''";
                break;

            case self::TABLE_ADDRESS :
                $sql = "UPDATE " . $this->getTargetTable(self::TABLE_IMMOOBJECTS) . " f
                        LEFT JOIN " . $this->getTargetTable($table) . " i
                        ON i.unr = f.unr AND i.wi = f.wi AND i.hs = f.hs
                        SET f.immoaddress = i.uid";
                break;

            default :
                \TYPO3\MbxRealestate\Controller\ImportAbstractCommandController::throwException('Failed to ' . __FUNCTION__ . '()! because no sql declared for table ' . $table);
                break;
        }

        if(($updated = $GLOBALS['TYPO3_DB']->sql_query($sql)) === false) {

            \TYPO3\MbxRealestate\Controller\ImportAbstractCommandController::throwException('Failed to generateForeign keys! ' . $sql);
        }  else {

            $updated = $GLOBALS['TYPO3_DB']->sql_affected_rows();

            $this->log('Updated ' . $updated . ' foreign keys for "' . $table . '"');

            return $updated;
        }
    }

    /**
     * Generating foreign keys for all immo tables.
     * Call this after importing immoobject to link the tables by the md5() value of the immoobjects.
     *
     * @return array
     */
    public function generateForeignAll() {

        return array(
//            self::TABLE_FEATURES    =>  $this->generateForeign(self::TABLE_FEATURES),
            self::TABLE_ENVIRONMENTS=>  $this->generateForeign(self::TABLE_ENVIRONMENTS),
            self::TABLE_AREAS       =>  $this->generateForeign(self::TABLE_AREAS),
            self::TABLE_IMAGES      =>  $this->generateForeign(self::TABLE_IMAGES),
            self::TABLE_ADDRESS     =>  $this->generateForeign(self::TABLE_ADDRESS)
        );
    }

    /**
     * Searches all nm-relations in the live table address-contacts-nm which are not present in the temporary table
     * and removes them from the live table.
     *
     * @return int affected rows
     */
    public function cleanTempAddressContactsMN() {

        return $this->cleanByTemporary(self::TABLE_ADRESS_CONTACTS_nm);
    }

    /**
     * Removes all address from the live table which arent present in the temporary address table
     * @return int affected rows
     */
    public function cleanTempAddress() {

        return $this->cleanByTemporary(self::TABLE_ADDRESS);
    }

    /**
     * Removes all contacts from the live table which arent present in the temporary contacts table
     * @return int affected rows
     */
    public function cleanTempContacts() {

        return $this->cleanByTemporary(self::TABLE_CONTACTS);
    }

    /**
     * Removes all areas from the live table which arent present in the temporary areas table
     * @return int affected rows
     */
    public function cleanTempAreas() {

        return $this->cleanByTemporary(self::TABLE_AREAS);
    }

    /**
     * Removes all features from the live table which arent present in the temporary features table
     * @return int affected rows
     */
//    public function cleanTempFeatures() {
//
//        return $this->cleanByTemporary(self::TABLE_FEATURES);
//    }

    /**
     * Removes all features from the live table which arent present in the temporary features table
     * @return int affected rows
     */
    public function cleanTempImmoobjectFeaturesMM() {

        return $this->cleanByTemporary(self::TABLE_IMMOOBJECT_FEATURES_nm);
    }

    /**
     * Removes all features from the live table which arent present in the temporary features table
     * @return int affected rows
     */
    public function cleanTempEnvironments() {

        return $this->cleanByTemporary(self::TABLE_ENVIRONMENTS);
    }

    /**
     * Removes all images from the live table which arent present in the temporary images table
     * @return int affected rows
     */
    public function cleanTempImages() {

        return $this->cleanByTemporary(self::TABLE_IMAGES);
    }

    /**
     * Removes all immoobjects from the live table which arent present in the temporary immoobjects table
     * @return int affected rows
     */
    public function cleanTempImmoobjects() {

        return $this->cleanByTemporary(self::TABLE_IMMOOBJECTS);
    }

    /**
     * @param string $message
     * @param boolean $nl
     */
    protected function log($message, $nl = true) {
        $this->importer->getCommandControllerReference()->log($message, $nl);
    }
}
