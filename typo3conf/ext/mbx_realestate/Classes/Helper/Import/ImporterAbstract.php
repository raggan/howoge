<?php

namespace TYPO3\MbxRealestate\Helper\Import;

use TYPO3\MbxRealestate\Controller\ImportAbstractCommandController;
use TYPO3\MbxRealestate\Domain\Repository\ImmoaddressRepository;
use TYPO3\MbxRealestate\Domain\Repository\ImmocontactRepository;
use TYPO3\MbxRealestate\Domain\Repository\ImmofeatureRepository;
use TYPO3\MbxRealestate\Domain\Repository\ImmoimageRepository;
use TYPO3\MbxRealestate\Domain\Repository\ImmoobjectRepository;

abstract class ImporterAbstract {

    /**
     * ImmoobjectRepository
     *
     * @var \TYPO3\MbxRealestate\Domain\Repository\ImmoobjectRepository
     */
    protected $immoobjectRepository;

    /**
     * ImmofeatureRepository
     *
     * @var \TYPO3\MbxRealestate\Domain\Repository\ImmofeatureRepository
     */
    protected $immofeatureRepository;

    /**
     * ImmoaddressRepository
     *
     * @var \TYPO3\MbxRealestate\Domain\Repository\ImmoaddressRepository
     */
    protected $immoaddressRepository;

    /**
     * ImmocontactRepository
     *
     * @var \TYPO3\MbxRealestate\Domain\Repository\ImmocontactRepository
     */
    protected $immocontactRepository;

    /**
     * ImmoimageRepository
     *
     * @var \TYPO3\MbxRealestate\Domain\Repository\ImmoimageRepository
     */
    protected $immoimageRepository;

    /**
     * @var array
     */
    protected $configuration = null;

    /**
     *
     * @var \TYPO3\CMS\Extbase\Object\ObjectManager
     */
    protected $objectManager = null;

    /**
     * @var \Resource
     */
    protected $handle = null;

    /**
     * @var \TYPO3\MbxRealestate\Helper\Import\Interfaces\ImporterSaveInterface
     */
    protected $saver;

    /**
     * Stores all immo object nodes.
     * @var \SimpleXMLElement[]|null
     */
    protected $items = null;

    /**
     * @var \SimpleXMLElement|null
     */
    protected $currentItem = null;

    /**
     * @var \array
     */
    protected $energyClassThresholds = array(
        30 => 'A+',
        50 => 'A',
        75 => 'B',
        100 => 'C',
        130 => 'D',
        160 => 'E',
        200 => 'F',
        250 => 'G',
        251 => 'H'
    );

    /**
     * @var \TYPO3\MbxRealestate\Helper\Import\ImmoDataItem
     */
    public $currentImmoDataItem = null;

    /**
     * @var string
     */
    private $archivename;

    /**
     * @var string
     */
    private $archive;

    /**
     * @var string
     */
    private $filename;

    /**
     * @var string
     */
    private $file;

    /**
     * @var string|null
     */
    private $scope;

    /**
     * @var int|null
     */
    private $storagePid;

    function __construct() {

        $this->objectManager = new \TYPO3\CMS\Extbase\Object\ObjectManager();
    }

    /**
     * @return \TYPO3\MbxRealestate\Helper\Import\ImporterAbstract
     */
    public function resetImporterSaver() {
        
        if($this->saver instanceof \TYPO3\MbxRealestate\Helper\Import\Interfaces\ImporterSaveInterface) {
            $this->saver = null;
        }

        return $this;
    }

    /**
     * @param \TYPO3\MbxRealestate\Helper\Import\ImporterSave $importerSave
     * @return \TYPO3\MbxRealestate\Helper\Import\ImporterAbstract
     */
    public function setImporterSaver(\TYPO3\MbxRealestate\Helper\Import\ImporterSave $importerSave) {
        
        $this->saver = $importerSave;
        return $this;
    }

    /**
     *
     * @return \TYPO3\MbxRealestate\Helper\Import\Interfaces\ImporterSaveInterface
     */
    public function getImporterSaver() {
        return $this->saver;
    }

    /**
     * @param string $scope
     * @return \TYPO3\MbxRealestate\Helper\Import\ImporterAbstract
     */
    public function setScope($scope) {
        $this->scope = $scope;
        return $this;
    }

    /**
     *
     * @return scope
     */
    public function getScope() {
        return $this->scope;
    }

    /**
     * @param \TYPO3\MbxRealestate\Domain\Repository\ImmoobjectRepository $immoobjectRepository
     * @return \TYPO3\MbxRealestate\Helper\Import\ImporterAbstract
     */
    public function setImmoobjectRepository(ImmoobjectRepository $immoobjectRepository) {

        $this->immoobjectRepository = $immoobjectRepository;
        return $this;
    }

    /**
     * @param \TYPO3\MbxRealestate\Domain\Repository\ImmofeatureRepository $immofeatureRepository
     * @return \TYPO3\MbxRealestate\Helper\Import\ImporterAbstract
     */
    public function setImmofeatureRepository(ImmofeatureRepository $immofeatureRepository) {

        $this->immofeatureRepository = $immofeatureRepository;
        return $this;
    }

    /**
     * @param \TYPO3\MbxRealestate\Domain\Repository\ImmoaddressRepository $immoaddressRepository
     * @return \TYPO3\MbxRealestate\Helper\Import\ImporterAbstract
     */
    public function setImmoaddressRepository(ImmoaddressRepository $immoaddressRepository) {

        $this->immoaddressRepository = $immoaddressRepository;
        return $this;
    }

    /**
     * @param \TYPO3\MbxRealestate\Domain\Repository\ImmocontactRepository $immocontactRepository
     * @return \TYPO3\MbxRealestate\Helper\Import\ImporterAbstract
     */
    public function setImmocontactRepository(ImmocontactRepository $immocontactRepository) {

        $this->immocontactRepository = $immocontactRepository;
        return $this;
    }

    /**
     * @param \TYPO3\MbxRealestate\Domain\Repository\ImmoimageRepository $immoimageRepository
     * @return \TYPO3\MbxRealestate\Helper\Import\ImporterAbstract
     */
    public function setImmoimageRepository(ImmoimageRepository $immoimageRepository) {

        $this->immoimageRepository = $immoimageRepository;
        return $this;
    }

    /**
     *
     * @param \TYPO3\MbxRealestate\Controller\ImportAbstractCommandController $commandController
     * @return \TYPO3\MbxRealestate\Helper\Import\ImporterAbstract
     */
    public function setCommandControllerReference(ImportAbstractCommandController $commandController) {

        $this->commandControllerReference = $commandController;
        return $this;
    }

    /**
     * @return \TYPO3\MbxRealestate\Controller\ImportAbstractCommandController
     */
    protected function getCommandControllerReference() {
        return $this->commandControllerReference;
    }

    // <editor-fold defaultstate="collapsed" desc="configuration handling">

    /**
     *
     * @param array $options
     * @return \TYPO3\MbxRealestate\Helper\Import\ImporterAbstract
     */
    public function setConfiguration(array $options) {

        $this->configuration = $options;
        return $this;
    }

    /**
     * Retrieves a TS configuration value for a required path.
     *
     * @param string $configurationPath - a configuration path of the TS configured value e.g. import.types.xml.xpathImmoNode
     * @return string - the configured value
     */
    public function getConfiguratioItem($configurationPath) {

        $configNode = $this->configuration;
        $path = explode('.', $configurationPath);

        while($pathItem = array_shift($path)) {

            // if no end of path reached we add the '.' to access next deeper configuration level
            if(count($path)) {
                $pathItem .= '.';
            }

            $configNode = $configNode[$pathItem];
        }

        return $configNode;
    }

    // </editor-fold>

    /**
     * @param int|null $storagePid
     * @return \TYPO3\MbxRealestate\Helper\Import\ImporterAbstract
     */
    public function setStoragePid($storagePid) {

        if(preg_match('/^\d+$/', $storagePid)) {
            $this->storagePid = $storagePid;
        } else {
            $this->storagePid = NULL;
        }

        return $this;
    }

    /**
     *
     * @return int
     */
    public function getStoragePid() {

        // use the custom defined storagePid (can be set as execute option in BE)
        if(!empty($this->storagePid)) {
            return $this->storagePid;
        }

        $scope = $this->getScope();
        $customScope = $this->getConfiguratioItem('settings.import.' . $scope . '.storagePid');

        if(!empty($customScope)) {
            return $customScope;
        } else {
            return $this->getConfiguratioItem('persistence.storagePid');
        }
    }

    public function getArchive() {

        if(empty($this->archive)) {

            $path = $this->getConfiguratioItem('settings.import.' . $this->getScope() . '.inputPath');
            $file = $this->getArchivename();

            $this->archive = PATH_site . $path . $file;
        }

        return $this->archive;

    }

    /**
     * @return string
     */
    public function getArchivename() {

        if(empty($this->archivename)) {

            $this->archivename = $this->getConfiguratioItem('settings.import.' . $this->getScope() . '.inputArchiveFile');
        }

        return $this->archivename;
    }

    /**
     * Returns the file to import from.
     * @return string
     */
    public function getFile() {

        if(empty($this->file)) {

            $path = $this->getConfiguratioItem('settings.import.' . $this->getScope() . '.inputPath');
            $file = $this->getFilename();

            $this->file = PATH_site . $path . $file;
        }

        return $this->file;
    }

    /**
     * @return string
     */
    public function getFilename() {

        if(empty($this->filename)) {

            $this->filename = $this->getConfiguratioItem('settings.import.' . $this->getScope() . '.inputFile');
        }

        return $this->filename;
    }

    /**
     * Returns the target path where the images of the immoobjects has to be copied
     * @param boolean $prependPath
     * @return string
     */
    public function getPublicImagePath($prependPath = true) {

        $var = 'configuration_target_image_path_' . (int)$prependPath;

        if(empty($this->{$var})) {
            $this->{$var} = ($prependPath ? PATH_site : '') . $this->getConfiguratioItem('settings.import.publicImagePath');
        }

        return $this->{$var};
    }

    /**
     * Returns the target path where the images of the immoobjects has to be copied
     * @param boolean $prependPath
     * @return string
     */
    public function getSourceImagePath($prependPath = true) {

        $var = 'configuration_source_image_path' . (int)$prependPath;

        if(empty($this->{$var})) {
            $this->{$var} = ($prependPath ? PATH_site : '') . $this->getConfiguratioItem('settings.import.sourceImagePath');
        }

        return $this->{$var};
    }

    // <editor-fold defaultstate="collapsed" desc="file handling">

    /**
     * Closes the CSV file.
     *
     * @return \TYPO3\MbxRealestate\Helper\Import\ImporterAbstract
     */
    public function closeFile() {

        if(is_resource($this->handle)) {

            @fclose($this->handle);
        }

        return $this;
    }

    /**
     * Extracting backup configuration from typoscript.
     * 
     * @param array $backupSettings
     * @return array
     */
    private function extractBackupInformation(array $backupSettings = null) {
        
        $data = array(
            'dir'       =>  $this->configuration['settings.']['import.']['backupDir'],
            'format'    =>  date('Y-m-d_H-i-s') . '_' . $this->getFilename()
        );
        
        if(empty($backupSettings)) {
            return $data;
        }
        
        return array_merge($data, (array)$backupSettings);
    }
    
    /**
     * @param string $format
     * @return string
     */
    private function buildBackupFilename($format) {
        
        $placeholders = array(
            '{curr_timestamp}'  =>  time(),
            '{filename}'        =>  pathinfo($this->getFile(), PATHINFO_FILENAME),
            '{basename}'        =>  pathinfo($this->getFile(), PATHINFO_BASENAME),
            '{ext}'             =>  pathinfo($this->getFile(), PATHINFO_EXTENSION),
            '{curr_datetime}'   =>  date('Y-m-d_H-i-s')
        );
        
        return str_replace(array_keys($placeholders), array_values($placeholders), $format);
    }
    
    /**
     * Checks if the backup option is set in the extension configuration and copies the import file
     * into the backup directory (TS: settings.import.backupFile).
     *
     * @return null|boolean
     */
    public function backupFile() {

        if(array_key_exists('backup.', $_path = $this->configuration['settings.']['import.'][$this->getScope() . '.'])) {
            $backupSettings = $_path['backup.'];
            $useBackup = (bool)$backupSettings['enabled'];
        } else {
            $useBackup = (bool)$this->configuration['settings.']['import.']['backupFile'];
        }            
                
        $this->saver->importer->getCommandControllerReference()->log(sprintf('Use backup: %s', (int)$useBackup));
        
        if(!$useBackup) {

            return null;
        }

        $info = $this->extractBackupInformation($backupSettings);
        $dir = $info['dir'];
        $format = $info['format'];

        $sourceFile = $this->getFile();
        $targetFile = PATH_site . $dir . $this->buildBackupFilename($format);

        $this->saver->importer->getCommandControllerReference()->log(sprintf('Copy backup to "%s"', $targetFile));

        return (@copy($sourceFile, $targetFile));
    }
    
    /**
     * remove the import file
     *
     * @return null|boolean
     */
    public function removeFile() {
        
        $removeFile = (bool)$this->configuration['settings.']['import.']['removeFile'];
        
        if($removeFile){
            
            $sourceFile = $this->getFile();
            return (@unlink($sourceFile));
            
        } else {
            
            return null;
        }
    }

    // </editor-fold>

    /**
     * Reprocesses the processed image files of $filename to update the thumbnails in case the original image has changed.
     *
     * @param string $filename
     * @return bool|\Exception|null
     */
    private function updateProcessedFiles($filename) {

        $relativeDestPath = $this->getPublicImagePath(false);

        if(substr($relativeDestPath, -1) !== '/') {
            $relativeDestPath .= '/';
        }

        $file = $relativeDestPath . $filename;

        // select the sys_file of the image to get the processed_images and reprocess them
        $resFile = $GLOBALS['TYPO3_DB']->exec_SELECTquery(
            'sys_file.*', // SELECT ...
            'sys_file', // FROM ...
            'sys_file.pid = 0 AND "' . $file . '" LIKE CONCAT("%", sys_file.identifier)', // WHERE...
            '', // GROUP BY...
            '', // ORDER BY...
            ''  // LIMIT ...
        );
        if (!($fileData = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($resFile))) {
            return null;
        }

        try {

            $ProcessedFileRepository = $this->objectManager->get('TYPO3\CMS\Core\Resource\ProcessedFileRepository');
            $StorageRepository = $this->objectManager->get('TYPO3\CMS\Core\Resource\StorageRepository');
            $ImageService = $this->objectManager->get('TYPO3\CMS\Extbase\Service\ImageService');

            $Storage = $StorageRepository->findByUid($fileData['storage']);
            $FileReference = $Storage->getFile($fileData['identifier']);

            $processedFiles = $ProcessedFileRepository->findAllByOriginalFile($FileReference);

            foreach($processedFiles as $processedFile) {
                $configuration = $processedFile->getProcessingConfiguration();

                $processedFile->delete();
                $ImageService->applyProcessingInstructions($FileReference, $configuration);
            }

            return true;

        } catch (\Exception $e) {
            return $e;
        }
    }

    /**
     * Copies the immoimage from the source folder to the public destination path
     * @param  string  $inputFile
     * @param  string  $destFile
     * @param  int     $fileSize
     * @return boolean
     */
    protected function doCopyFile($sourceFile, $filename, $fileSize, $destPathExtra) {

        //$sourceFile = $sourcePath . $sourceFilename;
        $destPathBase = $this->getPublicImagePath(true);

        // <editor-fold defaultstate="collapsed" desc="check if the destination path has to be extended">
        if(!empty($destPathExtra)) {

            if(substr($destPathExtra, -1) !== '/') {
                $destPathBase .= $destPathExtra . '/';
            } else {
                $destPathBase .= $destPathExtra;
            }
        }
        // </editor-fold>

        $destFile = $destPathBase . $filename;

        if(file_exists($destFile)) {

            // return true if size is same (no file to copy)
            if (filesize($destFile) == $fileSize) {
                return $destFile;
            } else {
                // try to drop target file to copy new file
                if(!unlink($destFile)) {
                    $msg = "   failed to unlink old file " . $destFile;
                    if( !chmod($destFile, 0777) || !unlink($destFile) ) {
                        $msg .= " (retry with chmod first failed too)";
                    } else {
                        $msg .= " (retry with chmod first succeeded)";
                    }
                    $this->saver->importer->getCommandControllerReference()->log($msg);
                }
            }
        }

        $transferOptions = array(
            'copy',
            'rename'
        );
        
        // remote files additional gets the possibility to get downloaded by file_get_contents() and cURL
        if($this->isRemoteFile($sourceFile)) {
            $transferOptions[] = array(__CLASS__, 'transferImageFileViaFilegetcontents');
            $transferOptions[] = array(__CLASS__, 'transferImageFileViaCurl');
        }

        while($callback = array_shift($transferOptions)) {

            if (call_user_func_array($callback, array($sourceFile, $destFile))) {
                if(chmod($destFile, 0777)) {

                    $processedFiles = $this->updateProcessedFiles($filename);

                    if($processedFiles instanceof \Exception) {
                        $this->saver->importer->getCommandControllerReference()->log("   failed to reprocessed the thumbnails of " . $destFile . " " . $processedFiles->getMessage());
                    }

                    return $destFile;
                } else {
                    $this->saver->importer->getCommandControllerReference()->log("   failed to chmod new file " . $destFile);
                }
            } else {

                $methodName = (is_array($callback) ? $callback[1] : $callback);
                $this->saver->importer->getCommandControllerReference()->log('   failed to ' . __FUNCTION__ . '() new file ' . $sourceFile . ' by method ' . $methodName . '()');
            }
        }

        return false;
    }
    
    /**
     * 
     * @param string $filename
     * @return boolean
     */
    protected function isRemoteFile($filename) {
        return preg_match('/^https?\:\/\//', $filename);
    }
    
    /**
     * This method is registered as callback for retrieving remote image files.
     * 
     * @param string $source the source URL of the file to be copied
     * @param string $target the target filename + path where to save the file
     * @return boolean
     */
    protected function transferImageFileViaFilegetcontents($source, $target) {
        
        if(!$this->isRemoteFile($source)) {
            return null;
        }

        $this->saver->importer->getCommandControllerReference()->log('   use ' . __FUNCTION__ . ' to copy file...');
        $res = file_put_contents($target, file_get_contents($source));
        
        return $res;
    }
    
    /**
     * This method is registered as callback for retrieving remote image files.
     * 
     * @param string $source the source URL of the file to be copied
     * @param string $target the target filename + path where to save the file
     * @return boolean
     */
    protected function transferImageFileViaCurl($source, $target) {
        
        if(!$this->isRemoteFile($source)) {
            return null;
        }
        $this->saver->importer->getCommandControllerReference()->log('   use ' . __FUNCTION__ . ' to copy file...');
        
        if(!function_exists('curl_init')) {
            return null;
        }
        
        // <editor-fold defaultstate="collapsed" desc="transfer file">
        $ch = curl_init ($source);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_BINARYTRANSFER,1);
        
        $raw = curl_exec($ch);
        $this->saver->importer->getCommandControllerReference()->log(var_export($raw, true));
        if($raw === false) {
            return false;
        }
        
        curl_close($ch);
        // </editor-fold>
        
        $isImage = getimagesizefromstring($raw);
        
        if(empty($isImage) || empty($isImage[0])) {
            return false;
        }
        
        // <editor-fold defaultstate="collapsed" desc="save file">
        if(file_exists($target)){
            @unlink($target);
        }
        
        $fp = fopen($target, 'x');
        $written = fwrite($fp, $raw);
        fclose($fp);
        // </editor-fold>
        
        return !empty($written);
    }

    /**
     * Copies the immoimage from the source folder to the public destination path
     * @param string $inputFile
     * @param string|null $destPathExtra
     * @return boolean
     */
    protected function copyImmoimageFile($inputFile, $destPathExtra = null) {

        $sourcePathBase = $this->getSourceImagePath(true);
//        $destPathBase = $this->getPublicImagePath(true);

        $sourcePath = pathinfo($sourcePathBase . $inputFile, PATHINFO_DIRNAME) . '/';
        $sourceFilename = pathinfo($sourcePathBase . $inputFile, PATHINFO_BASENAME);

        if(($sourceFilename = $this->similar_file_exists($sourcePath . $sourceFilename)) !== FALSE) {

            $sourceFile = $sourcePath . $sourceFilename;
/*
//            // <editor-fold defaultstate="collapsed" desc="check if the destination path has to be extended">
//            if(!empty($destPathExtra)) {
//
//                if(substr($destPathExtra, -1) !== '/') {
//                    $destPathBase .= $destPathExtra . '/';
//                } else {
//                    $destPathBase .= $destPathExtra;
//                }
//            }
//            // </editor-fold>
//
//            $targetFile = $destPathBase . $sourceFilename;
//
//            if(file_exists($targetFile)) {
//
//                // return true if size is same (no file to copy)
//                if (filesize($targetFile) == filesize($sourceFile)) {
//                    return $targetFile;
//                } else {
//                    // try to drop target file to copy new file
//                    unlink($targetFile);
//                }
//            }
//
//
//            if (copy($sourceFile, $targetFile)) {
//                return $targetFile;
//            }  else {
//                echo "Failed to copy " . $sourceFile . '<br>';
//            }
*/
            return $this->doCopyFile($sourceFile, $sourceFilename, filesize($sourceFile), $destPathExtra);
        }

        return false;
    }

    /**
    * Remote File Size Using cURL
    * @param srting $url
    * @return int || void
    */
    function remotefileSize($url) {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_NOBODY, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 0);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_MAXREDIRS, 3);
        curl_exec($ch);
        $filesize = curl_getinfo($ch, CURLINFO_CONTENT_LENGTH_DOWNLOAD);
        curl_close($ch);
        if ($filesize) {
            return $filesize;
        } else {
            return FALSE;
        }
    }

    /**
     * Copies the immoimage from a remote location to the public destination path
     * @param  string       $remotePath
     * @param  string       $filename
     * @param  string|null  $destPathExtra
     * @return boolean
     */
    protected function copyRemoteImmoimageFile($remotePath, $filename, $destPathExtra = null) {

        $hasLocalTemp = false;
        $remoteFile = $remotePath . $filename;
        $this->saver->importer->getCommandControllerReference()->log('### copy remote image file (' . date('d.m.Y H:i:s') . ')');
        $this->saver->importer->getCommandControllerReference()->log('  trying to access: ' . $remoteFile);
        $filesize = $this->remotefileSize($remoteFile);
        $this->saver->importer->getCommandControllerReference()->log('  remote filesize = ' . var_export($filesize, true));
        if($filesize !== FALSE) {
            // We need to save gif as jpg, because GraphicsMagick (used by ImageViewHelper) is buggy when cropping gifs
            if( \pathinfo($filename, PATHINFO_EXTENSION) == 'gif' ) {
                $hasLocalTemp = true;
                $filename = \pathinfo($filename, PATHINFO_FILENAME) . '.jpg';
                $image = imagecreatefromgif($remoteFile);
                if($image === false) {
                    $this->saver->importer->getCommandControllerReference()->log('  failed to create temporary image from gif!');
                }
                $remoteFile = $this->getPublicImagePath() . '/temp.jpg';
                if(!imagejpeg($image, $remoteFile)) {
                    $this->saver->importer->getCommandControllerReference()->log('  failed to create temporary jpeg image!');
                }
                if(!imagedestroy($image)) {
                    $this->saver->importer->getCommandControllerReference()->log('  failed to destroy temporary image!');
                }
            }
            $this->saver->importer->getCommandControllerReference()->log('  ... copy remote image');
            $result = $this->doCopyFile($remoteFile, $filename, intval($filesize), $destPathExtra);
            if($hasLocalTemp) { unlink($remoteFile); }
            return $result;
        }

        return false;
    }

    /**
     * http://stackoverflow.com/questions/14192515/php-skip-case-sensitivity-in-file-extension
     *
     * Alternative to file_exists() that will also return true if a file exists
     * with the same name in a different case.
     * eg. say there exists a file /path/product.class.php
     *  file_exists('/path/Product.class.php')
     *   => false
     *  similar_file_exists('/path/Product.class.php')
     *   => true
     * @param string $filename The filename to check.
     * @return string|boolean The similar found file or false if no such file exists
     */
    private function similar_file_exists($filename) {

        if (file_exists($filename)) {
            return pathinfo($filename, PATHINFO_BASENAME);
        }

        $dir = dirname($filename);
        $files = glob($dir . '/*');
        $lcaseFilename = strtolower($filename);

        foreach ($files as $file) {
            if (strtolower($file) == $lcaseFilename) {
                return pathinfo($file, PATHINFO_BASENAME);
            }
        }

        return false;
    }

    /**
     *
     * @param string $unr
     * @param string $wi
     * @param mixed $hs
     * @param mixed $me
     * @return \TYPO3\MbxRealestate\Domain\Model\Immoobject
     */
    protected function getImmoObjectById($unr, $wi, $hs = null, $me = null) {

        $immoobjectQueryResult = $this->immoobjectRepository->findAllByImmoId($unr, $wi, $hs, $me);

        if($immoobjectQueryResult->count()) {
            return $immoobjectQueryResult->getFirst();
        }  else {
            return $this->objectManager->get('TYPO3\MbxRealestate\Domain\Model\Immoobject');
        }
    }

    /**
     * @param int $uid
     * @return \TYPO3\MbxRealestate\Domain\Model\Immocontact
     */
    protected function getCustomerCenterById($uid) {

        $customercenterQueryResult = $this->immocontactRepository->findByUid($uid);

        if($customercenterQueryResult instanceof \TYPO3\MbxRealestate\Domain\Model\Immocontact) {
            return $customercenterQueryResult;
        }  else {
            return null;
        }
    }

    /**
     * Returns the Uid of the immocontact related to the customerCenter configured in TS.
     * Customer center get sanitized by /[^a-zA-Z0-9\_]/
     *
     * @param string|int $customerCenterStr
     * @return null|int
     */
    protected function getCustomerCenterUid($customerCenterStr) {

        $customerCenterStr = preg_replace('/[^a-zA-Z0-9\_]/', '_', $customerCenterStr);

        $mappingType = $this->getConfiguratioItem('settings.customerCenter.mappingType');
        $mapping = $this->getConfiguratioItem('settings.customerCenter.mapping.');

        switch($mappingType) {
//            case 'districts' :
//
//                if(array_key_exists($customerCenterStr, $mapping)) {
//                    return $mapping[$customerCenterStr];
//                }
//
//                break;

            case 'numeric' :

                if(array_key_exists($customerCenterStr, $mapping)) {
                    return $mapping[$customerCenterStr];
                }

                break;
        }

        return null;
    }

    /**
     *
     * @param string $unr
     * @param string $wi
     * @param string $hs
     * @return \TYPO3\MbxRealestate\Domain\Model\Immoaddress
     */
    protected function getImmoaddressById($unr, $wi, $hs) {

        $immoaddressResult = $this->immoaddressRepository->findByImmoId($unr, $wi, $hs);

        if($immoaddressResult instanceof \TYPO3\MbxRealestate\Domain\Model\Immoaddress) {
            return $immoaddressResult;
        }  else {
            return $this->objectManager->get('TYPO3\MbxRealestate\Domain\Model\Immoaddress');
        }
    }

    /**
     *
     * @param string $street
     * @param string $streetnr
     * @param string $zip
     * @param string $city
     * @return \TYPO3\MbxRealestate\Domain\Model\Immoaddress
     */
    protected function getImmoaddressByAddress($street, $streetnr, $zip, $city) {

        $immoaddressResult = $this->immoaddressRepository->findByAddress($street, $streetnr, $zip, $city);

        if($immoaddressResult instanceof \TYPO3\MbxRealestate\Domain\Model\Immoaddress) {
            return $immoaddressResult;
        }  else {
            return $this->objectManager->get('TYPO3\MbxRealestate\Domain\Model\Immoaddress');
        }
    }

    /**
     *
     * @param array $arrUniqueFields
     * @return \TYPO3\MbxRealestate\Domain\Model\Immocontact
     */
    protected function getImmocontactByUniques(array $arrUniqueFields) {

        $immocontactQueryResult = $this->immocontactRepository->findByUniques($arrUniqueFields);

        if($immocontactQueryResult->count()) {
            return $immocontactQueryResult->getFirst();
        }  else {
            return $this->objectManager->get('TYPO3\MbxRealestate\Domain\Model\Immocontact');
        }
    }

    /**
     *
     * @param array $arrUniqueFields
     * @return \TYPO3\MbxRealestate\Domain\Model\Immoimage
     */
    protected function getImmoimageByUniques(array $arrUniqueFields) {

        $immoimageQueryResult = $this->immoimageRepository->findByUniques($arrUniqueFields);

        if($immoimageQueryResult->count()) {
            return $immoimageQueryResult->getFirst();
        }  else {
            return $this->objectManager->get('TYPO3\MbxRealestate\Domain\Model\Immoimage');
        }
    }

    /**
     *
     * @param string $val
     * @return float
     */
    protected function makeFloat($val) {

        return (float)str_replace(array('.', ','), array('', '.'), $val);
    }

    /**
     * Casts the incoming value from CSV or XML. If the value is a string representing JA/YES/TRUE the function
     * will return a boolean as well as NEIN/NO/FALSE is set as string value. If value is a float/integer the
     * float value will be returned. If non-empty a casted string will represented. Otherwise NULL is returned for empty field
     * @param mixed $val
     * @return boolean|null|string|float
     */
    protected function castValue($val) {

        if(strtolower($val) === 'true' || strtolower($val) === 'ja' || strtolower($val) === 'yes' || $val === true) {
            return true;
        } elseif (strtolower($val) === 'false' || strtolower($val) === 'nein' || strtolower($val) === 'no' || $val === false) {
            return false;
        } elseif(is_float($val) || is_numeric($val)) {
            return (float)$val;
        } elseif (!empty($val)) {
            return (string)$val;
        }  else {
            return null;
        }
    }

    /**
     * Checked if incoming value represents TRUE or a filled numeric field larger zero
     * @param mixed $cast
     * @return boolean
     */
    protected function castIsTrue($cast) {

        return ($cast === true || ((is_float($cast) || is_numeric($cast)) && $cast >= 1));
    }

    /**
     * Checked if incoming value represents FALSE or a filled numeric field is zero
     * @param mixed $cast
     * @return boolean
     */
    protected function castIsFalse($cast) {

        return ($cast === false || ((is_float($cast) || is_numeric($cast)) && $cast === 0));
    }
}
