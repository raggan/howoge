<?php

namespace TYPO3\MbxRealestate\Helper\Import;

class ImporterXml extends \TYPO3\MbxRealestate\Helper\Import\ImporterAbstract implements \TYPO3\MbxRealestate\Helper\Import\Interfaces\ImporterInterface {

    /**
     * Contains the whole xml file string as SimpleXMLElement object
     * @var \SimpleXMLElement 
     */
    private $xml;
        
    function __construct() {
        parent::__construct();
    }
    
    /**
     * Openes the XML file and stores the content in $this->xml
     * @return boolean|\TYPO3\MbxRealestate\Helper\Exception\ImportImmoException
     */
    public function openFile() {
        
        if(!file_exists($file = $this->getFile()) || !is_file($file)) {
            
            return \TYPO3\MbxRealestate\Controller\ImportAbstractCommandController::throwException('Import file "' . $file . '" does not exists in ' . __FILE__ . '::' . __FUNCTION__ . '()');
            
        } elseif(!($this->handle = @fopen($file, 'r'))) {
            
            return \TYPO3\MbxRealestate\Controller\ImportAbstractCommandController::throwException('Unable to open file "' . $file . '" in ' . __FILE__ . '::' . __FUNCTION__ . '()');
        
        }  elseif ($this->readXmlFromFile() === false) {
            
            return \TYPO3\MbxRealestate\Controller\ImportAbstractCommandController::throwException('Failed to read XML from file "' . $file . '" in ' . __FILE__ . '::' . __FUNCTION__ . '()');
        }
        
        $this->items = null;
            
        return true;
    }

    /**
     * 
     * @param \SimpleXMLElement $xml
     * @return TYPO3\MbxRealestate\Helper\Import\ImporterXml
     */
    public function setXml($xml) { $this->xml = $xml; return $this; }
    
    /**
     * Returns the whole XML element from the immo file.
     * @return \SimpleXMLElement
     */
    public function getXml() { return $this->xml; }

    /**
     * Reads the XML content from file and returns it or false if read failed.
     * @param string $scope defines which node in TS to use to detect the file information
     * @return boolean|\SimpleXMLElement
     */
    public function readXmlFromFile($scope) {

        $xmlStr = file_get_contents($this->getFile($scope));
        
        $this->setXml(simplexml_load_string($xmlStr));
        
        if(!($this->getXml() instanceof \SimpleXMLElement)) {
            
            throw \TYPO3\MbxRealestate\Controller\ImportAbstractCommandController::throwException(__FUNCTION__ . '() failed in ' . __FILE__);
        }

        return $this->getXml();
    }
    
    /**
     * Returns the items of the XML immo file. (Attention: the items gets shifted each time iterateItem() is called!)
     * @return \SimpleXMLElement[]
     */
    public function &getItems() {
        
        if(is_null($this->items)) {
            $this->storeItems();
        }
        
        return $this->items;
    }
    
    /**
     * iterates to the next item of the immo file.
     * 
     * @return boolean|\SimpleXMLElement
     */
    public function iterateItem() {

        $items = &$this->getItems();
        
        if(($item = array_shift($items)) !== null) {
            return $item;
        }  else {
            return false;
        }                
    }

    /**
     * Returns the content of a xpath node.
     * @param string $path
     * @param \SimpleXMLElement|null $node
     * @return string
     */
    protected function extractXmlValue($path, $node = null) {
        
        if(is_null($node)) {
            $node = $this->currentItem;
        }
        return trim((string)array_shift($node->xpath($path)));
    }
    
    /**
     * Returns the attribute of a xpath node.
     * @param string $path
     * @param \SimpleXMLElement|null $node
     * @return mixed
     */
    protected function extractXmlAttributes($path, $node = null) {
        
        if(is_null($node)) {
            $node = $this->currentItem;
        }
        
        $attributes = array();
        $nodePath = $node->xpath($path);
        if(empty($nodePath)){
            
            return $attributes;
        }
        
        foreach($nodePath[0]->attributes() as $attribute => $value) {
            $attributes[$attribute] = (string)$value;
        }

        return $attributes;
    }
}