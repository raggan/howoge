<?php

namespace TYPO3\MbxRealestate\Helper\Import;

class ImporterImmo extends \TYPO3\MbxRealestate\Helper\Import\ImporterSave implements \TYPO3\MbxRealestate\Helper\Import\Interfaces\ImporterSaveInterface {

    /**
     * @var array
     */
    protected $requiredTables = array(
        self::TABLE_IMMOOBJECTS,
        self::TABLE_AREAS,
        self::TABLE_FEATURES,
        self::TABLE_ENVIRONMENTS,
        self::TABLE_IMAGES,
        self::TABLE_IMMOOBJECT_FEATURES_nm
    );

    /**
     * Contains the unr-wi-hs as key and the related md5-hashes of the immofeatures as array values
     * @var array
     */
    private $nm = array();

    function __construct($importer) {

        parent::__construct($importer);
    }

    /**
     *
     * @param \TYPO3\MbxRealestate\Helper\Import\ImmoDataItem $immoDataItem
     */
    public function saveObject($immoDataItem) {

        $Immoobject = $immoDataItem->getImmoobject();
        $Immoaddress = $Immoobject->getImmoaddress();

        if($Immoaddress instanceof \TYPO3\MbxRealestate\Domain\Model\Immoaddress) {
            $addressUid = $Immoaddress->getUid();
        }  else {
            $addressUid = null;
        }

        $this->pushInsert(array(
            $this->importer->getStoragePid(),
            $unr = $Immoobject->getUnr(),
            $wi = $Immoobject->getWi(),
            $hs = $Immoobject->getHs(),
            $me = $Immoobject->getMe(),
            $Immoobject->getFullObjectData(),
            $Immoobject->getRooms(),
            $Immoobject->getAvailableBy(),
            $Immoobject->getCostGross(),
            $Immoobject->getCostAttendant(),
            $Immoobject->getCostHeat(),
            $Immoobject->getCostNet(),
            $Immoobject->getCostBuy(),
            $Immoobject->getDeposit(),
            $Immoobject->getProvision(),
            $Immoobject->getProvisionNet(),
            $Immoobject->getProvisionBrut(),
            $Immoobject->getYearBuild(),
            $Immoobject->getYearRestoration(),
            $Immoobject->getNoticeRestoration(),
            $Immoobject->getFloors(),
            $Immoobject->getFloorsMax(),
            $Immoobject->getAreaGeneral(),
            $Immoobject->getAreaLand(),
            $Immoobject->getAreaUsable(),
            $Immoobject->getAreaOffice(),
            $Immoobject->getAreaStorage(),
            $Immoobject->getAreaAll(),
            $Immoobject->getNumberParkingSpace(),
            $Immoobject->getNorthPoint(),
            $Immoobject->getTypeEstate(),
            $Immoobject->getTypeEstatePrecise(),
            $Immoobject->getTypeOfUse(),
            $Immoobject->getTypeOfDisposition(),
            $Immoobject->getTypeBuilding(),
            $Immoobject->getRequiredWbs(),
            $Immoobject->getTitle(),
            $Immoobject->getIsInvestment(),
            $Immoobject->getBusinessShares(),
            $Immoobject->getBusinessSharesNum(),
            $Immoobject->getBusinessSharesDesc(),
            $Immoobject->getNotice(),
            $Immoobject->getDescription(),
            $Immoobject->getActionOffer(),
            $Immoobject->getEnergyCertificateType(),
            $Immoobject->getEnergyEfficiencyClass(),
            $Immoobject->getEpartValidTo(),
            $Immoobject->getThermalCharacteristic(),
            $Immoobject->getThermalCharacteristicElectricity(),
            $Immoobject->getThermalCharacteristicHeating(),
            $Immoobject->getThermalHeatingMethod(),
            $Immoobject->getEnergySource(),
            $Immoobject->getEnergyContainsWarmWater(),
            $Immoobject->getProtectedHistoricBuilding(),
            $Immoobject->getCorridor(),
            $Immoobject->getSplitting(),
            $Immoobject->getInfrastructureProvision(),
            $Immoobject->getBuildAfter(),
            $Immoobject->getSituationOfBuilding(),
            $Immoobject->getNumberFlats(),
            $Immoobject->getNumberCompanies(),
            $addressUid
        ), self::TABLE_IMMOOBJECTS);

        // <editor-fold defaultstate="collapsed" desc="store immo areas">
        if(is_array($areas = $immoDataItem->getArrImmoarea()) && count($areas)) {

            foreach($areas as $immoArea) {

                $this->pushInsert(array(
                    $this->importer->getStoragePid(),
                    $immoArea->getRoomType(),
                    $immoArea->getRoomSize(),
                    md5($Immoobject->getImmoobjectMd5Clear().strtolower($immoArea->getRoomType())),
                    $Immoobject->getImmoobjectMd5()
                ), self::TABLE_AREAS);
            }
        }
        // </editor-fold>

        $featuresMapping = array();

        // <editor-fold defaultstate="collapsed" desc="store features">
        if(is_array($features = $immoDataItem->getArrImmofeature()) && count($features)) {

            foreach($features as $immoFeature) {

                $immoFeature instanceof \TYPO3\MbxRealestate\Domain\Model\Immofeature;

                $immoFeatureTitle = $immoFeature->getFeatureTitle();

                if(empty($immoFeatureTitle)) {
                    continue;
                }

                $featuresMapping[] = $immoFeature->getFeatureType();

                $this->pushInsert(array(
                    $this->importer->getStoragePid(),
                    $immoFeature->getFeatureType(),
                    $immoFeature->getFeatureTitle(),
                    $immoFeature->getIsTopFeature(),
                    $immoFeature->getFeatureValue(),
                    $immoFeature->getFeatureCast(),
                    md5($this->importer->getStoragePid() . '|' . strtolower($immoFeature->getFeatureType())),    // previous: md5($Immoobject->getImmoobjectMd5Clear() . strtolower($immoFeature->getFeatureType())) | since we use nm-table no unique (immoobject+feature) required
                ), self::TABLE_FEATURES);

            }

            $this->nm[implode('-', array($unr, $wi, $hs, $me))] = $featuresMapping;
        }
        // </editor-fold>

        // <editor-fold defaultstate="collapsed" desc="store environments">
        if(is_array($environments = $immoDataItem->getArrImmoenvironment()) && count($environments)) {

            foreach($environments as $immoEnvironment) {

                $this->pushInsert(array(
                    $this->importer->getStoragePid(),
                    $immoEnvironment->getEnvironmentType(),
                    $immoEnvironment->getEnvironmentTitle(),
                    md5($Immoobject->getImmoobjectMd5Clear() . strtolower($immoEnvironment->getEnvironmentType())),
                    $Immoobject->getImmoobjectMd5()
                ), self::TABLE_ENVIRONMENTS);
            }
        }
        // </editor-fold>

        // <editor-fold defaultstate="collapsed" desc="store images">
        if(is_array($images = $immoDataItem->getArrImmoimage()) && count($images)) {

            foreach($images as $immoImage) {

                $this->pushInsert(array(
                    $this->importer->getStoragePid(),
                    $immoImage->getImageType(),
                    $immoImage->getImagePath(),
                    $immoImage->getFilename(),
                    $immoImage->getDescription(),
                    $immoImage->getTitle(),
                    $immoImage->getCategory(),
                    md5($Immoobject->getImmoobjectMd5Clear() . strtolower($immoImage->getFilename())),
                    $Immoobject->getImmoobjectMd5()
                ), self::TABLE_IMAGES);
            }
        }
        // </editor-fold>


    }

    /**
     * This method creates a temporary table containing the relations to build between contacts and address items.
     */
    public function createNM() {

        $liveImmoobjects = $this->importer->immoobjectRepository->findAll();
        $liveImmofeatures = $this->importer->immofeatureRepository->findAll();

        $mapping = array();

        /**
         * make array such as:
         * array(
         *  balcony => 89,
         *  ebk => 90,
         *  ....
         * )
         */
        foreach($liveImmofeatures as $Immofeature) {
            
            // ensure the storage pid of the feature is the same as the target storage pid of the immoobject to avoid
            // linking feature "balcony" (#123) of storagePid #1 instead of "balcony" (#456) of storagePid #2 when working 
            // on storage pic #2
            if($Immofeature->getPid() == $this->importer->getStoragePid()) {
                $mapping[$Immofeature->getFeatureType()] = $Immofeature->getUid();
            }
        }

        foreach($liveImmoobjects as $Immoobject) {

            $uniqueImmoobjectKey = $Immoobject->getUnr().'-'.$Immoobject->getWi().'-'.$Immoobject->getHs().'-'.$Immoobject->getMe();

            // check if immoobject from DB was found in previous feature import
            if(array_key_exists($uniqueImmoobjectKey, $this->nm)) {

                $featuresForImmoobject =& $this->nm[$uniqueImmoobjectKey];
                $sorting = 0;

                // iterate each feature items for the immoobject dataset
                foreach($featuresForImmoobject as $feature) {

                    if(array_key_exists($feature, $mapping)) {

                        $this->pushInsert(array(
                            $this->importer->getStoragePid(),
                            $Immoobject->getUid(),
                            $mapping[$feature],
                            ++$sorting
                        ), self::TABLE_IMMOOBJECT_FEATURES_nm);
                    }
                }

                unset($featuresForImmoobject);
            }

        }

        $this->sqlTempDataPush();
        $this->sqlTempDataTransfer(array(self::TABLE_IMMOOBJECT_FEATURES_nm));
    }


}