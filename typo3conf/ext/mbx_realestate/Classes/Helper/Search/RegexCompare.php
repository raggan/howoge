<?php

namespace TYPO3\MbxRealestate\Helper\Search;

class RegexCompare {


    /**
     * @var string
     */
    private $condition;
    
    public function __construct($condition) { 
        
        $this->setCondition($condition);
    }
    
    /**
     * @return string
     */
    public function getCondition() {
        return $this->condition;
    }

    /**
     * 
     * @param string $condition
     * @return \TYPO3\MbxRealestate\Helper\Search\RegexCompare
     */
    public function setCondition($condition) {
        $this->condition = $condition;
        return $this;
    }

}
