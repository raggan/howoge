<?php

namespace TYPO3\MbxRealestate\Helper\Search;

use TYPO3\CMS\Core\Utility\GeneralUtility;

class ImmoFindBySearchHelper {

    /**
     * @var \TYPO3\MbxRealestate\Helper\Search\ImmoFindBySearchHelper
     */
    private static $instance = null;
    
    /**
     * @var \TYPO3\CMS\Extbase\Object\ObjectManager
     */
    private $objectManager = null;
    
    /**
     * @var \TYPO3\MbxRealestate\Helper\ImmoPluginHelper
     */
    private $immoPluginHelper = NULL;
    
    /**
     * shortcut immoobject table
     * @var string
     */
    private $tblImmoObject = NULL;
    
    /**
     * immoobject table
     * @var string
     */
    private $tableImmoobject = NULL;
    
    /**
     * shortcut immoobject features table
     * @var string
     */
    private $tblImmofeatures = NULL;
    
    /**
     * immoobject features table
     * @var string
     */
    private $tableImmofeatures = NULL;
    
    /**
     * shortcut immoobject features mm table
     * @var string
     */
    private $tblImmofeaturesMM = NULL;
    
    /**
     * immoobject features mm table
     * @var string
     */
    private $tableImmofeaturesMM = NULL;
    
    /**
     * shortcut immoaddress table
     * @var string
     */
    private $tblImmoaddress = NULL;
    
    /**
     * immoaddress table
     * @var string
     */
    private $tableImmoaddress = NULL;

    /**
     * shortcut immo environments table
     * @var string
     */
    private $tblImmoenvironments = NULL;
    
    /**
     * immo environments table
     * @var string
     */
    private $tableImmoenvironments = NULL;
    
    /**
     *
     * @var mixed
     */
    private $where = array();
    
    /**
     *
     * @var mixed
     */
    private $joins = array();
      
    /**
     *
     * @var mixed
     */
    private $subSelects = array();
       
    /**
     *
     * @var mixed
     */
    private $having = array();
    
    /**
     *
     * @var string
     */
    private $distance = '';
    
    /**
     * searchItem
     * @var \TYPO3\MbxRealestate\Helper\Search\ImmoSearchItem
     */
    private $searchItem = NULL;

    /**
     * concatUniqueImmoId
     * @var mixed
     */
    private $concatUniqueImmoId = NULL;
    
    /**
     * typesOfDispositon
     * @var mixed
     */
    private $typesOfDispositon = NULL;

    /**
     * @return \TYPO3\MbxRealestate\Helper\Search\ImmoSearchHelper
     */
    public static function getInstance() {

        if(is_null(self::$instance) || !(self::$instance instanceof \TYPO3\MbxRealestate\Helper\Search\ImmoFindBySearchHelper)) {
            self::$instance = GeneralUtility::makeInstance('TYPO3\\MbxRealestate\\Helper\\Search\\ImmoFindBySearchHelper');
        }

        return self::$instance;
    }

    public function __construct($controller = null) {
            
        $this->objectManager = new \TYPO3\CMS\Extbase\Object\ObjectManager();
        $this->immoPluginHelper = \TYPO3\MbxRealestate\Helper\ImmoPluginHelper::getInstance();
    }
    
    /**
     * 
     * @return mixed
     */
    public function getObjectManager (){
        return $this->objectManager;
    }
    
    /**
     * 
     * @param string $tblImmoObject
     * @return \TYPO3\MbxRealestate\Helper\Search\ImmoFindBySearchHelper
     */
    public function setTblImmoObject($tblImmoObject){
        
        $this->tblImmoObject = $tblImmoObject;
        return $this;
    }
    
    /**
     * 
     * @return string
     */
    public function getTblImmoObject(){
        return $this->tblImmoObject;
    }
    
    /**
     * 
     * @param string $tableImmoobject
     * @return \TYPO3\MbxRealestate\Helper\Search\ImmoFindBySearchHelper
     */
    public function setTableImmoobject($tableImmoobject){
        
        $this->tableImmoobject = $tableImmoobject;
        return $this;
    }
    
    /**
     * 
     * @return string
     */
    public function getTableImmoobject(){
        return $this->tableImmoobject;
    }
    
    /**
     * 
     * @param string $tblImmofeatures
     * @return \TYPO3\MbxRealestate\Helper\Search\ImmoFindBySearchHelper
     */
    public function setTblImmofeatures($tblImmofeatures){
        
        $this->tblImmofeatures = $tblImmofeatures;
        return $this;
    }
    
    /**
     * 
     * @return string
     */
    public function getTblImmofeatures(){
        return $this->tblImmofeatures;
    }
    
    /**
     * 
     * @param string $tableImmofeatures
     * @return \TYPO3\MbxRealestate\Helper\Search\ImmoFindBySearchHelper
     */
    public function setTableImmofeatures($tableImmofeatures){
        
        $this->tableImmofeatures = $tableImmofeatures;
        return $this;
    }
    
    /**
     * 
     * @return string
     */
    public function getTableImmofeatures(){
        return $this->tableImmofeatures;
    }
    
    /**
     * 
     * @param string $tblImmofeaturesMM
     * @return \TYPO3\MbxRealestate\Helper\Search\ImmoFindBySearchHelper
     */
    public function setTblImmofeaturesMM($tblImmofeaturesMM){
        
        $this->tblImmofeaturesMM = $tblImmofeaturesMM;
        return $this;
    }
    
    /**
     * 
     * @return string
     */
    public function getTblImmofeaturesMM(){
        return $this->tblImmofeaturesMM;
    }
    
    /**
     * 
     * @param string $tableImmofeaturesMM
     * @return \TYPO3\MbxRealestate\Helper\Search\ImmoFindBySearchHelper
     */
    public function setTableImmofeaturesMM($tableImmofeaturesMM){
        
        $this->tableImmofeaturesMM = $tableImmofeaturesMM;
        return $this;
    }
    
    /**
     * 
     * @return string
     */
    public function getTableImmofeaturesMM(){
        return $this->tableImmofeaturesMM;
    }
    
    /**
     * 
     * @param string $tblImmoaddress
     * @return \TYPO3\MbxRealestate\Helper\Search\ImmoFindBySearchHelper
     */
    public function setTblImmoaddress($tblImmoaddress){
        
        $this->tblImmoaddress = $tblImmoaddress;
        return $this;
    }
    
    /**
     * 
     * @return string
     */
    public function getTblImmoaddress(){
        return $this->tblImmoaddress;
    }
    
    /**
     * 
     * @param string $tableImmoaddress
     * @return \TYPO3\MbxRealestate\Helper\Search\ImmoFindBySearchHelper
     */
    public function setTableImmoaddress($tableImmoaddress){
        
        $this->tableImmoaddress = $tableImmoaddress;
        return $this;
    }
    
    /**
     * 
     * @return string
     */
    public function getTableImmoaddress(){
        return $this->tableImmoaddress;
    }
    
    /**
     * 
     * @param string $tblImmoenvironments
     * @return \TYPO3\MbxRealestate\Helper\Search\ImmoFindBySearchHelper
     */
    public function setTblImmoenvironments($tblImmoenvironments){
        
        $this->tblImmoenvironments = $tblImmoenvironments;
        return $this;
    }
    
    /**
     * 
     * @return string
     */
    public function getTblImmoenvironments(){
        return $this->tblImmoenvironments;
    }
    
    /**
     * 
     * @param string $tableImmoenvironments
     * @return \TYPO3\MbxRealestate\Helper\Search\ImmoFindBySearchHelper
     */
    public function setTableImmoenvironments($tableImmoenvironments){
        
        $this->tableImmoenvironments = $tableImmoenvironments;
        return $this;
    }
    
    /**
     * 
     * @return string
     */
    public function getTableImmoenvironments(){
        return $this->tableImmoenvironments;
    }
    
    /**
     * 
     * @param \TYPO3\MbxRealestate\Helper\Search\ImmoSearchItem $searchItem
     * @return \TYPO3\MbxRealestate\Helper\Search\ImmoFindBySearchHelper
     */
    public function setSearchItem (\TYPO3\MbxRealestate\Helper\Search\ImmoSearchItem $searchItem){
        $this->searchItem = $searchItem;
        return $this;
    }
    
    /**
     * 
     * @return \TYPO3\MbxRealestate\Helper\Search\ImmoSearchItem
     */
    public function getSearchItem (){
        return $this->searchItem;
    }

    /**
     * 
     * @param mixed $concatUniqueImmoId
     */
    public function setConcatUniqueImmoId ($concatUniqueImmoId){
        $this->concatUniqueImmoId = $concatUniqueImmoId;
    }
    
    /**
     * 
     * @return miexed $concatUniqueImmoId
     */
    public function getConcatUniqueImmoId (){
        return $this->concatUniqueImmoId;
    }
    
    /**
     * 
     * @param mixed $typesOfDispositon
     */
    public function setTypesOfDispositon ($typesOfDispositon){
        $this->typesOfDispositon = $typesOfDispositon;
    }

    /**
     * 
     * @return miexed $where
     */
    public function getWhere (){
        return $this->where;
    }
    
    /**
     * 
     * @param mixed $where
     */
    public function setWhere ($where){
        $this->where = $where;
    }
    
    /**
     * 
     * @return miexed $having
     */
    public function getHaving (){
        return $this->having;
    }
    
    /**
     * 
     * @param mixed $having
     */
    public function setHaving ($having){
        $this->having = $having;
    }
    
    /**
     * 
     * @return miexed $joins
     */
    public function getJoins (){
        return $this->joins;
    }
    
    /**
     * 
     * @param mixed $joins
     */
    public function setJoins ($joins){
        $this->joins = $joins;
    }
    
    /**
     * 
     * @return miexed $typesOfDispositon
     */
    public function getTypesOfDispositon (){
        return $this->typesOfDispositon;
    }
    
    public function handleStorage (){
        
        if ($this->searchItem->hasStorageSet()) {

			$this->where[] = $this->getTblImmoobject() . '.pid = ' . $this->searchItem->getStorage();
            
			if(in_array($this->searchItem->getSort(), array('street ASC','street DESC')) && !$this->searchItem->hasGeoParams()) {
				$this->joins[] = ' JOIN ' . $this->getTableImmoaddress() . ' AS ' . $this->getTblImmoaddress()
					. ' ON ' . $this->getTableImmoaddress() . '.uid = ' . $this->getTblImmoobject() . '.immoaddress ';
			}
		} else {

			// <editor-fold defaultstate="collapsed" desc="handle 'type of use'">
			if ($this->searchItem->hasTypesOfUseSet()) {

				$typesOfUse = $this->searchItem->getTypesOfUse();
			} else {

				$typesOfUse = array(\TYPO3\MbxRealestate\Helper\ImmoPluginHelper::getInstance()->getTypeOfUseFallback());
			}

			$typesOfUseSql = array();
			foreach ($typesOfUse as $typeOfUse) {
				$typesOfUseSql[] = ' FIND_IN_SET(' . $this->quoteItems($typeOfUse, $this->getTableImmoobject()) . ', ' . $this->getTblImmoobject() . '.type_of_use) ';
			}

			$this->where[] = ' (' . implode(' OR ', $typesOfUseSql) . ') ';
			// </editor-fold>
		}

		$this->where[] = $this->getTblImmoobject() . '.hidden = 0';
		$this->where[] = $this->getTblImmoobject() . '.deleted = 0';
        
        $this->setConcatUniqueImmoId($this->immoPluginHelper->getUniqueImmoIdForSQL($this->getTblImmoobject()));
        
        return $this;
    }
    
    // <editor-fold defaultstate="collapsed" desc="to speed up SQL-query look first for excluded object">
    public function speedUpQuery (){
		
        if ($this->searchItem->hasExcludesSet() || $this->immoPluginHelper->hasCurrentImmoobject()) {

			$excludes = $this->searchItem->getExcludes();

			if (($currentIO = $this->immoPluginHelper->getCurrentImmoobject()) instanceof \TYPO3\MbxRealestate\Domain\Model\Immoobject) {

				array_push($excludes, $this->immoPluginHelper->getUniqueImmoIdByImmoobject($currentIO));
			}

			array_walk($excludes, array($this, 'quoteItems'), $this->getTableImmoobject());

			$this->where[] = $this->concatUniqueImmoId . ' NOT IN (' . implode(', ', $excludes) . ')';
		}
        
        return $this;
    }
    // </editor-fold>
    
    /**
     * @return \TYPO3\MbxRealestate\Helper\Search\ImmoFindBySearchHelper
     */
    public function handleIsInvestment() {
        
        if($this->getSearchItem()->hasIsInvestmentSet()) {
            
            $this->where[] = ' is_investment = ' . $this->getSearchItem()->getIsInvestment() . ' ';
        }
        
        return $this;
    }
    
    // <editor-fold defaultstate="collapsed" desc="handle 'type of disposition'">
    public function handleTypeOfDisposition (){
        
		if ($this->searchItem->hasTypesOfDispositionSet()) {

			$this->setTypesOfDispositon($this->searchItem->getTypesOfDisposition());
		} else {

			$this->setTypesOfDispositon(array(\TYPO3\MbxRealestate\Helper\ImmoPluginHelper::getInstance()->getTypeOfDispositionFallback()));
		}

		$typesOfDispositionSql = array();
		foreach ($this->getTypesOfDispositon() as $typeOfDisposition) {
            
            if($typeOfDisposition instanceof RegexCompare) {
                
                $this->addComparison($this->getTblImmoobject() . '.type_of_disposition', $typeOfDisposition, $typesOfDispositionSql);
            } else {
                
                $typesOfDispositionSql[] = ' FIND_IN_SET(' . $this->quoteItems($typeOfDisposition, $this->getTableImmoobject()) . ', ' . $this->getTblImmoobject() . '.type_of_disposition) ';
            }
		}

		$this->where[] = ' (' . implode(' OR ', $typesOfDispositionSql) . ') ';
        
        return $this;
    }
    // </editor-fold>
    
    /**
     * 
     * @param string $field
     * @param string|\TYPO3\MbxRealestate\Helper\Search\RegexCompare $value
     * @param array $arr
     */
    private function addComparison($field, $value, array &$arr) {
        
        if($value instanceof RegexCompare) {
                    
            $arr[] = $field . ' REGEXP "' . $value->getCondition() . '"';
            
        } else {

            $arr[] = $field . ' LIKE "%' . $value . '%"';
        }
    }
    
    // <editor-fold defaultstate="collapsed" desc="handle 'types of use'">
    public function handleTypesOfUse (){
        
		if ($this->searchItem->hasTypesOfUseSet()) {

			$types = $this->searchItem->getTypesOfUse();

            $typesOfUseSql = array();
            foreach ($types as $typeOfUse) {
                
                $this->addComparison('type_of_use', $typeOfUse, $typesOfUseSql);
            }

            $this->where[] = ' (' . implode(' OR ', $typesOfUseSql) . ') ';
        }
        
        return $this;
    }
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="handle 'types estate'">
    public function handleTypesEstate (){
        
		if ($this->searchItem->hasTypesEstateSet()) {

			$types = $this->searchItem->getTypesEstate();

            $typesEstateSql = array();
            foreach ($types as $typeEstate) {
                $this->addComparison('type_estate', $typeEstate, $typesEstateSql);
            }

            $this->where[] = ' (' . implode(' OR ', $typesEstateSql) . ') ';
        }
        
        return $this;
    }
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="handle living area">
    public function handleLivingArea (){
		
        if ($this->searchItem->hasAreaSet()) {

			$areaFrom = $this->searchItem->getAreaFrom();
			$areaTo = $this->searchItem->getAreaTo();

			if (!empty($areaFrom) && !empty($areaTo)) {

				$this->where[] = $this->getTblImmoobject() . '.area_general BETWEEN ' . (float)$areaFrom . ' AND ' . (float)$areaTo;

			} elseif (!empty($areaFrom)) {

				$this->where[] = $this->getTblImmoobject() . '.area_general >= ' . (float)$areaFrom;

			} elseif (!empty($areaTo)) {

				$this->where[] = $this->getTblImmoobject() . '.area_general <= ' . (float)$areaTo;
			}
		}
        
        return $this;
    }
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="handle rooms">
    public function handleRooms (){
		if ($this->searchItem->hasRoomsSet()) {

			$roomsFrom = $this->searchItem->getRoomsFrom();
			$roomsTo = $this->searchItem->getRoomsTo();

			if (!empty($roomsFrom) && !empty($roomsTo)) {

				$this->where[] = $this->getTblImmoobject() . '.rooms BETWEEN ' . (float)$roomsFrom . ' AND ' . (float)$roomsTo;

			} elseif (!empty($roomsFrom)) {

				$this->where[] = $this->getTblImmoobject() . '.rooms >= ' . (float)$roomsFrom;

			} elseif (!empty($roomsTo)) {

				$this->where[] = $this->getTblImmoobject() . '.rooms <= ' . (float)$roomsTo;
			}
		}
        
        return $this;
    }
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="handle costs">
    public function handleCosts (){
        $prices = array();

		// <editor-fold defaultstate="collapsed" desc="handle cost gross">
		if ($this->searchItem->hasCostGrossSet() && in_array($this->immoPluginHelper->getPluginSettings('settings.search.type_of_disposition_rent'), $this->getTypesOfDispositon())) {

			$costGrossFrom = $this->searchItem->getCostGrossFrom();
			$costGrossTo = $this->searchItem->getCostGrossTo();

			if (!empty($costGrossFrom) && !empty($costGrossTo)) {

				$prices[] = $this->getTblImmoobject() . '.cost_gross BETWEEN ' . (float)$costGrossFrom . ' AND ' . (float)$costGrossTo;

			} elseif (!empty($costGrossFrom)) {

				$prices[] = $this->getTblImmoobject() . '.cost_gross >= ' . (float)$costGrossFrom;

			} elseif (!empty($costGrossTo)) {

				$prices[] = $this->getTblImmoobject() . '.cost_gross <= ' . (float)$costGrossTo;
			}
		}
		// </editor-fold>

		// <editor-fold defaultstate="collapsed" desc="handle cost net">
		if ($this->searchItem->hasCostNetSet() && in_array($this->immoPluginHelper->getPluginSettings('settings.search.type_of_disposition_rent'), $this->getTypesOfDispositon())) {

			$costNetFrom = $this->searchItem->getCostNetFrom();
			$costNetTo = $this->searchItem->getCostNetTo();

			if (!empty($costNetFrom) && !empty($costNetTo)) {

				$prices[] = $this->getTblImmoobject() . '.cost_net BETWEEN ' . (float)$costNetFrom . ' AND ' . (float)$costNetTo;

			} elseif (!empty($costNetFrom)) {

				$prices[] = $this->getTblImmoobject() . '.cost_net >= ' . (float)$costNetFrom;

			} elseif (!empty($costNetTo)) {

				$prices[] = $this->getTblImmoobject() . '.cost_net <= ' . (float)$costNetTo;
			}
		}
		// </editor-fold>

		// <editor-fold defaultstate="collapsed" desc="handle cost buy">
		if ($this->searchItem->hasCostBuySet() && in_array($this->immoPluginHelper->getPluginSettings('settings.search.type_of_disposition_buy'), $this->getTypesOfDispositon())) {

			$costBuyFrom = $this->searchItem->getCostBuyFrom();
			$costBuyTo = $this->searchItem->getCostBuyTo();

			if (!empty($costBuyFrom) && !empty($costBuyTo)) {

				$prices[] = $this->getTblImmoobject() . '.cost_buy BETWEEN ' . (float)$costBuyFrom . ' AND ' . (float)$costBuyTo;

			} elseif (!empty($costBuyFrom)) {

				$prices[] = $this->tblImmoobject() . '.cost_buy >= ' . (float)$roomsFrom;

			} elseif (!empty($costBuyTo)) {

				$prices[] = $this->getTblImmoobject() . '.cost_buy <= ' . (float)$costBuyTo;
			}
		}
		// </editor-fold>

		// if 'cost_gross' or 'cost_buy' was set we search for them
		if (!empty($prices)) {

			$this->where[] = ' (' . implode(' OR ', $prices) . ') ';
		}
        
        return $this;
    }
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="handle year build">
    public function handleYearBuild (){
        if ($this->searchItem->hasYearBuildSet()) {

			$this->where[] = $this->getTblImmoobject() . '.year_build = ' . $this->searchItem->getYearBuild();

		} elseif ($this->searchItem->hasYearBuildRangeSet()) {

			$yearBuildFrom = $this->searchItem->getYearBuildFrom();
			$yearBuildTo = $this->searchItem->getYearBuildTo();

			if (!is_null($yearBuildFrom) && !is_null($yearBuildTo)) {

				$this->where[] = $this->getTblImmoobject() . '.year_build BETWEEN ' . $yearBuildFrom . ' AND ' . $yearBuildTo;

			} elseif (!is_null($yearBuildFrom)) {

				$this->where[] = $this->getTblImmoobject() . '.year_build >= ' . $yearBuildFrom;

			} else {

				$this->where[] = $this->getTblImmoobject() . '.year_build <= ' . $yearBuildTo;
			}
		}
        
        return $this;
    }
     // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="handle floors max set">
    public function handleFloorsMaxSet (){
		if ($this->searchItem->hasFloorsMaxSet()) {

			$floorsMaxFrom = $this->searchItem->getFloorsMaxFrom();
			$floorsMaxTo = $this->searchItem->getFloorsMaxTo();

			if (!is_null($floorsMaxFrom) && !is_null($floorsMaxTo)) {

				$this->where[] = $this->getTblImmoobject() . '.floors_max BETWEEN ' . $floorsMaxFrom . ' AND ' . $floorsMaxTo;

			} elseif (!is_null($floorsMaxFrom)) {

				$this->where[] = $this->getTblImmoobject() . '.floors_max >= ' . $floorsMaxFrom;

			} else {

				$this->where[] = $this->getTblImmoobject() . '.floors_max <= ' . $floorsMaxTo;
			}
		}
        
        return $this;
    }
     // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="handle floors">
    public function handleFloors (){
		if ($this->searchItem->hasFloorsSet()) {

			$floorsFrom = $this->searchItem->getFloorsFrom();
			$floorsTo = $this->searchItem->getFloorsTo();

			if (!is_null($floorsFrom) && !is_null($floorsTo)) {

				if (!is_null($floorsTS = $this->immoPluginHelper->getPluginSettings('settings.search.fieldsConfiguration.floors.'))) {

					$floorsConfig = $this->immoPluginHelper->parseTS($floorsTS);
					$floorsMax = $floorsConfig['max'];

					if ($floorsFrom == $floorsTo && $floorsTo == $floorsMax) {

						$this->where[] = $this->getTblImmoobject() . '.floors >= ' . $floorsFrom;

					} elseif ($floorsFrom == 0 && $floorsTo == $floorsMax) {

						// search all floors. So unneccessary to add statement here!

					} elseif ($floorsFrom >= 0 && $floorsTo <= $floorsMax && $floorsFrom != $floorsTo) {

						$this->where[] = $this->getTblImmoobject() . '.floors BETWEEN ' . $floorsFrom . ' AND ' . $floorsTo;

					} elseif ($floorsFrom == $floorsTo) {

						$this->where[] = $this->getTblImmoobject() . '.floors = ' . $floorsFrom;
					}

				} else {

					$this->where[] = $this->getTblImmoobject() . '.floors BETWEEN ' . $floorsFrom . ' AND ' . $floorsTo;
				}


			} elseif (!is_null($floorsFrom)) {

				$this->where[] = $this->getTblImmoobject() . '.floors >= ' . $floorsFrom;

			} elseif (!is_null($floorsTo)) {

				$this->where[] = $this->getTblImmoobject() . '.floors <= ' . $floorsFrom;
			}
		}
        
        return $this;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="handle offers">
    
    public function handleOffers(){
		if ($this->searchItem->hasOffersSet()) {

			$typesOfOffersSql = array();

			foreach ($this->searchItem->getOffers() as $offer) {
				$typesOfOffersSql[] = ' FIND_IN_SET(' . $this->quoteItems($offer, $this->getTableImmoobject()) . ', ' . $this->getTblImmoobject() . '.action_offer) ';
			}

			$this->where[] = ' (' . implode(' OR ', $typesOfOffersSql) . ') ';
		}
        
        return $this;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="handle features">
    public function handleFeatures (){
		if ($this->searchItem->hasFeaturesSet()) {

			if ($this->searchItem->hasStorageSet()) {
				$featurePid = $this->searchItem->getStorage();
			} else {
				$featurePid = null;
			}

			// <editor-fold defaultstate="collapsed" desc="query features once to get features uid's by their slugs">
			// Optimized to soften the SQL (avoid Subselect with JOINs onto the mm table by using a IN-statement in addition with HAVING COUNT)
			$featuresRepository = $this->objectManager->get('TYPO3\MbxRealestate\\Domain\\Repository\\ImmofeatureRepository');
			$featuresAll = $featuresRepository->getUniqueFeatures($featurePid);
			$availableFeatures = array();
			$availableFeaturesTitle = array();

			foreach ($featuresAll as $featureModel) {
				$featureModel instanceof \TYPO3\MbxRealestate\Domain\Model\Immofeature;
                
                $availableFeatures[$featureModel->getFeatureType()] = $featureModel->getUid();
				$availableFeaturesTitle[$featureModel->getFeatureTitle()] = $featureModel->getUid();
			}
			// </editor-fold>
            
            $immoPluginHelper = new \TYPO3\MbxRealestate\Helper\ImmoPluginHelper($this);
            
            $featuresVirtual = $immoPluginHelper->getVirtualFeatures();
            
            $featureGroups = array();
            $featureAndArray = array(
                'compare'   =>  'AND',
                'features'  =>  array()
            );
            
            $featureGroups[] =& $featureAndArray;
            
			foreach ($this->searchItem->getFeatures() as $featureType) {
                
                if(!preg_match('/^VIRT_(?<FEATURE>.*)$/', $featureType)) {
                    if (array_key_exists($featureType, $availableFeatures)) {

                        $featureAndArray['features'][] = $availableFeatures[$featureType]; //$this->quoteItems($featureType, $tableImmofeatures);
                    }
                    continue;
                }
                
                if(array_key_exists($featureType, $featuresVirtual)) {
                    
                    $virtualFeature = $featuresVirtual[$featureType];
                    $featureList = array();
                    
                    foreach($virtualFeature['features'] as $relatedFeature) {
                        
                        // related sub-feature in the list is identified by a Title. We have to search in the titles array
                        if(array_key_exists($relatedFeature, $availableFeaturesTitle)) {
                            $featureList[] = $availableFeaturesTitle[$relatedFeature];
                            
                        // related sub-feature in the list is identified by featureType format as seen in DB
                        } elseif(preg_match('/^[a-zA-Z0-9]+\_[a-z0-9]+$/', $relatedFeature)) {
                            $featureList[] = $availableFeatures[$relatedFeature];
                        }                            
                    }
                    
                    $featureGroups[] = array(
                        'compare'   =>  'OR',
                        'features'  =>  $featureList
                    );
                }
			}

			$tblImmofeaturesMMHaving = $this->getTblImmofeaturesMM() . 'Having';

            foreach($featureGroups as $i => $featureGroupData) {
                
                $features = $featureGroupData['features'];
                $compareType = $featureGroupData['compare'];
                $compare = ($compareType === 'OR' ? '>' : '=');
                $compareCnt = ($compareType === 'OR' ? 0 : count($features));
                
                if(count($features)) {

                    $tmpTableAlias = $tblImmofeaturesMMHaving . $i;
                    
                    $this->joins[] = ' JOIN ' . $this->getTableImmofeaturesMM() . ' ' . $tmpTableAlias . ' ON ' . $tmpTableAlias . '.uid_local = ' . $this->getTblImmoobject() . '.uid ';
                    $this->where[] = ' ' . $tmpTableAlias . '.uid_foreign IN (' . implode(',', $features) . ') ';
                    $this->having[] = ' COUNT(DISTINCT ' . $tmpTableAlias . '.uid_foreign) ' . $compare . ' ' . $compareCnt . ' ';
                }
            }
		}
        
        return $this;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="handle environments">
    public function handleEnvironments (){
		if ($this->searchItem->hasEnvironmentsSet()) {
            
			foreach ($this->searchItem->getEnvironments() as $tmpKey => $environmentType) {
				$tblImmoenvironmentsI = $this->getTblImmoenvironments() . 'F' . $tmpKey;

				$this->where[] = ' io.uid IN (SELECT ' . $tblImmoenvironmentsI . '.immoobject'
					. ' FROM ' . $this->getTableImmoenvironments() . ' AS ' . $tblImmoenvironmentsI
					. ' WHERE ' . $tblImmoenvironmentsI . '.environment_type = ' . $this->quoteItems($environmentType, $this->getTableImmoenvironments())
					. ' AND ' . $tblImmoenvironmentsI . '.hidden = 0 '
					. ' AND ' . $tblImmoenvironmentsI . '.deleted = 0 )';
			}
		}
        
        return $this;
    }
    // </editor-fold>
		
    // <editor-fold defaultstate="collapsed" desc="handle estate precise types">
    public function handleEstatePreciseType (){
		if ($this->searchItem->hasTypesEstatePreciseSet()) {
            
            $limitationPreciseEstateType = array();
			foreach ($this->searchItem->getTypesEstatePrecise() as $value) {
                
                $this->addComparison('type_estate_precise', $value, $limitationPreciseEstateType);
//                if($value instanceof RegexCompare) {
//                    
//                    $this->addComparison($this->getTblImmoobject() . '.type_estate_precise', $value, $limitationPreciseEstateType);
//                } else {
//                    
//                    array_push($limitationPreciseEstateType, $this->getTblImmoobject() . '.type_estate_precise LIKE "%' . $value . '%"');
//                }
			}
            
            if(sizeof($limitationPreciseEstateType) > 0 ){
                $this->where[] = ' (' . implode(' OR ', $limitationPreciseEstateType) . ')';
            }
		}
        
        return $this;
    }
    // </editor-fold>
        
    // <editor-fold defaultstate="collapsed" desc="handle required wbs">
    public function handleRequireWbs (){
		if ((int)$this->searchItem->getRequiredWbs() === 1) {
            
            $this->where[] = $this->getTblImmoobject() . '.required_wbs = 1';
		}
        
        return $this;
    }
    // </editor-fold>
        
    // <editor-fold defaultstate="collapsed" desc="handle thermal heating method">
    public function handleThermalHeatingMethod (){
		if ($this->searchItem->hasThermalHeatingMethodsSet()) {
            
            $thermalHeatingMethods = array();
			foreach ($this->searchItem->getThermalHeatingMethods() as $value) {

                $this->addComparison('thermal_heating_method', $value, $thermalHeatingMethods);
			}
            
            if(sizeof($thermalHeatingMethods) > 0 ){
                $this->where[] = ' (' . implode(' OR ', $thermalHeatingMethods) . ')';
            }
		}

        return $this;
    }
    // </editor-fold>
        
    // <editor-fold defaultstate="collapsed" desc="handle provision">
    public function handleProvision (){
		if ($this->searchItem->getProvision() !== 2) {
            $this->where[] = $this->getTblImmoobject() . '.provision = ' . $this->searchItem->getProvision();
		}
        
        return $this;
    }
    // </editor-fold>
        
    // <editor-fold defaultstate="collapsed" desc="handle keywords">
    public function handleKeywords (){
		if ($this->searchItem->hasKeywordsSet()) {

            $search = array();
            foreach($this->searchItem->getKeywords() as $word){
                array_push($search, $this->getTblImmoobject() . '.title LIKE "%' . $word . '%" OR ' . $this->getTblImmoobject() . '.notice LIKE "%' . $word . '%" OR ' . $this->getTblImmoobject() . '.description LIKE "%' . $word . '%"');
            }
            
            $this->where[] = '(' . implode(' OR ', $search) . ')';
		}
        
        return $this;
    }
    // </editor-fold>
        
    // <editor-fold defaultstate="collapsed" desc="handle type building">
    public function handleTypeBuilding (){
		if ($this->searchItem->hasTypeBuildingSet()) {
            
            $this->where[] = $this->getTblImmoobject() . '.type_building LIKE "%' . $this->searchItem->getTypeBuilding() . '%"';
		}
        
        return $this;
    }
    // </editor-fold>

    public function handleLocation (){
		if ($this->searchItem->hasGeoParams()) {
			$geoLocation = $this->searchItem->getGeoLocation();
			$geoDistance = $this->searchItem->getGeoDistance();


			$this->joins[] = ' JOIN ' . $this->getTableImmoaddress() . ' AS ' . $this->getTblImmoaddress()
				. ' ON ' . $this->getTblImmoaddress() . '.uid = ' . $this->getTblImmoobject() . '.immoaddress ';

			$this->distance = ', ACOS('
				. ' SIN(RADIANS(' . $geoLocation[0] . ')) * SIN(RADIANS(' . $this->getTblImmoaddress() . '.geo_x)) + '
				. ' COS(RADIANS(' . $geoLocation[0] . ')) * COS(RADIANS(' . $this->getTblImmoaddress() . '.geo_x)) * '
				. ' COS(RADIANS(' . $geoLocation[1] . ') - RADIANS(' . $this->getTblImmoaddress() . '.geo_y))
                            ) * 6380 AS distance';

			$geoDistanceMapping = $this->immoPluginHelper->getPluginSettings('settings.geoDistanceMapping.');
			$this->having[] = ' distance < ' . (!empty($geoDistanceMapping[$geoDistance]) ? $geoDistanceMapping[$geoDistance] : $geoDistance);

		} else {
			// <editor-fold defaultstate="collapsed" desc="handle street">
			if (($street = $this->searchItem->getStreet()) && !empty($street)) {

				$this->joins[] = ' JOIN ' . $this->getTableImmoaddress() . ' AS ' . $this->getTblImmoaddress()
					. ' ON ' . $this->getTblImmoaddress() . '.street LIKE "%' . $GLOBALS['TYPO3_DB']->escapeStrForLike($street, $this->getTableImmoaddress()) . '%" '
					. ' AND ' . $this->getTblImmoaddress() . '.uid = ' . $this->getTblImmoobject() . '.immoaddress ';
			}
			// </editor-fold>

			// <editor-fold defaultstate="collapsed" desc="handle districts">
			if ($this->searchItem->hasDistrictsSet()) {

				$districts = $this->searchItem->getDistricts();
				$tblImmoaddressDistrict = $this->getTblImmoaddress() . 'District';

				$this->joins[] = ' JOIN ' . $this->getTableImmoaddress() . ' AS ' . $tblImmoaddressDistrict
					. ' ON ' . $tblImmoaddressDistrict . '.uid = ' . $this->getTblImmoobject() . '.immoaddress ';

				switch ($this->immoPluginHelper->getPluginSettings('settings.districtMappingType')) {
					case \TYPO3\MbxRealestate\Helper\ImmoPluginHelper::DISTRICT_MAPPING_TS :

						array_walk($districts, array($this, 'quoteItems'), $this->getTableImmoaddress());
						$this->where[] = $tblImmoaddressDistrict . '.district IN (' . implode(', ', $districts) . ')';

						break;

					case \TYPO3\MbxRealestate\Helper\ImmoPluginHelper::DISTRICT_MAPPING_ZIP :

						$postalCodes = $this->immoPluginHelper->getPostalCodesByDistricts($districts);

						if (count($postalCodes) == 0) {

							// drop latest JOIN because we would pass an empty IN()-statement and causes an error
							array_pop($this->joins);
						} else {

							// add the WHERE-clause to filter the immo-objects by ZIP
							array_walk($postalCodes, array($this, 'quoteItems'), $this->getTableImmoaddress());
							$this->where[] = $tblImmoaddressDistrict . '.zip IN (' . implode(', ', $postalCodes) . ')';
						}

						break;

					default :
						throw new \TYPO3\CMS\Core\Exception('Unsupported districtMappingType defined in TS for querying immoobjects by districts in ' . __FILE__ . ' on line ' . __LINE__);
				}
			}
			// </editor-fold>
            
            // <editor-fold defaultstate="collapsed" desc="handle city">
            if (($city = $this->searchItem->getCity()) && !empty($city)) {
                
                $this->joins[] = ' JOIN ' . $this->getTableImmoaddress() . ' AS ' . $this->getTblImmoaddress() . 'City'
					. ' ON ' . $this->getTblImmoaddress() . 'City.city LIKE "%' . $GLOBALS['TYPO3_DB']->escapeStrForLike($city, $this->getTableImmoaddress()) . '%" '
					. ' AND ' . $this->getTblImmoaddress() . 'City.uid = ' . $this->getTblImmoobject() . '.immoaddress ';
            }
            // </editor-fold>
		}

        return $this;
    }

    // <editor-fold defaultstate="collapsed" desc="build final SQL">
    public function buildSQLStatement (){

        $sql = 'SELECT DISTINCT(' . $this->getTblImmoobject() . '.pid), '
			. ' ' . $this->concatUniqueImmoId . ' AS uniqueImmo, '
			. $this->getTblImmoobject() . '.* '
			. $this->distance

			. ' FROM ' . $this->getTableImmoobject() . ' AS ' . $this->getTblImmoobject()
			. implode(' ', $this->joins)
			. (!empty($this->subSelects) ? ', ' . implode(', ', $this->subSelects) : '')
			. ' WHERE ' . (count($this->where) ? implode(' AND ', $this->where) : '1')
			. ' GROUP BY ' . $this->getTblImmoobject() . '.uid '
			. (!empty($this->having) ? ' HAVING ' . implode(' AND ', $this->having) : '');

        return $sql;
    }    
    
    /**
	 * Called by array_walk() as callback
	 *
	 * @param string $item
	 * @param string $table (by default not necessary but TYPO3 requires this without using this param in any way -.-)
	 *   view TYPO3\CMS\Core\Database\DatabaseConnection::quoteStr()
	 *
	 * @return string
	 */
	protected function quoteItems(&$item, $table = null) {

		$item = '"' . $GLOBALS['TYPO3_DB']->quoteStr($item, $table) . '"';

		return $item;
	}
    
}
