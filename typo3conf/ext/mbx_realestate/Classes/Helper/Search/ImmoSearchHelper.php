<?php

namespace TYPO3\MbxRealestate\Helper\Search;

use TYPO3\CMS\Core\Utility\GeneralUtility;

class ImmoSearchHelper {

    CONST SESSION_STORAGE_KEY = 'immosearch_query';

    /**
     * @var \TYPO3\CMS\Extbase\Object\ObjectManager
     */
    private $objectManager = null;

    /**
     * @var \TYPO3\MbxRealestate\Domain\Session\SessionHandler
     */
    private $sessionHandler = null;

    /**
     * @var \TYPO3\MbxRealestate\Helper\Search\ImmoSearchHelper
     */
    private static $instance = null;

    /**
     *
     * @var \TYPO3\MbxRealestate\Controller\AbstractController
     */
    private $controllerReference;

    /**
     * @return \TYPO3\MbxRealestate\Helper\Search\ImmoSearchHelper
     */
    public static function getInstance() {

        if(is_null(self::$instance) || !(self::$instance instanceof \TYPO3\MbxRealestate\Helper\Search\ImmoSearchHelper)) {
            self::$instance = GeneralUtility::makeInstance('TYPO3\\MbxRealestate\\Helper\\Search\\ImmoSearchHelper');
        }

        return self::$instance;
    }

    public function __construct($controller = null) {

        $this->objectManager = new \TYPO3\CMS\Extbase\Object\ObjectManager();
        $this->sessionHandler = $this->objectManager->get('TYPO3\MbxRealestate\Domain\Session\SessionHandler');

        if(!empty($controller)) {
            $this->controllerReference = $controller;
        }
    }

    /**
     * Extends the session key by the storagePid to differ between the different search forms
     *
     * @param string $sessionKey
     * @return string
     */
    private function getSessionKeyForStorage($sessionKey) {

        if(empty($this->controllerReference)) {

            $pid = \TYPO3\MbxRealestate\Helper\ImmoPluginHelper::getInstance()->getStoragePidByName(null);

        } else {

            $pid = $this->controllerReference->getCurrentStoragePidOfPlugin();
        }

        if(!empty($pid)) {

            $sessionKey .= '_' . $pid;
        }

        return $sessionKey;
    }

    /**
     * Stores the users immosearch parameters in the SESSION
     * @param array $searchArray
     * @param string $sessionKey
     * @return \TYPO3\MbxRealestate\Helper\Search\ImmoSearchHelper
     */
    public function storeSearchInSession($searchArray, $sessionKey = self::SESSION_STORAGE_KEY) {

        $search = (array)$searchArray;
        $currentSession = (array)$this->retrieveSearchFromSession($sessionKey);

        // <editor-fold defaultstate="collapsed" desc="merge search to session if "merge" was provied by new search query">
        if(array_key_exists('merge', $search)) {
            $search = array_merge($currentSession, $search);
            unset($search['merge']);
        }
        // </editor-fold>

        $sessionKey = $this->getSessionKeyForStorage($sessionKey);

        $this->sessionHandler->{$sessionKey} = json_encode($search);
        $this->sessionHandler->saveSession();
        return $this;
    }

    /**
     * Returns the SESSION value for the users immosearch parameters
     * @param string $sessionKey
     * @return null|array
     */
    public function retrieveSearchFromSession($sessionKey = self::SESSION_STORAGE_KEY) {

        $sessionKey = $this->getSessionKeyForStorage($sessionKey);

        if($this->sessionHandler->{$sessionKey}) {
            return $this->jsonDecodeSearch($this->sessionHandler->{$sessionKey});
        }  else {
            return null;
        }
    }

    /**
     * Returns the current search query as a json_encoded() string
     * @return string
     */
    public function getEncodedSearchFromSession() {

        return json_encode($this->retrieveSearchFromSession());
    }

    /**
     * Checks if a search is stored in the SESSION
     * @param string $sessionKey
     * @return boolean
     */
    public function storedSearchInSession($sessionKey = self::SESSION_STORAGE_KEY) {

        $sessionValue = $this->retrieveSearchFromSession($sessionKey);
        return !empty($sessionValue);
    }

    /**
     * @param stdClass $search
     * @return array the array representation of the input stdClass
     */
    public function jsonDecodeSearch($search) {

        $search = json_decode($search);
        return $this->objectToArray($search);
    }

    /**
     * The by default configured search query to prefill forms and results lists.
     * @return array
     */
    public function retrieveSearchFromConfiguration() {

        $immoPluginHelper = \TYPO3\MbxRealestate\Helper\ImmoPluginHelper::getInstance();

        $storageName = $this->controllerReference->getCurrentStorageNameOfPlugin();
        $customStorageSearch = $immoPluginHelper->getPluginSettings('settings.defaultSearchStorage.' . $storageName . '.');

        if(!empty($customStorageSearch)) {
            $search = $customStorageSearch;
        } else {
            $search = $immoPluginHelper->getPluginSettings('settings.defaultSearch.');
        }

        return (array)$immoPluginHelper->parseTS($search);
    }

    /**
     * Searchs in the TS for a defined search and their data identified by $customKey such as settings.customSearchOptions.$customKey
     *
     * @param string $customKey
     * @param string $settingKey Path to options definition in settings array
     * @return null|array
     */
    public function retrieveSearchFromCustomConfiguration($customKey, $settingKey = 'settings.customSearchOptions.') {

        $immoPluginHelper = \TYPO3\MbxRealestate\Helper\ImmoPluginHelper::getInstance();

        $customSearchOptions = $immoPluginHelper->getPluginSettings($settingKey);

        if(!empty($customSearchOptions)) {

            $customSearchOptionsParsed = (array)$immoPluginHelper->parseTS($customSearchOptions);

            if(array_key_exists($customKey, $customSearchOptionsParsed)) {

                return $customSearchOptionsParsed[$customKey]['options'];
            }
        }

        return null;
    }

    /**
     * Returns the field configuration for a search
     * @return array
     */
    public function getPredefined() {

        $request = \TYPO3\CMS\Core\Utility\GeneralUtility::_GP('tx_mbxrealestate_pi1');

        if(!empty($request['search_realestate'])) {

            return $this->retrieveSearchFromSession();

        } elseif (!empty($request['searchagent_id'])) {

            return $this->objectManager->get('TYPO3\MbxRealestate\Controller\SearchagentController')->getQueryArrayBySearchagent($request['searchagent_id']);

        } elseif($this->storedSearchInSession()) {

            // get stored immo search request from session to pre-fill form
            return $this->retrieveSearchFromSession();

        } else {

            // read search request from default request configuration
            return $this->retrieveSearchFromConfiguration();
        }

    }

    /**
     * @param stdClass $d
     * @return array
     */
    private function objectToArray($d) {

        if (is_object($d)) {
            // Gets the properties of the given object
            // with get_object_vars function
            $d = get_object_vars($d);
        }

        if (is_array($d)) {
            /*
             * Return array converted to object
             * Using __FUNCTION__ (Magic constant)
             * for recursive call
             */
            return array_map(array($this, __FUNCTION__), $d);
        } else {
            // Return array
            return $d;
        }
    }

}
