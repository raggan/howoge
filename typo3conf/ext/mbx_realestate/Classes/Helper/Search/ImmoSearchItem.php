<?php

namespace TYPO3\MbxRealestate\Helper\Search;

class ImmoSearchItem {

    /**
     * Contains the search arguments passed by FORM or a SEARCH AGENT ITEM
     * @var array
     */
    private $args = null;

    /**
     * @var array
     */
    private $typesOfUse = array();

    /**
     * @var array
     */
    private $typesOfDisposition = array();

    /**
     * @var array
     */
    private $typesEstate = array();

    /**
     * @var array
     */
    private $typesEstatePrecise = array();

    
    /**
     * @var int|null
     */
    private $requiredWbs = null;
    
    /**
     * @var int|null
     */
    private $areaFrom = null;

    /**
     * @var int|null
     */
    private $areaTo = null;

    /**
     * @var int|null
     */
    private $roomsFrom = null;

    /**
     * @var int|null
     */
    private $roomsTo = null;

    /**
     * @var int|null
     */
    private $costGrossFrom = null;

    /**
     * @var int|null
     */
    private $costGrossTo = null;

    /**
     * @var int|null
     */
    private $costNetFrom = null;

    /**
     * @var int|null
     */
    private $costNetTo = null;

    /**
     * @var int|null
     */
    private $costBuyFrom = null;

    /**
     * @var int|null
     */
    private $costBuyTo = null;

    /**
     * @var int|null
     */
    private $floorsFrom = null;

    /**
     * @var int|null
     */
    private $floorsTo = null;

    /**
     * @var string|null
     */
    private $floorsRange = null;

    /**
     * @var int|null
     */
    private $floorsMaxFrom = null;

    /**
     * @var int|null
     */
    private $floorsMaxTo = null;

    /**
     * @var string|null
     */
    private $floorsMaxRange = null;


    private $yearBuild = null;

    private $yearBuildFrom = null;
    private $yearBuildTo = null;

    /**
     * @var string|null
     */
    private $street = null;

    /**
     * @var string
     */
    private $city = null;
    
    /**
     * @var array
     */
    private $features = array();

    /**
     * @var array
     */
    private $environments = array();

    /**
     * @var array
     */
    private $districts = array();

    /**
     * Geolocation (e.g. on mobile website)
     *
     * @var array
     */
    private $geoLocation = array();

    /**
     * Distance radius for geolocation (e.g. on mobile website)
     *
     * @var string
     */
    private $geoDistance = null;

    /**
     * @var array
     */
    private $offers = array();

    /**
     * @var int
     */
    private $isInvestment = null;
    
    /**
     * 
     * @var array
     */
    private $keywords = null;
    
    /**
     * Contains a list of unique immoIds which has to be excluded from search results
     * @var array
     */
    private $excludes = array();

    /**
     * Defines how to order the resulting items (ASC|DESC)
     * @var string|null
     */
    private $order = null;
    
    /* 
     * Type of Building
     * @var string|null
     */
    private $typeBuilding = null;

    /* 
     * provision
     * @var boolean
     */
    private $provision = 2;

    /**
     * Defines which field is used to order the resulting items
     * @var string|null
     */
    private $orderBy = null;

    /**
     * Can be set to a TS configured sort of typing TS:settings.search.listSort
     * @var string|null
     */
    private $sort = null;

    /**
     * @var string|null
     */
    private $thermalHeatingMethods = null;

    /**
     * Defines how to limit the resulting items
     * @var int|null|string
     */
    private $limit = null;

    /**
     * Stores the per page display variable
     * @var int|null
     */
    private $perPage = null;
    
    /**
     * Represents the storagePid to search in
     * @var int|null
     */
    private $storage = null;
    
    public function __construct() { }


    /**
     * Sets the request arguments or the arguments restored from a search agent item
     * @param array $args
     * @return \TYPO3\MbxRealestate\Helper\Search\ImmoSearchItem
     */
    public function setArguments(array $args) {

        $this->args = $args;
        return $this;
    }

    /**
     *
     * @param string $arg the argument name that is aspected to be in the available $this->args array list
     * @param string|null $setterFunction a function to set the value
     * @return boolean|null
     */
    public function passArgumentToSearch($arg, $setterFunction = null) {

        if(!is_null($value = $this->getArgumentFromSearch($arg))) {

            // use configured setterFunction to pass value internally
            if(!is_null($setterFunction)) {
                $this->$setterFunction($value);
            } else {

                $camelCasedArg = str_replace(' ', '', ucwords(str_replace('_', ' ', $arg)));
                $setterFunction = 'set' . ucfirst($camelCasedArg);

                if(method_exists($this, $setterFunction)) {
                    // use auto-discovered function to set value
                    $this->$setterFunction($value);
                } else {
                    // fall back and set value as variable to class
                    $this->{lcfirst($camelCasedArg)} = $value;
                }
            }

            return true;
        }  else {
            return null;
        }
    }

    /**
     * Retrieves the search value for $arg or NULL if not set by search
     * @param string $arg the argument that should be in the search arguments
     * @return null|string
     */
    public function getArgumentFromSearch($arg) {

        if(array_key_exists($arg, $this->args)) {
            return $this->args[$arg];
        } else {
            return null;
        }
    }

    /**
     *
     * @param int|null $areaFrom
     * @return \TYPO3\MbxRealestate\Helper\Search\ImmoSearchItem
     */
    public function setAreaFrom($areaFrom) {
        $this->areaFrom = $areaFrom;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getAreaFrom() {
        return $this->areaFrom;
    }

    /**
     *
     * @param int|null $areaTo
     * @return \TYPO3\MbxRealestate\Helper\Search\ImmoSearchItem
     */
    public function setAreaTo($areaTo) {
        $this->areaTo = $areaTo;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getAreaTo() {
        return $this->areaTo;
    }


    /**
     * Checks whether "from area" or "to area" was requested
     * @return boolean
     */
    public function hasAreaSet() {

        $areaFrom = $this->getAreaFrom();
        $areaTo = $this->getAreaTo();

        return (!empty($areaFrom) || !empty($areaTo));
    }

    /**
     * 
     * @return int
     */
    public function getIsInvestment() {
        return $this->isInvestment;
    }

    /**
     * 
     * @param int $isInvestment
     * @return \TYPO3\MbxRealestate\Helper\Search\ImmoSearchItem
     */
    public function setIsInvestment($isInvestment) {
                
        if(is_numeric($isInvestment)) {
            $this->isInvestment = $isInvestment;
        } else {
            $this->isInvestment = null;
        }
        
        return $this;
    }

    /**
     * 
     * @return boolean
     */
    public function hasIsInvestmentSet() {
        
        $val = $this->getIsInvestment();
        
        return !empty($val);
    }
        
    /**
     * @return int|null
     */
    public function getRoomsFrom() {
        return $this->roomsFrom;
    }

    /**
     * @return int|null
     */
    public function getRoomsTo() {
        return $this->roomsTo;
    }

    /**
     * Checks whether "from rooms" or "to rooms" was requested
     * @return boolean
     */
    public function hasRoomsSet() {

        $roomsFrom = $this->getRoomsFrom();
        $roomsTo = $this->getRoomsTo();

        return (!empty($roomsFrom) || !empty($roomsTo));
    }

    /**
     * @return int|null
     */
    public function getCostGrossFrom() {
        return $this->costGrossFrom;
    }

    /**
     * @return int|null
     */
    public function getCostGrossTo() {
        return $this->costGrossTo;
    }

    /**
     * Checks whether "from cost gross" or "to cost gross" was requested ('Warmmiete')
     * @return boolean
     */
    public function hasCostGrossSet() {

        $costGrossFrom = $this->getCostGrossFrom();
        $costGrossTo = $this->getCostGrossTo();

        return (!empty($costGrossFrom) || !empty($costGrossTo));
    }

    /**
     * @return int|null
     */
    public function getCostNetFrom() {
        return $this->costNetFrom;
    }

    /**
     * @return int|null
     */
    public function getCostNetTo() {
        return $this->costNetTo;
    }

    /**
     * Checks whether "from cost net" or "to cost net" was requested ('Kaltmiete')
     * @return boolean
     */
    public function hasCostNetSet() {

        $costNetFrom = $this->getCostNetFrom();
        $costNetTo = $this->getCostNetTo();

        return (!empty($costNetFrom) || !empty($costNetTo));
    }

    /**
     * @return int|null
     */
    public function getCostBuyFrom() {
        return $this->costBuyFrom;
    }

    /**
     * @return int|null
     */
    public function getCostBuyTo() {
        return $this->costBuyTo;
    }

    /**
     * Checks whether "from cost Buy" or "to cost Buy" was requested ('Kaufpreis')
     * @return boolean
     */
    public function hasCostBuySet() {

        $costBuyFrom = $this->getCostBuyFrom();
        $costBuyTo = $this->getCostBuyTo();

        return (!empty($costBuyFrom) || !empty($costBuyTo));
    }

    /**
     * @return int|null
     */
    public function getFloorsFrom() {
        return preg_match('/\d+/', $this->floorsFrom) ? (int)$this->floorsFrom : null;
    }

    /**
     * @return int|null
     */
    public function getFloorsTo() {
        return preg_match('/\d+/', $this->floorsTo) ? (int)$this->floorsTo : null;
    }

    /**
     * Checks whether "from floor" or "to floor" was requested ('Etage')
     * @return boolean
     */
    public function hasFloorsSet() {

        $floorsFrom = $this->getFloorsFrom();
        $floorsTo = $this->getFloorsTo();

        return (is_numeric($floorsFrom) || is_numeric($floorsTo));
    }

    /**
     *
     * @param string $range from-to e.g. 5-21
     * @return \TYPO3\MbxRealestate\Helper\Search\ImmoSearchItem
     */
    public function setFloorsRange($range) {

        list($from, $to) = explode('-', $range);

        if(preg_match('/^\d+\-\d+$/', $range)) {

            $this->floorsFrom = $from;
            $this->floorsTo = $to;

        } elseif (preg_match('/^\-\d+$/', $range)) {

            $this->floorsTo = $to;

        } elseif (preg_match('/^\d+\-$/', $range)) {

            $this->floorsFrom = $from;
        }

        return $this;
    }

    /**
     * @return int|null
     */
    public function getFloorsMaxFrom() {
        return preg_match('/\d+/', $this->floorsMaxFrom) ? (int)$this->floorsMaxFrom : null;
    }

    /**
     * @return int|null
     */
    public function getFloorsMaxTo() {
        return preg_match('/\d+/', $this->floorsMaxTo) ? (int)$this->floorsMaxTo : null;
    }

    /**
     * Checks whether "from max floor" or "to max floor" was requested ('Etagenanzahl Gebäude')
     * @return boolean
     */
    public function hasFloorsMaxSet() {

        $floorsMaxFrom = $this->getFloorsMaxFrom();
        $floorsMaxTo = $this->getFloorsMaxTo();

        return (is_numeric($floorsMaxFrom) || is_numeric($floorsMaxTo));
    }

    /**
     *
     * @param string $range from-to e.g. 5-21
     * @return \TYPO3\MbxRealestate\Helper\Search\ImmoSearchItem
     */
    public function setFloorsMaxRange($range) {

        list($from, $to) = explode('-', $range);

        if(preg_match('/^\d+\-\d+$/', $range)) {

            $this->floorsMaxFrom = $from;
            $this->floorsMaxTo = $to;

        } elseif (preg_match('/^\-\d+$/', $range)) {

            $this->floorsMaxTo = $to;

        } elseif (preg_match('/^\d+\-$/', $range)) {

            $this->floorsMaxFrom = $from;
        }

        return $this;
    }

    /**
     * @return int|null
     */
    public function getYearBuild() {
        return preg_match('/\d+/', $this->yearBuild) ? (int)$this->yearBuild : null;
    }

    /**
     * @param string|int $year
     * @return \TYPO3\MbxRealestate\Helper\Search\ImmoSearchItem
     */
    public function setYearBuildFrom($year) {
        
        if(preg_match('/\d+/', $year)) {
            $this->yearBuildFrom = $year;
        } else {
            $this->yearBuildFrom = null;
        }
        
        return $this;
    }
    
    /**
     * @return int|null
     */
    public function getYearBuildFrom() {
        return preg_match('/\d+/', $this->yearBuildFrom) ? (int)$this->yearBuildFrom : null;
    }

    /**
     * @param string|int $year
     * @return \TYPO3\MbxRealestate\Helper\Search\ImmoSearchItem
     */
    public function setYearBuildTo($year) {
        
        if(preg_match('/\d+/', $year)) {
            $this->yearBuildTo = $year;
        } else {
            $this->yearBuildTo = null;
        }
        
        return $this;
    }
    
    /**
     * @return int|null
     */
    public function getYearBuildTo() {
        return preg_match('/\d+/', $this->yearBuildTo) ? (int)$this->yearBuildTo : null;
    }

    /**
     * @return boolean
     */
    public function hasYearBuildRangeSet() {

        $yearBuildFrom = $this->getYearBuildFrom();
        $yearBuildTo = $this->getYearBuildTo();

        return (is_numeric($yearBuildFrom) || is_numeric($yearBuildTo));
    }

    /**
     * @return boolean
     */
    public function hasYearBuildSet() {

        $yearBuild = $this->getYearBuild();

        return (is_numeric($yearBuild));
    }

    /**
     *
     * @param string $range from-to e.g. 5-21
     * @return \TYPO3\MbxRealestate\Helper\Search\ImmoSearchItem
     */
    public function setYearBuildRange($range) {

        list($from, $to) = explode('-', $range);

        if(preg_match('/^\d+\-\d+$/', $range)) {

            $this->yearBuildFrom = $from;
            $this->yearBuildTo = $to;

        } elseif (preg_match('/^\-\d+$/', $range)) {

            $this->yearBuildTo = $to;

        } elseif (preg_match('/^\d+\-$/', $range)) {

            $this->yearBuildFrom = $from;
        }

        return $this;
    }

    /**
     * Returns the street which was searched for
     * @return string|false
     */
    public function getStreet() {
        return !empty($this->street) ? $this->street : false;
    }
   
    /**
     * sets the street which was searched for
     * @param string $street
     * @return \TYPO3\MbxRealestate\Helper\Search\ImmoSearchItem
     */
    public function setStreet($street) {
        $this->street = $street;
        return $this;
    }

    /**
     * Returns the city which was searched for
     * @return string|false
     */
    public function getCity() {
        return !empty($this->city) ? $this->city : false;
    }
    
    /**
     * sets the city which was searched for
     * @param integer $city
     * @return \TYPO3\MbxRealestate\Helper\Search\ImmoSearchItem
     */
    public function setCity($city) {
        $this->city = $city;
        return $this;
    }
    
    /**
     * Returns type building
     * @return string|false
     */
    public function getTypeBuilding() {
        return $this->typeBuilding;
    }

    /**
     * set type building
     * @return string|false
     */
    public function setTypeBuilding($typeBuilding) {
        $this->typeBuilding = $typeBuilding;
    }
    
    /**
     * @return boolean
     */
    public function hasTypeBuildingSet() {

        $typeBuilding = $this->getTypeBuilding();

        return !empty($typeBuilding);
    }
    
    /**
     *
     * @param mixed $features
     * @return \TYPO3\MbxRealestate\Helper\Search\ImmoSearchItem
     */
    public function setFeatures($features) {
        
        if(!empty($features)) {
            $this->features = $features;
        }  else {
            $this->features = array();
        }

        return $this;
    }

    /**
     * Containing the features set in search (...if they were set)
     * @return array
     */
    public function getFeatures() {
        return (array)$this->features;
    }

    /**
     * Checks if any features were searched.
     * @return boolean
     */
    public function hasFeaturesSet() {

        return count($this->getFeatures()) > 0;
    }

    /**
     *
     * @param mixed $environments
     * @return \TYPO3\MbxRealestate\Helper\Search\ImmoSearchItem
     */
    public function setEnvironments($environments) {

        if(!empty($environments)) {
            $this->environments = $environments;
        }  else {
            $this->environments = array();
        }

        return $this;
    }

    /**
     * Containing the environments set in search (...if they were set)
     * @return array
     */
    public function getEnvironments() {
        return (array)$this->environments;
    }

    /**
     * Checks if any environments were searched.
     * @return boolean
     */
    public function hasEnvironmentsSet() {

        return count($this->getEnvironments()) > 0;
    }

    /**
     *
     * @param mixed $typesOfUse
     * @return \TYPO3\MbxRealestate\Helper\Search\ImmoSearchItem
     */
    public function setTypesOfUse($typesOfUse) {

        if(!empty($typesOfUse)) {
            $this->typesOfUse = (array)$typesOfUse;
        }  else {
            $this->typesOfUse = array();
        }

        return $this;
    }

    /**
     * @return array|null
     */
    public function getTypesOfUse() {
        return (!empty($this->typesOfUse) ? (array)$this->typesOfUse : null);
    }

    /**
     * Checks whether for a custom type of use was searched
     * @return boolean
     */
    public function hasTypesOfUseSet() {

        $typesOfUse = $this->getTypesOfUse();

        return !empty($typesOfUse);
    }

    /**
     *
     * @param mixed $typesOfDisposition
     * @return \TYPO3\MbxRealestate\Helper\Search\ImmoSearchItem
     */
    public function setTypesOfDisposition($typesOfDisposition) {

        if(!empty($typesOfDisposition)) {
            $this->typesOfDisposition = (array)$typesOfDisposition;
        }  else {
            $this->typesOfDisposition = array();
        }

        return $this;
    }

    /**
     * @return array|null
     */
    public function getTypesOfDisposition() {
        return (!empty($this->typesOfDisposition) ? (array)$this->typesOfDisposition : null);
    }

    /**
     * Checks whether for a custom type of disposition was searched
     * @return boolean
     */
    public function hasTypesOfDispositionSet() {

        $typesOfDisposition = $this->getTypesOfDisposition();

        return !empty($typesOfDisposition);
    }
    
    /**
     * 
     * @param mixed $typesEstate
     * @return \TYPO3\MbxRealestate\Helper\Search\ImmoSearchItem
     */
    public function setTypesEstate($typesEstate) {
        
        if(!empty($typesEstate)) {
            $this->typesEstate = (array)$typesEstate;
        } else {
            $this->typesEstate = array();
        }
            
        return $this;
    }
    
    /**
     * @return array|null
     */
    public function getTypesEstate() {
        return (!empty($this->typesEstate) ? (array)$this->typesEstate : null);
    }

    /**
     * Checks whether for a custom types of estate was searched
     * @return boolean
     */
    public function hasTypesEstateSet() {
        
        $data = $this->getTypesEstate();
        
        return (!empty($data));
    }
    
    /**
     * 
     * @param mixed $typesEstatePrecise
     * @return \TYPO3\MbxRealestate\Helper\Search\ImmoSearchItem
     */
    public function setTypesEstatePrecise($typesEstatePrecise) {

        if(!empty($typesEstatePrecise)) {
            $this->typesEstatePrecise = (array)$typesEstatePrecise;
        } else {
            $this->typesEstatePrecise = array();
        }
            
        return $this;
    }
    
    /**
     * @return array|null
     */
    public function getTypesEstatePrecise() {
        return (!empty($this->typesEstatePrecise) ? (array)$this->typesEstatePrecise : null);
    }

    /**
     * Checks whether for a custom types of estatePrecise was searched
     * @return boolean
     */
    public function hasTypesEstatePreciseSet() {
        
        $data = $this->getTypesEstatePrecise();
        
        return (!empty($data));
    }
    
    /**
     * 
     * @param array|string $keywords Array or an comma seperated list
     * @return \TYPO3\MbxRealestate\Helper\Search\ImmoSearchItem
     */
    public function setKeywords($keywords) {

        if(empty($keywords)) {
            $this->keywords = array();
        } else {

            if(is_array($keywords)) {
                $this->keywords = $keywords;
            } else {
                $this->keywords = preg_split('/(\,\s+|\,)/', trim($keywords));
            }
        }
        
        return $this;
    }

    /**
     * 
     * @return array|null
     */
    public function getKeywords() {
        return (!empty($this->keywords) ? (array)$this->keywords : null);
    }
        
    /**
     * Checks whether for keywords was searched
     * @return boolean
     */
    public function hasKeywordsSet() {
        
        $data = $this->getKeywords();
        
        return (!empty($data));
    }
    
    /**
     *
     * @param mixed $districts
     * @return \TYPO3\MbxRealestate\Helper\Search\ImmoSearchItem
     */
    public function setDistricts($districts) {

        if(!empty($districts)) {
            
            if(!is_array($districts)) {
                $districts = (array)$districts;
            }
            
            foreach ($districts as $key => $value) {
                if (empty($value)) {
                    unset($districts[$key]);
                }
            }
            $this->districts = (array)$districts;
        }  else {
            $this->districts = array();
        }

        return $this;
    }

    /**
     * Containing the districts set in search (...if they were set)
     * @return array
     */
    public function getDistricts() {
        return (array)$this->districts;
    }

    /**
     * Checks if any districts were searched.
     * @return boolean
     */
    public function hasDistrictsSet() {

        return count($this->getDistricts()) > 0;
    }

    /**
     *
     * @param string $geoLocation
     * @return \TYPO3\MbxRealestate\Helper\Search\ImmoSearchItem
     */
    public function setGeoLocation($geoLocation) {

        if(!empty($geoLocation)) {
            $this->geoLocation = explode(',', $geoLocation);
        }  else {
            $this->geoLocation = array();
        }

        return $this;
    }

    /**
     *
     * @return array
     */
    public function getGeoLocation() {
        return (array)$this->geoLocation;
    }

    /**
     *
     * @param string $geoDistance
     * @return \TYPO3\MbxRealestate\Helper\Search\ImmoSearchItem
     */
    public function setGeoDistance($geoDistance) {
        $this->geoDistance = (int)$geoDistance;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getGeoDistance() {
        return (int)$this->geoDistance;
    }

    public function hasGeoParams() {
        $geoLocation = $this->getGeoLocation();
        $geoDistance = $this->getGeoDistance();
        return (count($geoLocation) == 2 && $geoDistance > 0) ? true : false;
    }

    /**
     *
     * @param integer $requiredWbs
     * @return \TYPO3\MbxRealestate\Helper\Search\ImmoSearchItem
     */
    public function setRequiredWbs($requiredWbs) {
        $this->requiredWbs = (int)$requiredWbs;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getRequiredWbs() {
        return (int)$this->requiredWbs;
    }
    
    /**
     *
     * @param integer $provision
     * @return \TYPO3\MbxRealestate\Helper\Search\ImmoSearchItem
     */
    public function setProvision($provision) {
        $this->provision = (int)$provision;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getProvision() {
        return (int)$this->provision;
    }
    
    /**
     *
     * @param mixed $offers
     * @return \TYPO3\MbxRealestate\Helper\Search\ImmoSearchItem
     */
    public function setOffers($offers) {

        if(!empty($offers)) {
            $this->offers = $offers;
        }  else {
            $this->offers = array();
        }

        return $this;
    }

    /**
     * Containing the offers set in search (...if they were set)
     * @return array
     */
    public function getOffers() {
        return (array)$this->offers;
    }

    /**
     * Checks if any offers were searched.
     * @return boolean
     */
    public function hasOffersSet() {

        return count($this->getOffers()) > 0;
    }

    /**
     *
     * @param mixed $excludes
     * @return \TYPO3\MbxRealestate\Helper\Search\ImmoSearchItem
     */
    public function setExcludes($excludes) {

        $excludes = array_filter( $excludes, 'strlen' );

        if(!empty($excludes)) {
            $this->excludes = $excludes;
        }  else {
            $this->excludes = array();
        }

        return $this;
    }

    /**
     * Containing the immoobject to exclude from in search (...if they were set)
     * @return array
     */
    public function getExcludes() {
        return (array)$this->excludes;
    }

    /**
     * Checks if any excludes were defined.
     * @return boolean
     */
    public function hasExcludesSet() {

        return count($this->getExcludes()) > 0;
    }

    /**
     * Sets the order.
     * @param string $order (a,d,ASC,DESC)
     * @return \TYPO3\MbxRealestate\Helper\Search\ImmoSearchItem
     */
    public function setOrder($order) {

        if(strlen($order) === 1 && in_array($order = strtolower($order), array('a', 'd'))) {
            if($order == 'a') {
                $this->order = 'ASC';
            } else {
                $this->order = 'DESC';
            }
        } elseif($order == 'ASC') {
            $this->order = 'ASC';
        } elseif($order == 'DESC') {
            $this->order = 'DESC';
        }

        return $this;
    }

    /**
     * @return string|null
     */
    public function getOrder() {
        return $this->order;
    }

    /**
     * Returns whether an ordering is set
     * @return boolean
     */
    public function hasOrderSet() {

        $order = $this->getOrder();
        return !empty($order);
    }

    /**
     * Sets the orderBy field. The value doesnt get validated! You have to check the value yourself
     * when using to query any information to prevent SQL-Injections!!
     *
     * @param string $orderBy
     * @return \TYPO3\MbxRealestate\Helper\Search\ImmoSearchItem
     */
    public function setOrderBy($orderBy) {

        $this->orderBy = $orderBy;
        return $this;
    }

    /**
     * Returns the orderBy field. The value doesnt get validated! You have to check the value yourself
     * when using to query any information to prevent SQL-Injections!!
     *
     * @return string|null
     */
    public function getOrderBy() {
        return $this->orderBy;
    }

    /**
     * Checks whether an orderBy is set.
     * @return boolean
     */
    public function hasOrderBySet() {

        $orderBy = $this->getOrderBy();
        return !empty($orderBy);
    }

    /**
     * Sets the limit for the search.
     * @param string $limit - A LIMIT statement in format \d+,\d+
     * @return \TYPO3\MbxRealestate\Helper\Search\ImmoSearchItem
     */
    public function setLimit($limit) {

        if(preg_match('/^\d+,(\s)?\d+$/', $limit) || preg_match('/^\d+$/', $limit)) {
            $this->limit = $limit;
        }

        return $this;
    }

    /**
     * @return string|int|null
     */
    public function getLimit() {
        $limit = $this->limit;
        
        if(preg_match('/^\d+$/', $limit)) {
            return (int)$limit;
        } else {
            return $limit;
        }
    }

    /**
     * Checks whether a LIMIT was set.
     * @return boolean
     */
    public function hasLimitSet() {
        $limit = $this->getLimit();
        return !empty($limit);
    }

    /**
     *
     * @param string $sort
     * @return \TYPO3\MbxRealestate\Helper\Search\ImmoSearchItem
     */
    public function setSort($sort) {

        $pluginHelper = \TYPO3\MbxRealestate\Helper\ImmoPluginHelper::getInstance();
        $sortTs = $pluginHelper->getPluginSettings('settings.search.listSort.');

        if(!empty($sortTs)) {

            $sortItems = $pluginHelper->parseTS($sortTs);
            if(array_key_exists($sort, $sortItems)) {

                $this->sort = $sortItems[$sort]['sort'];
            }  else {

                // no supported/valid $sort provided
                $this->sort = null;
            }

        }  else {

            // no sort settings configured in TS
            $this->sort = null;
        }

        return $this;
    }

    /**
     * @return string|null
     */
    public function getSort() {
        return $this->sort;
    }
    
    /**
     * 
     * @param mixed $thermalHeatingMethods
     * @return \TYPO3\MbxRealestate\Helper\Search\ImmoSearchItem
     */
    public function setThermalHeatingMethods($thermalHeatingMethods) {

        if(!empty($thermalHeatingMethods)) {
            $this->thermalHeatingMethods = (array)$thermalHeatingMethods;
        } else {
            $this->thermalHeatingMethods = array();
        }
            
        return $this;
    }
    
    /**
     * @return array|null
     */
    public function getThermalHeatingMethods() {
        return (!empty($this->thermalHeatingMethods) ? (array)$this->thermalHeatingMethods : null);
    }
    
    /**
     * Checks whether for a custom types of thermalHeatingMethod was searched
     * @return boolean
     */
    public function hasThermalHeatingMethodsSet() {
        
        $data = $this->getThermalHeatingMethods();
        
        return (!empty($data));
    }

    /**
     * Checks if a valid sort was defined
     * @return boolean
     */
    public function hasSortSet() {

        $sort = $this->getSort();

        return (!empty($sort));
    }

    /**
     * @return int|null
     */
    public function getStorage() {
        return $this->storage;
    }

    /**
     * 
     * @param int|null $storage
     * @return \TYPO3\MbxRealestate\Helper\Search\ImmoSearchItem
     */
    public function setStorage($storage) {
        $this->storage = $storage;
        return $this;
    }

    /**
     * 
     * @return boolean
     */
    public function hasStorageSet() {
        
        $storagePid = $this->storage;
        
        if(preg_match('/^\d+$/', $storagePid)) {
            return true;
        } else {
            return false;
        }
    }
}
