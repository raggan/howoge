<?php

class mbxCntCommunicator {

    CONST TASK_STATE_OK = '200';
    CONST TASK_STATE_ERROR = '300';
    
    CONST USER_AGENT = 'cntHandler';
    CONST CNT_URI_STRUCT = 'http://cnt.mindbox.de/backend/cnt/log/log/task/%s/secret/%s';

    /**
     * @var int
     */
    protected $state;

    /**
     * @var int
     */
    protected $taskId;

    /**
     * @var string
     */
    protected $taskSecret;

    /**
     * @var string
     */
    protected $message;

    /**
     * @var string
     */
    protected $latestRequestUrl;
    
    /**
     * 
     * @param int $taskId
     * @param string $taskSecret
     * @param int $state
     * @param string $message
     */
    function __construct($taskId = null, $taskSecret = null, $state = null, $message = null) {

        $this->setTaskId($taskId);
        $this->setTaskSecret($taskSecret);
        $this->setState($state);
        $this->setMessage($message);
    }

    /**
     * @return int
     */
    public function getTaskId() {

        return $this->taskId;
    }

    /**
     * @return string
     */
    public function getTaskSecret() {

        return $this->taskSecret;
    }

    /**
     * @return int
     */
    public function getState() {

        return $this->state;
    }

    /**
     * @return string
     */
    public function getMessage() {

        return $this->message;
    }

    /**
     * @param int $taskId
     * @return \mbxCntNotification
     */
    public function setTaskId($taskId) {

        $this->taskId = $taskId;
        return $this;
    }

    /**
     * @param string $taskSecret
     * @return \mbxCntNotification
     */
    public function setTaskSecret($taskSecret) {

        $this->taskSecret = $taskSecret;
        return $this;
    }

    /**
     * @param int $state
     * @return \mbxCntNotification
     */
    public function setState($state) {

        $this->state = $state;
        return $this;
    }

    /**
     * @param string $message
     * @return \mbxCntNotification
     */
    public function setMessage($message) {

        $this->message = $message;
        return $this;
    }

    /**
     * @return string
     */
    public function getLatestRequestUrl() {
        return $this->latestRequestUrl;
    }

    /**
     * @param string $latestRequestUrl
     * @return \mbxCntNotification
     */
    public function setLatestRequestUrl($latestRequestUrl) {
        $this->latestRequestUrl = $latestRequestUrl;
        return $this;
    }

    /**
     * @return string
     */    
    protected function getCntUri() {
        
        return sprintf(self::CNT_URI_STRUCT, $this->getTaskId(), $this->getTaskSecret());
    }
    
    /**
     * 
     * 
     * @return boolean|array
     */
    public function submit() {

        $url = $this->getCntUri();

        $this->setLatestRequestUrl($url);
        
        try {

            $postFields = array(
                'message'       =>  $this->getMessage(),
                'state'         =>  $this->getState()
            );
            
            if (function_exists('curl_version')) {

                $ch = curl_init();

                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_USERAGENT, self::USER_AGENT);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $postFields);                

                $cntReponseText = curl_exec($ch);
                curl_close($ch);

            } else {

                $data_url = http_build_query($postFields);
                
                $options = array(
                    'http' => array(
                        'header'  => 'User-Agent: ' . self::USER_AGENT,
                        'method'  => 'POST',
                        'content' => $data_url
                    )
                );

                $context = stream_context_create($options);

                $cntReponseText = file_get_contents($url, false, $context);
            }
            
            $this->evaluateCntResponse($cntReponseText);
            
            return true;

        } catch (Exception $ex) {

            return array(
                'status' => $ex->getCode(),
                'message' => $ex->getMessage()
            );
        }
    }

    /**
     * Checking if the request was successfully transported and stored in the Mindbox CNT
     * 
     * @param string $executeCall
     * @return boolean
     * @throws Exception
     */
    protected function evaluateCntResponse($executeCall) {

        $response = json_decode($executeCall);

        if (!json_last_error() === JSON_ERROR_NONE) {
            throw new Exception('Bad JSON Response', 400);
        }

        if(!empty($response->callback_status) || preg_match('/^\d+$/', $response->callback_status)) {
            
            if($response->callback_status === 200) {
                return true;
            }
            
            throw new Exception('Bad API Response: ' . $response->message, $response->callback_status);        
        }

        throw new Exception('Unfetched bad API Response', 500);        
    }

}
