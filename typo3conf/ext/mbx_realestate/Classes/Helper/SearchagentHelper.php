<?php

namespace TYPO3\MbxRealestate\Helper;

use TYPO3\MbxRealestate\Controller\ImmoobjectController;
use TYPO3\MbxRealestate\Domain\Repository\ImmofeatureRepository;
use TYPO3\MbxRealestate\Helper\Search\ImmoSearchHelper;

class SearchagentHelper {
    
    /**
     * @var \TYPO3\CMS\Extbase\Object\ObjectManager
     */
    private $objectManager = null;
    
    /**
     * @var SearchagentHelper
     */
    private static $instance = null;
    
    
    /**
     * @return SearchagentHelper
     */
    public static function getInstance() {
        
        if(is_null(self::$instance) || !(self::$instance instanceof SearchagentHelper)) {
            self::$instance = new SearchagentHelper();
        }
        
        return self::$instance;
    }
    
    public function __construct() {
        
        $this->objectManager = new \TYPO3\CMS\Extbase\Object\ObjectManager();
    }
    
    /**
     * returns view link for an searchagent entry
     * @param int $uid
     * @param string $email md5()-hashed email or email address
     * @return string return the view link
     */
    public function getViewLink($uid, $email) {
        
        $pageId = (int)ImmoPluginHelper::getInstance()->getPluginSettings('settings.searchAgent.targetPage');

        return \tx_pagepath_api::getPagePath($pageId, array(
            'tx_mbxrealestate_pi1' => array(
                'searchagent_id'            =>  $uid,
                'action'                    =>  'list',
                'controller'                =>  'Immoobject',
                'email'                     =>  filter_var($email, FILTER_VALIDATE_EMAIL) ? md5(strtolower($email)) : $email
            )
        ));
    }    
    
    /**
     * returns unsubscribe link for an searchagent entry
     * @param int $uid
     * @param string $email md5()-hashed email or email address
     * @return string return the unsubscribe link
     */
    public function getUnsubscribeLink($uid, $email) {
        
        $pageId = (int)ImmoPluginHelper::getInstance()->getPluginSettings('settings.searchAgent.targetPage');
        
        return \tx_pagepath_api::getPagePath($pageId, array(
            'tx_mbxrealestate_pi1' => array(
                'unregister_searchagent'    =>  1,
                'unregister_searchagent_id' =>  $uid,
                'action'                    =>  'list',
                'controller'                =>  'Immoobject',
                'email'                     =>  filter_var($email, FILTER_VALIDATE_EMAIL) ? md5(strtolower($email)) : $email
            )
        ));
        
    }    
    
    /**
     * 
     * @param string $mime - plain|html
     * @return null|string
     */
    public function getMailSubscribeData($mime) {

        $immoPluginHelper = ImmoPluginHelper::getInstance();
        
        $emailView = $this->objectManager->get('TYPO3\\CMS\\Fluid\\View\\StandaloneView');

        $templatePath = $immoPluginHelper->getPluginSettings('settings.searchAgent.subscribe.email.templateFilePath');
        $templateFile = $immoPluginHelper->getPluginSettings('settings.searchAgent.subscribe.email.templateFileNameSubscribeData' . ucfirst($mime));
        
        $file = \TYPO3\CMS\Core\Utility\GeneralUtility::getFileAbsFileName($templatePath) . $templateFile;
        
        if(file_exists($file)) {
            
            $immoobjectController = $this->objectManager->get(ImmoobjectController::class);
            $immofeatureRepository = $this->objectManager->get(ImmofeatureRepository::class);
            
            $featureList = $immofeatureRepository->getUniqueFeatures();
            
            $features = array();
            foreach($featureList as $featureItem) {
                $features[$featureItem->getFeatureType()] = $featureItem->getFeatureTitle();
            }

            $emailView->setTemplatePathAndFilename($file);
            $emailView->assignMultiple(array(
                'features'  => $features,
                'search'    => $search = ImmoSearchHelper::getInstance()->retrieveSearchFromSession(),
                'isi'       => $immoobjectController->getSearchAttributes($search)
            ));

            return $emailView->render();
        }

        return null;
        
    }
}