<?php

namespace TYPO3\MbxRealestate\Helper;

use TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface;
use TYPO3\MbxRealestate\Domain\Model\Immoobject;

class ImmoPluginHelper {

    CONST DISTRICT_MAPPING_TS = 'ts';       // district-mapping based on the TS configuration (it will check table-value tx_mbxrealestate_domain_model_immoaddress.district vs the requested IDs)
    CONST DISTRICT_MAPPING_ZIP = 'zip';     // district-mapping based on the postal-code

    /**
     * @var \TYPO3\CMS\Extbase\Object\ObjectManager
     */
    private $objectManager = null;

    /**
     * @var \TYPO3\MbxRealestate\Helper\ImmoPluginHelper
     */
    private static $instance = null;

    /**
     *
     * @var string|null
     */
    private $currentImmoObject = null;

    /**
     * @return \TYPO3\MbxRealestate\Helper\ImmoPluginHelper
     */
    public static function getInstance() {

        if(is_null(self::$instance) || !(self::$instance instanceof ImmoPluginHelper)) {
            self::$instance = new ImmoPluginHelper();
        }

        return self::$instance;
    }

    public function __construct() {

        $this->objectManager = new \TYPO3\CMS\Extbase\Object\ObjectManager();
    }

    /**
     * Returns the configured 'types of use' set in setup.txt [Path: plugin.tx_mbxrealestate.settings.search.field_types_of_use]
     * @return array
     */
    public function getConfiguredTypesOfUse() {

        return $this->getPluginSettings('settings.search.field_types_of_use.');
    }

    /**
     * Returns the configured 'types of disposition' set in setup.txt [Path: plugin.tx_mbxrealestate.settings.search.field_types_of_disposition]
     * @return array
     */
    public function getConfiguredTypesOfDisposition() {

        return $this->getPluginSettings('settings.search.field_types_of_disposition.');
    }

    /**
     * Returns the default selection value for the 'type of use'
     * @return string
     */
    public function getTypeOfUseFallback() {

        return $this->getPluginSettings('settings.search.type_of_use_default');
    }

    /**
     * Returns the default selection value for the 'type of disposition'
     * @return string
     */
    public function getTypeOfDispositionFallback() {

        return $this->getPluginSettings('settings.search.type_of_disposition_default');
    }

    /**
     * Returns an array where custom keys represents the districts which can be searched in the DB.
     * @return array
     */
    public function getDistrictsMapping() {

        return $this->getPluginSettings('settings.districts.');
    }

    /**
     * @return array
     */
    public function getPostalCodesMapping() {

        return $this->getPluginSettings('settings.districtZips.');
    }

    /**
     * Returns an array where custom keys represents the offers which can be searched in the DB.
     * @return array
     */
    public function getOffersMapping() {

        return $this->getPluginSettings('settings.offers.');
    }

    /**
     * Searches the postal-codes defined in TS by their districts and extracts them to an array.
     *
     * @param array $districts
     * @return array
     */
    public function getPostalCodesByDistricts($districts) {

        $postalCodes = array();
        $configuredPostalCodes = $this->getPostalCodesMapping();

        foreach($districts as $districtId) {

            $postalCodesForDistrict = '';

            if(array_key_exists($districtId, $configuredPostalCodes)) {
                $postalCodesForDistrict = $configuredPostalCodes[$districtId];
            } elseif(array_key_exists($districtId . '.', $configuredPostalCodes)) {
                $postalCodesForDistrict = implode(',', $configuredPostalCodes);
            }

            foreach(explode(',', $postalCodesForDistrict) as $tmpPostalCodeData) {

                if(strpos($tmpPostalCodeData, '-') === FALSE) {
                    $postalCodes[] = $tmpPostalCodeData;
                } else {
                    list($postalCodeFrom, $postalCodeTo) = explode('-', $tmpPostalCodeData);

                    if((int)$postalCodeFrom < (int)$postalCodeTo) {
                        for($i = (int)$postalCodeFrom; $i <= (int)$postalCodeTo; $i++) {
                            $postalCodes[] = str_pad($i, 5, '0', STR_PAD_LEFT);
                        }
                    }

                }
            }
        }

        return $postalCodes;
    }

    /**
     * Returns the in TS configured ID for the district by its string
     *
     * @param string $district
     * @return null|int
     */
    public function getDistrictIdByDistrictStr($district) {

        $districts = $this->getDistrictsMapping();

        foreach($districts as $districtId => $districtTitle) {

            if($districtTitle == $district) {
                return $districtId;
            }
        }

        return NULL;
    }

    /**
     *
     * @param array $settings
     * @return int
     */
    public function getCurrentStoragePidOfPlugin($settings) {

        $flexStorageName = $settings['storagePid'];

        if(!empty($flexStorageName)) {
            return $this->getStoragePidByName($flexStorageName);
        } else {
            return $this->getStoragePidByName(null);
        }
    }

    /**
     *
     * @param array $settings
     * @return string|null
     */
    public function getCurrentStorageNameOfPlugin($settings) {

        $flexStorageName = $settings['storagePid'];

        if(!empty($flexStorageName)) {
            return $flexStorageName;
        } else {
            return null;
        }
    }

    /**
     * Returns the storagePid for a configured name
     *
     * @param string|mixed $name
     * @return int
     */
    public function getStoragePidByName($name) {

        $storages = $this->getStorages();

        if(array_key_exists($name, $storages)) {
            return $storages[$name]['pid'];
        } else {
            return $this->getPluginSettings('persistence.storagePid');
        }
    }

    /**
     * @return array
     */
    public function getStorages() {

        $ret = array();
        $customStorages = $this->getPluginSettings('settings.storages.');

        if(empty($customStorages)) {

            $ret['default'] = array(
                'title'     =>  'Default',
                'pid'       =>  $this->getPluginSettings('persistence.storagePid')
            );

        } else {

            foreach(array_keys($customStorages) as $storageKey) {

                $storage =& $customStorages[$storageKey];

                $ret[str_replace('.', '', $storageKey)] = array(
                    'title' =>  $storage['title'],
                    'pid'   =>  $storage['pid']
                );
            }
        }

        return $ret;
    }

    /**
     * Replaces the uniqueId scheme defined in the TS with the data from the $immoobject.
     * The internal callback function build getters to retrieve the corresponding data from
     * the object.
     *
     * @param \TYPO3\MbxRealestate\Domain\Model\Immoobject $immoobject
     * @return string
     */
    public function getUniqueImmoIdByImmoobject(Immoobject $immoobject) {

        // edit in your TS configuration. Use %FIELDNAME% as palceholders to define the fieldnames to get
        // replaces with the DB data of the immoobject item
        $uniqueScheme = $this->getPluginSettings('settings.uniqeImmoIdScheme');

        return preg_replace_callback('/\%([a-zA-Z0-9\|]*)\%/Uis', function($match) use ($immoobject) {

            list($field, $length, $fill, $direction) = explode('|', $match[1]);

            $val = $immoobject->{'get' . ucfirst($field)}();

            if(!empty($length)) {
                return str_pad($val, $length, $fill, $direction);
            }  else {
                return $val;
            }

        }, $uniqueScheme);
    }

    /**
     * Creates sth like this from TS-configuration: CONCAT(LPAD(io.unr, 4, "0"),"/",LPAD(io.wi, 4, "0"))
     *
     * @param string $table
     * @return string
     */
    public function getUniqueImmoIdForSQL($table) {

        // edit in your TS configuration. Use %FIELDNAME% as palceholders to define the fieldnames to get
        // replaces with the DB data of the immoobject item
        $uniqueScheme = $this->getPluginSettings('settings.uniqeImmoIdSchemeSQL');

        $str = preg_replace_callback('/\%([a-zA-Z0-9\|]*)\%/Uis', function($match) use ($table) {

            list($field, $length, $fill, $direction) = explode('|', $match[1]);

            if(!empty($length)) {

                $func = ($direction == 0
                      ? 'LPAD(%s)'
                      : 'RPAD(%s)'
                    );

                $pad = sprintf('%s, %s, "%s"', $table . '.' . $field, (int)$length, $fill);
                return sprintf($func, $pad);

            }  else {

                return $table . '.' . $field;
            }

        }, $uniqueScheme);

        return sprintf('CONCAT(%s)', $str);
    }

    /**
     * @param \TYPO3\MbxRealestate\Domain\Model\Immoobject $immoobject
     */
    public function registerCurrentImmoobject(Immoobject $immoobject = null) {
        $this->currentImmoObject = $immoobject;
    }

    /**
     * @return boolean
     */
    public function hasCurrentImmoobject() {
        return (!empty($this->currentImmoObject) && $this->currentImmoObject instanceof Immoobject);
    }

    /**
     * @return \TYPO3\MbxRealestate\Domain\Model\Immoobject
     */
    public function getCurrentImmoobject() {
        return $this->currentImmoObject;
    }

    /**
     *
     * @param string|null $path
     * @return string|array|null
     */
    public function getPluginSettings($path = null) {

        $configurationManager = $this->objectManager->get('TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface');
        $configuration = $configurationManager->getConfiguration(ConfigurationManagerInterface::CONFIGURATION_TYPE_FULL_TYPOSCRIPT);
        $extensionConfiguration = $configuration['plugin.']['tx_mbxrealestate.'];

        if(empty($path)) {
            return $extensionConfiguration;
        } else {

            $path = explode('.', $path);

            while($pathItem = array_shift($path)) {

                // if no end of path reached we add the '.' to access next deeper configuration level
                if(count($path)) {
                    $pathItem .= '.';
                }

                if(is_array($extensionConfiguration) && array_key_exists($pathItem, $extensionConfiguration)) {

                    $extensionConfiguration = $extensionConfiguration[$pathItem];
                }  else {
                    $extensionConfiguration = NULL;
                }
            }

            return $extensionConfiguration;
        }
    }


    /**
     * Replaces all '.' in the TS configuration and returns a clean PHP array
     * @param array $ts
     * @return array|string
     */
    public function parseTS($ts) {

        return \TYPO3\CMS\Core\Utility\GeneralUtility::removeDotsFromTS($ts);
    }
    
    /**
     * Returns the list of displayable 
     * @return array
     */
    public function getVirtualFeatures() {
        
        $virtualSettings = $this->getPluginSettings('settings.search.featuresLimited.');
        
        if(empty($virtualSettings)) {
            return array();
        }
        
        $features = array();
        
        foreach($virtualSettings as $virtualSetting) {
            
            $featuresList = array();
            
            if(!is_array($virtualSetting)) {
                $key = $virtualSetting;
                $title = $virtualSetting;
                $feature = $virtualSetting;
            } else {
                
                $key = null;
                $title = $virtualSetting['title'];
                $feature = $title;
                
                if(array_key_exists('feature', $virtualSetting)) {
                    $key = $virtualSetting['feature'];
                    $feature = $key;
                }                
                
                if(array_key_exists('key', $virtualSetting)) {
                    $key = $virtualSetting['key'];
                }

                if(!empty($virtualSetting['features.'])) {
                    $featuresList = $virtualSetting['features.'];
                    if(empty($key) || !preg_match('/^VIRT_/', $virtualSetting['key'])) {
                        $key = 'VIRT_' . $virtualSetting['key'];
                    }
                }
            }
            
            $features[$key] = array(
                'featureType'   =>  $key,
                'featureTitle'  =>  $title,
                'feature'       =>  $feature,
                'features'      =>  $featuresList
            );
        }   
        
        return $features;
    }
}