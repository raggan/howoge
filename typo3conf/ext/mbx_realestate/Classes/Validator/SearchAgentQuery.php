<?php

namespace TYPO3\MbxRealestate\Validator;

use TYPO3\MbxRealestate\Helper\Search\ImmoSearchHelper;
use Typoheads\Formhandler\Validator\AbstractValidator;

class SearchAgentQuery extends AbstractValidator
{

	protected $restrictErrorChecks     = [];
	protected $disableErrorCheckFields = [];

	/**
	 * Method to set GET/POST for this class and load the configuration
	 *
	 * @param array The GET/POST values
	 * @param array The TypoScript configuration
	 * @return void
	 */
	public function init($gp, $tsConfig)
    {

    }

	/**
	 * Validates if a search is saved in the current session to store in DB
	 *
	 * @param array &$errors Reference to the errors array to store the errors occurred
	 * @return boolean
	 */
	public function validate(&$errors)
    {
        $immoSearchHelper = ImmoSearchHelper::getInstance();

        if (null === $immoSearchHelper->retrieveSearchFromSession()) {
            $errors[] = '...';
        }

        return empty($errors);
	}

}