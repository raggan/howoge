<?php

namespace TYPO3\MbxRealestate\Finisher;

use Typoheads\Formhandler\Finisher\Mail;
use TYPO3\MbxRealestate\Helper\SearchagentHelper;

class SearchagentSubscribe extends Mail
{

    /**
     * The main method called by the controller
     *
     * @return array The probably modified GET/POST parameters
     */
    public function process()
    {
        parent::process();
        
        $searchAgentHelper = SearchagentHelper::getInstance();
        $this->gp['searchagentJobView'] = $searchAgentHelper->getViewLink($this->gp['inserted_uid'], $this->gp['email']);
        $this->gp['searchagentJobViewUnsubscribe'] = $searchAgentHelper->getUnsubscribeLink($this->gp['inserted_uid'], $this->gp['email']);
        
        return $this->gp;
    }
    
    /**
     * Returns the final template code for given mode and suffix with substituted markers.
     *
     * @param string $mode user/admin
     * @param string $suffix plain/html
     * @return string The template code
     */
    protected function parseTemplate($mode, $suffix)
    {
        $searchAgentHelper = SearchagentHelper::getInstance();
        
        $rendered = str_replace([
            '[[searchRequestHml]]',
            '[[searchRequestPlain]]',
            '[[searchagentJobView]]',
            '[[searchagentJobViewUnsubscribe]]'
        ], [
            $searchAgentHelper->getMailSubscribeData('html'),
            $searchAgentHelper->getMailSubscribeData('plain'),
            $searchAgentHelper->getViewLink($this->gp['inserted_uid'], $this->gp['email']),
            $searchAgentHelper->getUnsubscribeLink($this->gp['inserted_uid'], $this->gp['email'])
        ], parent::parseTemplate($mode, $suffix));
        
        return $rendered;
    }

}
