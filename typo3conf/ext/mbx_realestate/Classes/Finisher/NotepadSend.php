<?php

namespace TYPO3\MbxRealestate\Finisher;

use TYPO3\MbxRealestate\Helper\NotepadHelper;
use Typoheads\Formhandler\Finisher\Mail;

/**
 * Class NotepadSend
 * @package TYPO3\MbxRealestate\Finisher
 */
class NotepadSend extends Mail
{

    /**
     * Returns the final template code for given mode and suffix with substituted markers.
     *
     * @param string $mode user/admin
     * @param string $suffix plain/html
     * @return string The template code
     */
    protected function parseTemplate($mode, $suffix) {

        $notepadHelper = NotepadHelper::getInstance();
        
        $rendered = str_replace(array(
            '[[notepadHtml]]',
            '[[notepadPlain]]'
        ), array(
            $notepadHelper->getMailSendData('html'),
            $notepadHelper->getMailSendData('plain')
        ), parent::parseTemplate($mode, $suffix));
        
        return $rendered;
    }

}
