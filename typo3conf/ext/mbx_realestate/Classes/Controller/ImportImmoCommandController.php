<?php

namespace TYPO3\MbxRealestate\Controller;

use TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface;
use TYPO3\MbxRealestate\Domain\Repository\ImmoaddressRepository;
use TYPO3\MbxRealestate\Domain\Repository\ImmocontactRepository;
use TYPO3\MbxRealestate\Domain\Repository\ImmofeatureRepository;
use TYPO3\MbxRealestate\Domain\Repository\ImmoimageRepository;
use TYPO3\MbxRealestate\Domain\Repository\ImmoobjectRepository;
use TYPO3\MbxRealestate\Helper\Import\ImmoDataItem;
use TYPO3\MbxRealestate\Helper\Import\ImporterImmo;
use TYPO3\MbxRealestate\Helper\Import\Interfaces\ImporterCommandControllerInterface;

/**
 * Description of ImportCommandController
 * 
 * Controller for Cronjobs using Sheduler Extension
 *
 * @author denis.krueger
 * @package mbx_realestate
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 */
class ImportImmoCommandController extends ImportAbstractCommandController implements ImporterCommandControllerInterface {

    /**
     * Imports real estate objects from file
     * 
     * @return boolean
     */
    public function importCommand() {
        
        try {
            
            $this->initCommandController($this);
            parent::importCommand();

            $configuration = $this->configurationManager->getConfiguration(ConfigurationManagerInterface::CONFIGURATION_TYPE_FULL_TYPOSCRIPT);
            $extensionConfiguration = $configuration['plugin.']['tx_mbxrealestate.'];
            
            $filetype = (string)$extensionConfiguration['settings.']['import.']['immo.']['inputType'];
            $importerClass = (string)$extensionConfiguration['settings.']['import.']['immo.']['importClass'];
            $importerMode = $this->immoPluginHelper->getPluginSettings('settings.import.immo.importType');
            $importerMaxLoop = (int)$this->immoPluginHelper->getPluginSettings('settings.import.immo.maxImportLoop');
            
            if(empty($importerClass)) {
                $importerClass = '\TYPO3\MbxRealestate\Helper\Import\ImporterImmo' . ucfirst(strtolower($filetype));
            }
            
            // <editor-fold defaultstate="collapsed" desc="validate defaults">
            if(!$this->isValidFileTypeConfigured($filetype)) {
            
                self::throwException('Unsupported filetype "' . $filetype . '" configured in ' . __CLASS__ . '::' . __FUNCTION__);
                
            } elseif(!($this->immoobjectRepository instanceof ImmoobjectRepository)) {
                
                self::throwException('$this->immoobjectRepository not of type \TYPO3\MbxRealestate\Domain\Repository\ImmoobjectRepository');
            
            } elseif(!($this->immoaddressRepository instanceof ImmoaddressRepository)) {
                
                self::throwException('$this->immoaddressRepository not of type \TYPO3\MbxRealestate\Domain\Repository\ImmoaddressRepository');
                
            } elseif(!($this->immocontactRepository instanceof ImmocontactRepository)) {
                
                self::throwException('$this->immocontactRepository not of type \TYPO3\MbxRealestate\Domain\Repository\ImmocontactRepository');
                
            } elseif(!($this->immofeatureRepository instanceof ImmofeatureRepository)) {
                
                self::throwException('$this->immofeatureRepository not of type \TYPO3\MbxRealestate\Domain\Repository\ImmofeatureRepository');
                
            } elseif(!($this->immoimageRepository instanceof ImmoimageRepository)) {

                self::throwException('$this->immoimageRepository not of type \TYPO3\MbxRealestate\Domain\Repository\ImmoimageRepository');
                
            } elseif(!class_exists($importerClass)) {
                
                self::throwException('Unable to load importer class "' . $importerClass . '"');
            }
            // </editor-fold>

            $this->log('Used Class for Import: ' . $importerClass);

            /*
             * - create new importer class
             * - uses importerAbstract for IDE to provide docu
             * - uses ImporterInterface to define class construction for future file extensions
             */
            
            $this->importer = new $importerClass();
            
            // assignment required for IDEs auto suggests
            $importer =& $this->importer;
            $importer->setCommandControllerReference($this);                        // set the reference to $this to provide logging etc. into importer
            $importer   ->setConfiguration($extensionConfiguration)                 // set the extension configuration
                        ->setScope('immo')
                        ->setImmoobjectRepository($this->immoobjectRepository)      // set the immoobject repository
                        ->setImmoaddressRepository($this->immoaddressRepository)    // set the immoaddress repository
                        ->setImmocontactRepository($this->immocontactRepository)    // set the immocontact repository
                        ->setImmoimageRepository($this->immoimageRepository)        // set the immoimage repository
                        ->setImmofeatureRepository($this->immofeatureRepository)    // set the immofeature repository
                    ;

            do {

                if(($openedFile = $importer->openFile('immo')) !== true) {

                    self::throwException('Failed to open import file: ' . (string)$openedFile);
                }  else {
                    $this->log('Load import file ' . $importer->getFile());
                }

                $this->log(sprintf('Import mode is "%s"', $importerMode));
                $this->log(count($importer->getItems()) . ' objects found');

                $saved = $dropped = $i = 0;
                $droppableItems = array();  
                
                // unbind the currently binded Saver to remove Database tables
                // before our new importer will create them again. This prevents 
                // an immediately removement of the tables right after their 
                // were created!
                $importer->resetImporterSaver();    
                $importer->setImporterSaver(new ImporterImmo($importer));

                // iterate each items and update them in the database
                while($immoItem = $importer->iterateItem()) {

                    $immoDataItem = $importer->prepareItem($immoItem);  // collect information from item

                    $immoID = $immoDataItem->getImmoobject()->getUnr() . '/'
                            . $immoDataItem->getImmoobject()->getWi() . '/'
                            . $immoDataItem->getImmoobject()->getHs() . '/'
                            . $immoDataItem->getImmoobject()->getMe();

                    $this->log('Handle object "' . $immoID . '"');
                    $this->log('Item mode: "' . $immoDataItem->getMode() . '"');

                    if($immoDataItem->getMode() == ImmoDataItem::MODE_SAVE) {

                        $importer->getImporterSaver()->saveObject($immoDataItem);        // prepare item data for inserting into temporary table

                    } elseif($importerMode === self::IMPORT_TYPE_SINGLE) {

                        $droppableItems[] = $immoDataItem;                                    
                    }


                    if(!($i++%100)) {

                        // insert current items into temporary table(s)
                        $importer->getImporterSaver()->sqlTempDataPush();
                    }

                }

                $importer->getImporterSaver()->sqlTempDataPush();           // insert current items into temporary table(s)            
                $importer->getImporterSaver()->sqlTempDataTransfer();       // transfer temporary items to target            
                $importer->getImporterSaver()->createNM();                  // link immoobjects to immofeatures

                if($importerMode === self::IMPORT_TYPE_FULL) {

                    $importer->getImporterSaver()->cleanTempImmoobjectFeaturesMM();         // remove all features from the live table which arent present in the temporary table
                    $importer->getImporterSaver()->cleanTempAreas();            // remove all areas from the live table which arent present in the temporary table
                    $importer->getImporterSaver()->cleanTempEnvironments();     // remove all features from the live table which arent present in the temporary table
                    $importer->getImporterSaver()->cleanTempImages();           // remove all features from the live table which arent present in the temporary table
                    $importer->getImporterSaver()->cleanTempImmoobjects();      // remove all immoobjects from the live table which arent present in the temporary table

                } else {

                    $this->log(sprintf('Delete following objects [count %s]...', count($droppableItems)));

                    $DB = $GLOBALS['TYPO3_DB'];

                    foreach($droppableItems as $droppableItem) {

                        $immoID = $droppableItem->getImmoobject()->getUnr() . '/'
                                . $droppableItem->getImmoobject()->getWi() . '/'
                                . $droppableItem->getImmoobject()->getHs() . '/'
                                . $droppableItem->getImmoobject()->getMe();

                        $this->log(sprintf('Drop item %s from database.', $immoID));

                        $sqlSearchImmoobject = sprintf('SELECT mbx_realestate_get_immoobject(%s, %s, %s, %s) as uid;', 
                            $droppableItem->getImmoobject()->getUnr(),
                            $droppableItem->getImmoobject()->getWi(),
                            $droppableItem->getImmoobject()->getHs(),
                            $droppableItem->getImmoobject()->getMe()
                        );

                        $res = $DB->sql_query($sqlSearchImmoobject);
                        $data = $DB->sql_fetch_assoc($res);

                        if(empty($data) || empty($data['uid'])) {

                            // Immoobject does not exists
                            $this->log('Remove not required because immoobject does not exists.');

                            continue;
                        }

                        // @TODO / INFO the stored procedure does not work within typo 
                        // because the mysql resource is blocked by the multi response 
                        // of the stored procedure resultset. So any following sql 
                        // statements will fail !
                        // 
    //                        $sqlRemoveImmoobject = sprintf('CALL mbx_realeaste_remove_immoobject(%s, %s, %s, %s);', 
    //                            $droppableItem->getImmoobject()->getUnr(),
    //                            $droppableItem->getImmoobject()->getWi(),
    //                            $droppableItem->getImmoobject()->getHs(),
    //                            $droppableItem->getImmoobject()->getMe()
    //                        );
    //
    //                        if(($res = $DB->sql_query($sqlRemoveImmoobject))) {
    //
    //                            $this->log(sprintf('Removed item %s and its relations from database (if exists).', $immoID));
    //                        } else {
    //
    //                            $this->log(sprintf('Failed to remove item %s and its relations from database! Query: %s', $immoID, $sqlRemoveImmoobject));
    //                        }

                        // WORKAROUND for stored procedure ... sadly :-/

                        $removeRelations = array(
                            'tx_mbxrealestate_immoobject_immofeature_mm->uid_local',
                            'tx_mbxrealestate_immoaddress_immocontact_mm->uid_local',
                            'tx_mbxrealestate_domain_model_immoarea->immoobject',
                            'tx_mbxrealestate_domain_model_immoenvironment->immoobject',
                            'tx_mbxrealestate_domain_model_immoimage->immoobject',
                            'tx_mbxrealestate_domain_model_immoobject->uid'
                        );

                        foreach($removeRelations as $removeRelation) {

                            $match = null;

                            if(!preg_match('/^(?<TABLE>.*)\-\>(?<COLUMN>.*)$/', $removeRelation, $match)) {
                                self::throwException('No valid format of "tablename->column" defined for removing data: ' . $removeRelation); 
                            }

                            $sqlRemoveStatement = sprintf('DELETE FROM %s WHERE %s = %s', 
                                    $match['TABLE'],
                                    $match['COLUMN'],
                                    $data['uid']
                            );

                            if(($updated = $DB->sql_query($sqlRemoveStatement)) === false) {

                                self::throwException('Failed to remove from database! ' . $sqlRemoveStatement);

                            }  else {

                                $this->log(sprintf('Removed item in %s (%s occurences)', $match['TABLE'], $DB->sql_affected_rows()));
                            }
                        }

                    }    

                }

                $importer->getImporterSaver()->generateForeignAll();


                $this->log('Backup import file');
                $importer->backupFile();
                $importer->closeFile();
                $importer->removeFile();
                
                $this->log($logMessage =  $saved . ' items were passed to save / ' . $dropped . ' items were passed to drop');                
            
                // give some time before next Loop will run (especially to prevent 
                // same backup filename if the script runs two imports per second)
                sleep(2);
                
            } while(--$importerMaxLoop > 0);
            
        } catch (\Exception $ex) {

            $this->log('Exception: ' . (string)$ex);
            $sent = $this->sendNotification((string)$ex, self::STATE_ERROR);
            
            $this->log('Sent notification: ' . var_export($sent, true));
            
            throw $ex;
        }

        $this->log('Current date: ' . date('Y-m-d H:i:s'));
        
        $this->sendNotification('Import of immo objects succeeded: ' . $logMessage);
        
        return true;
    }

}