<?php
namespace TYPO3\MbxRealestate\Controller;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2014 Denis Krüger <denis.krueger@mindbox.de>, Mindbox GmbH
 *  Anke Häslich <anke.haeslich@mindbox.de>, Mindbox GmbH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use TYPO3\MbxRealestate\Domain\Model\Immocontact;

/**
 * Controller to enable search in immmadresses and lisiting of contacts
 *
 * @package mbx_realestate
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 */
class ImmocontactController extends \TYPO3\MbxRealestate\Controller\AbstractController {

    /**
     * immocontactRepository
     *
     * @var \TYPO3\MbxRealestate\Domain\Repository\ImmocontactRepository
     * @inject
     */
    protected $immocontactRepository;

    /**
     * action list
     *
     * @return void
     */
    public function listAction() {
        $customConfiguration = $this->retrieveCustomConfiguration();
        $contactTypes = explode(',', $customConfiguration['contactTypes']);

        if(!empty($customConfiguration)) {

            $this->view->assign('customConfiguration', $customConfiguration);
        }

        if (!empty($contactTypes)) {
            $immocontacts = $this->immocontactRepository->findByTypes($contactTypes);

            $this->orderContactsByTypes($immocontacts, $contactTypes);
            $this->view->assign('contacts', $immocontacts);
        }
    }

    /**
     * Sort the immocontacts list by the contact-type according to the searched list in $contactTypes
     * 
     * @param array $immocontacts
     * @param array $contactTypes
     */
    private function orderContactsByTypes(array &$immocontacts, array $contactTypes) {

        if(empty($contactTypes) || count($contactTypes) < 2) {
            return;
        }

        $newContacts = array();

        while($contactType = array_shift($contactTypes)) {
            foreach($immocontacts as $_ => $immocontact) {

                if($immocontact->getContactType() !== $contactType) {
                    continue;
                }

                $newContacts[] = $immocontact;
                unset($immocontacts[$_]);
            }
        }

        $immocontacts = $newContacts;
    }

    /**
     * Show immocontact selected by setting
     *
     * @param \TYPO3\MbxRealestate\Domain\Model\Immocontact $immocontact
     */
    public function showAction(Immocontact $immocontact = null) {

		if (is_null($immocontact)) {
			$immocontactId = ((int)$this->settings['singleImmocontact'] > 0) ? $this->settings['singleImmocontact'] : 0;
			if ($immocontactId > 0) {
                $immocontact = $this->immocontactRepository->findByUid($immocontactId);
			}
		}

        $this->view->assign('contactItem', $immocontact);
    }

    /**
     * Checks the custom configuration for a search stored in $this->settings configured in an embedded Plugin element.
     * @return boolean|array|null
     */
    private function retrieveCustomConfiguration() {
        if(!empty($this->settings) && !empty($this->settings['customAddressSearch'])) {
            return $this->getImmoSearchHelper()->retrieveSearchFromCustomConfiguration($this->settings['customAddressSearch'], 'settings.customAddressSearchOptions.');
        }

        return false;
    }
}