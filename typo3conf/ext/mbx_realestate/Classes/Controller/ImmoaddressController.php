<?php
namespace TYPO3\MbxRealestate\Controller;

use TYPO3\MbxRealestate\Domain\Model\Immoaddress;


/**
 * Controller to enable search in immmadresses and lisiting of contacts
 *
 * @package mbx_realestate
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 */
class ImmoaddressController extends AbstractController {

    CONST SESSION_STORAGE_KEY = 'immosearch_street';

    /**
     * immoaddressRepository
     *
     * @var \TYPO3\MbxRealestate\Domain\Repository\ImmoaddressRepository
     * @inject
     */
    protected $immoaddressRepository;

    /**
     * action searchForm
     *
     * @return void
     */
    public function searchFormAction() {
        $selectedStreet = $submitted = false;
        $streetItem = array();

        // store street search request in session
        if($this->request->hasArgument('search_readdress')) {
            $selectedStreet = $this->request->getArgument('immoaddress');
            $this->getImmoSearchHelper()->storeSearchInSession($selectedStreet, self::SESSION_STORAGE_KEY);
            $submitted = true;

        } elseif ($this->getImmoSearchHelper()->storedSearchInSession(self::SESSION_STORAGE_KEY)) {
            $selectedStreet = $this->getImmoSearchHelper()->retrieveSearchFromSession(self::SESSION_STORAGE_KEY);
        }

        $customConfiguration = $this->retrieveCustomConfiguration();
        if($selectedStreet) {
            $immoaddresses = $this->immoaddressRepository->findBySearch($customConfiguration, $selectedStreet['street']);

            foreach ($immoaddresses as $streetObj) {
                $streetObj instanceof Immoaddress;
                $street = preg_replace('/ {2,}/', ' ', $streetObj->getStreet());

                if(strrpos($street, $selectedStreet['street'], -strlen($street)) !== false) {
                    $streetItem = $streetObj;
                    $selectedStreet['street'] = $street;
                    break;
                }
            }
        }

        # load all streets with raw sql to have a small foot print
        $rawImmoAddresses = $this->immoaddressRepository->findRawBySearch($customConfiguration);
        foreach ($rawImmoAddresses as $rawImmoAddress) {
            $street = preg_replace('/ {2,}/', ' ', $rawImmoAddress['street']);
            $streets[] = $street;
        }

        $this->view->assignMultiple(array(
            'streets' => $streets,
            'selectedStreet' => array_merge((array)$selectedStreet, array('submitted' => $submitted)),
            'streetItem' => $streetItem,
            'contactTypes' => !empty($customConfiguration['contactTypes']) ? explode(',', $customConfiguration['contactTypes']) : 'all'
        ));
    }

    public function old__searchFormAction() {
        $selectedStreet = $submitted = false;
        $streetItem = array();

        // store street search request in session
        if($this->request->hasArgument('search_readdress')) {
            $selectedStreet = $this->request->getArgument('immoaddress');
            $this->getImmoSearchHelper()->storeSearchInSession($selectedStreet, self::SESSION_STORAGE_KEY);
            $submitted = true;

        } elseif ($this->getImmoSearchHelper()->storedSearchInSession(self::SESSION_STORAGE_KEY)) {
            $selectedStreet = $this->getImmoSearchHelper()->retrieveSearchFromSession(self::SESSION_STORAGE_KEY);

        }
        $customConfiguration = $this->retrieveCustomConfiguration();

        $immoaddresses = $this->immoaddressRepository->findBySearch($customConfiguration);

        $streets = array();
        foreach ($immoaddresses as $streetObj) {

            $streetObj instanceof Immoaddress;
            $street = preg_replace('/ {2,}/', ' ', $streetObj->getStreet());

            $streets[] = $street;
            if ($street == $selectedStreet['street']) {
                $streetItem = $streetObj;
            }
        }


        $this->view->assignMultiple(array(
          'streets' => $streets,
          'selectedStreet' => array_merge((array)$selectedStreet, array('submitted' => $submitted)),
          'streetItem' => $streetItem,
          'contactTypes' => !empty($customConfiguration['contactTypes']) ? explode(',', $customConfiguration['contactTypes']) : 'all'
        ));
    }

    /**
     * Checks the custom configuration for a search stored in $this->settings configured in an embedded Plugin element.
     * @return boolean|array|null
     */
    private function retrieveCustomConfiguration() {
        if(!empty($this->settings) && !empty($this->settings['customAddressSearch'])) {
            return $this->getImmoSearchHelper()->retrieveSearchFromCustomConfiguration($this->settings['customAddressSearch'], 'settings.customAddressSearchOptions.');
        }

        return false;
    }
}
