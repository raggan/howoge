<?php

namespace TYPO3\MbxRealestate\Controller;

use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface;
use TYPO3\MbxRealestate\Domain\Repository\ImmoaddressRepository;
use TYPO3\MbxRealestate\Domain\Repository\ImmocontactRepository;
use TYPO3\MbxRealestate\Domain\Repository\ImmofeatureRepository;
use TYPO3\MbxRealestate\Domain\Repository\ImmoimageRepository;
use TYPO3\MbxRealestate\Domain\Repository\ImmoobjectRepository;
use TYPO3\MbxRealestate\Helper\Exception\ImportImmoException;
use TYPO3\MbxRealestate\Helper\ImmoPluginHelper;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;

/**
 * Description of ExportCommandController
 * 
 * Controller for Cronjobs using Sheduler Extension
 *
 * @author denis.krueger
 * @package mbx_realestate
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 */
class ExportAbstractCommandController extends \TYPO3\CMS\Extbase\Mvc\Controller\CommandController {

    CONST EXPORT_TYPE_XML = 'XML';
    CONST LOG_ROUTE = 80;
    
    CONST STATE_OK = 'OK';
    CONST STATE_ERROR = 'ERROR';
    CONST STATE_NOTIFICATION = 'NOTIFICATION';
    
    /**
     * @var \TYPO3\MbxRealestate\Helper\Export\Interfaces\ExporterInterface
     */
    protected $exporter;
    
    /**
     * Contains the child process name (used for logging etc.)
     * @var string
     */
    private $commandType;
    
    /**
     * The timestamp when started the import
     * @var int
     */
    protected $startTime;
    
    /**
     * The command controller that is extending this class. (e.g. ExportImmoCommandController)
     * @var \TYPO3\CMS\Extbase\Mvc\Controller\CommandController 
     */
    protected $childCommandController;
    
    /**
     * Contains the supported file types for importing
     * @var array
     */
    private static $supportedExportTypes = array(
        self::EXPORT_TYPE_XML
    );
       
    /**
     * @var \TYPO3\MbxRealestate\Helper\ImmoPluginHelper
     */
    public $immoPluginHelper;
    
    /**
     * configurationManager
     * @var \TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface
     */
    protected $configurationManager;

    /**
     * @var string
     */
    protected $notificationTsPath = null;

    /**
     * ImmoobjectRepository
     *
     * @var ImmoobjectRepository
     */
    protected $immoobjectRepository;

    /**
     * ImmofeatureRepository
     *
     * @var ImmofeatureRepository
     */
    protected $immofeatureRepository;

    /**
     * ImmoaddressRepository
     *
     * @var ImmoaddressRepository
     */
    protected $immoaddressRepository;

    /**
     * ImmocontactRepository
     *
     * @var ImmocontactRepository
     */
    protected $immocontactRepository;
    
    /**
     * ImmoimageRepository
     *
     * @var ImmoimageRepository
     */
    protected $immoimageRepository;
    
    /**
     * injectConfigurationManager
     * 
     * @param ConfigurationManagerInterface $configurationManager
     */
    public function injectConfigurationManager(ConfigurationManagerInterface $configurationManager) {
        $this->configurationManager = $configurationManager;
    }
    
    /**
     * injectImmoobjectRepository
     *
     * @param ImmoobjectRepository $immoobjectRepository
     */
    public function injectImmoobjectRepository(ImmoobjectRepository $immoobjectRepository) {
        $this->immoobjectRepository = $immoobjectRepository;
    }

    /**
     * injectImmoaddressRepository
     *
     * @param ImmoaddressRepository $immoaddressRepository
     */
    public function injectImmoaddressRepository(ImmoaddressRepository $immoaddressRepository) {
        $this->immoaddressRepository = $immoaddressRepository;
    }
    
    /**
     * injectImmocontactRepository
     *
     * @param ImmocontactRepository $immocontactRepository
     */
    public function injectImmocontactRepository(ImmocontactRepository $immocontactRepository) {
        $this->immocontactRepository = $immocontactRepository;
    }
    
    /**
     * injectImmofeatureRepository
     *
     * @param ImmofeatureRepository $immofeatureRepository
     */
    public function injectImmofeatureRepository(ImmofeatureRepository $immofeatureRepository) {
        $this->immofeatureRepository = $immofeatureRepository;
    }
    
    /**
     * injectImmoimageRepository
     *
     * @param ImmoimageRepository $immoimageRepository
     */
    public function injectImmoimageRepository(ImmoimageRepository $immoimageRepository) {
        $this->immoimageRepository = $immoimageRepository;
    }
    
    /**
     * initialize the command controller and starts logging
     * 
     * @param class $childClass
     * @return ExportAbstractCommandController
     */
    protected function initCommandController($childClass) {
        
        $className = str_replace('CommandController', '', get_class($childClass));
        $namespacePath = explode('\\', $className);
        
        $this->childCommandController = $childClass;
        $this->commandType = array_pop($namespacePath);
        $this->startTime = time();
        
        $this->immoPluginHelper = ImmoPluginHelper::getInstance();
        
        $this->initLog();
        $this->log('Current date: ' . date('Y-m-d H:i:s', $this->startTime));
        
        return $this;
    }
    
    /**
     * 
     * @return boolean
     */
    public function exportCommand() {
        
    }

    /**
     * @param string $msg
     * @throws ImportImmoException
     */
    public static function throwException($msg) {
        
        throw new ImportImmoException($msg);
    }
    
    /**
     * This method checks if the configured filetype is valid for parsing
     * 
     * @param string $filetype
     * @return boolean
     */
    protected function isValidFileTypeConfigured($filetype) {
        
        return in_array(strtoupper($filetype), self::$supportedExportTypes);
    }


    /**
     * Opens the logging file
     */
    protected function initLog() {
        
        $file = $this->immoPluginHelper->getPluginSettings($path = 'settings.export.log.' . lcfirst($this->commandType) . '.logfile');
        $this->fHandle = fopen(GeneralUtility::getFileAbsFileName($file), 'a+');

        $this->log($this->getPaddedString());
        $this->log($this->getPaddedString(' START ' . $this->commandType . ' COMMANDER '));
        $this->log($this->getPaddedString());
    }
    
    /**
     * Closing the log file
     */
    protected function closeLog() {
        
        $this->log($this->getPaddedString());
        $this->log($this->getPaddedString(' END ' . $this->commandType . ' COMMANDER '));
        $this->log($this->getPaddedString());
        
        fclose($this->fHandle);
    }
    
    /**
     * @param string $str
     * @return string
     */
    protected function getPaddedString($str = '') {
        
        return str_pad($str, self::LOG_ROUTE, '#', 2);
    }
    
    /**
     * @param string $message
     */
    public function log($message) {
        
        fwrite($this->fHandle, $message . "\n");
    }
    
    /**
     * @return string|null
     */
    protected function getNotificationTsPath() {
        return $this->notificationTsPath;
    }

    /**
     * Returns the command specific TS notification path.
     * @return boolean|string
     */
    protected function getNotificationPath() {
        
        $path = $this->getNotificationTsPath();
        
        if(empty($path)) {
            
            if(!empty($this->exporter)) {
                return 'settings.export.' . $this->exporter->getScope() . '.notification';
            }
            
            return false;
        }
        
        return $path;        
    }
        
    /**
     * Sending an email message to the admin who is registered in typoscript. The message contains the 
     * current state of the import process and can be used for sending success and/or error messages.
     * 
     * @param string $msg
     * @param string $state
     * @return boolean|null|int
     */
    protected function sendNotification($msg, $state = self::STATE_OK) {
        
        $helper = $this->immoPluginHelper;
        
        $notificationPath = $this->getNotificationPath();
        
        if(empty($notificationPath)) {
            return null;
        }

        $notificationSettingsMerged = $helper->getPluginSettings($notificationPath . '.');
        
        if(empty($notificationSettingsMerged)) {
            return null;
        }

        if(!empty($notificationSettingsMerged['mbx_cnt.'])) {
            
            $mbxCntTask = $notificationSettingsMerged['mbx_cnt.']['task'];
            $mbxCntSecret = $notificationSettingsMerged['mbx_cnt.']['secret'];
            
            $mbxCntCommunicatorFile = ExtensionManagementUtility::extPath('mbx_realestate') . 'Classes/Helper/MbxCntCommunicator.php';
            
            if(!file_exists($mbxCntCommunicatorFile) || !require_once($mbxCntCommunicatorFile)) {
                $this->log('Unable to communicate MBX CNT because communicator class file does not exists or could not be loaded: ' . $mbxCntCommunicatorFile);
            }

            $mbxCntCommunicator = new \mbxCntCommunicator($mbxCntTask, $mbxCntSecret);
            $mbxCntCommunicator->setMessage($msg);
            
            switch($state) {
                case self::STATE_OK :
                    $mbxCntCommunicator->setState(\mbxCntCommunicator::TASK_STATE_OK);
                    break;
                case self::STATE_ERROR :
                case self::STATE_NOTIFICATION :
                default :
                    $mbxCntCommunicator->setState(\mbxCntCommunicator::TASK_STATE_ERROR);
                    break;
            }
            
            $apiResponse = $mbxCntCommunicator->submit();
            
            if($apiResponse === true) {
                $this->log('Notified successfully the MBX CNT!');
                return true;
            }
                    
            $this->log('MBX CNT Request: ' . $mbxCntCommunicator->getLatestRequestUrl());
            $this->log('Failed to notify the MBX CNT: ' . (string)$apiResponse['message'] . ', Code ' . (string)$apiResponse['status']);
            
            return false;
        }
        
    }
}

