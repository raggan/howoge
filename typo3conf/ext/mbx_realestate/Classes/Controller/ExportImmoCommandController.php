<?php

namespace TYPO3\MbxRealestate\Controller;

use TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface;
use TYPO3\MbxRealestate\Domain\Repository\ImmoaddressRepository;
use TYPO3\MbxRealestate\Domain\Repository\ImmocontactRepository;
use TYPO3\MbxRealestate\Domain\Repository\ImmofeatureRepository;
use TYPO3\MbxRealestate\Domain\Repository\ImmoimageRepository;
use TYPO3\MbxRealestate\Domain\Repository\ImmoobjectRepository;
use TYPO3\MbxRealestate\Helper\Export\Interfaces\ExporterCommandControllerInterface;

/**
 * Description of ExportImmoCommandController
 * 
 * Controller for Cronjobs using Sheduler Extension
 *
 * @author denis.krueger
 * @package mbx_realestate
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 */
class ExportImmoCommandController extends ExportAbstractCommandController implements ExporterCommandControllerInterface {

    /**
     * Export real estate objects from DB to file
     * 
     * @return boolean
     */
    public function exportCommand() {
        
        try {
            
            $scope = 'immo';
            $this->initCommandController($this);
            parent::exportCommand();

            $configuration = $this->configurationManager->getConfiguration(ConfigurationManagerInterface::CONFIGURATION_TYPE_FULL_TYPOSCRIPT);
            $extensionConfiguration = $configuration['plugin.']['tx_mbxrealestate.'];
            
            $filetype = (string)$extensionConfiguration['settings.']['export.'][$scope . '.']['outputType'];
            $exporterClass = (string)$extensionConfiguration['settings.']['export.'][$scope . '.']['exportClass'];
            
            if(empty($exporterClass)) {
                $exporterClass = '\TYPO3\MbxRealestate\Helper\Export\ExporterImmo' . ucfirst(strtolower($filetype));
            }

            $this->log('Generate Exporter Classname: ' . $exporterClass);
            $this->log('Validate defaults/settings before startup...');

            // <editor-fold defaultstate="collapsed" desc="validate defaults">
            if(!$this->isValidFileTypeConfigured($filetype)) {
            
                self::throwException('Unsupported filetype "' . $filetype . '" configured in ' . __CLASS__ . '::' . __FUNCTION__);
                
            } elseif(!($this->immoobjectRepository instanceof ImmoobjectRepository)) {
                
                self::throwException('$this->immoobjectRepository not of type \TYPO3\MbxRealestate\Domain\Repository\ImmoobjectRepository');
            
            } elseif(!($this->immoaddressRepository instanceof ImmoaddressRepository)) {
                
                self::throwException('$this->immoaddressRepository not of type \TYPO3\MbxRealestate\Domain\Repository\ImmoaddressRepository');
                
            } elseif(!($this->immocontactRepository instanceof ImmocontactRepository)) {
                
                self::throwException('$this->immocontactRepository not of type \TYPO3\MbxRealestate\Domain\Repository\ImmocontactRepository');
                
            } elseif(!($this->immofeatureRepository instanceof ImmofeatureRepository)) {
                
                self::throwException('$this->immofeatureRepository not of type \TYPO3\MbxRealestate\Domain\Repository\ImmofeatureRepository');
                
            } elseif(!($this->immoimageRepository instanceof ImmoimageRepository)) {

                self::throwException('$this->immoimageRepository not of type \TYPO3\MbxRealestate\Domain\Repository\ImmoimageRepository');
                
            } elseif(!class_exists($exporterClass)) {

                self::throwException('Unable to load exporter class "' . $exporterClass . '"');
            }
            // </editor-fold>

            $this->log('Settings valid, continue ...');

            /*
             * - create new exporter class
             * - uses exporterAbstract for IDE to provide docu
             * - uses ExporterInterface to define class construction for future file extensions
             */

            $this->log('Use Exporter: ' . $exporterClass);

            $this->exporter = new $exporterClass();

            $this->log('Exporter instanciated');

            // assignment required for IDEs auto suggests
            $exporter =& $this->exporter;
            $exporter->setCommandControllerReference($this);                        // set the reference to $this to provide logging etc. into importer
            $exporter   ->setConfiguration($extensionConfiguration)                 // set the extension configuration
                        ->setScope($scope)
                        ->setImmoobjectRepository($this->immoobjectRepository)      // set the immoobject repository
                        ->setImmoaddressRepository($this->immoaddressRepository)    // set the immoaddress repository
                        ->setImmocontactRepository($this->immocontactRepository)    // set the immocontact repository
                        ->setImmoimageRepository($this->immoimageRepository)        // set the immoimage repository
                        ->setImmofeatureRepository($this->immofeatureRepository)    // set the immofeature repository
                    ;

            $this->log('Create empty export file...');

            if(($openedFile = $exporter->createFile()) !== true) {

                self::throwException('Failed to create export file: ' . (string)$openedFile);
            }  else {
                $this->log('Empty export file created: ' . $exporter->getFile());
            }
            
            $itemsCount = count($exporter->getEnabledItems());
            $this->log($itemsCount . ' objects to export found');
            
            // iterate each found item from DB and put in in the export file
            while($immoobject = $exporter->iterateItem()) {

                $immoDataItem = $exporter->prepareItem($immoobject);  // collect information from item
                $this->log('Handle object ' . implode('.', array(
                    $immoDataItem['wi'],
                    $immoDataItem['hs'],
                    $immoDataItem['me'],
                )));
                $exporter->addItem($immoDataItem);                
            }

            
            $exporter->saveFile();
            $exporter->closeFile();
            $exporter->backupFile();
            $transfered = $exporter->transferFile();
            
            if($transfered instanceof \Exception) {
                throw $transfered;
            }
            
            if(is_null($transfered)) {
                $this->sendNotification(sprintf('Export of immo objects successful. Exported %s items.', $itemsCount));
            } else {
                $this->sendNotification(sprintf('Export of immo objects successful. Exported %s items. Additional file was transfered to target Server.', $itemsCount));
            }
            
        } catch (\Exception $ex) {

            $this->log('Exception: ' . (string)$ex);
            $sent = $this->sendNotification((string)$ex, self::STATE_ERROR);
            
            $this->log('Sent MBX CNT notification: ' . var_export($sent, true));
            
            throw $ex;
        }

        $this->log('Current date: ' . date('Y-m-d H:i:s'));
        $this->closeLog();        
        
        return true;
    }
}