<?php

namespace TYPO3\MbxRealestate\Controller;

use TYPO3\CMS\Extbase\Utility\DebuggerUtility;
use TYPO3\MbxRealestate\Domain\Model\Immoobject;
use TYPO3\MbxRealestate\Domain\Repository\ImmoaddressRepository;
use TYPO3\MbxRealestate\Domain\Repository\ImmocontactRepository;
use TYPO3\MbxRealestate\Domain\Repository\ImmoenvironmentRepository;
use TYPO3\MbxRealestate\Domain\Repository\ImmofeatureRepository;
use TYPO3\MbxRealestate\Domain\Repository\ImmoimageRepository;
use TYPO3\MbxRealestate\Helper\ImmoPluginHelper;

/**
 *
 *
 * @package mbx_realestate
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
//class ImmoobjectController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController {
class ImmoobjectController extends AbstractController {

    /**
     * immoobjectRepository
     *
     * @var \TYPO3\MbxRealestate\Domain\Repository\ImmoobjectRepository
     * @inject
     */
    protected $immoobjectRepository;

    /**
     *
     * Used to access auto-suggest methods by the IDE
     * @var \TYPO3\MbxRealestate\Helper\Search\ImmoSearchItem
     */
    private $immoSearchItem;

    /**
     * action list
     *
     * @return void
     */
    public function listAction() {

        $immoobjects = $this->immoobjectRepository->findBySearch($this->getSearchAttributes());

        // store immo search request in session
        if($this->request->hasArgument('search_realestate')) {

            $formArguments = $this->checkOnTabedForms($this->request->getArgument('immosearch'));
            
            $this->getImmoSearchHelper()->storeSearchInSession($formArguments);
        }
        
        $this->view->assignMultiple(array(
            'immoobjects'           => $immoobjects,
            'searchArgs'            => (array)$this->request->getArguments(),
            'sortItems'             => $this->prepareSortItems(),
            'perPageItems'          => $this->preparePerPageItems(),
            'perPage'               => $this->getCurrentPerPage(),
            'predefined'            => $this->getImmoSearchHelper()->getPredefined(),
            'immoaddressRepository' => $this->objectManager->get(ImmoaddressRepository::class),
            'immoimageRepository'   => $this->objectManager->get(ImmoimageRepository::class),
            'record'                => $this->getRecord()
        ));
    }

    /**
     * action show
     *
     * @param \TYPO3\MbxRealestate\Domain\Model\Immoobject $immoobject
     * @return void
     */
    public function showAction(Immoobject $immoobject = null) {

        if(!($immoobject instanceof Immoobject)) {
            if ((int)$this->getImmoPluginHelper()->getPluginSettings('settings.forceNotFoundHandlingOnDeletedEntity') > 0) {
                $GLOBALS['TSFE']->pageNotFoundAndExit();
            }
            header('Location: /');
        }

        // toggle between print and default layout
        if(!empty($_REQUEST['type']) && $_REQUEST['type'] == $this->getImmoPluginHelper()->getPluginSettings('settings.printPage')) {
            $this->view->assign('childsection', 'print');
        }  else {
            $this->view->assign('childsection', 'content');
        }
        
        $this->getImmoPluginHelper()->registerCurrentImmoobject($immoobject);
        
        $immoFeatures = array();
        foreach($immoobject->getImmofeature() as $immoFeature){
            array_push($immoFeatures, $immoFeature->getFeatureTitle());
        }
        
        $pluginHelper = ImmoPluginHelper::getInstance();
        
        $districtsMapping = $pluginHelper->getDistrictsMapping();
        $districts = $pluginHelper->parseTS($districtsMapping);
        
        $immoAddress = $immoobject->getImmoaddress();
        $district = '';
        
        if(!empty($immoAddress)){
            $district = $districts[$immoAddress->getDistrict()];
        }
        
        $metaDescription = '';
        
        $immoValues = array(
            'rooms' => array('labelBefore' => '', 'labelAfter' => ' Zimmer', 'noSepartor' => 0, 'value' => $immoobject->getRooms()),
            'district' => array('labelBefore' => ' in ', 'labelAfter' => '', 'noSepartor' => 1, 'value' => $district),
            'area' => array('labelBefore' => 'Größe: ', 'labelAfter' => ' qm', 'noSepartor' => 0, 'value' => $immoobject->getAreaGeneral()),
            'features' => array('labelBefore' => 'Ausstattungsmerkmale: ', 'labelAfter' => '', 'noSepartor' => 0, 'value' => implode(', ', $immoFeatures)),
        );
        
        foreach($immoValues as $key => $values){
            
            if(empty($values['value'])){
                continue;
            }
            
            $labelBefore = $values['labelBefore'];
            $labelAfter = $values['labelAfter'];
            $value = $values['value'];
            $noSeperator = (int)$values['noSepartor'];
            
            if($noSeperator == 0 && !empty($metaDescription)){
                $metaDescription .= ', ';
            } else if($key === 'district' && empty($metaDescription)){
                $labelBefore = '';
            }
            
            
            $metaDescription .= $labelBefore . $value . $labelAfter;
        }
        
        $this->response->addAdditionalHeaderData('<meta name="description" content="' . $metaDescription . '" >');
        $this->view->assignMultiple(array(
            'immoobject'            => $immoobject,
            'predefined'            => $this->getImmoSearchHelper()->getPredefined(),
            'searchArgs'            => (array)$this->request->getArguments(),
            'immoaddress'           => ($immoobject !== null) ? $immoobject->getImmoaddress() : null,
            'immoaddressRepository' => $this->objectManager->get(ImmoaddressRepository::class),
            'immocontactRepository' => $this->objectManager->get(ImmocontactRepository::class),
            'immoimageRepository'   => $this->objectManager->get(ImmoimageRepository::class)
        ));
    }

    /**
     * lists the notepad items stored in Cookie
     *
     * @todo add sorting options for notepad !!!
     * @return void
     */
    public function listNotepadAction() {

        $immoobjects = $this->immoobjectRepository->findByNotepad($this->getSearchAttributes());

        // store immo search request in session
        if($this->request->hasArgument('search_realestate')) {

            $formArguments = $this->checkOnTabedForms($this->request->getArgument('immosearch'));
            
            $this->getImmoSearchHelper()->storeSearchInSession($formArguments);
        }

        $this->view->assignMultiple(array(
            'immoobjects'           => $immoobjects,
            'sortItems'             => $this->prepareSortItems(),
            'perPageItems'          => $this->preparePerPageItems(),
            'perPage'               => $this->getCurrentPerPage(),
            'searchArgs'            => (array)$this->request->getArguments(),
            'predefined'            => $this->getImmoSearchHelper()->getPredefined(),
            'immoaddressRepository' => $this->objectManager->get(ImmoaddressRepository::class),
            'immoimageRepository'   => $this->objectManager->get(ImmoimageRepository::class)
        ));
    }

    public function mailtoNotepadAction() {

        $immoobjects = $this->immoobjectRepository->findByNotepad($this->getSearchAttributes());

        $this->view->assignMultiple(array(
            'immoobjects'           => $immoobjects,
            'immoaddressRepository' => $this->objectManager->get(ImmoaddressRepository::class),
            'immoimageRepository'   => $this->objectManager->get(ImmoimageRepository::class)
        ));
    }

    /**
     * action searchForm
     *
     * @return void
     */
    public function searchFormAction() {

        $immofeatureRepository = $this->objectManager->get(ImmofeatureRepository::class);
        $immoenvironmentRepository = $this->objectManager->get(ImmoenvironmentRepository::class);
        $immoaddressRepository = $this->objectManager->get(ImmoaddressRepository::class);

        // store immo search request in session
        if($this->request->hasArgument('search_realestate')) {

            $formArguments = $this->checkOnTabedForms($this->request->getArgument('immosearch'));

            $this->getImmoSearchHelper()->storeSearchInSession($formArguments);
        }

        if(($customConfiguration = $this->retrieveSearchFromCustomConfiguration()) && !empty($customConfiguration)) {
            $preDefined = $customConfiguration;
        } else {
            $preDefined = $this->getImmoSearchHelper()->getPredefined();
        }

        $buildingTypes = array();
        $streets = array();
        $cities = array();
        
//        $streetObjects = $immoaddressRepository->findAll();
//
//        foreach($streetObjects as $streetObj) {
//
//            $streetObj instanceof \TYPO3\MbxRealestate\Domain\Model\Immoaddress;
//            $streets[] = $streetObj->getStreet();
//        }
//
//        $allImmoObjects = $this->immoobjectRepository->findAll();
//
//        foreach($allImmoObjects as $immoObject) {
//
//            $immoObject instanceof \TYPO3\MbxRealestate\Domain\Model\Immoobject;
//            $buildingTypes[] = $immoObject->getTypeBuilding();
//        }

        
        
        // using native mySQL is faster than typo3 (2015-05-07 by denis.krueger) !!!
        $resAddress = $GLOBALS['TYPO3_DB']->exec_SELECTquery(
                'address.*', // SELECT ... 
                'tx_mbxrealestate_domain_model_immoaddress address JOIN tx_mbxrealestate_domain_model_immoobject object ON address.uid = object.immoaddress', // FROM ... 
                'address.hidden = 0 AND address.deleted = 0 AND object.hidden = 0 AND object.deleted = 0', // WHERE... 
                '', // GROUP BY... 
                '', // ORDER BY... 
                ''  // LIMIT ... 
        );
        while ($address = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($resAddress)) {
            $streets[] = $address['street'];
            if(!in_array($address['city'], $cities)) {
                $cities[] = $address['city'];
            }
        }

        sort($streets);
        sort($cities);

        // using native mySQL is faster than typo3 (2015-05-07 by denis.krueger) !!!
        $allImmoObjects = 0;
        $resImmoobjects = $GLOBALS['TYPO3_DB']->exec_SELECTquery(
                '*', // SELECT ... 
                'tx_mbxrealestate_domain_model_immoobject', // FROM ... 
                'hidden = 0 AND deleted = 0', // WHERE... 
                '', // GROUP BY... 
                '', // ORDER BY... 
                ''  // LIMIT ... 
        );
        while ($immoobject = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($resImmoobjects)) {
            $buildingTypes[] = $immoobject['type_building'];
            ++$allImmoObjects;
        }

        $featuresVirtual = $this->getImmoPluginHelper()->getVirtualFeatures();

        $this->view->assignMultiple(array(
            'immocount'             =>  $allImmoObjects,
            'predefined'            =>  $this->cleanSearchArguments($preDefined),
            'features'              =>  $immofeatureRepository->getUniqueAvailableFeatures($this->getCurrentStoragePidOfPlugin()),
            'featuresAll'           =>  $immofeatureRepository->getUniqueFeatures($this->getCurrentStoragePidOfPlugin()),
            'featuresVirtual'       =>  $featuresVirtual,
            'environments'          =>  $immoenvironmentRepository->getUniqueEnvironments($this->getCurrentStoragePidOfPlugin()),
            'streets'               =>  array_unique($streets),
            'cities'                =>  array_unique($cities),
            'districts'             =>  $this->getImmoPluginHelper()->getDistrictsMapping(),
            'buildingtypes'         =>  array_unique($buildingTypes),
            'offers'                =>  $this->getImmoPluginHelper()->getOffersMapping(),
            'types_of_use'          =>  $this->getImmoPluginHelper()->getConfiguredTypesOfUse(),
            'types_of_disposition'  =>  $this->getImmoPluginHelper()->getConfiguredTypesOfDisposition()
        ));

        $this->assignSearchFromFieldsConfiguration();
    }

    /**
     * validates the search paramater for valid types
     * @param array $search
     * @return array
     */
    protected function cleanSearchArguments($search) {

        if(is_array($search)) {

            if(array_key_exists('floors_from', $search) && !is_numeric($search['floors_from'])) {
                unset($search['floors_from']);
            }

            if(array_key_exists('floors_to', $search) && !is_numeric($search['floors_to'])) {
                unset($search['floors_to']);
            }
        }

        return $search;
    }

    /**
     */
    protected function assignSearchFromFieldsConfiguration() {

        if(!is_null($fieldsConfiguration = $this->getImmoPluginHelper()->getPluginSettings('settings.search.fieldsConfiguration.'))) {

            $fieldsConfiguration = $this->getImmoPluginHelper()->parseTS($fieldsConfiguration);

            foreach($fieldsConfiguration as $field => $data) {
                $fieldsConfiguration[$field] = array(
                    'native'    =>  $data,
                    'json'      =>  json_encode($data)
                );
            }

            $this->view->assign('fieldsConfiguration', $fieldsConfiguration);
        }
    }

    /**
     * @param null|array $arguments The search arguments
     * @return \TYPO3\MbxRealestate\Helper\Search\ImmoSearchItem
     */
    public function getSearchAttributes($arguments = null) {

        $this->immoSearchItem = $this->objectManager->get('TYPO3\MbxRealestate\Helper\Search\ImmoSearchItem');

        $immoSearchItem =& $this->immoSearchItem;

        // use the search configured for this customly embedded plugin from BE
        if(($customConfiguration = $this->retrieveSearchFromCustomConfiguration()) && !empty($customConfiguration)) {

            $tsSearch = (array)$customConfiguration;

            // <editor-fold defaultstate="collapsed" desc="if property 'merge' was defined in TS we merge the TS configuration with the search form action">
            if(!empty($tsSearch['merge']) && (int)$tsSearch['merge'] === 1) {
                if(!empty($arguments)) {

                    $tsSearch = array_merge_recursive($arguments, $tsSearch);

                // use search arguments (REQUEST)
                } elseif($this->request->hasArgument('search_realestate')) {

                    $postSearch = (array)$this->request->getArgument('immosearch');
                    $tsSearch = array_merge($postSearch, $tsSearch);

                    // check if "merge" was set for search query. So we can add multiple search params without flushing current search
                    // this might be helpful for adding a sorting to the current search query
                    if(array_key_exists('merge', $postSearch) && $this->getImmoSearchHelper()->storedSearchInSession()) {

                        $tsSearch = array_merge((array)$this->getImmoSearchHelper()->retrieveSearchFromSession(), $tsSearch);
                    }
                    
                } elseif ($this->getImmoSearchHelper()->storedSearchInSession()) {

                    $tsSearch = array_merge((array)$this->getImmoSearchHelper()->retrieveSearchFromSession(), $tsSearch);
                }
                
                unset($tsSearch['merge']);
            }
            // </editor-fold>
                
            $immoSearchItem->setArguments($tsSearch);

        // use passed function arguments
        } elseif(!empty($arguments)) {

            $immoSearchItem->setArguments((array)$arguments);

        // use search arguments (REQUEST)
        } elseif($this->request->hasArgument('search_realestate')) {

            $immosearch = (array)$this->checkOnTabedForms($this->request->getArgument('immosearch'));
            
            // check if "merge" was set for search query. So we can add multiple search params without flushing current search
            // this might be helpful for adding a sorting to the current search query
            if(array_key_exists('merge', $immosearch) && $this->getImmoSearchHelper()->storedSearchInSession()) {

                $immosearch = array_merge((array)$this->getImmoSearchHelper()->retrieveSearchFromSession(), $immosearch);
                unset($immosearch['merge']);
            }

            $immoSearchItem->setArguments($immosearch);

        // use arguments by searchagent
        } elseif ($this->request->hasArgument('searchagent_id')) {

            $immoSearchItem->setArguments($this->getSearchagentController()->getQueryArrayBySearchagent($this->request->getArgument('searchagent_id')));

        // use arguments stored in session
        } elseif ($this->getImmoSearchHelper()->storedSearchInSession()) {

            $immoSearchItem->setArguments((array)$this->getImmoSearchHelper()->retrieveSearchFromSession());

        // use the default configured arguments for searches stored in TS
        } else {
            
            $immoSearchItem->setArguments($this->getImmoSearchHelper()->retrieveSearchFromConfiguration());
        }

        $immoSearchItem->passArgumentToSearch('storage');
        $immoSearchItem->passArgumentToSearch('excludes');

        $immoSearchItem->passArgumentToSearch('types_of_use');
        $immoSearchItem->passArgumentToSearch('types_estate_precise');
        $immoSearchItem->passArgumentToSearch('types_of_disposition');
        $immoSearchItem->passArgumentToSearch('type_building');

        $immoSearchItem->passArgumentToSearch('is_investment');
        $immoSearchItem->passArgumentToSearch('required_wbs');
        $immoSearchItem->passArgumentToSearch('provision');
        $immoSearchItem->passArgumentToSearch('area_from');
        $immoSearchItem->passArgumentToSearch('area_to');

        $immoSearchItem->passArgumentToSearch('rooms_from');
        $immoSearchItem->passArgumentToSearch('rooms_to');

        $immoSearchItem->passArgumentToSearch('cost_gross_from');
        $immoSearchItem->passArgumentToSearch('cost_gross_to');

        $immoSearchItem->passArgumentToSearch('cost_net_from');
        $immoSearchItem->passArgumentToSearch('cost_net_to');

        $immoSearchItem->passArgumentToSearch('cost_buy_from');
        $immoSearchItem->passArgumentToSearch('cost_buy_to');

        $immoSearchItem->passArgumentToSearch('floors_from');
        $immoSearchItem->passArgumentToSearch('floors_to');
        $immoSearchItem->passArgumentToSearch('floors_range');

        $immoSearchItem->passArgumentToSearch('floors_max');
        $immoSearchItem->passArgumentToSearch('floors_max_from');
        $immoSearchItem->passArgumentToSearch('floors_max_to');
        $immoSearchItem->passArgumentToSearch('floors_max_range');

        $immoSearchItem->passArgumentToSearch('thermal_heating_methods');

        $immoSearchItem->passArgumentToSearch('year_build');
        $immoSearchItem->passArgumentToSearch('year_build_range');
        $immoSearchItem->passArgumentToSearch('year_build_from');
        $immoSearchItem->passArgumentToSearch('year_build_to');

        $immoSearchItem->passArgumentToSearch('keywords');
        $immoSearchItem->passArgumentToSearch('offers');
        $immoSearchItem->passArgumentToSearch('street');
        $immoSearchItem->passArgumentToSearch('districts');
        $immoSearchItem->passArgumentToSearch('city');
        $immoSearchItem->passArgumentToSearch('geo_location');
        $immoSearchItem->passArgumentToSearch('geo_distance');
        $immoSearchItem->passArgumentToSearch('features');
        $immoSearchItem->passArgumentToSearch('environments');


        $immoSearchItem->passArgumentToSearch('order');
        $immoSearchItem->passArgumentToSearch('order_by');
        $immoSearchItem->passArgumentToSearch('limit');
        $immoSearchItem->passArgumentToSearch('sort');          // configured in TS for sort-select box so it can be validated before SQLing

        $immoSearchItem->passArgumentToSearch('per_page');
        
        $storagePid = $this->getImmoPluginHelper()->getCurrentStoragePidOfPlugin($this->settings);

        if(!empty($storagePid)) {
            $immoSearchItem->setStorage($storagePid);
        }

        // set limit by ts or flexform value
        if ((int)$this->settings['search']['limit'] > 0) {
            $immoSearchItem->setLimit((int)$this->settings['search']['limit']);
        }
        
        //set shuffel by ts or flexform value
        if ((int)$this->settings['search']['shuffel'] > 0) {
            $immoSearchItem->setOrderBy('Rand()');
        }
        
        //set shuffel by ts or flexform value
        if ((int)$this->settings['search']['excludeImmoUidByRequest'] > 0 && $this->request->hasArgument('immoobject')) {
            $immoObject = $this->immoobjectRepository->findByUid($this->request->getArgument('immoobject'));
            $uniqueImmoId = rtrim(str_pad($immoObject->getUnr(), 4, '0', STR_PAD_LEFT) . '/' . str_pad($immoObject->getWi(), 4, '0', STR_PAD_LEFT), "0");
            $immoSearchItem->setExcludes(array($uniqueImmoId));
        }

        // sort by default value if no sort defined
        if(!$immoSearchItem->hasSortSet() && !$immoSearchItem->hasOrderBySet()) {

            $pluginHelper = ImmoPluginHelper::getInstance();
            $sortTs = $pluginHelper->getPluginSettings('settings.search.listSort.');

            if(!empty($sortTs)) {

                $sortItems = $pluginHelper->parseTS($sortTs);
                $sortKeys = array_keys($sortItems);

                $sortDefault = array_shift($sortKeys);
                $immoSearchItem->setSort($sortDefault);

            }
        }

        return $immoSearchItem;
    }

    /**
     * Checks the custom configuration for a search stored in $this->settings configured in an embedded Plugin element.
     * @return boolean|array|null
     */
    public function retrieveSearchFromCustomConfiguration() {

        $ret = false;

        if(!empty($this->settings) && !empty($this->settings['customSearch'])) {

            return $this->getImmoSearchHelper()->retrieveSearchFromCustomConfiguration($this->settings['customSearch']);
        }

        return $ret;
    }

    /**
     * Returns an array for the FE to display immo sort options
     * @return array
     */
    protected function prepareSortItems() {

        $sortItems = array();

        $pluginHelper = $this->getImmoPluginHelper();
        $sortConfigurationTs = $pluginHelper->getPluginSettings('settings.search.listSort.');

        if(!empty($sortConfigurationTs)) {
            $sortConfiguration = $pluginHelper->parseTS($sortConfigurationTs);

            foreach($sortConfiguration as $sortType => $sortData) {
                $sortItems[$sortType] = $sortData['label'];
            }
        }

        return $sortItems;
    }

    /**
     * Returns an array for the FE to display immo per page dsiplay options
     * @return array
     */
    protected function preparePerPageItems() {

        $pluginHelper = $this->getImmoPluginHelper();
        $perPageConfiguration = $pluginHelper->getPluginSettings('settings.search.perPageSteps');

        if(!empty($perPageConfiguration)) {

            $items = explode(',', preg_replace('/^[^0-9]$/', '', (string)$perPageConfiguration));
            $perPageArr = array();

            foreach($items as $perPage) {
                $perPageArr[$perPage] = $perPage;
            }

            return $perPageArr;
        }

        return null;
    }

    /**
     * Returns the currently selected perPage value or returns the fallback value.
     *
     * @return int
     */
    protected function getCurrentPerPage() {

        if($this->request->hasArgument('perPage')) {

            $perPage = $this->request->getArgument('perPage');

        } elseif(($preDefined = $this->getImmoSearchHelper()->getPredefined())
            && array_key_exists('perPage', $preDefined)
            && preg_match('/^([0-9]+)$/', $preDefined['perPage'])
        ) {

            $perPage = $preDefined['perPage'];
        }

        if(!isset($perPage) || !in_array($perPage, $this->preparePerPageItems())) {

            $perPage = (int)$this->getImmoPluginHelper()->getPluginSettings('settings.search.perPage');
        }

        return $perPage;
    }

    /**
     * Override processRequest function of parent class to call pageNotFoundHandler
     * if immoobject doesn't exist anymore.
     *
     *
     * @param \TYPO3\CMS\Extbase\Mvc\RequestInterface $request
     * @param \TYPO3\CMS\Extbase\Mvc\ResponseInterface $response
     * @throws \Exception|\TYPO3\CMS\Extbase\Property\Exception
     */
    public function processRequest(\TYPO3\CMS\Extbase\Mvc\RequestInterface $request, \TYPO3\CMS\Extbase\Mvc\ResponseInterface $response) {
        try {
            parent::processRequest($request, $response);
        } catch (\TYPO3\CMS\Extbase\Property\Exception $e) {
            if ($e->getPrevious() instanceof \TYPO3\CMS\Extbase\Property\Exception\TargetNotFoundException) {
                $GLOBALS['TSFE']->pageNotFoundAndExit();
            } else {
                throw $e;
            }
        }
    }

    /**
     * 
     * @param mixed $sendedFormArguments
     * @return mixed
     */
    protected function checkOnTabedForms($sendedFormArguments){
        
        $arguments = $formArguments = $sendedFormArguments;

        if(isset($arguments['tabedForm'])){

            $formArguments = $arguments[$arguments['tabedForm']];
            $formArguments['_tabedForm'] = $arguments['tabedForm'];
            
        }
        
        return $formArguments;
    }

}
