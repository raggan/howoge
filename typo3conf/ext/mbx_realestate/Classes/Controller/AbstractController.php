<?php
namespace TYPO3\MbxRealestate\Controller;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2013 Denis Krueger <denis.krueger@mindbox.de>, Mindbox GmbH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/
use TYPO3\MbxRealestate\Helper\ImmoPluginHelper;
use TYPO3\MbxRealestate\Helper\Search\ImmoSearchHelper;
use TYPO3\MbxRealestate\Helper\SearchagentHelper;

/**
 *
 *
 * @package mbx_realestate
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
abstract class AbstractController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController {

    /**
     * @var \TYPO3\MbxRealestate\Helper\Search\ImmoSearchHelper
     */
    private $immoSearchHelper;

    /**
     * @var \TYPO3\MbxRealestate\Helper\ImmoPluginHelper
     */
    private $immoPluginHelper;

    /**
     * @var type \TYPO3\MbxRealestate\Controller\SearchagentController
     */
    private $searchagentController;

    /**
     * @var \TYPO3\MbxRealestate\Helper\SearchagentHelper
     */
    private $searchagentHelper;

//    
//    /**
//     * @param \TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface $configurationManager
//     * @return void
//     */
//    public function injectConfigurationManager(\TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface $configurationManager) {
//        $this->configurationManager = $configurationManager;
//        $this->settings = $this->configurationManager->getConfiguration(\TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface::CONFIGURATION_TYPE_SETTINGS);
//    }
//    
    /**
     * @return array
     */
    public function getSettings() {
        
        return $this->settings;
    }
    
    public function __construct() {

        parent::__construct();

//        $this->immoSearchHelper = \TYPO3\MbxRealestate\Helper\Search\ImmoSearchHelper::getInstance();
//        $this->immoPluginHelper = \TYPO3\MbxRealestate\Helper\ImmoPluginHelper::getInstance();
//        $this->searchagentHelper = \TYPO3\MbxRealestate\Helper\SearchagentHelper::getInstance();
        $this->immoSearchHelper = new ImmoSearchHelper($this);
        $this->immoPluginHelper = new ImmoPluginHelper($this);
        $this->searchagentHelper = new SearchagentHelper($this);
//        $this->searchagentController = $this->objectManager->get('TYPO3\MbxRealestate\Controller\SearchagentController');
    }

    /**
     * 
     * @return int|null
     */
    public function getCurrentStoragePidOfPlugin() {
        
        return $this->getImmoPluginHelper()->getCurrentStoragePidOfPlugin($this->getSettings());
    }
    
    /**
     * 
     * @return string|null
     */
    public function getCurrentStorageNameOfPlugin() {
        
        return $this->getImmoPluginHelper()->getCurrentStorageNameOfPlugin($this->getSettings());
    }
    
    /**
     * @return ImmoSearchHelper
     */
    protected function getImmoSearchHelper() {
        return $this->immoSearchHelper;
    }

    /**
     * @return ImmoPluginHelper
     */
    protected function getImmoPluginHelper() {
        return $this->immoPluginHelper;
    }

    /**
     * @return SearchagentHelper
     */
    protected function getSearchagentHelper() {
        return $this->searchagentHelper;
    }

    /**
     * @return SearchagentController
     */
    protected function getSearchagentController() {
        if(empty($this->searchagentController)) {
            $this->searchagentController = $this->objectManager->get(SearchagentController::class);
        }
        return $this->searchagentController;
    }

	/**
	 * @return array
	 */
	public function getRecord() {
		$row = $this->configurationManager->getContentObject()->data;
        return (array) $row;
    }
}
