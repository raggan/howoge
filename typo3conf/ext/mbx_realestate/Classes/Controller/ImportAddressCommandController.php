<?php

namespace TYPO3\MbxRealestate\Controller;

use TYPO3\MbxRealestate\Domain\Repository\ImmoaddressRepository;
use TYPO3\MbxRealestate\Domain\Repository\ImmocontactRepository;
use TYPO3\MbxRealestate\Domain\Repository\ImmoimageRepository;
use TYPO3\MbxRealestate\Domain\Repository\ImmoobjectRepository;
use TYPO3\MbxRealestate\Helper\GeoLocation;
use TYPO3\MbxRealestate\Helper\Import\AddressDataItem;
use TYPO3\MbxRealestate\Helper\Import\ImporterAddress;

/**
 * Description of ImportCommandController
 *
 * Controller for Cronjobs using Sheduler Extension
 *
 * @author denis.krueger
 * @package mbx_realestate
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 */
class ImportAddressCommandController extends \TYPO3\MbxRealestate\Controller\ImportAbstractCommandController implements \TYPO3\MbxRealestate\Helper\Import\Interfaces\ImporterCommandControllerInterface {

    /**
     * Imports real estate objects from file
     *
     * @return boolean
     */
    public function importCommand() {

        try {

            $this->initCommandController($this);
            parent::importCommand();

            $configuration = $this->configurationManager->getConfiguration(\TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface::CONFIGURATION_TYPE_FULL_TYPOSCRIPT);
            $extensionConfiguration = $configuration['plugin.']['tx_mbxrealestate.'];

            $filetype = (string)$extensionConfiguration['settings.']['import.']['address.']['inputType'];
            $importerClass = (string)$extensionConfiguration['settings.']['import.']['address.']['importClass'];
            $importerMode = $this->immoPluginHelper->getPluginSettings('settings.import.address.importType');
            $importerMaxLoop = (int)$this->immoPluginHelper->getPluginSettings('settings.import.address.maxImportLoop');

            if(empty($importerClass)) {
                $importerClass = '\TYPO3\MbxRealestate\Helper\Import\ImporterAddress' . ucfirst(strtolower($filetype));
            }

            if(!$this->isValidFileTypeConfigured($filetype)) {

                self::throwException('Unsupported filetype "' . $filetype . '" configured in ' . __CLASS__ . '::' . __FUNCTION__);

            } elseif(!($this->immoobjectRepository instanceof ImmoobjectRepository)) {

                self::throwException('$this->immoobjectRepository not of type \TYPO3\MbxRealestate\Domain\Repository\ImmoobjectRepository');

            } elseif(!($this->immoaddressRepository instanceof ImmoaddressRepository)) {

                self::throwException('$this->immoaddressRepository not of type \TYPO3\MbxRealestate\Domain\Repository\ImmoaddressRepository');

            } elseif(!($this->immocontactRepository instanceof ImmocontactRepository)) {

                self::throwException('$this->immocontactRepository not of type \TYPO3\MbxRealestate\Domain\Repository\ImmocontactRepository');

            } elseif(!($this->immoimageRepository instanceof ImmoimageRepository)) {

                self::throwException('$this->immoimageRepository not of type \TYPO3\MbxRealestate\Domain\Repository\ImmoimageRepository');

            } elseif(!class_exists($importerClass)) {

                self::throwException('Unable to load importer class "' . $importerClass . '"');
            }

            $this->log('Used Class for Import: ' . $importerClass);

            /*
             * - create new importer class
             * - uses importerAbstract for IDE to provide docu
             * - uses ImporterInterface to define class construction for future file extensions
             */

            $this->importer = new $importerClass();

            // assignment required for IDEs auto suggests
            $importer =& $this->importer;
            $importer->setCommandControllerReference($this);                        // set the reference to $this to provide logging etc. into importer
            $importer   ->setConfiguration($extensionConfiguration)                 // set the extension configuration
                        ->setScope('address')
                        ->setImmoobjectRepository($this->immoobjectRepository)      // set the immoobject repository
                        ->setImmoaddressRepository($this->immoaddressRepository)    // set the immoaddress repository
                        ->setImmocontactRepository($this->immocontactRepository)    // set the immocontact repository
                        ->setImmoimageRepository($this->immoimageRepository)        // set the immoimage repository
                    ;

            do {
                if(($openedFile = $importer->openFile()) !== true) {

                    self::throwException('Failed to open import file: ' . (string)$openedFile);
                }  else {
                    $this->log('Load import file ' . $importer->getFile());
                }
                $this->log(count($importer->getItems()) . ' objects found');

                $saved = $dropped = $i = 0;

                // unbind the currently binded Saver to remove Database tables
                // before our new importer will create them again. This prevents
                // an immediately removement of the tables right after their
                // were created!
                $importer->resetImporterSaver();
                $importer->setImporterSaver(new ImporterAddress($importer));

                // iterate each items and update them in the database
                while($addressItem = $importer->iterateItem()) {

                    /**
                     * @var \TYPO3\MbxRealestate\Helper\Import\AddressDataItem
                     */
                    $addressDataItem = $importer->prepareItem($addressItem);  // collect information from item
                    $addressDataItem instanceof AddressDataItem;

                    $addressID = $addressDataItem->getImmoaddress()->getUnr() . '/'
                            . $addressDataItem->getImmoaddress()->getWi() . '/'
                            . $addressDataItem->getImmoaddress()->getHs();

                    $this->log('Handle object "' . $addressID . '"');
                    $importer->getImporterSaver()->saveObject($addressDataItem);        // prepare item data for inserting into temporary table

                    if(!($i++%100)) {

                        // insert current items into temporary table(s)
                        $importer->getImporterSaver()->sqlTempDataPush();
                    }
                }


                $importer->getImporterSaver()->sqlTempDataPush();           // insert current items into temporary table(s)
                $importer->getImporterSaver()->sqlTempDataTransfer();       // transfer temporary items to target
                $importer->getImporterSaver()->createNM();                  // link immoaddress to immocontact

                if($importerMode === self::IMPORT_TYPE_FULL) {

                    $importer->getImporterSaver()->cleanTempAddressContactsMN();// remove all nm-relations from the live table which arent present in the temporary table
                    $importer->getImporterSaver()->cleanTempAddress();          // remove all address from the live table which arent present in the temporary table
                    $importer->getImporterSaver()->cleanTempContacts();         // remove all contacts from the live table which arent present in the temporary table
                }

                $this->log('Backup import file');
                $importer->backupFile();
                $importer->closeFile();

                $importer->removeFile();

                $this->log($logMessage = $saved . ' items were passed to save / ' . $dropped . ' items were passed to drop');

                
                // give some time before next Loop will run (especially to prevent 
                // same backup filename if the script runs two imports per second)
                sleep(2);
                
            } while(--$importerMaxLoop > 0);
                
        } catch (\Exception $ex) {

            $this->log('Exception: ' . (string)$ex);
            $sent = $this->sendNotification((string)$ex, self::STATE_ERROR);
            
            $this->log('Sent notification: ' . var_export($sent, true));
            throw $ex;
        }

        $this->log('Current date: ' . date('Y-m-d H:i:s'));
        $this->sendNotification('Import of immo addresses succeeded: ' . $logMessage);
//        $this->closeLog();
        
        return true;
    }



    /**
	 * Adds Geo Locations to immoaddress
     *
	 * @return void
	 */
    public function addGeoLocationsCommand() {

        try {
            $this->initCommandController($this);
            $immoaddresses = $this->immoaddressRepository->findAllWithoutGeolocation();

            $this->log('Found missing geolocations: ' . count($immoaddresses));

            if (!empty($immoaddresses) && is_array($immoaddresses)) {
                foreach (array_keys($immoaddresses) as $ak) {
                    $immoaddress = & $immoaddresses[$ak];

                    $locationObj = GeoLocation::getGeoLocation($immoaddress->getStreet() . ' ' . $immoaddress->getStreetnr() . ' ' . $immoaddress->getZip() . ' ' . $immoaddress->getCity());

                    if ($locationObj) {
                        $immoaddress->setGeoX($locationObj->lat);
                        $immoaddress->setGeoY($locationObj->lng);

                        $this->immoaddressRepository->update($immoaddress);
                    }
                }
            }
        } catch (\Exception $ex) {

            $this->log('Exception: ' . (string) $ex);
            throw $ex;
        }

        $this->log('Current date: ' . date('Y-m-d H:i:s'));
//        $this->closeLog();

        return true;
    }

}

