<?php
namespace TYPO3\MbxRealestate\Controller;

use \TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use \TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Utility\DebuggerUtility;
use TYPO3\MbxRealestate\Domain\Model\Immoobject;
use TYPO3\MbxRealestate\Helper\Cookie;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2013 Tobias Jüschke <tobias.jueschke@mindbox.de>, Mindbox GmbH
 *  Anke Häslich <anke.haeslich@mindbox.de>, Mindbox GmbH
 *  
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 *
 *
 * @package mbx_realestate
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class CookieController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController {

    /**
     * immoobjectRepository
     *
     * @var \TYPO3\MbxRealestate\Domain\Repository\ImmoobjectRepository
     * @inject
     */
    protected $immoobjectRepository;
    
    /**
     * @var array
     */
    protected $redirectConfiguration;
    
    /**
     * @var string
     */
    protected $cookieName;
    
    /**
     * @var mixed
     */
    protected $cookieValue;
    
    /**
     * action initialisize
     * 
     */
    
    public function initializeAction(){

        if($this->request->hasArgument('redirectConfiguration')){
            $this->redirectConfiguration = $this->request->getArgument('redirectConfiguration');
        }
        
        if($this->request->hasArgument('cookie')){
            $this->cookieName = $this->request->getArgument('cookie');
        }  else {
            throw new \TYPO3\CMS\Core\Exception('No cookie name defined!');
        }
        
        if($this->request->hasArgument('value')){
            $this->cookieValue = $this->request->getArgument('value');
        }  else {
            throw new \TYPO3\CMS\Core\Exception('No cookie value defined!');
        }
        
//        $this->immoobjectRepository = new \TYPO3\MbxRealestate\Domain\Repository\ImmoobjectRepository();
    }
    
    /**
     * @return boolean
     */
    protected function isAjaxRequest() {
        return (strtolower(filter_input(INPUT_SERVER, 'HTTP_X_REQUESTED_WITH')) === 'xmlhttprequest');
    }
    
    /**
     * @return boolean
     */
    protected function isMobile() {
        
        if(!ExtensionManagementUtility::isLoaded('tk_mobiledetector')) {
            return false;
        }
        
        require_once(PATH_typo3conf . '/ext/tk_mobiledetector/Classes/Hook/DeviceInfo.php');
        
        $cls = GeneralUtility::makeInstance('DeviceInfo');
        $ua = filter_input(INPUT_SERVER, 'HTTP_USER_AGENT');
        
        return ($cls->getDeviceType(array('userAgent' => $ua)) === 'mobi');
    }
    
    public function addCookieAction() {

        if($this->isAjaxRequest() && !$this->isMobile()) {
            Cookie::add($this->cookieName, $this->cookieValue, Cookie::sixMonths);

            return json_encode($this->getImmoobjectIdsFromNotepad());
            
        } else {
            if($this->redirectConfiguration){
                /* push cookie */
                Cookie::add($this->cookieName, $this->cookieValue, Cookie::sixMonths);

                $this->cookieRedirect();
            }
        }
    }
    
    /**
     * action cookie
     */
    public function setCookieAction(){
        
        if($this->redirectConfiguration){

            /* set cookie */
            Cookie::delete($this->cookieName);
            Cookie::set($this->cookieName, $this->cookieValue, Cookie::sixMonths);
         
            $this->cookieRedirect();
        }
    }
    
    private function cookieRedirect() {
        /* redirect to page id action controller params empty */
//        if(empty($this->redirectConfiguration['action']) && empty($this->redirectConfiguration['controller'])){
//            $uri = $this->uriBuilder->reset()
//                ->setTargetPageUid($this->redirectConfiguration['pageUid'])
//                ->build();
//
//            header('Location: '.$uri);
//            exit;
//        } else {
            /* redirect to given page */
            $this->redirect(
                $this->redirectConfiguration['action'], 
                $this->redirectConfiguration['controller'], 
                $this->redirectConfiguration['plugin'], 
                $this->redirectConfiguration['arguments'],
                $this->redirectConfiguration['pageUid']
            );
//        }
    }

    /**
     * removes an item from a cookie (doesnt fully drops the cookie)
     */
    public function dropCookieAction() {

        if($this->isAjaxRequest() && !$this->isMobile()) {
            
            Cookie::drop($this->cookieName, $this->cookieValue);

            return json_encode($this->getImmoobjectIdsFromNotepad());
            
        } else {
            
            if($this->redirectConfiguration){

                /* remove cookie */
                Cookie::drop($this->cookieName, $this->cookieValue);

                /* redirect to given page */
                $this->cookieRedirect();
            }
        }
    }


    /**
     * action cookie
     */
    public function deleteCookieAction(){

        if($this->redirectConfiguration){

            /* delete cookie */
            Cookie::delete($this->cookieName);
            
            /* redirect to given page */
            $this->cookieRedirect();
        }
    }
    
    /**
     * 
     * @return array
     */
    private function getImmoobjectIdsFromNotepad() {
        
        $immoobjects = $this->immoobjectRepository->findByNotepad();
        $ids = array();

        foreach($immoobjects as $immoobject) {
            $immoobject instanceof Immoobject;
            $ids[] = $immoobject->getUid();
        }
        
        return $ids;
    }
}