<?php
namespace TYPO3\MbxRealestate\Controller;


use TYPO3\CMS\Extbase\Persistence\Generic\PersistenceManager;
use TYPO3\MbxRealestate\Domain\Repository\SearchagentRepository;
use TYPO3\MbxRealestate\Helper\Exception\SearchagentException;
use TYPO3\MbxRealestate\Helper\Search\ImmoSearchHelper;

/**
 *
 *
 * @package mbx_realestate
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class SearchagentController extends AbstractController {

    /**
     * SearchagentRepository
     *
     * @var SearchagentRepository
     */
    protected $searchagentRepository;

    /**
     * injectSearchagentRepository
     *
     * @param SearchagentRepository $searchagentRepository
     */
    public function injectSearchagentRepository(SearchagentRepository $searchagentRepository) {
        $this->searchagentRepository = $searchagentRepository;
    }
    
    /**
     * @return void
     */
    public function formAction() {
        // by default we display the register view (even if we send a register event -> code below)
        $this->view->assign('display', 'register');

        if($this->request->hasArgument('unregister_searchagent')) {

            $this->performUnregister();
        }


        // pass the searchagent data to the unregister form
        if ($this->request->hasArgument('searchagent_id')) {

            $this->view->assignMultiple(array(
                'display'           =>  'unregister',
                'unsubscribe_url'   =>  $this->getSearchagentHelper()->getUnsubscribeLink($this->request->getArgument('searchagent_id'), $this->request->getArgument('email'))
            ));
        }
        
    }
    
    /**
     * Removes an existing searchagent entry
     * @return boolean
     * @throws \TYPO3\MbxRealestate\Helper\Exception\SearchagentException
     */
    private function performUnregister() {
        
        $email = strtolower($this->request->getArgument('email'));
        $uid = $this->request->getArgument('unregister_searchagent_id');

        $this->view->assign('display', 'unregister');
        
        try {

            // no valid uid passed
            if(!is_numeric($uid)) {

                throw new SearchagentException('Invalid searchagent ID was passed to remove searchagent entry.', 3);

            // no searchagent item found by uid
            } elseif(($searchagentObject = $this->getSearchagentObjectById($uid)) === false) {

                /**
                 * no search agent entry found for uid
                 * @todo: echoing error or what we do if an invalid (maybe already dropped) searchagent was passed
                 */

            // compare email with the DB stored email (the email can be passed md5()-hashed)
            } elseif(!(($searchagentEmail = strtolower($searchagentObject->getContactEmail())) && in_array($email, array($searchagentEmail, md5($searchagentEmail) )))) {

                throw new SearchagentException('Invalid email passed to remove searchagent entry.', 4);

            // passed all tests: continue and remove item from DB    
            } else {

                // remove item from DB
                $this->searchagentRepository->remove($searchagentObject);                    

                // commit changes
                $this->objectManager->get(PersistenceManager::class)->persistAll();

                $this->view->assign('success', true);  
                
                return true;
            }

        } catch (SearchagentException $ex) {

            $this->view->assign('error', (string)$ex);
            echo $ex;
        } catch (Exception $ex) {

            $this->view->assign('error', 'Error occured');
            echo $ex;
        }
        
        return false;
    }
    
    /**
     * Returns the data object for the requested searchagent object $id
     * @param int $id
     * @return boolean|object
     */
    private function getSearchagentObjectById($id) {
        
        $searchagentRepository = $this->objectManager->get(SearchagentRepository::class);

        $searchagentObjectAll = $searchagentRepository->findBySearchagentId($id);
        
        // entry for searchagent found?
        if($searchagentObjectAll->count() === 1) {
            
            return $searchagentObjectAll->getFirst();
            
        } else {
            return false;
        }
    }
    
    /**
     * Returns the form fields that were filled when the user saved them as a searchagent entry
     * @param int $id the uid of the searchagent dataset
     * @return array
     */
    public function getQueryArrayBySearchagent($id) {
        
        if(($searchagentObject = $this->getSearchagentObjectById($id)) !== false ) {
            
            $searchagent_query = $searchagentObject->getSearchQuery(); // JSON encoded string

            return (array)ImmoSearchHelper::getInstance()->jsonDecodeSearch($searchagent_query);
            
        } else {
            
            return array();
        }
    }
    
}