<?php

namespace TYPO3\MbxRealestate\Controller;

use TYPO3\CMS\Core\Mail\MailMessage;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface;
use TYPO3\CMS\Extbase\Persistence\Generic\PersistenceManager;
use TYPO3\CMS\Extbase\Persistence\Generic\Storage\Exception\SqlErrorException;
use TYPO3\CMS\Fluid\View\StandaloneView;
use TYPO3\MbxRealestate\Domain\Model\Searchagent;
use TYPO3\MbxRealestate\Domain\Repository\ImmoobjectRepository;
use TYPO3\MbxRealestate\Domain\Repository\SearchagentRepository;
use TYPO3\MbxRealestate\Helper\ImmoPluginHelper;
use TYPO3\MbxRealestate\Helper\Search\ImmoSearchHelper;
use TYPO3\MbxRealestate\Helper\SearchagentHelper;

/**
 * Description of SearchagentCommandController
 * 
 * Controller for Cronjobs using Sheduler Extension
 *
 * @author denis.krueger
 * @package mbx_realestate
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 */
class SearchagentCommandController extends ImportAbstractCommandController {

    CONST LOG_ROUTE = 80;
    
    /**
     * ImmoobjectRepository
     *
     * @var \TYPO3\MbxRealestate\Domain\Repository\ImmoobjectRepository
     */
    protected $immoobjectRepository;

    /**
     * SearchagentRepository
     *
     * @var \TYPO3\MbxRealestate\Domain\Repository\SearchagentRepository
     */
    protected $searchagentRepository;

    /**
     * configurationManager
     * @var \TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface
     */
    protected $configurationManager;

    /**
     * @var \TYPO3\MbxRealestate\Controller\ImmoobjectController
     */
    protected $immoobjectController;

    /**
     * @var \TYPO3\MbxRealestate\Controller\SearchagentController
     */
    protected $searchagentController;
    
    /**
     * @var \TYPO3\MbxRealestate\Helper\SearchagentHelper
     */
    protected $searchagentHelper;
    
    /**
     * @var \TYPO3\MbxRealestate\Helper\ImmoPluginHelper
     */
    public $immoPluginHelper;
    
    /**
     *
     * @var Resource
     */
    protected $fHandle;
    
    /**
     * injectConfigurationManager
     * 
     * @param \TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface $configurationManager
     */
    public function injectConfigurationManager(ConfigurationManagerInterface $configurationManager) {
        $this->configurationManager = $configurationManager;
    }
    
    /**
     * injectSearchagentRepository
     *
     * @param \TYPO3\MbxRealestate\Domain\Repository\SearchagentRepository $searchagentRepository
     */
    public function injectSearchagentRepository(SearchagentRepository $searchagentRepository) {
        $this->searchagentRepository = $searchagentRepository;
    }
    
    /**
     * injectImmoobjectRepository
     *
     * @param \TYPO3\MbxRealestate\Domain\Repository\ImmoobjectRepository $immoobjectRepository
     */
    public function injectImmoobjectRepository(ImmoobjectRepository $immoobjectRepository) {
        $this->immoobjectRepository = $immoobjectRepository;
    }

    
    /**
     * Imports real estate objects from file
     * 
     * @return boolean
     */
    public function notificationCommand() {
        
        try {

            $this->searchagentHelper = SearchagentHelper::getInstance();
            $this->searchagentController = $this->objectManager->get(SearchagentController::class);
            $this->immoobjectController = $this->objectManager->get(ImmoobjectController::class);
            $this->immoPluginHelper = ImmoPluginHelper::getInstance();
            
            $this->initLog();
            
            $className = str_replace('CommandController', '', get_class($this));
            $namespacePath = explode('\\', $className);

            $this->childCommandController = $this;
            $this->commandType = array_pop($namespacePath);
            
            $searchagents = $this->searchagentRepository->findAll();
            $persistenceManager = $this->objectManager->get(PersistenceManager::class);
            
            $this->log('Current date: ' . date('Y-m-d H:i:s'));
            $this->log('Found ' . count($searchagents) . ' entries to process!');
            
            $iSentMessages = 0;
            
            foreach($searchagents as $searchagentObject) {
                
                $searchagentObject instanceof Searchagent;
            
                $this->log('Handle searchagent #' . $searchagentObject->getUid());
                
                $searchagent_query = $searchagentObject->getSearchQuery(); // JSON encoded string

                // extract JSON to an valid array containing the search data
                $searchagent_data = (array)ImmoSearchHelper::getInstance()->jsonDecodeSearch($searchagent_query);
                
                // skip searchagent search if no query is set
                if(empty($searchagent_data)) {
                    $this->log('Skip searchagent because no search data defined!');
                    continue;
                }

                // extract search data into generic object
                $immoSearchItem = $this->immoobjectController->getSearchAttributes($searchagent_data);
                
                // set manually the uniqueImmoIDs which were already found for this query to exclude from search 
                $excludedImmoIDsExploded = explode(',', $searchagentObject->getResultsSent());
                
                // filter empty items and drop them
                $excludedImmoIDs = array_filter($excludedImmoIDsExploded, 'strlen');
                $immoSearchItem->setExcludes($excludedImmoIDs);
                
                // start search for new items
                $immoobjects = $this->immoobjectRepository->findBySearch($immoSearchItem);
                
                $this->log('Found ' . count($immoobjects) . ' new items');
                
                // any new immoobjects found?
                if($immoobjects->count() > 0) {
                    
                    // get the uniqueImmoIDs of the new immoobjects to store in the searchagents dataset 
                    // to prevent sending searchagent notification to the user more than one (1) times!!
                    foreach($immoobjects as $immoobject) {
                        
                        $uniqueImmoID = $this->immoPluginHelper->getUniqueImmoIdByImmoobject($immoobject);
                        $excludedImmoIDs[] = $uniqueImmoID;
                    }
                    
                    $this->log('Send notification to ' . $searchagentObject->getContactEmail());
                    
                    // successfully sent email to recipient?
                    if(($sent = $this->sendSearchagentNotification($searchagentObject, $immoobjects)) == true) {
                        
                        $this->log('E-Mail successfully sent');
                        
                        $searchagentObject->setResultsSent(implode(',', $excludedImmoIDs));     // set the sent items to the object
                        $this->searchagentRepository->update($searchagentObject);               // update object in database
                        $persistenceManager->persistAll();                                      // commit changes
                        
                        ++$iSentMessages;
                        
                    }  else {
                        
                        $this->log('Failed to sent E-Mail');
                    }
                }
            }

        } catch (SqlErrorException $ex) {

            $this->log('Exception: ' .(string)$ex);
            $this->log('Exception: ' . var_export($searchagent_data, true));
            
            throw $ex;
            
        } catch (\Exception $ex) {

            $this->log('Exception: ' .(string)$ex);
            
            throw $ex;
        }

        $this->closeLog();
        $this->setNotificationTsPath('settings.searchAgent.notification');
        $this->sendNotification('Sending of searchagents succeeded. ' . $iSentMessages . ' messages were sent.');
        
        return true;
    }

    /**
     * 
     * @param \TYPO3\MbxRealestate\Domain\Model\Searchagent $searchagentObject
     * @param \TYPO3\CMS\Extbase\Persistence\Generic\QueryResult $immoobjects
     * @return boolean
     * @todo make sender email variable/dynamic !!!
     */
    private function sendSearchagentNotification(Searchagent $searchagentObject, $immoobjects) {

        $mailTypes = array(
            'plain'     =>  array(
                'mime'  => 'text/plain'
            ),
            'html'      =>  array(
                'mime'  => 'text/html'
            )
        );
        
        $viewURL = $this->searchagentHelper->getViewLink($searchagentObject->getUid(), $searchagentObject->getContactEmail());
        $unsubscribeURL = $this->searchagentHelper->getUnsubscribeLink($searchagentObject->getUid(), $searchagentObject->getContactEmail());
        
        /** 
         * @var $message \TYPO3\CMS\Core\Mail\MailMessage 
         */
        $message = $this->objectManager->get(MailMessage::class);
        
        foreach($mailTypes as $fileType => $mailData) {
            
            $emailView = $this->objectManager->get(StandaloneView::class);

            $emailView->setTemplatePathAndFilename($this->getEmailFile($fileType));
            $emailView->assignMultiple(array(
                'searchagentJobView'        => $viewURL,
                'searchagentJobUnsubscribe' => $unsubscribeURL,
                'immoobjects'               => $immoobjects,
                'immocount'                 => $immoobjects->count()
            ));
            
            $message->setBody($emailView->render(), $mailData['mime']);
            
            // unset var for next loop
            unset($emailView);
        }

        $to = $searchagentObject->getContactEmail();
        $from = $this->immoPluginHelper->getPluginSettings('settings.searchAgent.email');
        
        if(!filter_var($from, FILTER_VALIDATE_EMAIL) || !filter_var($to, FILTER_VALIDATE_EMAIL)) {

            throw new \Exception('Failed to send E-Mail because recipient "' . $to . '" or sender "' . $from . '" invalid!');
            
        }  else {
            
            $message->setTo($to)
                    ->setFrom($from)
                    ->setSubject($this->immoPluginHelper->getPluginSettings('settings.searchAgent.notification.email.subject'))
                    ;

            return $message->send();
        }
        
    }
    
    /**
     * Returns the absolute path where the email template file is located
     * @param string $filetype
     * @return string
     */
    private function getEmailFile($filetype = 'plain') {
        
        $templatePath = $this->immoPluginHelper->getPluginSettings('settings.searchAgent.notification.email.templateFilePath');
        $templateFile = $this->immoPluginHelper->getPluginSettings('settings.searchAgent.notification.email.templateFileName' . ucfirst($filetype));
        
        return GeneralUtility::getFileAbsFileName($templatePath) . $templateFile;
    }

    /**
     * Opens the logging file
     */
    protected function initLog() {
        
        $file = $this->immoPluginHelper->getPluginSettings('settings.searchAgent.logfile');
        $this->fHandle = fopen(GeneralUtility::getFileAbsFileName($file), 'a+');
        
        $this->log($this->getPaddedString());
        $this->log($this->getPaddedString(' START SEARCH-AGENT COMMANDER '));
        $this->log($this->getPaddedString());
    }
    
    /**
     * Closing the log file
     */
    protected function closeLog() {
        
        $this->log($this->getPaddedString());
        $this->log($this->getPaddedString(' END SEARCH-AGENT COMMANDER '));
        $this->log($this->getPaddedString());
        
        fclose($this->fHandle);
    }
    
    /**
     * @param string $str
     * @return string
     */
//    protected function getPaddedString($str = '') {
//        
//        return str_pad($str, self::LOG_ROUTE, '#', 2);
//    }
    
    /**
     * @param string $message
     */
    public function log($message) {
        
        fwrite($this->fHandle, $message . "\n");
    }
}

