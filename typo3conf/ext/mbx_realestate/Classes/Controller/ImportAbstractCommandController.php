<?php

namespace TYPO3\MbxRealestate\Controller;


use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface;
use TYPO3\MbxRealestate\Domain\Repository\ImmoaddressRepository;
use TYPO3\MbxRealestate\Domain\Repository\ImmocontactRepository;
use TYPO3\MbxRealestate\Domain\Repository\ImmofeatureRepository;
use TYPO3\MbxRealestate\Domain\Repository\ImmoimageRepository;
use TYPO3\MbxRealestate\Domain\Repository\ImmoobjectRepository;
use TYPO3\MbxRealestate\Helper\Exception\ImportImmoException;
use TYPO3\MbxRealestate\Helper\ImmoPluginHelper;
use TYPO3\MbxRealestate\Helper\Import\Interfaces\ImporterInterface;

/**
 * Description of ImportCommandController
 * 
 * Controller for Cronjobs using Sheduler Extension
 *
 * @author denis.krueger
 * @package mbx_realestate
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 */
class ImportAbstractCommandController extends \TYPO3\CMS\Extbase\Mvc\Controller\CommandController {

    CONST IMPORT_TYPE_CSV = 'CSV';
    CONST IMPORT_TYPE_XML = 'XML';
    CONST LOG_ROUTE = 80;
    
    CONST STATE_OK = 'OK';
    CONST STATE_ERROR = 'ERROR';
    CONST STATE_NOTIFICATION = 'NOTIFICATION';
    
    CONST IMPORT_TYPE_FULL = 'full';
    CONST IMPORT_TYPE_SINGLE = 'single';    
    
    /**
     * @var ImporterInterface
     */
    protected $importer;
    
    /**
     * Contains the child process name (used for logging etc.)
     * @var string
     */
    private $commandType;
    
    /**
     * The timestamp when started the import
     * @var int
     */
    protected $startTime;
    
    /**
     * The command controller that is extending this class. (e.g. ImportImmoCommandController)
     * @var \TYPO3\CMS\Extbase\Mvc\Controller\CommandController 
     */
    protected $childCommandController;
    
    /**
     * Contains the supported file types for importing
     * @var array
     */
    private static $supportedImportTypes = array(
        self::IMPORT_TYPE_CSV, self::IMPORT_TYPE_XML
    );
       
    /**
     * @var ImmoPluginHelper
     */
    public $immoPluginHelper;
    
    /**
     * configurationManager
     * @var \TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface
     */
    protected $configurationManager;

    /**
     * @var string
     */
    protected $notificationTsPath = null;
    
    // <editor-fold defaultstate="collapsed" desc="repositories">

    /**
     * ImmoobjectRepository
     *
     * @var ImmoobjectRepository
     */
    protected $immoobjectRepository;

    /**
     * ImmofeatureRepository
     *
     * @var ImmofeatureRepository
     */
    protected $immofeatureRepository;

    /**
     * ImmoaddressRepository
     *
     * @var ImmoaddressRepository
     */
    protected $immoaddressRepository;

    /**
     * ImmocontactRepository
     *
     * @var \TYPO3\MbxRealestate\Domain\Repository\ImmocontactRepository
     */
    protected $immocontactRepository;
    
    /**
     * ImmoimageRepository
     *
     * @var \TYPO3\MbxRealestate\Domain\Repository\ImmoimageRepository
     */
    protected $immoimageRepository;
    
    // </editor-fold>
    
    
    // <editor-fold defaultstate="collapsed" desc="injects">
    
    /**
     * injectConfigurationManager
     * 
     * @param \TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface $configurationManager
     */
    public function injectConfigurationManager(ConfigurationManagerInterface $configurationManager) {
        $this->configurationManager = $configurationManager;
    }
    
    /**
     * injectImmoobjectRepository
     *
     * @param \TYPO3\MbxRealestate\Domain\Repository\ImmoobjectRepository $immoobjectRepository
     */
    public function injectImmoobjectRepository(ImmoobjectRepository $immoobjectRepository) {
        $this->immoobjectRepository = $immoobjectRepository;
    }

    /**
     * injectImmoaddressRepository
     *
     * @param \TYPO3\MbxRealestate\Domain\Repository\ImmoaddressRepository $immoaddressRepository
     */
    public function injectImmoaddressRepository(ImmoaddressRepository $immoaddressRepository) {
        $this->immoaddressRepository = $immoaddressRepository;
    }
    
    /**
     * injectImmocontactRepository
     *
     * @param \TYPO3\MbxRealestate\Domain\Repository\ImmocontactRepository $immocontactRepository
     */
    public function injectImmocontactRepository(ImmocontactRepository $immocontactRepository) {
        $this->immocontactRepository = $immocontactRepository;
    }
    
    /**
     * injectImmofeatureRepository
     *
     * @param \TYPO3\MbxRealestate\Domain\Repository\ImmofeatureRepository $immofeatureRepository
     */
    public function injectImmofeatureRepository(ImmofeatureRepository $immofeatureRepository) {
        $this->immofeatureRepository = $immofeatureRepository;
    }
    
    /**
     * injectImmoimageRepository
     *
     * @param \TYPO3\MbxRealestate\Domain\Repository\ImmoimageRepository $immoimageRepository
     */
    public function injectImmoimageRepository(ImmoimageRepository $immoimageRepository) {
        $this->immoimageRepository = $immoimageRepository;
    }
    
    // </editor-fold>
    
    /**
     * initialize the command controller and starts logging
     * 
     * @param class $childClass
     * @return \TYPO3\MbxRealestate\Controller\ImportAbstractCommandController
     */
    protected function initCommandController($childClass) {
        
        $className = str_replace('CommandController', '', get_class($childClass));
        $namespacePath = explode('\\', $className);
        
        $this->childCommandController = $childClass;
        $this->commandType = array_pop($namespacePath);
        $this->startTime = time();
        
        $this->immoPluginHelper = ImmoPluginHelper::getInstance();
        
        $this->initLog();
        $this->log('Current date: ' . date('Y-m-d H:i:s', $this->startTime));
        
        return $this;
    }
    
    /**
     * 
     * @return boolean
     */
    public function importCommand() {
        
    }

    /**
     * @param string $msg
     * @throws \TYPO3\MbxRealestate\Helper\Exception\ImportImmoException
     */
    public static function throwException($msg) {
        
        throw new ImportImmoException($msg);
    }
    
    /**
     * This method checks if the configured filetype is valid for parsing
     * 
     * @param string $filetype
     * @return boolean
     */
    protected function isValidFileTypeConfigured($filetype) {
        
        return in_array(strtoupper($filetype), self::$supportedImportTypes);
    }


    /**
     * Opens the logging file
     */
    protected function initLog() {
        
        $file = $this->immoPluginHelper->getPluginSettings($path = 'settings.import.log.' . lcfirst($this->commandType) . '.logfile');
        $this->fHandle = fopen(GeneralUtility::getFileAbsFileName($file), 'a+');
        
        $this->log($this->getPaddedString());
        $this->log($this->getPaddedString(' START ' . $this->commandType . ' COMMANDER '));
        $this->log($this->getPaddedString());
    }
    
    /**
     * Closing the log file
     */
    protected function closeLog() {
        
        $this->log($this->getPaddedString());
        $this->log($this->getPaddedString(' END ' . $this->commandType . ' COMMANDER '));
        $this->log($this->getPaddedString());
        
        fclose($this->fHandle);
    }
    
    /**
     * @param string $str
     * @return string
     */
    protected function getPaddedString($str = '') {
        
        return str_pad($str, self::LOG_ROUTE, '#', 2);
    }
    
    /**
     * @param string $message
     * @param boolean $nl
     */
    public function log($message, $nl = true) {
        
        fwrite($this->fHandle, $message . ($nl ? "\n" : ""));
    }
    
    /**
     * @return string|null
     */
    protected function getNotificationTsPath() {
        return $this->notificationTsPath;
    }

    /**
     * @param string $notificationTsPath
     * @return \TYPO3\MbxRealestate\Controller\ImportAbstractCommandController
     */
    protected function setNotificationTsPath($notificationTsPath) {
        $this->notificationTsPath = $notificationTsPath;
        return $this;
    }

    /**
     * Returns the command specific TS notification path.
     * @return boolean|string
     */
    protected function getNotificationPath() {
        
        $path = $this->getNotificationTsPath();
        
        if(empty($path)) {
            
            if(!empty($this->importer)) {
                return 'settings.import.' . $this->importer->getScope() . '.notification';
            }
            
            return false;
        }
        
        return $path;        
    }
        
    /**
     * Sending an email message to the admin who is registered in typoscript. The message contains the 
     * current state of the import process and can be used for sending success and/or error messages.
     * 
     * @param string $msg
     * @param string $state
     * @return boolean|null|int
     */
    protected function sendNotification($msg, $state = self::STATE_OK) {
        
        $helper = $this->immoPluginHelper;
        
        $notificationPath = $this->getNotificationPath();
        $notificationSettingsGlobal = $helper->getPluginSettings('settings.import.notification.');
        
        if(!empty($notificationPath)) {
            
            $notificationSettingsLocal   = $helper->getPluginSettings($notificationPath);
            $notificationSettingsComplex = $helper->getPluginSettings($notificationPath . '.');
        
            if(empty($notificationSettingsLocal)) {

                if(empty($notificationSettingsComplex)) {
                    return null;
                }

                $notificationSettingsLocal = $notificationSettingsComplex;
            } else {

                $notificationSettingsLocal = array('to' => $notificationSettingsLocal);
            }
        }
        
        $notificationSettingsMerged = array_merge((array)$notificationSettingsGlobal, (array)$notificationSettingsLocal);

        if(!empty($notificationSettingsMerged['mbx_cnt.'])) {
            
            $mbxCntTask = $notificationSettingsMerged['mbx_cnt.']['task'];
            $mbxCntSecret = $notificationSettingsMerged['mbx_cnt.']['secret'];
            
            $mbxCntCommunicatorFile = \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('mbx_realestate') . 'Classes/Helper/MbxCntCommunicator.php';
            
            if(!file_exists($mbxCntCommunicatorFile) || !require_once($mbxCntCommunicatorFile)) {
                $this->log('Unable to communicate MBX CNT because communicator class file does not exists or could not be loaded: ' . $mbxCntCommunicatorFile);
            }

            $mbxCntCommunicator = new \mbxCntCommunicator($mbxCntTask, $mbxCntSecret);
            $mbxCntCommunicator->setMessage($msg);
            
            switch($state) {
                case self::STATE_OK :
                    $mbxCntCommunicator->setState(\mbxCntCommunicator::TASK_STATE_OK);
                    break;
                case self::STATE_ERROR :
                case self::STATE_NOTIFICATION :
                default :
                    $mbxCntCommunicator->setState(\mbxCntCommunicator::TASK_STATE_ERROR);
                    break;
            }
            
            $apiResponse = $mbxCntCommunicator->submit();
            
            if($apiResponse === true) {
                $this->log('Notified successfully the MBX CNT!');
                return true;
            }
                    
            $this->log('MBX CNT Request: ' . $mbxCntCommunicator->getLatestRequestUrl());
            $this->log('Failed to notify the MBX CNT: ' . (string)$apiResponse['message'] . ', Code ' . (string)$apiResponse['status']);
            
            return false;
            
        } else {
            
            $subject = $notificationSettingsMerged['subject'];
            $emailTo = $notificationSettingsMerged['to'];
            $emailFrom = $notificationSettingsMerged['from'];

            if(!isset($emailTo) || !filter_var($emailTo, FILTER_VALIDATE_EMAIL)) {

                $this->log('No valid email set to send notification about current import process state! The value for email is "' . $emailTo . '".');
                return false;
            }

            $childClassName = get_class($this->childCommandController);

            $subject = (empty($subject) ? 'Command controller state for ' . $childClassName : $subject);
            $emailFrom = (empty($emailFrom) || !filter_var($emailFrom, FILTER_VALIDATE_EMAIL) ? $emailTo : $emailFrom);

            $body = 'Command state: <strong>' . $state . '</strong><br />'
                . 'Command controller: <strong>' . $childClassName . '</strong><br/>'
                . 'The importer <strong>' . $this->commandType . '</strong> is responding the following message:<br/>'
                . '<br/>'
                . $msg
                . '<br /><br />---End of message';

            $message = $this->objectManager->get(MailMessage::class);
            $message
                ->setSubject($subject)
                ->setBody($body, 'text/html')
                ->setTo($emailTo)
                ->setFrom($emailFrom)
            ;

            return $message->send();
        }
        
    }
    
    public function __destruct() {
        
//        $saver = $this->importer->getImporterSaver();
//        
//        unset($saver);
//        unset($this->importer);
        
//        $this->closeLog();
    }
}