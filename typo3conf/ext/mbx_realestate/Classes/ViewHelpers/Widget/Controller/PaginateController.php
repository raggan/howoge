<?php

/**
 * @link http://www.npostnik.de/typo3/eigenes-template-im-fluid-pagination-widget/
 */

namespace TYPO3\MbxRealestate\ViewHelpers\Widget\Controller;

/**
 * This class is a demo view helper for the Fluid templating engine.
 *
 * @package TYPO3
 * @subpackage Fluid
 * @version
 */
class PaginateController extends \TYPO3\CMS\Fluid\ViewHelpers\Widget\Controller\PaginateController {

    /**
     * @var array
     */
    protected $configuration = array('itemsPerPage' => 10, 'customQuery' => FALSE, 'maximumNumberOfLinks' => 99);

    /**
     * @param integer $currentPage
     * @return void
     */
    public function indexAction($currentPage = 1) {
        // set current page
        $this->currentPage = (integer) $currentPage;

        if ($this->currentPage < 1) {
            $this->currentPage = 1;
        }
        
        if ($this->currentPage > $this->numberOfPages) {
            // set $modifiedObjects to NULL if the page does not exist
            $modifiedObjects = NULL;
        } else {
            // modify query
            $itemsPerPage = (integer) $this->configuration['itemsPerPage'];
            
            if((bool)(int)$this->configuration['customQuery'] === true) {

                $modifiedObjects = array_slice($this->objects->toArray(), (integer) ($itemsPerPage * ($this->currentPage - 1)), $itemsPerPage);
                
            }  else {
                
                $query = $this->objects->getQuery();
                $query->setLimit($itemsPerPage);
                if ($this->currentPage > 1) {
                    $query->setOffset((integer) ($itemsPerPage * ($this->currentPage - 1)));
                }
                $modifiedObjects = $query->execute();
            }
            
        }

        $this->view->assign('configuration', $this->configuration);
        $this->view->assign('pagination', $this->buildPagination());

        $GLOBALS['paginated_' . $this->widgetConfiguration['as']] = $modifiedObjects;
    }

    /**
     * to define your own template you can use following TS:
     * plugin.tx_YOUREXT {
          view {
              widget.TYPO3\YOUREXT\ViewHelpers\Widget\PaginateViewHelper.templateRootPath = ...
          }
       }
     * 
     * @param \TYPO3\CMS\Fluid\View\TemplateView $view
     * @return void
     * @todo implement logic for overriding widget template paths (tx_extension.view.widget.<WidgetViewHelperClassName>.templateRootPath...)
     */
    protected function setViewConfiguration(\TYPO3\CMS\Fluid\View\TemplateView $view) {
        $extbaseFrameworkConfiguration = $this->configurationManager->getConfiguration(\TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface::CONFIGURATION_TYPE_FRAMEWORK);
        $widgetViewHelperClassName = $this->request->getWidgetContext()->getWidgetViewHelperClassName();
        if (isset($extbaseFrameworkConfiguration['view']['widget'][$widgetViewHelperClassName]['templateRootPath']) && strlen($extbaseFrameworkConfiguration['view']['widget'][$widgetViewHelperClassName]['templateRootPath']) > 0 && method_exists($view, 'setTemplateRootPath')) {
            $view->setTemplateRootPath(\TYPO3\CMS\Core\Utility\GeneralUtility::getFileAbsFileName($extbaseFrameworkConfiguration['view']['widget'][$widgetViewHelperClassName]['templateRootPath']));
        }
    }

}
