<?php
namespace TYPO3\MbxRealestate\ViewHelpers\Widget;

use TYPO3\MbxRealestate\ViewHelpers\Widget\Controller\PaginateController;

/**
 * 
 * 
 * <namespace:widget.paginate objects="{originalObjectsQueryResult}" as="iterateAbleListName" configuration="{customQuery : 1||0, asPager : 'renderedPager', itemsPerPage: n, maximumNumberOfLinks: n}">                
        
        <div class="paginator-wrapper">
            <strong>Page</strong>
            <div>{renderedPager}</div>
        </div>

        <ul class="general-results-list">            
            <f:for each="{iterateAbleListName}" as="iterateItem" iteration="itemIterator">
                <li class="realestate-list-item">{iterateItem.value}...</li>
            </f:for>
        </ul>
 
    </namespace:widget.paginate>
 * 
 */
class PaginateViewHelper extends \TYPO3\CMS\Fluid\ViewHelpers\Widget\PaginateViewHelper {

	/**
	 * @var PaginateController
	 */
	protected $controller;

	/**
	 * @param PaginateController $controller
	 * @return void
	 */
	public function injectPaginateController(PaginateController $controller) {
		$this->controller = $controller;
	}

        /**
	 * @param \TYPO3\CMS\Extbase\Persistence\QueryResultInterface $objects
	 * @param string $as
	 * @param array $configuration
	 * @return string
	 */
	public function render(\TYPO3\CMS\Extbase\Persistence\QueryResultInterface $objects, $as, array $configuration = array('itemsPerPage' => 10, 'maximumNumberOfLinks' => 99)) {
        $this->templateVariableContainer->add($configuration['asPager'], $this->initiateSubRequest());
        $this->templateVariableContainer->add($as, $GLOBALS['paginated_' . $as]);

        return $this->renderChildren();
	}
}