<?php

namespace TYPO3\MbxRealestate\ViewHelpers;

use TYPO3\MbxRealestate\Domain\Model\Immoobject;
use TYPO3\MbxRealestate\Domain\Repository\ImmoimageRepository;
use TYPO3\MbxRealestate\Helper\ImmoPluginHelper;

/**
 * This class is a demo view helper for the Fluid templating engine.
 *
 * @package TYPO3
 * @subpackage Fluid
 * @version
 */
class GalleryViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper {

    /**
     * 
     * @param \TYPO3\MbxRealestate\Domain\Model\Immoobject $immoobject
     * @param \TYPO3\MbxRealestate\Domain\Repository\ImmoimageRepository $immoimageRepository
     * 
     * @return string
     */
    public function render(Immoobject $immoobject, ImmoimageRepository $immoimageRepository) {
        
        $pluginHelper = ImmoPluginHelper::getInstance();
        
        $floorplanCategory = $pluginHelper->getPluginSettings('settings.images.floorplanCategory');

        // search all images of $immoobject NOT of type floorplan
        $images = $immoimageRepository->findByType(array($floorplanCategory), $immoobject, ImmoimageRepository::TYPE_NOT_IN);
        
        if($images instanceof \TYPO3\CMS\Extbase\Persistence\Generic\QueryResult) {
            
            // drop old assignment if 'images' was already declared
            if($this->templateVariableContainer->exists('images')) {
                
                $this->templateVariableContainer->remove('images');
            }
            $this->templateVariableContainer->add('images', $images);
            
            return $this->renderChildren();
            
        }  else {
            
            return '';
        }
        
    }
}

?>