<?php

namespace TYPO3\MbxRealestate\ViewHelpers\Immocontact;

use TYPO3\MbxRealestate\Domain\Model\Immoaddress;
use TYPO3\MbxRealestate\Domain\Repository\ImmoaddressRepository;
use TYPO3\MbxRealestate\Domain\Repository\ImmocontactRepository;

/**
 * This class is a demo view helper for the Fluid templating engine.
 *
 * @package TYPO3
 * @subpackage Fluid
 * @version
 */
class AssignViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper {

    /**
     *
     * @param \TYPO3\MbxRealestate\Domain\Model\Immoaddress $immoaddress
     * @param \TYPO3\MbxRealestate\Domain\Repository\ImmocontactRepository $immocontactRepository
     * @param array $address
     * @param string|array $contactType
     * @param string $assign
     * @param string $contactOrder
     *
     * @return type
     */
    public function render(Immoaddress $immoaddress = null, ImmocontactRepository $immocontactRepository = null, $address = null, $contactType = null, $assign = null, $contactOrder = null) {

        $varName = (empty($assign) ? 'immocontact' : $assign);

        // drop old assignment if 'immocontact' was already declared
        if($this->templateVariableContainer->exists($varName)) {

            $this->templateVariableContainer->remove($varName);
        }

        if (!is_null($immoaddress) || (is_null($immoaddress) && !empty($address))) {

            if (is_null($immocontactRepository)) {
                $immocontactRepository = $this->objectManager->get(ImmocontactRepository::class);
            }

            if (is_null($immoaddress) && !empty($address)) {
                $immoaddressRepository = $this->objectManager->get(ImmoaddressRepository::class);
                $immoaddress = $immoaddressRepository->findByAddress($address['street'],$address['streetnr'],$address['zip'],$address['city']);
            }

            $immocontactResult = $immocontactRepository->findByAddressAndType($immoaddress, $contactType);

            if($immocontactResult->count()) {

                // store contact types as array keys to access properly in templates
                if(empty($contactType) || is_array($contactType)) {

                    $results = array();

                    foreach($immocontactResult as $immocontact) {
                        $results[$immocontact->getContactType()] = $immocontact;
                    }

                    // <editor-fold defaultstate="collapsed" desc="order the contacts if contactOrder property in viewHelper was set">
                    if(!empty($contactOrder)) {
                        if(is_string($contactOrder)) {
                            $contactOrder = explode(',', $contactOrder);
                        }
                        
                        $resultsOrdered = array();
                        
                        foreach($contactOrder as $orderKey) {
                            if(array_key_exists($orderKey, $results)) {
                                $resultsOrdered[$orderKey] = $results[$orderKey];
                                unset($results[$orderKey]);
                            }
                        }

                        $results = array_merge($resultsOrdered, $results);
                    }
                    // </editor-fold>
                    
                    $this->templateVariableContainer->add($varName, $results);

                // store first contact for usage in template
                }  else {

                    $this->templateVariableContainer->add($varName, $immocontactResult->getFirst());
                }

            }
        } else {
            $this->templateVariableContainer->add($varName, array());
        }

        return $this->renderChildren();
    }
}

?>