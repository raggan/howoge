<?php
namespace TYPO3\MbxRealestate\ViewHelpers\Immocontact;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2014 Denis Krüger <denis.krueger@mindbox.de>, Mindbox GmbH
 *  Anke Häslich <anke.haeslich@mindbox.de>, Mindbox GmbH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * ViewHelper to generate a json of contact objects used in a google map
 *
 * @package mbx_realestate
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 */
class GetJavascriptMapDataViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper {

    /**
     *
     * @param array $immocontacts
     * @param string $as
     * @return string
     */
    public function render(array $immocontacts, $as = 'immoJson') {

        // drop old assignment if 'immoJson' was already declared
        if($this->templateVariableContainer->exists($as)) {
            $this->templateVariableContainer->remove($as);
        }

        $data = array();
        foreach ($immocontacts as $immocontact) {
            $address = $immocontact->getContactStreet() . ' ' . $immocontact->getContactStreetnr()
                        . ',' . $immocontact->getContactZip()
                        . ',' .  $immocontact->getContactCity();

            $data[] = array(
                'address' =>  $address,
                'data' => array(
                    'name'      => $immocontact->getContactName(),
                    'address'   => $address,
                    'url'       => $immocontact->getContactUrl()
                )
            );
        }

        $this->templateVariableContainer->add($as, json_encode($data));

        return $this->renderChildren();
    }
}