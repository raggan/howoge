<?php

namespace TYPO3\MbxRealestate\ViewHelpers\ArrayOp;

/**
 * This class is a demo view helper for the Fluid templating engine.
 *
 * @package TYPO3
 * @subpackage Fluid
 * @version
 */
class DynamicViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper {

    /**
     * 
     * @param string $format
     * @param string $assign
     * @param array $data
     */
    public function render($format, $assign, $data) {

        $path = explode('.', $format);
        $val = $data[array_shift($path)];
        $match = null;

        while($key = array_shift($path)) {
            
            if(preg_match('/^\[(?<KEY>.*)\]$/', $key, $match)) {
                $key = $data[$match['KEY']];
            }
            
            $val = $val[$key];
        }
        
        if ($this->templateVariableContainer->exists($assign) === tue) {
            $this->templateVariableContainer->remove($assign);
        }
        
        $this->templateVariableContainer->add($assign, $val);
    }
}