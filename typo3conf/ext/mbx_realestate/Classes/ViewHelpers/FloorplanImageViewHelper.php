<?php

namespace TYPO3\MbxRealestate\ViewHelpers;

/**
 * This class is a demo view helper for the Fluid templating engine.
 *
 * @package TYPO3
 * @subpackage Fluid
 * @version
 */
class FloorplanImageViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper {

    /**
     * 
     * @param \TYPO3\MbxRealestate\Domain\Model\Immoobject $immoobject
     * @return string
     */
    public function render(\TYPO3\MbxRealestate\Domain\Model\Immoobject $immoobject) {
        
        $immoimage = null;
        $pluginHelper = \TYPO3\MbxRealestate\Helper\ImmoPluginHelper::getInstance();
        
        $previewCategory = $pluginHelper->getPluginSettings('settings.images.floorplanCategory');
        $imagePath = $pluginHelper->getPluginSettings('settings.import.publicImagePath');
        
        foreach($immoobject->getImmoimage() as $tmpImage) {
        
            if($tmpImage->getCategory() == $previewCategory) {
                
                $immoimage = $tmpImage;
                break;
            }
        }
        
        if($immoimage instanceof \TYPO3\MbxRealestate\Domain\Model\Immoimage) {
            
            // drop old assignment if 'src' was already declared
            if($this->templateVariableContainer->exists('src')) {
                
                $this->templateVariableContainer->remove('src');
            }
            $this->templateVariableContainer->add('src', $imagePath . $immoimage->getFilename());
            
            return $this->renderChildren();
            
        }  else {
            
            return '';
        }
        
    }
}

?>