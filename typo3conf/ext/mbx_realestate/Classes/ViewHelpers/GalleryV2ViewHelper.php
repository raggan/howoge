<?php

namespace TYPO3\MbxRealestate\ViewHelpers;
use TYPO3\MbxRealestate\Domain\Model\Immoobject;
use TYPO3\MbxRealestate\Domain\Repository\ImmoimageRepository;

/**
 * This class is a demo view helper for the Fluid templating engine.
 *
 * @package TYPO3
 * @subpackage Fluid
 * @version
 */
class GalleryV2ViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper {

    /**
     * 
     * @param \TYPO3\MbxRealestate\Domain\Model\Immoobject $immoobject
     * @param \TYPO3\MbxRealestate\Domain\Repository\ImmoimageRepository $immoimageRepository
     * @param array $configuration Additional configuration
     * 
     * @return string
     */
    public function render(Immoobject $immoobject, ImmoimageRepository $immoimageRepository, $configuration) {

        $categories = array();
        $typeSearch = ImmoimageRepository::TYPE_NOT_IN;
        $limit = null;
        
        if(!empty($configuration['type'])) {
            
            if(in_array($type = $configuration['type'], array(ImmoimageRepository::TYPE_NOT_IN, ImmoimageRepository::TYPE_IN))) {
                $typeSearch = $type;
            } elseif($type == 'include') {
                $typeSearch = ImmoimageRepository::TYPE_IN;
            }
        }
        
        if(!empty($configuration['categories'])) {
            $categories = $configuration['categories'];
        }
        
        if(!empty($configuration['limit'])) {
            $limit = $configuration['limit'];
        }
        
        $images = $immoimageRepository->findByType((array)$categories, $immoobject, $typeSearch, $limit);
        
        if($images instanceof \TYPO3\CMS\Extbase\Persistence\Generic\QueryResult) {
            
            // drop old assignment if 'images' was already declared
            if($this->templateVariableContainer->exists('images')) {
                
                $this->templateVariableContainer->remove('images');
            }
            $this->templateVariableContainer->add('images', $images);
            
            return $this->renderChildren();
            
        }  else {
            
            return '';
        }
        
    }
}