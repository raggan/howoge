<?php

namespace TYPO3\MbxRealestate\ViewHelpers\Notepad;

use TYPO3\MbxRealestate\Domain\Repository\ImmoobjectRepository;

/**
 * This class is a demo view helper for the Fluid templating engine.
 *
 * @package TYPO3
 * @subpackage Fluid
 * @version
 */
class HasImmoobjectsViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractConditionViewHelper {

    /**
     * @return string
     */
    public function render() {
        $immoobjectRepository = $this->objectManager->get(ImmoobjectRepository::class);
        $cookieObjects = $immoobjectRepository->findByNotepad();

        if ($cookieObjects->count() > 0) {
            return $this->renderThenChild();
        } else {
            return $this->renderElseChild();
        }
    }
}

?>