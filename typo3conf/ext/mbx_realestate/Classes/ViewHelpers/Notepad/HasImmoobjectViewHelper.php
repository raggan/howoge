<?php

namespace TYPO3\MbxRealestate\ViewHelpers\Notepad;

/**
 * This class is a demo view helper for the Fluid templating engine.
 *
 * @package TYPO3
 * @subpackage Fluid
 * @version
 */
class HasImmoobjectViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractConditionViewHelper {

    /**
     * 
     * @param \TYPO3\MbxRealestate\Domain\Model\Immoobject $immoobject
     * 
     * @return string
     */
    public function render(\TYPO3\MbxRealestate\Domain\Model\Immoobject $immoobject = null) {
        
        $cookie = (array)json_decode(\TYPO3\MbxRealestate\Helper\Cookie::get('notepad'));
        
        if(in_array($immoobject->getUid(), $cookie)) {
            return $this->renderThenChild();
        }  else {
            return $this->renderElseChild();
        }
        
    }
}

?>