<?php

namespace TYPO3\MbxRealestate\ViewHelpers;

/**
 * <mbxrevh:assignFloorplan immoobject="{immoobject}" configuration="{as : 'floorplan', injectInto : images, injectPosition : 2, injectName : 'images'}">
 * 
 * select floorplanImage from {immoobject} 
 * and inject the image into the already existing array 'images' at the position '2' 
 * and assign this new array as variable called 'images'
 * Additionally assign variable 'floorplan' contaning the immoimage data!
 * 
 * This class is a demo view helper for the Fluid templating engine.
 *
 * @package TYPO3
 * @subpackage Fluid
 * @version
 */
class AssignFloorplanViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper {

    /**
     * @var null
     */
    protected $configuration = NULL;

    /**
     * @var string
     */
    protected $as = 'src';

    /**
     * @var null
     */
    protected $immoobject = NULL;

    /**
     * @var null
     */
    protected $previewCategory = NULL;

    /**
     * @var bool
     */
    protected $multipleFloorplans = false;

    /**
     * @var int
     */
    protected $injectPosition = 0;

    /**
     * 
     * @param \TYPO3\MbxRealestate\Domain\Model\Immoobject $immoobject
     * @param array $configuration
     * @return string
     */
    public function render(\TYPO3\MbxRealestate\Domain\Model\Immoobject $immoobject, $configuration) {

        $this->configuration = $configuration;
        $this->immoobject = $immoobject;

        if(!empty($this->configuration['as'])) {
            $this->as = $this->configuration['as'];
        }

        if(!empty($this->configuration['injectPosition'])){
            $this->injectPosition = $this->configuration['injectPosition'];
        }

        $pluginHelper = \TYPO3\MbxRealestate\Helper\ImmoPluginHelper::getInstance();
        
        $this->previewCategory = $pluginHelper->getPluginSettings('settings.images.floorplanCategory');
        $floorplanMultiple = NULL;
        $floorplanMultiple = $pluginHelper->getPluginSettings('settings.images.floorplanMultiple');

        if(isset($floorplanMultiple) && (boolean)$floorplanMultiple){
            $this->multipleFloorplans = TRUE;
        }


/*
        foreach($this->iterateImages() as $image){
            $this->injectImage($image);
            $this->injectPosition++;
        }
*/

        foreach($this->immoobject->getImmoimage() as $tmpImage) {

            if($tmpImage->getCategory() == $this->previewCategory) {

                $this->injectImage($tmpImage);
                $this->injectPosition++;

                if(!$this->multipleFloorplans) {
                    break;
                }
            }
        }

        return $this->renderChildren();
    }
    
    /**
     * @param string $var
     * @param mixed $value
     * @return \TYPO3\MbxRealestate\ViewHelpers\AssignFloorplanViewHelper
     */
    private function addVar($var, $value) {
        
        $this->resetVar($var);
        $this->templateVariableContainer->add($var, $value);
        
        return $this;
    }
    
    /**
     * @param string $var
     * @return \TYPO3\MbxRealestate\ViewHelpers\AssignFloorplanViewHelper
     */
    private function resetVar($var) {
        
        if($this->templateVariableContainer->exists($var)) {
            $this->templateVariableContainer->remove($var);
        }
        
        return $this;
    }

    /**
     * @param $immoimage
     */
    private function injectImage($immoimage){

        if($immoimage instanceof \TYPO3\MbxRealestate\Domain\Model\Immoimage) {

            $this->addVar('has' . ucfirst($this->as), true);
            $this->addVar($this->as, $immoimage);

            if(!empty($this->configuration['injectInto'])) {
//                \TYPO3\CMS\Extbase\Utility\DebuggerUtility::var_dump($this->configuration['injectInto']);
                $currentImages = $this->configuration['injectInto'];
                $injectedImages = array();
                $appended = false;

                // position to inject is set
                if(preg_match('/^\d+$/', $this->injectPosition)) {

                    if(($injectPosition = (int)$this->injectPosition) >= 0) {

                        $a = 0; // a tmp counter to find correct position to add image

                        foreach($currentImages as $currentImage) {
                            if($injectPosition == $a) {
                                $appended = true;
                                $injectedImages[] = $immoimage;
                            }

                            $injectedImages[] = $currentImage;
                            ++$a;
                        }
                    }

                } else {
                    // no inject position set
                    $injectedImages = $currentImages;
                }

                if(!$appended) {

                    // append image to end of currently existing images
                    $injectedImages[] = $immoimage;
                }

                $this->configuration['injectInto'] = $injectedImages;
                $this->addVar($this->configuration['injectName'], $injectedImages);
            }
        }
    }

    /**
     * @return \Generator
     */
    private function iterateImages(){
/*
        foreach($this->immoobject->getImmoimage() as $tmpImage) {

            if($tmpImage->getCategory() == $this->previewCategory) {

                yield $tmpImage;
                if(!$this->multipleFloorplans) break;
            }
        }
*/
    }
}