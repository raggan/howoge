<?php

namespace TYPO3\MbxRealestate\ViewHelpers\Immoobject;

use TYPO3\MbxRealestate\Domain\Model\Immoobject;
use TYPO3\MbxRealestate\Helper\ImmoPluginHelper;

/**
 * This class is a demo view helper for the Fluid templating engine.
 *
 * @package TYPO3
 * @subpackage Fluid
 * @version
 */
class GetShowUrlViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper {

    /**
     * @param \TYPO3\MbxRealestate\Domain\Model\Immoobject $immoobject
     * @param string $as
     *
     * @return type
     */
    public function render(Immoobject $immoobject, $as = null) {

        $pluginHelper = ImmoPluginHelper::getInstance();
        $uriBuilder = $this->controllerContext->getUriBuilder();

        $arguments = array(
            'immoobject'    =>  $immoobject->getUid()
        );

        $uri = $uriBuilder  ->reset()
                            ->setCreateAbsoluteUri(true)
                            ->setTargetPageUid($pluginHelper->getPluginSettings('settings.search.detailPage'))
                            ->setArgumentsToBeExcludedFromQueryString(array(
                                'cHash'
                            ))
                            ->uriFor(null, $arguments, 'Immoobject', 'mbxrealestate', 'pi1');

        $varName = (empty($as) ? 'immoUrl' : $as);

        if(empty($as)) {

            return $uri;

        } else {

            // drop old assignment if 'immoUrl' was already declared
            if($this->templateVariableContainer->exists($varName)) {

                $this->templateVariableContainer->remove($varName);
            }

            $this->templateVariableContainer->add($varName, $uri);

            return $this->renderChildren();
        }
    }

}