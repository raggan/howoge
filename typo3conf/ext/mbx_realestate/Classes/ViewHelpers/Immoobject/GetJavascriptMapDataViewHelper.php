<?php

namespace TYPO3\MbxRealestate\ViewHelpers\Immoobject;


use TYPO3\MbxRealestate\Domain\Model\Immoimage;
use TYPO3\MbxRealestate\Domain\Model\Immoobject;
use TYPO3\MbxRealestate\Domain\Repository\ImmoaddressRepository;
use TYPO3\MbxRealestate\Domain\Repository\ImmoimageRepository;
use TYPO3\MbxRealestate\Helper\Cookie;
use TYPO3\MbxRealestate\Helper\ImmoPluginHelper;

/**
 * This class is a demo view helper for the Fluid templating engine.
 *
 * @package TYPO3
 * @subpackage Fluid
 * @version
 */
class GetJavascriptMapDataViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper {

    /**
     * @var \TYPO3\MbxRealestate\Domain\Repository\ImmoaddressRepository
     */
    private $immoaddressRepository;

    /**
     * @var \TYPO3\MbxRealestate\Domain\Repository\ImmoimageRepository
     */
    private $immoimageRepository;

    /**
     * @var \TYPO3\MbxRealestate\Domain\Model\Immoobject
     */
    private $currentImmoObject;

    /**
     * @var \TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer
     */
    protected $contentObject;

    /**
     * @var \TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface
     */
    protected $configurationManager;

    /**
     * @param \TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface $configurationManager
     *
     * @return void
     */
    public function injectConfigurationManager(\TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface $configurationManager) {
            $this->configurationManager = $configurationManager;
            $this->contentObject = $this->configurationManager->getContentObject();
    }


    /**
     *
     * @param \TYPO3\CMS\Extbase\Persistence\Generic\QueryResult  $immoobjects
     * @param string $as
     *
     * @return type
     */
    public function render(\TYPO3\CMS\Extbase\Persistence\Generic\QueryResult  $immoobjects, $as = null) {

        $pluginHelper = ImmoPluginHelper::getInstance();
        $cookieNotepad = (array)json_decode(Cookie::get('notepad'));

        $districtsMapping = $pluginHelper->getDistrictsMapping();
        $districts = $pluginHelper->parseTS($districtsMapping);

        $this->immoaddressRepository = $this->objectManager->get(ImmoaddressRepository::class);
        $this->immoimageRepository = $this->objectManager->get(ImmoimageRepository::class);

        $varName = (empty($as) ? 'immoJson' : $as);

        // drop old assignment if 'immoJson' was already declared
        if($this->templateVariableContainer->exists($varName)) {

            $this->templateVariableContainer->remove($varName);
        }

        $data = array();
        $uriBuilder = $this->controllerContext->getUriBuilder();

        foreach($immoobjects as $immoobject) {

            $this->currentImmoObject = $immoobject;
            $io =& $this->currentImmoObject;

            $arguments = array(
                'immoobject'    =>  $io->getUid()
            );

            $address = $this->immoaddressRepository->findByImmoId($io->getUnr(), $io->getWi(), $io->getHs());
            $uri = $uriBuilder  ->reset()
                                ->setTargetPageUid($pluginHelper->getPluginSettings('settings.search.detailPage'))
                                ->uriFor('show', $arguments);

            if(!empty($address)) {
                
                $tmp = array();
                $tmp['address'] = array(
                    'street'    =>  $address->getStreet() . ' ' . $address->getStreetnr(),
                    'zip'       =>  $address->getZip(),
                    'city'      =>  $address->getCity(),
                    'district'  =>  $districts[$address->getDistrict()]
                );

                $tmp['general'] = array(
                    'immo'          =>  $io->getUid(),
                    'costGross'     =>  $io->getCostGross(),
                    'areaGeneral'   =>  $io->getAreaGeneral(),
                    'rooms'         =>  $io->getRooms(),
                    'url'           =>  $uri
                );
                $tmp['geo_x'] = $address->getGeoX();
                $tmp['geo_y'] = $address->getGeoY();
                $tmp['time_of_visit'] = $address->getTimeOfVisit();
                $tmp['notepad'] = in_array($io->getUid(), $cookieNotepad);
                $tmp['img'] = $this->getImage($immoobject);

                $data[] = $tmp;
            }
            
        }

        $this->templateVariableContainer->add($varName, json_encode($data));

        return $this->renderChildren();
    }

    /**
     *
     * @param \TYPO3\MbxRealestate\Domain\Model\Immoobject $immoobject
     * @return string|null
     */
    private function getImage(Immoobject $immoobject) {

        $pluginHelper = ImmoPluginHelper::getInstance();
        $previewCategory = $pluginHelper->getPluginSettings('settings.images.previewCategory');
        //$imagePath = $pluginHelper->getPluginSettings('settings.import.publicImagePath');

        $imageSetup = array(
            'width'     => '77c',
            'height'    => '77c'
        );

        foreach($immoobject->getImmoimage() as $tmpImage) {

            if($tmpImage->getCategory() == $previewCategory) {

                $immoimage = $tmpImage;
                break;
            }
        }

        if($immoimage instanceof Immoimage) {

            $src = $immoimage->getImagePath() . $immoimage->getFilename();
            $imageInfo = $this->contentObject->getImgResource($src, $imageSetup);

            if (is_array($imageInfo)) {
                $imageInfo[3] = \TYPO3\CMS\Core\Utility\GeneralUtility::png_to_gif_by_imagemagick($imageInfo[3]);
                $imageSource = $GLOBALS['TSFE']->absRefPrefix . \TYPO3\CMS\Core\Utility\GeneralUtility::rawUrlEncodeFP($imageInfo[3]);
            }

        }  else {

            $src = null;
            $imageSource = null;
        }

        return $imageSource;
    }
}