<?php

namespace TYPO3\MbxRealestate\ViewHelpers\Immoaddress;

use TYPO3\MbxRealestate\Domain\Repository\ImmoaddressRepository;

/**
 * This class is a demo view helper for the Fluid templating engine.
 *
 * @package TYPO3
 * @subpackage Fluid
 * @version
 */
class AssignViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper {

    /**
     * 
     * @param \TYPO3\MbxRealestate\Domain\Model\Immoobject $immoobject
     * @param \TYPO3\MbxRealestate\Domain\Repository\ImmoaddressRepository $immoaddressRepository
     * 
     * @return string
     */
    public function render(\TYPO3\MbxRealestate\Domain\Model\Immoobject $immoobject, ImmoaddressRepository $immoaddressRepository = null) {
        
        // drop old assignment if 'immoaddress' was already declared
        if($this->templateVariableContainer->exists('immoaddress')) {

            $this->templateVariableContainer->remove('immoaddress');
        }

        if (is_null($immoaddressRepository)) {
            $immoaddressRepository = $this->objectManager->get(ImmoaddressRepository::class);
        }
        
        $immoaddressResult = $immoaddressRepository->findByImmoId($immoobject->getUnr(), $immoobject->getWi(), $immoobject->getHs());
        
        if($immoaddressResult instanceof \TYPO3\MbxRealestate\Domain\Model\Immoaddress) {
            
            $this->templateVariableContainer->add('immoaddress', $immoaddressResult);
        }
        

        return $this->renderChildren();
    }
}

?>