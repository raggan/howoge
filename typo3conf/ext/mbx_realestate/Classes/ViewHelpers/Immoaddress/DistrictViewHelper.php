<?php

namespace TYPO3\MbxRealestate\ViewHelpers\Immoaddress;

/**
 * This class is a demo view helper for the Fluid templating engine.
 *
 * @package TYPO3
 * @subpackage Fluid
 * @version
 */
class DistrictViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper {

    /**
     * @param \TYPO3\MbxRealestate\Domain\Model\Immoaddress $immoaddress
     * 
     * @return string
     */
    public function render(\TYPO3\MbxRealestate\Domain\Model\Immoaddress $immoaddress = null) {

        $pluginHelper = \TYPO3\MbxRealestate\Helper\ImmoPluginHelper::getInstance();
        
        $districtsMapping = $pluginHelper->getDistrictsMapping();
        $districts = $pluginHelper->parseTS($districtsMapping);
        
        if(!empty($immoaddress)) {
            
            return $districts[$immoaddress->getDistrict()];
        } else {
            return '';
        }
        
    }
}