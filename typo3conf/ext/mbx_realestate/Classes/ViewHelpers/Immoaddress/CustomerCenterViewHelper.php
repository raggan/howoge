<?php

namespace TYPO3\MbxRealestate\ViewHelpers\Immoaddress;

/**
 * This class is a demo view helper for the Fluid templating engine.
 *
 * @package TYPO3
 * @subpackage Fluid
 * @version
 */
class CustomerCenterViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper {

    /**
     * 
     * @param \TYPO3\MbxRealestate\Domain\Model\Immoaddress $immoaddress
     * @param \TYPO3\MbxRealestate\Domain\Repository\ImmocontactRepository $immocontactRepository
     * 
     * @return type
     */
    public function render(\TYPO3\MbxRealestate\Domain\Model\Immoaddress $immoaddress, \TYPO3\MbxRealestate\Domain\Repository\ImmocontactRepository $immocontactRepository) {

        // drop old assignment if 'customercenter' was already declared
        if($this->templateVariableContainer->exists('customercenter')) {

            $this->templateVariableContainer->remove('customercenter');
        }
        
        $immocontactResult = $immocontactRepository->findByUid($immoaddress->getCustomerCenter());
        
        if($immocontactResult instanceof \TYPO3\MbxRealestate\Domain\Model\Immocontact) {
            
            $this->templateVariableContainer->add('customercenter', $immocontactResult);
        }

        return $this->renderChildren();
    }
}

?>