<?php

namespace TYPO3\MbxRealestate\ViewHelpers\Immoaddress;

/**
 * This class is a demo view helper for the Fluid templating engine.
 *
 * @package TYPO3
 * @subpackage Fluid
 * @version
 */
class GetRawDataViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractConditionViewHelper {

    /**
     * @param \TYPO3\MbxRealestate\Domain\Model\Immoaddress $immoaddress
     * @param string $path
     * @param string $equals
     * @param string $assign
     * @return type
     */
    public function render(\TYPO3\MbxRealestate\Domain\Model\Immoaddress $immoaddress, $path, $equals = null, $assign = null) {

        $data = json_decode($immoaddress->getFullObjectData());

        if(empty($data)) {
            return $this->renderElseChild();
        }

        $val = $this->getItem($data, $path);

        if(is_null($equals)) {
            
            if(empty($val)) {
                return $this->renderElseChild();
            } else {
                
                $this->assignData($val, $assign);
                
                return $this->renderThenChild();
            }
            
        } elseif($equals === $val) {
            
            $this->assignData($val, $assign);
            
            return $this->renderThenChild();
        } else {
            
            return $this->renderElseChild();
        }
    }

    /**
     * 
     * @param mixed $data
     * @param string $assign
     */
    private function assignData($data, $assign = null) {
        
        $varName = (empty($assign) ? 'rawdata' : $assign);

        if($this->templateVariableContainer->exists($varName)) {

            $this->templateVariableContainer->remove($varName);
        }

        $this->templateVariableContainer->add($varName, $data);
    }
    
    /**
     * 
     * @param stdClass $node
     * @param string $key
     * @return mixed
     */
    private function getItem($node, $key) {

        $path = explode('/', $key);
        $firstItem = array_shift($path);

        $data = $node->$firstItem;

        if(is_null($data)) {
            return null;
        }

        if(!empty($path)) {
                   
            while(($pathItem = array_shift($path))) {

                if(empty($data->{$pathItem})) {
                    return null;
                }

                $data = $data->{$pathItem};
            }
        }        

        if(!preg_match('/^\@/', $pathItem)) {

            $data = (array)$data;
            $newArr = array();
            
            foreach($data as $key => $val) {
                $newArr[(is_numeric($key) ? (int)$key : $key)] = $val;
            }

            return $newArr[0];
        } else {
            
            return $data;
        }
        
    }
}