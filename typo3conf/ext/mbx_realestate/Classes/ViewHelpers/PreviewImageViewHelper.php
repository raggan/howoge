<?php

namespace TYPO3\MbxRealestate\ViewHelpers;

use TYPO3\MbxRealestate\Domain\Model\Immoimage;
use TYPO3\MbxRealestate\Domain\Model\Immoobject;
use TYPO3\MbxRealestate\Helper\ImmoPluginHelper;

/**
 * This class is a demo view helper for the Fluid templating engine.
 *
 * @package TYPO3
 * @subpackage Fluid
 * @version
 */
class PreviewImageViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper {

    /**
     *
     * @param \TYPO3\MbxRealestate\Domain\Model\Immoobject $immoobject
     * @return string
     */
    public function render(Immoobject $immoobject) {
        $immoimage = null;
        $pluginHelper = ImmoPluginHelper::getInstance();

        $previewCategory = $pluginHelper->getPluginSettings('settings.images.previewCategory');
        foreach($immoobject->getImmoimage() as $tmpImage) {
            if($tmpImage->getCategory() == $previewCategory) {

                $immoimage = $tmpImage;
                break;
            }
        }

        if($immoimage instanceof Immoimage) {

            // drop old assignment if 'src' was already declared
            if($this->templateVariableContainer->exists('src')) {

                $this->templateVariableContainer->remove('src');
            }

            $this->templateVariableContainer->add('src', $immoimage->getImagePath() . $immoimage->getFilename());
            
            // drop old assignment if 'image' was already declared
            if($this->templateVariableContainer->exists('details')) {
                $this->templateVariableContainer->remove('details');
            }
            $this->templateVariableContainer->add('details', $immoimage);
            

            return $this->renderChildren();

        }  else {

            return '';
        }

    }
}

?>
