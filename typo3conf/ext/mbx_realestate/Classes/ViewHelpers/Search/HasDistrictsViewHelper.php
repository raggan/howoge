<?php

namespace TYPO3\MbxRealestate\ViewHelpers\Search;

/**
 * This class is a demo view helper for the Fluid templating engine.
 *
 * @package TYPO3
 * @subpackage Fluid
 * @version
 */
class HasDistrictsViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractConditionViewHelper {

    /**
     * @param \TYPO3\MbxRealestate\Helper\Search\ImmoSearchItem $immoSearchItem
     * 
     * @return type
     */
    public function render(\TYPO3\MbxRealestate\Helper\Search\ImmoSearchItem $immoSearchItem) {
        
        if($immoSearchItem->hasDistrictsSet()) {
            return $this->renderThenChild();
        }  else {
            return $this->renderElseChild();
        }
    }
}