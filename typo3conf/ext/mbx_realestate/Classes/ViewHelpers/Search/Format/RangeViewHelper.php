<?php

namespace TYPO3\MbxRealestate\ViewHelpers\Search\Format;

/**
 * This class is a demo view helper for the Fluid templating engine.
 *
 * @package TYPO3
 * @subpackage Fluid
 * @version
 */
class RangeViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper {
    
    /**
     * @param \TYPO3\MbxRealestate\Helper\Search\ImmoSearchItem $immoSearchItem
     * @param array $configuration
     * @param string $as
     * 
     * @return type
     */
    public function render(\TYPO3\MbxRealestate\Helper\Search\ImmoSearchItem $immoSearchItem, $configuration, $as = null) {
        
        $varName = (empty($as) ? 'range' : $as);

        $html = '';
        
        if(!empty($configuration['key'])) {
            
            $unit = (!empty($configuration['unit']) ? $configuration['unit'] : '');
            $key = $this->camelCase($configuration['key']);
            
            $reflector = new \ReflectionClass($immoSearchItem);
            $funcSet = 'has' . ucfirst($key) . 'Set';
            
            if($reflector->hasMethod($funcSet)) {
                
                if($immoSearchItem->$funcSet()) {
                    
                    $funcFrom = 'get' . ucfirst($key) . 'From';
                    $funcTo = 'get' . ucfirst($key) . 'To';
                    
                    if($reflector->hasMethod($funcFrom) && $reflector->hasMethod($funcTo)) {
                        
                        $resFrom = $immoSearchItem->$funcFrom();
                        $resTo = $immoSearchItem->$funcTo();

                        $html .= '<strong>' . $configuration['label'] .'</strong> ';

                        if(!empty($resFrom) && !empty($resTo)) {
                            $html .= '<i>' . $resFrom . $unit . ' - ' . $resTo . $unit . '</i>';
                        } elseif (!empty($resFrom)) {
                            $html .= '<i> ab ' . $resFrom . $unit . '</i>';
                        } else {
                            $html .= '<i> bis ' . $resTo . $unit . '</i>';
                        }
                    }
                    
                }
            }
        }
        
        if(empty($as)) {
        
            return $html;
            
        } else {
            
            // drop old assignment if 'immoUrl' was already declared
            if($this->templateVariableContainer->exists($varName)) {

                $this->templateVariableContainer->remove($varName);
            }

            $this->templateVariableContainer->add($varName, $html);

            return $this->renderChildren();
        }
    }
    
    /**
     * @param string $var
     * @return string
     */
    private function camelCase($var) {
        
        $parts = explode('_', $var);
        $str = ucwords(implode(' ', $parts));
        $cc = str_replace(' ', '', $str);
        
        return lcfirst($cc);
    }
}