<?php

namespace TYPO3\MbxRealestate\ViewHelpers\Search\Format;

/**
 * This class is a demo view helper for the Fluid templating engine.
 *
 * @package TYPO3
 * @subpackage Fluid
 * @version
 */
class DistrictViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper {

    /**
     * @param int $id
     * @param null|string $as
     * 
     * @return string
     */
    public function render($id, $as = null) {

        $varName = (empty($as) ? 'range' : $as);
        
        $pluginHelper = \TYPO3\MbxRealestate\Helper\ImmoPluginHelper::getInstance();
        
        $districtsMapping = $pluginHelper->getDistrictsMapping();
        $districts = $pluginHelper->parseTS($districtsMapping);
        
        $district = $districts[$id];
        
        if(empty($as)) {
        
            return $district;
            
        } else {
            
            // drop old assignment if 'immoUrl' was already declared
            if($this->templateVariableContainer->exists($varName)) {

                $this->templateVariableContainer->remove($varName);
            }

            $this->templateVariableContainer->add($varName, $district);

            return $this->renderChildren();
        }
    }
}