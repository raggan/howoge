<?php

namespace TYPO3\MbxRealestate\Domain\Model;

/* * *************************************************************
 *  Copyright notice
 *
 *  (c) 2013 Denis Krüger <denis.krueger@mindbox.de>, Mindbox GmbH
 *  Anke Häslich <anke.haeslich@mindbox.de>, Mindbox GmbH
 *  
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 * ************************************************************* */

/**
 *
 *
 * @package mbx_realestate
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class Immoenvironment extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity {

    /**
     * environmentType
     *
     * @var \string
     */
    protected $environmentType;

    /**
     * environmentTitle
     *
     * @var \string
     */
    protected $environmentTitle;

    /**
     * Returns the environmentType
     *
     * @return \string $featureType
     */
    public function getEnvironmentType() {
        return $this->environmentType;
    }

    /**
     * Sets the environmentType
     *
     * @param \string $environmentType
     * @return void
     */
    public function setEnvironmentType($environmentType) {
        $this->environmentType = $environmentType;
    }

    /**
     * Returns the environmentTitle
     *
     * @return \string $environmentTitle
     */
    public function getEnvironmentTitle() {
            return $this->environmentTitle;
    }

    /**
     * Sets the environmentTitle
     *
     * @param \string $environmentTitle
     * @return void
     */
    public function setEnvironmentTitle($environmentTitle) {
            $this->environmentTitle = $environmentTitle;
    }

    
    /**
     * Returns a sanitized string that represents the environment title
     * @return string
     */
    public function getEnvironmentTypeByTitle() {

        $title = strtolower($this->getEnvironmentTitle());

        return substr(preg_replace('/[^a-zA-Z0-9]/', '', $title), 0, 10) 
               .'_'
               . substr(md5($title), 0, 16);
    }
}

?>