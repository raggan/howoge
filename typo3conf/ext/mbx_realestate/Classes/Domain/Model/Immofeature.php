<?php
namespace TYPO3\MbxRealestate\Domain\Model;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2013 Denis Krüger <denis.krueger@mindbox.de>, Mindbox GmbH
 *  Anke Häslich <anke.haeslich@mindbox.de>, Mindbox GmbH
 *  
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 *
 *
 * @package mbx_realestate
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class Immofeature extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity {

	/**
	 * featureType
	 *
	 * @var \string
	 */
	protected $featureType;
        	
	/**
	 * featureValue
	 *
	 * @var \string
	 */
	protected $featureValue;

	/**
	 * featureTitle
	 *
	 * @var \string
	 */
	protected $featureTitle;

	/**
	 * isTopFeature
	 *
	 * @var \boolean
	 */
	protected $isTopFeature;

	/**
	 * featureCast
	 *
	 * @var \string
	 */
	protected $featureCast;

	/**
	 * Returns the featureType
	 *
	 * @return \string $featureType
	 */
	public function getFeatureType() {
		return $this->featureType;
	}

    /**
     * Returns a sanitized string that represents the feature title
     * @return string
     */
    public function getFeatureTypeByTitle() {

        $title = strtolower($this->getFeatureTitle());

        return substr(preg_replace('/[^a-zA-Z0-9]/', '', $title), 0, 10) 
               .'_'
               . substr(md5($title), 0, 16);
    }

    /**
     * Returns a sanitized string that represents the feature title
     * @return string
     */
    public function getFeatureTypeByTitleAndValue() {

        $title = strtolower($this->getFeatureTitle());
        $value = strtolower($this->getFeatureValue());

        return substr(preg_replace('/[^a-zA-Z0-9]/', '', $title . $value), 0, 10) 
               .'_'
               . substr(md5($title . $value), 0, 16);
    }

    /**
	 * Sets the featureType
	 *
	 * @param \string $featureType
	 * @return void
	 */
	public function setFeatureType($featureType) {
		$this->featureType = $featureType;
	}
	
	/**
	 * Returns the featureValue
	 *
	 * @return \string $featureValue
	 */
	public function getFeatureValue() {
		return $this->featureValue;
	}

	/**
	 * Sets the featureValue
	 *
	 * @param \string $featureValue
	 * @return void
	 */
	public function setFeatureValue($featureValue) {
		$this->featureValue = $featureValue;
	}

	/**
	 * Returns the featureTitle
	 *
	 * @return \string $featureTitle
	 */
	public function getFeatureTitle() {
		return $this->featureTitle;
	}

	/**
	 * Sets the featureTitle
	 *
	 * @param \string $featureTitle
	 * @return void
	 */
	public function setFeatureTitle($featureTitle) {
		$this->featureTitle = $featureTitle;
	}

	/**
	 * Returns the isTopFeature
	 *
	 * @return \boolean $isTopFeature
	 */
	public function getIsTopFeature() {
		return ($this->isTopFeature == 1);
	}

	/**
	 * Sets the isTopFeature
	 *
	 * @param \boolean $isTopFeature
	 * @return void
	 */
	public function setIsTopFeature($isTopFeature) {
		$this->isTopFeature = ($isTopFeature == true ? 1 : 0);
	}

	/**
	 * Returns the featureCast
	 *
	 * @return \string $featureCast
	 */
	public function getFeatureCast() {
		return $this->featureCast;
	}

	/**
	 * Sets the featureCast
	 *
	 * @param \string $featureCast
	 * @return void
	 */
	public function setFeatureCast($featureCast) {
		$this->featureCast = $featureCast;
	}

}
?>