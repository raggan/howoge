<?php
namespace TYPO3\MbxRealestate\Domain\Model;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2013 Denis Krüger <denis.krueger@mindbox.de>, Mindbox GmbH
 *  Anke Häslich <anke.haeslich@mindbox.de>, Mindbox GmbH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 *
 *
 * @package mbx_realestate
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class Immocontact extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity {

        CONST FIELD_contactName = 'contact_name';
        CONST FIELD_contactStreet = 'contact_street';
        CONST FIELD_contactStreetnr = 'contact_streetnr';
        CONST FIELD_contactZip = 'contact_zip';
        CONST FIELD_contactCity = 'contact_city';
        CONST FIELD_contactCompany = 'contact_company';
        CONST FIELD_contactPhone = 'contact_phone';
        CONST FIELD_contactFax = 'contact_fax';
        CONST FIELD_contactMail = 'contact_mail';
        CONST FIELD_contactMobile = 'contact_mobile';
        CONST FIELD_contactUrl = 'contact_url';
        CONST FIELD_contactType = 'contact_type';

	/**
	 * contactName
	 *
	 * @var \string
	 */
	protected $contactName;

	/**
	 * contactStreet
	 *
	 * @var \string
	 */
	protected $contactStreet;

	/**
	 * contactStreetnr
	 *
	 * @var \string
	 */
	protected $contactStreetnr;

	/**
	 * contactZip
	 *
	 * @var \string
	 */
	protected $contactZip;

	/**
	 * contactCity
	 *
	 * @var \string
	 */
	protected $contactCity;

	/**
	 * contactCompany
	 *
	 * @var \string
	 */
	protected $contactCompany;

	/**
	 * contactPhone
	 *
	 * @var \string
	 */
	protected $contactPhone;

	/**
	 * contactFax
	 *
	 * @var \string
	 */
	protected $contactFax;

	/**
	 * contactMail
	 *
	 * @var \string
	 */
	protected $contactMail;

	/**
	 * contactMobile
	 *
	 * @var \string
	 */
	protected $contactMobile;

	/**
	 * contactUrl
	 *
	 * @var \string
	 */
	protected $contactUrl;

	/**
	 * contactType
	 *
	 * @var \string
	 */
	protected $contactType;

    /**
     * contactDesc
     *
     * @var \string
     */
    protected $contactDesc;

    /**
	 * contactMd5
	 *
	 * @var \string
	 */
	protected $contactMd5;



	/**
	 * Returns the contactMd5
	 *
	 * @return \string $contactMd5
	 */
	public function getContactMd5() {
            return md5(strtolower($this->getContactName())
                    . strtolower($this->getContactStreet())
                    . strtolower($this->getContactStreetnr())
                    . strtolower($this->getContactZip())
                    . strtolower($this->getContactCity())
                    . strtolower($this->getContactMail())
                    . strtolower($this->getContactType())
            );
	}

        /**
         * Sets the contactMd5 representing a unique identifier for the dataset
         *
         * @param string $contactMd5
	 * @return void
         */
        public function setContactMd5($contactMd5 = null) {

            if(empty($contactMd5)) {
                $contactMd5 = $this->getContactMd5();
            }

            $this->contactMd5 = $contactMd5;
        }

	/**
	 * Returns the contactName
	 *
	 * @return \string $contactName
	 */
	public function getContactName() {
		return $this->contactName;
	}

	/**
	 * Sets the contactName
	 *
	 * @param \string $contactName
	 * @return void
	 */
	public function setContactName($contactName) {
		$this->contactName = $contactName;
	}

	/**
	 * Returns the contactStreet
	 *
	 * @return \string $contactStreet
	 */
	public function getContactStreet() {
		return $this->contactStreet;
	}

	/**
	 * Sets the contactStreet
	 *
	 * @param \string $contactStreet
	 * @return void
	 */
	public function setContactStreet($contactStreet) {
		$this->contactStreet = $contactStreet;
	}

	/**
	 * Returns the contactStreetnr
	 *
	 * @return \string $contactStreetnr
	 */
	public function getContactStreetnr() {
		return $this->contactStreetnr;
	}

	/**
	 * Sets the contactStreetnr
	 *
	 * @param \string $contactStreetnr
	 * @return void
	 */
	public function setContactStreetnr($contactStreetnr) {
		$this->contactStreetnr = $contactStreetnr;
	}

	/**
	 * Returns the contactZip
	 *
	 * @return \string $contactZip
	 */
	public function getContactZip() {
		return $this->contactZip;
	}

	/**
	 * Sets the contactZip
	 *
	 * @param \string $contactZip
	 * @return void
	 */
	public function setContactZip($contactZip) {
		$this->contactZip = $contactZip;
	}

	/**
	 * Returns the contactCity
	 *
	 * @return \string $contactCity
	 */
	public function getContactCity() {
		return $this->contactCity;
	}

	/**
	 * Sets the contactCity
	 *
	 * @param \string $contactCity
	 * @return void
	 */
	public function setContactCity($contactCity) {
		$this->contactCity = $contactCity;
	}

	/**
	 * Returns the contactCompany
	 *
	 * @return \string $contactCompany
	 */
	public function getContactCompany() {
		return $this->contactCompany;
	}

	/**
	 * Sets the contactCompany
	 *
	 * @param \string $contactCompany
	 * @return void
	 */
	public function setContactCompany($contactCompany) {
		$this->contactCompany = $contactCompany;
	}

	/**
	 * Returns the contactPhone
	 *
	 * @return \string $contactPhone
	 */
	public function getContactPhone() {
		return $this->contactPhone;
	}

	/**
	 * Sets the contactPhone
	 *
	 * @param \string $contactPhone
	 * @return void
	 */
	public function setContactPhone($contactPhone) {
		$this->contactPhone = $contactPhone;
	}

	/**
	 * Returns the contactFax
	 *
	 * @return \string $contactFax
	 */
	public function getContactFax() {
		return $this->contactFax;
	}

	/**
	 * Sets the contactFax
	 *
	 * @param \string $contactFax
	 * @return void
	 */
	public function setContactFax($contactFax) {
		$this->contactFax = $contactFax;
	}

	/**
	 * Returns the contactMail
	 *
	 * @return \string $contactMail
	 */
	public function getContactMail() {
		return $this->contactMail;
	}

	/**
	 * Sets the contactMail
	 *
	 * @param \string $contactMail
	 * @return void
	 */
	public function setContactMail($contactMail) {
		$this->contactMail = $contactMail;
	}

	/**
	 * Returns the contactMobile
	 *
	 * @return \string $contactMobile
	 */
	public function getContactMobile() {
		return $this->contactMobile;
	}

	/**
	 * Sets the contactMobile
	 *
	 * @param \string $contactMobile
	 * @return void
	 */
	public function setContactMobile($contactMobile) {
		$this->contactMobile = $contactMobile;
	}

	/**
	 * Returns the contactUrl
	 *
	 * @return \string $contactUrl
	 */
	public function getContactUrl() {
		return $this->contactUrl;
	}

	/**
	 * Sets the contactUrl
	 *
	 * @param \string $contactUrl
	 * @return void
	 */
	public function setContactUrl($contactUrl) {
		$this->contactUrl = $contactUrl;
	}

	/**
	 * Returns the contactType
	 *
	 * @return \string $contactType
	 */
	public function getContactType() {
		return $this->contactType;
	}

	/**
	 * Sets the contactType
	 *
	 * @param \string $contactType
	 * @return void
	 */
	public function setContactType($contactType) {
		$this->contactType = $contactType;
	}

	/**
	 * Returns the contactDesc
	 *
	 * @return \string $contactDesc
	 */
	public function getContactDesc() {
		return $this->contactDesc;
	}

	/**
	 * Sets the contactDesc
	 *
	 * @param \string $contactDesc
	 * @return void
	 */
	public function setContactDesc($contactDesc) {
		$this->contactDesc = $contactDesc;
	}

}
?>