<?php
namespace TYPO3\MbxRealestate\Domain\Model;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2013 Denis Krüger <denis.krueger@mindbox.de>, Mindbox GmbH
 *  Anke Häslich <anke.haeslich@mindbox.de>, Mindbox GmbH
 *  
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 *
 *
 * @package mbx_realestate
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class Immoaddress extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity {

	/**
	 * Unternehmensnummer
	 *
	 * @var \string
	 */
	protected $unr;

	/**
	 * Wirtschaftseinheit
	 *
	 * @var \string
	 */
	protected $wi;

	/**
	 * Haus
	 *
	 * @var \string
	 */
	protected $hs;

	/**
	 * street
	 *
	 * @var \string
	 */
	protected $street;

	/**
	 * streetnr
	 *
	 * @var \string
	 */
	protected $streetnr;

	/**
	 * zip
	 *
	 * @var \string
	 */
	protected $zip;

	/**
	 * district
	 *
	 * @var \integer
	 */
	protected $district;

	/**
	 * city
	 *
	 * @var \string
	 */
	protected $city;

	/**
	 * locationDesc
	 *
	 * @var \string
	 */
	protected $locationDesc;

	/**
	 * contacts
	 *
	 * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\MbxRealestate\Domain\Model\Immocontact>
	 * @lazy
	 */
	protected $contacts;

	/**
	 * customerCenter
	 *
	 * @var \TYPO3\MbxRealestate\Domain\Model\Immocontact
	 * @lazy
	 */
	protected $customerCenter;

	/**
	 * geoX
	 *
	 * @var \float
	 */
	protected $geoX;

	/**
	 * geoY
	 *
	 * @var \float
	 */
	protected $geoY;

    /**
     * Vollständige Objektdaten
     *
     * @var \string
     */
    protected $fullObjectData;

    /**
     * timeOfVisit
     *
     * @var \string
     */
    protected $timeOfVisit;

    /**
	 * __construct
	 *
	 * @return Immoaddress
	 */
	public function __construct() {
		//Do not remove the next line: It would break the functionality
		$this->initStorageObjects();
	}

	/**
	 * Initializes all ObjectStorage properties.
	 *
	 * @return void
	 */
	protected function initStorageObjects() {
		/**
		 * Do not modify this method!
		 * It will be rewritten on each save in the extension builder
		 * You may modify the constructor of this class instead
		 */
		$this->contacts = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
	}

	/**
	 * Returns the unr
	 *
	 * @return \string $unr
	 */
	public function getUnr() {
		return $this->unr;
	}

	/**
	 * Sets the unr
	 *
	 * @param \string $unr
	 * @return void
	 */
	public function setUnr($unr) {
		$this->unr = $unr;
	}

	/**
	 * Returns the wi
	 *
	 * @return \string $wi
	 */
	public function getWi() {
		return $this->wi;
	}

	/**
	 * Sets the wi
	 *
	 * @param \string $wi
	 * @return void
	 */
	public function setWi($wi) {
		$this->wi = $wi;
	}

	/**
	 * Returns the hs
	 *
	 * @return \string $hs
	 */
	public function getHs() {
		return $this->hs;
	}

	/**
	 * Sets the hs
	 *
	 * @param \string $hs
	 * @return void
	 */
	public function setHs($hs) {
		$this->hs = $hs;
	}

	/**
	 * Returns the street
	 *
	 * @return \string $street
	 */
	public function getStreet() {
		return $this->street;
	}

	/**
	 * Sets the street
	 *
	 * @param \string $street
	 * @return void
	 */
	public function setStreet($street) {
		$this->street = $street;
	}

	/**
	 * Returns the streetnr
	 *
	 * @return \string $streetnr
	 */
	public function getStreetnr() {
		return $this->streetnr;
	}

	/**
	 * Sets the streetnr
	 *
	 * @param \string $streetnr
	 * @return void
	 */
	public function setStreetnr($streetnr) {
		$this->streetnr = $streetnr;
	}

	/**
	 * Returns the zip
	 *
	 * @return \string $zip
	 */
	public function getZip() {
		return $this->zip;
	}

	/**
	 * Sets the zip
	 *
	 * @param \string $zip
	 * @return void
	 */
	public function setZip($zip) {
		$this->zip = $zip;
	}

	/**
	 * Returns the district
	 *
	 * @return \integer $district
	 */
	public function getDistrict() {
		return $this->district;
	}

	/**
	 * Sets the district
	 *
	 * @param \integer $district
	 * @return void
	 */
	public function setDistrict($district) {
		$this->district = $district;
	}

	/**
	 * Returns the city
	 *
	 * @return \string $city
	 */
	public function getCity() {
		return $this->city;
	}

	/**
	 * Sets the city
	 *
	 * @param \string $city
	 * @return void
	 */
	public function setCity($city) {
		$this->city = $city;
	}

	/**
	 * Returns the locationDesc
	 *
	 * @return \string $city
	 */
	public function getLocationDesc() {
		return $this->locationDesc;
	}

	/**
	 * Sets the locationDesc
	 *
	 * @param \string $city
	 * @return void
	 */
	public function setLocationDesc($locationDesc) {
		$this->locationDesc = $locationDesc;
	}

	/**
	 * Adds a Immocontact
	 *
	 * @param \TYPO3\MbxRealestate\Domain\Model\Immocontact $contact
	 * @return void
	 */
	public function addContact(\TYPO3\MbxRealestate\Domain\Model\Immocontact $contact) {
		$this->contacts->attach($contact);
	}

	/**
	 * Removes a Immocontact
	 *
	 * @param \TYPO3\MbxRealestate\Domain\Model\Immocontact $contactToRemove The Immocontact to be removed
	 * @return void
	 */
	public function removeContact(\TYPO3\MbxRealestate\Domain\Model\Immocontact $contactToRemove) {
		$this->contacts->detach($contactToRemove);
	}

	/**
	 * Returns the contacts
	 *
	 * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\MbxRealestate\Domain\Model\Immocontact> $contacts
	 */
	public function getContacts() {
		return $this->contacts;
	}

	/**
	 * Sets the contacts
	 *
	 * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\MbxRealestate\Domain\Model\Immocontact> $contacts
	 * @return void
	 */
	public function setContacts(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $contacts) {
		$this->contacts = $contacts;
	}

	/**
	 * Returns the customerCenter
	 *
	 * @return \TYPO3\MbxRealestate\Domain\Model\Immocontact $customerCenter
	 */
	public function getCustomerCenter() {
		return $this->customerCenter;
	}

	/**
	 * Sets the customerCenter
	 *
	 * @param \TYPO3\MbxRealestate\Domain\Model\Immocontact $customerCenter
	 * @return void
	 */
	public function setCustomerCenter(\TYPO3\MbxRealestate\Domain\Model\Immocontact $customerCenter) {
		$this->customerCenter = $customerCenter;
	}

        /**
	 * Returns the geoX
	 *
	 * @return \float $geoX
	 */
	public function getGeoX() {
		return $this->geoX;
	}

	/**
	 * Sets the geoX
	 *
	 * @param \float $geoX
	 * @return void
	 */
	public function setGeoX($geoX) {
		$this->geoX = $geoX;
	}

	/**
	 * Returns the geoY
	 *
	 * @return \float $geoY
	 */
	public function getGeoY() {
		return $this->geoY;
	}

	/**
	 * Sets the geoY
	 *
	 * @param \float $geoY
	 * @return void
	 */
	public function setGeoY($geoY) {
		$this->geoY = $geoY;
	}

    /**
     *
     * @return \string $fullObjectData
     */
    public function getFullObjectData() {
        return $this->fullObjectData;
    }

    /**
     * @param \string $fullObjectData
     * @return \TYPO3\MbxRealestate\Domain\Model\Immoaddress
     */
    public function setFullObjectData($fullObjectData) {
        $this->fullObjectData = $fullObjectData;
        return $this;
    }

	/**
	 * Returns the timeOfVisit
	 *
	 * @return \string $timeOfVisit
	 */
	public function getTimeOfVisit() {
		return $this->timeOfVisit;
	}

	/**
	 * Sets the timeOfVisit
	 *
	 * @param \string $timeOfVisit
	 * @return void
	 */
	public function setTimeOfVisit($timeOfVisit) {
		$this->timeOfVisit = $timeOfVisit;
	}
}
