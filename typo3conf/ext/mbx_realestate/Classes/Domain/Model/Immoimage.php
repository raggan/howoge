<?php

namespace TYPO3\MbxRealestate\Domain\Model;

/* * *************************************************************
 *  Copyright notice
 *
 *  (c) 2013 Denis Krüger <denis.krueger@mindbox.de>, Mindbox GmbH
 *  Anke Häslich <anke.haeslich@mindbox.de>, Mindbox GmbH
 *  
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 * ************************************************************* */

/**
 *
 *
 * @package mbx_realestate
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class Immoimage extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity {

    CONST FIELD_imageType = 'image_type';
    CONST FIELD_imagePath = 'image_path';
    CONST FIELD_ctime = 'ctime';
    CONST FIELD_category = 'category';
    CONST FIELD_description = 'description';
    CONST FIELD_title = 'title';
    CONST FIELD_filename = 'filename';
    CONST FIELD_immoobject = 'immoobject';

    /**
     * imageType
     *
     * @var \string
     */
    protected $imageType;

    /**
     * imagePath
     *
     * @var \string
     */
    protected $imagePath;

    /**
     * filename
     * 
     * @var \string
     */
    protected $filename;

    /**
     * ctime
     * 
     * @var \int
     */
    protected $ctime;

    /**
     * description
     * 
     * @var \string
     */
    protected $description;

    /**
     * title
     * 
     * @var \string
     */
    protected $title;

    /**
     * category
     * 
     * @var \string
     */
    protected $category;

    /**
     * Returns the imageType
     *
     * @return \string $imageType
     */
    public function getImageType() {
        return $this->imageType;
    }

    /**
     * Sets the imageType
     *
     * @param \string $imageType
     * @return void
     */
    public function setImageType($imageType) {
        $this->imageType = $imageType;
    }

    /**
     * Returns the imagePath
     *
     * @return \string $imagePath
     */
    public function getImagePath() {
        return $this->imagePath;
    }

    /**
     * Sets the imagePath
     *
     * @param \string $imagePath
     * @return void
     */
    public function setImagePath($imagePath) {
        $this->imagePath = $imagePath;
    }

    /**
     * Returns the filename
     *
     * @return \string $filename
     */
    public function getFilename() {
        return $this->filename;
    }

    /**
     * Sets the filename
     *
     * @param \string $filename
     * @return void
     */
    public function setFilename($filename) {
        $this->filename = $filename;
    }

    /**
     * Returns the ctime
     *
     * @return \string $ctime
     */
    public function getCtime() {
        return $this->ctime;
    }

    /**
     * Sets the ctime
     *
     * @param \string $ctime
     * @return void
     */
    public function setCtime($ctime) {
        $this->ctime = $ctime;
    }

    /**
     * Returns the description
     *
     * @return \string $description
     */
    public function getDescription() {
        return $this->description;
    }

    /**
     * Sets the description
     *
     * @param \string $description
     * @return void
     */
    public function setDescription($description) {
        $this->description = $description;
    }

    /**
     * Returns the title
     *
     * @return \string $title
     */
    public function getTitle() {
        return $this->description;
    }

    /**
     * Sets the title
     *
     * @param \string $title
     * @return void
     */
    public function setTitle($title) {
        $this->title = $title;
    }

    /**
     * Returns the category
     *
     * @return \string $category
     */
    public function getCategory() {
        return $this->category;
    }

    /**
     * Sets the category
     *
     * @param \string $category
     * @return void
     */
    public function setCategory($category) {
        $this->category = $category;
    }

}

?>