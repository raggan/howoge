<?php
namespace TYPO3\MbxRealestate\Domain\Model;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2013 Denis Krüger <denis.krueger@mindbox.de>, Mindbox GmbH
 *  Anke Häslich <anke.haeslich@mindbox.de>, Mindbox GmbH
 *  
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 *
 *
 * @package mbx_realestate
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class Immoarea extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity {

	/**
	 * roomType
	 *
	 * @var \string
	 */
	protected $roomType;

	/**
	 * roomSize
	 *
	 * @var \float
	 */
	protected $roomSize;

	/**
	 * Returns the roomType
	 *
	 * @return \string $roomType
	 */
	public function getRoomType() {
		return $this->roomType;
	}

	/**
	 * Sets the roomType
	 *
	 * @param \string $roomType
	 * @return void
	 */
	public function setRoomType($roomType) {
		$this->roomType = $roomType;
	}

	/**
	 * Returns the roomSize
	 *
	 * @return \float $roomSize
	 */
	public function getRoomSize() {
		return $this->roomSize;
	}

	/**
	 * Sets the roomSize
	 *
	 * @param \float $roomSize
	 * @return void
	 */
	public function setRoomSize($roomSize) {
		$this->roomSize = $roomSize;
	}

}
?>