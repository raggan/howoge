<?php
namespace TYPO3\MbxRealestate\Domain\Model;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2013 Denis Krüger <denis.krueger@mindbox.de>, Mindbox GmbH
 *  Anke Häslich <anke.haeslich@mindbox.de>, Mindbox GmbH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 *
 *
 * @package mbx_realestate
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class Immoobject extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity {

	/**
	 * Unternehmensnummer
	 *
	 * @var \string
	 */
	protected $unr;

	/**
	 * Wirtschaftseinheit
	 *
	 * @var \string
	 */
	protected $wi;

	/**
	 * Haus
	 *
	 * @var \string
	 */
	protected $hs;

	/**
	 * Mieteinheit
	 *
	 * @var \string
	 */
	protected $me;

    /**
	 * Vollständige Objektdaten
	 *
	 * @var \string
	 */
	protected $fullObjectData;

	/**
	 * Gesamtanzahl der Räume in der Mieteinheit
	 *
	 * @var \float
	 */
	protected $rooms;

	/**
	 * zu welchem Zeitpunkt steht die Wohnung zur Vermietung bereit, wenn kleiner oder gleich Tagesdatum dann sofort
	 *
	 * @var \string
	 */
	protected $availableBy;

	/**
	 * die Miete, zu der vermietet werden soll
	 *
	 * @var \float
	 */
	protected $costGross;

	/**
	 * auch bekannt als Betriebskosten
	 *
	 * @var \float
	 */
	protected $costAttendant;

	/**
	 * auch bekannt als Heizkosten, falls extra ausgezeichnet
	 *
	 * @var \float
	 */
	protected $costHeat;

    /**
     * gibt an ob die Heizkosten enthalten sind
     *
     * @var \int
     */
	protected $costAttendantContainsHeat;

	/**
	 * Miete ohne Nebenkosten
	 *
	 * @var \float
	 */
	protected $costNet;

	/**
	 * costBuy
	 *
	 * @var \float
	 */
	protected $costBuy;

	/**
	 * deposit
	 *
	 * @var \float
	 */
	protected $deposit;
	
    /**
	 * provisionNet
	 *
	 * @var \float
	 */
	protected $provisionNet;
	
    /**
	 * provisionBrut
	 *
	 * @var \float
	 */
	protected $provisionBrut;

	/**
	 * Jahresangabe Gebäude errichtet
	 *
	 * @var \integer
	 */
	protected $yearBuild;

	/**
	 * letztes Sanierungsjahr
	 *
	 * @var \integer
	 */
	protected $yearRestoration;
	
    /**
	 * Anzahl Wohneinheiten
	 *
	 * @var \integer
	 */
	protected $numberFlats;

    /**
	 * Anzahl Gewerbeeinheiten
	 *
	 * @var \integer
	 */
	protected $numberCompanies;

	/**
	 * Anzahl der Stockwerke des Hauses
	 *
	 * @var \float
	 */
	protected $floors;

	/**
	 * wir haben im System gekennzeichnet 11-; 5; 6; 8 geschossig nicht 1-4!!!!!!
	 *
	 * @var \string
	 */
	protected $floorsMax;

	/**
	 * Wohnfläche
	 *
	 * @var \float
	 */
	protected $areaGeneral;
	
    /**
	 * Grundstücksfläche
	 *
	 * @var \float
	 */
	protected $areaLand;

    /**
	 * Nutzfläche
	 *
	 * @var \float
	 */
	protected $areaUsable;
    
    /**
	 * Bürofläche
	 *
	 * @var \float
	 */
	protected $areaOffice;
    
    /**
	 * Lagerfläche
	 *
	 * @var \float
	 */
	protected $areaStorage;
    
    /**
	 * Gesamtfläche
	 *
	 * @var \float
	 */
	protected $areaAll;

	/**
	 * numberParkingSpace
	 *
	 * @var \integer
	 */
	protected $numberParkingSpace;
	
    /**
	 * northPoint
	 *
	 * @var \integer
	 */
	protected $northPoint;

	/**
	 * Angebot der Woche
	 *
	 * @var boolean
	 */
	protected $featured = FALSE;
	
    /**
	 * Denkmalgeschützt
	 *
	 * @var boolean
	 */
	protected $protectedHistoricBuilding = FALSE;

	/**
	 * Art der Mieteinheit; Wohnung; Gewerbe; Gästewohnung
	 *
	 * @var \string
	 */
	protected $typeEstate;
	
    /**
	 * Definierter Wohnungstyp (Loft, Maisonette ...)
	 *
	 * @var \string
	 */
	protected $typeEstatePrecise;

	/**
	 * Nutzungsart: Wohnen, Gewerbe, Stellplatz
	 *
	 * @var \string
	 */
	protected $typeOfUse;
	
    /**
	 * Energiepass gültig bis
	 *
	 * @var \string
	 */
	protected $epartValidTo;

	/**
	 * Vermarktungsart: Kauf, Miete, Pacht
	 *
	 * @var \string
	 */
	protected $typeOfDisposition;

	/**
	 * Art des Hauses;z.B.  Altbau; Altneubau; Hochhaus; Neubau
	 *
	 * @var \string
	 */
	protected $typeBuilding;

	/**
	 * requiredWbs
	 *
	 * @var boolean
	 */
	protected $requiredWbs = FALSE;

    /**
	 * provision
	 *
	 * @var boolean
	 */
	protected $provision = FALSE;


	/**
	 * title
	 *
	 * @var \string
	 */
	protected $title;

	/**
	 * isInvestment
	 *
	 * @var \int
	 */
	protected $isInvestment;
    
	/**
	 * businessShares
	 *
	 * @var \float
	 */
	protected $businessShares;

	/**
	 * businessShares
	 *
	 * @var \int
	 */
	protected $businessSharesNum;

	/**
	 * businessSharesDesc
	 *
	 * @var \string
	 */
	protected $businessSharesDesc;

	/**
	 * notice
	 *
	 * @var \string
	 */
	protected $notice;
	
    /**
	 * noticeRestoration
	 *
	 * @var \string
	 */
	protected $noticeRestoration;

	/**
	 * description
	 *
	 * @var \string
	 */
	protected $description;

	/**
	 * actionOffer
	 *
	 * @var \string
	 */
	protected $actionOffer;

    /**
	 * energy certificate type (not set, demand or consumption)
	 *
	 * @var \integer
	 */
	protected $energyCertificateType;

    /**
     * efficiency class
     *
     * @var \string
     */
    protected $energyEfficiencyClass;

    /**
     * Energieverbrauchskennwert bzw. Endenergiebedarf
     *
     * @var \float
     */
    protected $thermalCharacteristic;

    /**
     * Based on certificate type contains electricity
     * energy demand or energy consumption
     *
     * @var \float
     */
    protected $thermalCharacteristicElectricity;

    /**
     * Based on certificate type contains heating
     * energy demand or energy consumption
     *
     * @var \float
     */
    protected $thermalCharacteristicHeating;

    /**
     * If thermalCharacteristic contains warm water (not set, yes or no)
     *
     * @var \integer
     */
    protected $energyContainsWarmWater;

    /**
     * Main energy source
     *
     * @var \string
     */
    protected $energySource;

    /**
     * Flurstück
     *
     * @var \string
     */
    protected $corridor;
    /**
     * Erschließung
     *
     * @var \string
     */
    protected $infrastructureProvision;
    
    /**
     * Bebaubar nach
     *
     * @var \string
     */
    protected $buildAfter;

    /**
     * Objektzustand
     *
     * @var \string
     */
    protected $situationOfBuilding;
    
    /**
     * Fläche teilbar
     *
     * @var \integer
     */
    protected $splitting;

    /**
     * Method of heating
     *
     * @var \string
     */
    protected $thermalHeatingMethod;

	/**
	 * immoaddress
	 *
	 * @var \TYPO3\MbxRealestate\Domain\Model\Immoaddress
	 */
	protected $immoaddress;

	/**
	 * immoarea
	 *
	 * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\MbxRealestate\Domain\Model\Immoarea>
	 * @lazy
	 */
	protected $immoarea;

	/**
	 * immofeature
	 *
	 * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\MbxRealestate\Domain\Model\Immofeature>
	 * @lazy
	 */
	protected $immofeature;

	/**
	 * immoimage
	 *
	 * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\MbxRealestate\Domain\Model\Immoimage>
	 * @lazy
	 */
	protected $immoimage;


	/**
	 * immoobjectMd5
	 *
	 * @var \string
	 */
	protected $immoobjectMd5;



	/**
	 * Returns the immoobjectMd5
	 *
	 * @return \string $immoobjectMd5
	 */
	public function getImmoobjectMd5() {
            return md5($this->getImmoobjectMd5Clear());
	}

	/**
	 * Returns the immoobjectMd5 as string before applying md5() to it
	 *
	 * @return \string
	 */
	public function getImmoobjectMd5Clear() {
            return strtolower($this->getUnr())
                . strtolower($this->getWi())
                . strtolower($this->getMe())
            ;
	}

        /**
         * Sets the immoobjectMd5 representing a unique identifier for the dataset
         *
         * @param string $immoobjectMd5
	 * @return void
         */
        public function setImmoobjectMd5($immoobjectMd5 = null) {

            if(empty($immoobjectMd5)) {
                $immoobjectMd5 = $this->getImmoobjectMd5();
            }

            $this->immoobjectMd5 = $immoobjectMd5;
        }


	/**
	 * __construct
	 *
	 * @return Immoobject
	 */
	public function __construct() {
		//Do not remove the next line: It would break the functionality
		$this->initStorageObjects();
	}

	/**
	 * Initializes all ObjectStorage properties.
	 *
	 * @return void
	 */
	protected function initStorageObjects() {
		/**
		 * Do not modify this method!
		 * It will be rewritten on each save in the extension builder
		 * You may modify the constructor of this class instead
		 */

            $this->immoimage = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
            $this->immoarea = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
            $this->immofeature = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
	}

	/**
	 * Returns the unr
	 *
	 * @return \string $unr
	 */
	public function getUnr() {
		return $this->unr;
	}

	/**
	 * Sets the unr
	 *
	 * @param \string $unr
	 * @return void
	 */
	public function setUnr($unr) {
		$this->unr = $unr;
	}

	/**
	 * Returns the wi
	 *
	 * @return \string $wi
	 */
	public function getWi() {
		return $this->wi;
	}

	/**
	 * Sets the wi
	 *
	 * @param \string $wi
	 * @return void
	 */
	public function setWi($wi) {
		$this->wi = $wi;
	}

	/**
	 * Returns the hs
	 *
	 * @return \string $Hs
	 */
	public function getHs() {
		return $this->hs;
	}

	/**
	 * Sets the hs
	 *
	 * @param \string $hs
	 * @return void
	 */
	public function setHs($hs) {
		$this->hs = $hs;
	}

	/**
	 * Returns the me
	 *
	 * @return \string $me
	 */
	public function getMe() {
		return $this->me;
	}

	/**
	 * Sets the me
	 *
	 * @param \string $me
	 * @return void
	 */
	public function setMe($me) {
		$this->me = $me;
	}

    /**
     * 
     * @return \string $fullObjectData
     */
    public function getFullObjectData() {
        return $this->fullObjectData;
    }

    /**
     * @param \string $fullObjectData
     * @return \TYPO3\MbxRealestate\Domain\Model\Immoobject
     */
    public function setFullObjectData($fullObjectData) {
        $this->fullObjectData = $fullObjectData;
        return $this;
    }

        
	/**
	 * Returns the rooms
	 *
	 * @return \float $rooms
	 */
	public function getRooms() {
		return $this->rooms;
	}

	/**
	 * Sets the rooms
	 *
	 * @param \float $rooms
	 * @return void
	 */
	public function setRooms($rooms) {
		$this->rooms = $rooms;
	}

	/**
	 * Returns the availableBy
	 *
	 * @return \string $availableBy
	 */
	public function getAvailableBy() {
		return $this->availableBy;
	}

	/**
	 * Sets the availableBy
	 *
	 * @param \string $availableBy
	 * @return void
	 */
	public function setAvailableBy($availableBy) {
		$this->availableBy = $availableBy;
	}

	/**
	 * Returns the costGross
	 *
	 * @return \float $costGross
	 */
	public function getCostGross() {
		return $this->costGross;
	}

	/**
	 * Sets the costGross
	 *
	 * @param \float $costGross
	 * @return void
	 */
	public function setCostGross($costGross) {
		$this->costGross = $costGross;
	}

	/**
	 * Returns the costAttendant
	 *
	 * @return \float $costAttendant
	 */
	public function getCostAttendant() {
		return $this->costAttendant;
	}

	/**
	 * Sets the costAttendant
	 *
	 * @param \float $costAttendant
	 * @return void
	 */
	public function setCostAttendant($costAttendant) {
		$this->costAttendant = $costAttendant;
	}

	/**
	 * Returns the costHeat
	 *
	 * @return \float $costHeat
	 */
	public function getCostHeat() {
		return $this->costHeat;
	}

	/**
	 * Sets the costHeat
	 *
	 * @param \float $costHeat
	 * @return void
	 */
	public function setCostHeat($costHeat) {
		$this->costHeat = $costHeat;
	}

    /**
     * @return int
     */
    public function getCostAttendantContainsHeat() {
        return $this->costAttendantContainsHeat;
    }

    /**
     * @param int $costAttendantContainsHeat
     * @return Immoobject
     */
    public function setCostAttendantContainsHeat($costAttendantContainsHeat) {
        $this->costAttendantContainsHeat = $costAttendantContainsHeat;
        return $this;
    }

	/**
	 * Returns the costNet
	 *
	 * @return \float $costNet
	 */
	public function getCostNet() {
		return $this->costNet;
	}

	/**
	 * Sets the costNet
	 *
	 * @param \float $costNet
	 * @return void
	 */
	public function setCostNet($costNet) {
		$this->costNet = $costNet;
	}

	/**
	 * Returns the costBuy
	 *
	 * @return \float $costBuy
	 */
	public function getCostBuy() {
		return $this->costBuy;
	}

	/**
	 * Sets the costBuy
	 *
	 * @param \float $costBuy
	 * @return void
	 */
	public function setCostBuy($costBuy) {
		$this->costBuy = $costBuy;
	}

	/**
	 * Returns the deposit
	 *
	 * @return \float $deposit
	 */
	public function getDeposit() {
		return $this->deposit;
	}

	/**
	 * Sets the deposit
	 *
	 * @param \float $deposit
	 * @return void
	 */
	public function setDeposit($deposit) {
		$this->deposit = $deposit;
	}
	
    /**
	 * Returns the provisionNet
	 *
	 * @return \float $provisionNet
	 */
	public function getProvisionNet() {
		return $this->provisionNet;
	}

	/**
	 * Sets the provisionNet
	 *
	 * @param \float $provisionNet
	 * @return void
	 */
	public function setProvisionNet($provisionNet) {
		$this->provisionNet = $provisionNet;
	}
   
    /**
	 * Returns the provisionBrut
	 *
	 * @return \float $provisionBrut
	 */
	public function getProvisionBrut() {
		return $this->provisionBrut;
	}

	/**
	 * Sets the provisionBrut
	 *
	 * @param \float $provisionBrut
	 * @return void
	 */
	public function setProvisionBrut($provisionBrut) {
		$this->provisionBrut = $provisionBrut;
	}

	/**
	 * Returns the yearBuild
	 *
	 * @return \integer $yearBuild
	 */
	public function getYearBuild() {
		return $this->yearBuild;
	}

	/**
	 * Sets the yearBuild
	 *
	 * @param \integer $yearBuild
	 * @return void
	 */
	public function setYearBuild($yearBuild) {
		$this->yearBuild = $yearBuild;
	}
	
    /**
	 * Returns the splitting
	 *
	 * @return \integer $splitting
	 */
	public function getSplitting() {
		return $this->splitting;
	}

	/**
	 * Sets the splitting
	 *
	 * @param \integer $splitting
	 * @return void
	 */
	public function setSplitting($splitting) {
		$this->splitting = $splitting;
	}
   
    /**
	 * Returns the numberFlats
	 *
	 * @return \integer $numberFlats
	 */
	public function getNumberFlats() {
		return $this->numberFlats;
	}

	/**
	 * Sets the numberFlats
	 *
	 * @param \integer $numberFlats
	 * @return void
	 */
	public function setNumberFlats($numberFlats) {
		$this->numberFlats = $numberFlats;
	}

    /**
	 * Returns the numberCompanies
	 *
	 * @return \integer $numberCompanies
	 */
	public function getNumberCompanies() {
		return $this->numberCompanies;
	}

	/**
	 * Sets the numberCompanies
	 *
	 * @param \integer $numberCompanies
	 * @return void
	 */
	public function setNumberCompanies($numberCompanies) {
		$this->numberCompanies = $numberCompanies;
	}

	/**
	 * Returns the yearRestoration
	 *
	 * @return \integer $yearRestoration
	 */
	public function getYearRestoration() {
		return $this->yearRestoration;
	}

	/**
	 * Sets the yearRestoration
	 *
	 * @param \integer $yearRestoration
	 * @return void
	 */
	public function setYearRestoration($yearRestoration) {
		$this->yearRestoration = $yearRestoration;
	}

	/**
	 * Returns the floors
	 *
	 * @return \float $floors
	 */
	public function getFloors() {
		return $this->floors;
	}

	/**
	 * Sets the floors
	 *
	 * @param \float $floors
	 * @return void
	 */
	public function setFloors($floors) {
		$this->floors = $floors;
	}

	/**
	 * Returns the floorsMax
	 *
	 * @return \string $floorsMax
	 */
	public function getFloorsMax() {
		return $this->floorsMax;
	}

	/**
	 * Sets the floorsMax
	 *
	 * @param \string $floorsMax
	 * @return void
	 */
	public function setFloorsMax($floorsMax) {
		$this->floorsMax = $floorsMax;
	}

	/**
	 * Returns the areaGeneral
	 *
	 * @return \float $areaGeneral
	 */
	public function getAreaGeneral() {
		return $this->areaGeneral;
	}

	/**
	 * Sets the areaGeneral
	 *
	 * @param \float $areaGeneral
	 * @return void
	 */
	public function setAreaGeneral($areaGeneral) {
		$this->areaGeneral = $areaGeneral;
	}
	
    /**
	 * Returns the areaLand
	 *
	 * @return \float $areaLand
	 */
	public function getAreaLand() {
		return $this->areaLand;
	}

	/**
	 * Sets the areaLand
	 *
	 * @param \float $areaLand
	 * @return void
	 */
	public function setAreaLand($areaLand) {
		$this->areaLand = $areaLand;
	}
    
    /**
	 * Returns the areaUsable
	 *
	 * @return \float $areaUsable
	 */
	public function getAreaUsable() {
		return $this->areaUsable;
	}

	/**
	 * Sets the areaUsable
	 *
	 * @param \float $areaUsable
	 * @return void
	 */
	public function setAreaUsable($areaUsable) {
		$this->areaUsable = $areaUsable;
	}
    
    /**
	 * Returns the areaOffice
	 *
	 * @return \float $areaOffice
	 */
	public function getAreaOffice() {
		return $this->areaOffice;
	}

	/**
	 * Sets the areaOffice
	 *
	 * @param \float $areaOffice
	 * @return void
	 */
	public function setAreaOffice($areaOffice) {
		$this->areaOffice = $areaOffice;
	}

    /**
	 * Returns the areaStorage
	 *
	 * @return \float $areaStorage
	 */
	public function getAreaStorage() {
		return $this->areaStorage;
	}

	/**
	 * Sets the areaStorage
	 *
	 * @param \float $areaStorage
	 * @return void
	 */
	public function setAreaStorage($areaStorage) {
		$this->areaStorage = $areaStorage;
	}
    
    /**
	 * Returns the areaAll
	 *
	 * @return \float $areaAll
	 */
	public function getAreaAll() {
		return $this->areaAll;
	}

	/**
	 * Sets the areaAll
	 *
	 * @param \float $areaAll
	 * @return void
	 */
	public function setAreaAll($areaAll) {
		$this->areaAll = $areaAll;
	}

	/**
	 * Returns the numberParkingSpace
	 *
	 * @return \integer $numberParkingSpace
	 */
	public function getNumberParkingSpace() {
		return $this->numberParkingSpace;
	}

	/**
	 * Sets the numberParkingSpace
	 *
	 * @param \integer numberParkingSpace
	 * @return void
	 */
	public function setNumberParkingSpace($numberParkingSpace) {
		$this->numberParkingSpace = $numberParkingSpace;
	}
	
    /**
	 * Returns the northPoint
	 *
	 * @return \integer $northPoint
	 */
	public function getNorthPoint() {
		return $this->northPoint;
	}

	/**
	 * Sets the northPoint
	 *
	 * @param \integer $northPoint
	 * @return void
	 */
	public function setNorthPoint($northPoint) {
		$this->northPoint = $northPoint;
	}

	/**
	 * Sets the customerCenter
	 *
	 * @param \integer $customerCenter
	 * @return void
	 */
	public function setCustomerCenter($customerCenter) {
		$this->customerCenter = $customerCenter;
	}

	/**
	 * Returns the featured
	 *
	 * @return boolean $featured
	 */
	public function getFeatured() {
		return $this->featured;
	}

	/**
	 * Sets the featured
	 *
	 * @param boolean $featured
	 * @return void
	 */
	public function setFeatured($featured) {
		$this->featured = $featured;
	}

	/**
	 * Returns the boolean state of featured
	 *
	 * @return boolean
	 */
	public function isFeatured() {
		return $this->getFeatured();
	}

	/**
	 * Returns the typeEstate
	 *
	 * @return \string $typeEstate
	 */
	public function getTypeEstate() {
		return $this->typeEstate;
	}

	/**
	 * Sets the typeEstate
	 *
	 * @param \string $typeEstate
	 * @return void
	 */
	public function setTypeEstate($typeEstate) {
		$this->typeEstate = $typeEstate;
	}
	
    /**
	 * Returns the typeEstatePrecise
	 *
	 * @return \string $$typeEstatePrecise
	 */
	public function getTypeEstatePrecise() {
		return $this->typeEstatePrecise;
	}

	/**
	 * Sets the typeEstatePrecise
	 *
	 * @param \string $typeEstatePrecise
	 * @return void
	 */
	public function setTypeEstatePrecise($typeEstatePrecise) {
		$this->typeEstatePrecise = $typeEstatePrecise;
	}

	/**
	 * Returns the typeOfUse
	 *
	 * @return \string $typeOfUse
	 */
	public function getTypeOfUse() {
		return $this->typeOfUse;
	}

	/**
	 * Sets the typeOfUse
	 *
	 * @param \string $typeOfUse
	 * @return void
	 */
	public function setTypeOfUse($typeOfUse) {
		$this->typeOfUse = $typeOfUse;
	}

	/**
	 * Returns the typeOfDisposition
	 *
	 * @return \string $typeOfDisposition
	 */
	public function getTypeOfDisposition() {
		return $this->typeOfDisposition;
	}

	/**
	 * Sets the typeOfDisposition
	 *
	 * @param \string $typeOfDisposition
	 * @return void
	 */
	public function setTypeOfDisposition($typeOfDisposition) {
		$this->typeOfDisposition = $typeOfDisposition;
	}

	/**
	 * Returns the typeBuilding
	 *
	 * @return \string $typeBuilding
	 */
	public function getTypeBuilding() {
		return $this->typeBuilding;
	}

	/**
	 * Sets the typeBuilding
	 *
	 * @param \string $typeBuilding
	 * @return void
	 */
	public function setTypeBuilding($typeBuilding) {
		$this->typeBuilding = $typeBuilding;
	}

	/**
	 * Returns the requiredWbs
	 *
	 * @return boolean $requiredWbs
	 */
	public function getRequiredWbs() {
		return $this->requiredWbs;
	}

	/**
	 * Sets the requiredWbs
	 *
	 * @param boolean $requiredWbs
	 * @return void
	 */
	public function setRequiredWbs($requiredWbs) {
		$this->requiredWbs = $requiredWbs;
	}
	
    /**
	 * Returns the provision
	 *
	 * @return boolean $provision
	 */
	public function getProvision() {
		return $this->provision;
	}

	/**
	 * Sets the provision
	 *
	 * @param boolean $provision
	 * @return void
	 */
	public function setProvision($provision) {
		$this->provision = $provision;
	}
	
    /**
	 * Returns the protectedHistoricBuilding
	 *
	 * @return boolean $protectedHistoricBuilding
	 */
	public function getProtectedHistoricBuilding() {
		return $this->protectedHistoricBuilding;
	}

	/**
	 * Sets the protectedHistoricBuilding
	 *
	 * @param boolean $protectedHistoricBuilding
	 * @return void
	 */
	public function setProtectedHistoricBuilding($protectedHistoricBuilding) {
		$this->protectedHistoricBuilding = $protectedHistoricBuilding;
	}

	/**
	 * Returns the boolean state of requiredWbs
	 *
	 * @return boolean
	 */
	public function isRequiredWbs() {
		return $this->getRequiredWbs();
	}

	/**
	 * Returns the title
	 *
	 * @return \string $title
	 */
	public function getTitle() {
		return $this->title;
	}

	/**
	 * Sets the title
	 *
	 * @param \string $title
	 * @return void
	 */
	public function setTitle($title) {
		$this->title = $title;
	}
	
    /**
     * 
     * @return \int
     */
    public function getIsInvestment() {
        return $this->isInvestment;
    }

    /**
     * 
     * @param \int $isInvestment
     * @return void
     */
    public function setIsInvestment($isInvestment) {
        $this->isInvestment = $isInvestment;
    }

        
    /**
	 * Returns the infrastructureProvision
	 *
	 * @return \string $infrastructureProvision
	 */
	public function getInfrastructureProvision() {
		return $this->infrastructureProvision;
	}

	/**
	 * Sets the infrastructureProvision
	 *
	 * @param \string $infrastructureProvision
	 * @return void
	 */
	public function setInfrastructureProvision($infrastructureProvision) {
		$this->infrastructureProvision = $infrastructureProvision;
	}

    /**
	 * Returns the buildAfter
	 *
	 * @return \string $buildAfter
	 */
	public function getBuildAfter() {
		return $this->buildAfter;
	}

	/**
	 * Sets the buildAfter
	 *
	 * @param \string $buildAfter
	 * @return void
	 */
	public function setBuildAfter($buildAfter) {
		$this->buildAfter = $buildAfter;
	}
    
    /**
	 * Returns the situationOfBuilding
	 *
	 * @return \string $situationOfBuilding
	 */
	public function getSituationOfBuilding() {
		return $this->situationOfBuilding;
	}

	/**
	 * Sets the situationOfBuilding
	 *
	 * @param \string $situationOfBuilding
	 * @return void
	 */
	public function setSituationOfBuilding($situationOfBuilding) {
		$this->situationOfBuilding = $situationOfBuilding;
	}

	/**
	 * Returns the businessShares
	 *
	 * @return \float $businessShares
	 */
	public function getBusinessShares() {
		return $this->businessShares;
	}

	/**
	 * Sets the businessShares
	 *
	 * @param \float $businessShares
	 * @return void
	 */
	public function setBusinessShares($businessShares) {
		$this->businessShares = $businessShares;
	}

	/**
	 * Returns the businessSharesNum
	 *
	 * @return \int $businessSharesNum
	 */
	public function getBusinessSharesNum() {
		return $this->businessSharesNum;
	}

	/**
	 * Sets the businessSharesNum
	 *
	 * @param \int $businessSharesNum
	 * @return void
	 */
	public function setBusinessSharesNum($businessSharesNum) {
		$this->businessSharesNum = $businessSharesNum;
	}

	/**
	 * Returns the businessSharesDesc
	 *
	 * @return \string $businessSharesDesc
	 */
	public function getBusinessSharesDesc() {
		return $this->businessSharesDesc;
	}

	/**
	 * Sets the businessSharesDesc
	 *
	 * @param \string $businessSharesDesc
	 * @return void
	 */
	public function setBusinessSharesDesc($businessSharesDesc) {
		$this->businessSharesDesc = $businessSharesDesc;
	}

	/**
	 * Returns the notice
	 *
	 * @return \string $notice
	 */
	public function getNotice() {
		return $this->notice;
	}

	/**
	 * Sets the notice
	 *
	 * @param \string $notice
	 * @return void
	 */
	public function setNotice($notice) {
		$this->notice = $notice;
	}

    /**
	 * Returns the corridor
	 *
	 * @return \string $corridor
	 */
	public function getCorridor() {
		return $this->corridor;
	}

	/**
	 * Sets the corridor
	 *
	 * @param \string $corridor
	 * @return void
	 */
	public function setCorridor($corridor) {
		$this->corridor = $corridor;
	}
	
    /**
	 * Returns the noticeRestoration
	 *
	 * @return \string $noticeRestoration
	 */
	public function getNoticeRestoration() {
		return $this->noticeRestoration;
	}

	/**
	 * Sets the noticeRestoration
	 *
	 * @param \string $noticeRestoration
	 * @return void
	 */
	public function setNoticeRestoration($noticeRestoration) {
		$this->noticeRestoration = $noticeRestoration;
	}
    
    /**
	 * Returns the epartValidTo
	 *
	 * @return \string $epartValidTo
	 */
	public function getEpartValidTo() {
		return $this->epartValidTo;
	}

	/**
	 * Sets the epartValidTo
	 *
	 * @param \string $epartValidTo
	 * @return void
	 */
	public function setEpartValidTo($epartValidTo) {
		$this->epartValidTo = $epartValidTo;
	}

	/**
	 * Returns the description
	 *
	 * @return \string $description
	 */
	public function getDescription() {
		return $this->description;
	}

	/**
	 * Sets the description
	 *
	 * @param \string $description
	 * @return void
	 */
	public function setDescription($description) {
		$this->description = $description;
	}

	/**
	 * Returns the actionOffer
	 *
	 * @return \string $actionOffer
	 */
	public function getActionOffer() {
		return $this->actionOffer;
	}

	/**
	 * Sets the actionOffer
	 *
	 * @param \string $actionOffer
	 * @return void
	 */
	public function setActionOffer($actionOffer) {
		$this->actionOffer = $actionOffer;
	}

    /**
     * Returns the energy certificate type
     *
     * @return \integer
     */
    public function getEnergyCertificateType() {
        return $this->energyCertificateType;
    }

    /**
     * Sets the energy certificate type
     *
     * @param \integer $energyCertificateType
	 * @return void
     */
    public function setEnergyCertificateType($energyCertificateType) {
        $this->energyCertificateType = $energyCertificateType;
    }

    /**
     * Returns energyEfficiencyClass
     *
     * @return \string
     */
    public function getEnergyEfficiencyClass() {
        return $this->energyEfficiencyClass;
    }

    /**
     * Sets energyEfficiencyClass
     *
     * @param \string $energyEfficiencyClass
	 * @return void
     */
    public function setEnergyEfficiencyClass($energyEfficiencyClass) {
        $this->energyEfficiencyClass = $energyEfficiencyClass;
    }

    /**
     * Returns thermalCharacteristic
     *
     * @return \float
     */
    public function getThermalCharacteristic() {
        return $this->thermalCharacteristic;
    }

    /**
     * Sets thermalCharacteristic
     *
     * @param \float thermalCharacteristic
	 * @return void
     */
    public function setThermalCharacteristic($thermalCharacteristic) {
        $this->thermalCharacteristic = $thermalCharacteristic;
    }

    /**
     * Returns thermalCharacteristicElectricity
     *
     * @return \float
     */
    public function getThermalCharacteristicElectricity() {
        return $this->thermalCharacteristicElectricity;
    }

    /**
     * Sets thermalCharacteristicElectricity
     *
     * @param \float thermalCharacteristicElectricity
	 * @return void
     */
    public function setThermalCharacteristicElectricity($thermalCharacteristicElectricity) {
        $this->thermalCharacteristicElectricity = $thermalCharacteristicElectricity;
    }

    /**
     * Returns thermalCharacteristicHeating
     *
     * @return \float
     */
    public function getThermalCharacteristicHeating() {
        return $this->thermalCharacteristicHeating;
    }

    /**
     * Sets thermalCharacteristicHeating
     *
     * @param \float $thermalCharacteristicHeating
	 * @return void
     */
    public function setThermalCharacteristicHeating($thermalCharacteristicHeating) {
        $this->thermalCharacteristicHeating = $thermalCharacteristicHeating;
    }

    /**
     * Returns thermalHeatingMethod
     *
     * @return \string
     */
    public function getThermalHeatingMethod() {
        return $this->thermalHeatingMethod;
    }

    /**
     * Sets thermalHeatingMethod
     *
     * @param \string $thermalHeatingMethod
	 * @return void
     */
    public function setThermalHeatingMethod($thermalHeatingMethod) {
        $this->thermalHeatingMethod = $thermalHeatingMethod;
    }

    /**
     * Returns energy source
     *
     * @return \string
     */
    public function getEnergySource() {
        return $this->energySource;
    }

    /**
     * Sets energy source
     *
     * @param \string $energySource
	 * @return void
     */
    public function setEnergySource($energySource) {
        $this->energySource = $energySource;
    }

    /**
     * Returns energyContainsWarmWater
     *
     * @return \integer
     */
    public function getEnergyContainsWarmWater() {
        return $this->energyContainsWarmWater;
    }

    /**
     * Sets energyContainsWarmWater
     *
     * @param \integer $energyContainsWarmWater
	 * @return void
     */
    public function setEnergyContainsWarmWater($energyContainsWarmWater) {
        $this->energyContainsWarmWater = $energyContainsWarmWater;
    }

	/**
	 * Returns the immoaddress
	 *
	 * @return \TYPO3\MbxRealestate\Domain\Model\Immoaddress $immoaddress
	 */
	public function getImmoaddress() {
		return $this->immoaddress;
	}

	/**
	 * Sets the immoaddress
	 *
	 * @param \TYPO3\MbxRealestate\Domain\Model\Immoaddress $immoaddress
	 * @return void
	 */
	public function setImmoaddress(\TYPO3\MbxRealestate\Domain\Model\Immoaddress $immoaddress) {
		$this->immoaddress = $immoaddress;
	}

	/**
	 * Adds a Immoarea
	 *
	 * @param \TYPO3\MbxRealestate\Domain\Model\Immoarea $immoarea
	 * @return void
	 */
	public function addImmoarea(\TYPO3\MbxRealestate\Domain\Model\Immoarea $immoarea) {
		$this->immoarea->attach($immoarea);
	}

	/**
	 * Removes a Immoarea
	 *
	 * @param \TYPO3\MbxRealestate\Domain\Model\Immoarea $immoareaToRemove The Immoarea to be removed
	 * @return void
	 */
	public function removeImmoarea(\TYPO3\MbxRealestate\Domain\Model\Immoarea $immoareaToRemove) {
		$this->immoarea->detach($immoareaToRemove);
	}

	/**
	 * Returns the immoarea
	 *
	 * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\MbxRealestate\Domain\Model\Immoarea> $immoarea
	 */
	public function getImmoarea() {
		return $this->immoarea;
	}

	/**
	 * Sets the immoarea
	 *
	 * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\MbxRealestate\Domain\Model\Immoarea> $immoarea
	 * @return void
	 */
	public function setImmoarea(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $immoarea) {
		$this->immoarea = $immoarea;
	}

	/**
	 * Adds a Immofeature
	 *
	 * @param \TYPO3\MbxRealestate\Domain\Model\Immofeature $immofeature
	 * @return void
	 */
	public function addImmofeature(\TYPO3\MbxRealestate\Domain\Model\Immofeature $immofeature) {
		$this->immofeature->attach($immofeature);
	}

	/**
	 * Removes a Immofeature
	 *
	 * @param \TYPO3\MbxRealestate\Domain\Model\Immofeature $immofeatureToRemove The Immofeature to be removed
	 * @return void
	 */
	public function removeImmofeature(\TYPO3\MbxRealestate\Domain\Model\Immofeature $immofeatureToRemove) {
		$this->immofeature->detach($immofeatureToRemove);
	}

	/**
	 * Returns the immofeature
	 *
	 * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\MbxRealestate\Domain\Model\Immofeature> $immofeature
	 */
	public function getImmofeature() {
		return $this->immofeature;
	}

	/**
	 * Sets the immofeature
	 *
	 * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\MbxRealestate\Domain\Model\Immofeature> $immofeature
	 * @return void
	 */
	public function setImmofeature(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $immofeature) {
		$this->immofeature = $immofeature;
	}
	/**
	 * Adds a Immoimage
	 *
	 * @param \TYPO3\MbxRealestate\Domain\Model\Immoimage $immoimage
	 * @return void
	 */
	public function addImmoimage(\TYPO3\MbxRealestate\Domain\Model\Immoimage $immoimage) {
		$this->immoimage->attach($immoimage);
	}

	/**
	 * Removes a Immoimage
	 *
	 * @param \TYPO3\MbxRealestate\Domain\Model\Immoimage $immoimageToRemove The immoimage to be removed
	 * @return void
	 */
	public function removeImmoimage(\TYPO3\MbxRealestate\Domain\Model\Immoimage $immoimageToRemove) {
		$this->immoimage->detach($immoimageToRemove);
	}

	/**
	 * Returns the Immoimage
	 *
	 * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\MbxRealestate\Domain\Model\Immoimage> $immoimage
	 */
	public function getImmoimage() {
		return $this->immoimage;
	}

	/**
	 * Sets the Immoimage
	 *
	 * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\MbxRealestate\Domain\Model\Immoimage> $immoimage
	 * @return void
	 */
	public function setImmoimage(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $immoimage) {
		$this->immoimage = $immoimage;
	}

}
?>