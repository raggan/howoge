<?php
namespace TYPO3\MbxRealestate\Domain\Model;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2013 Denis Krüger <denis.krueger@mindbox.de>, Mindbox GmbH
 *  Anke Häslich <anke.haeslich@mindbox.de>, Mindbox GmbH
 *  
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 *
 *
 * @package mbx_realestate
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class Searchagent extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity {

	/**
	 * Contains the interested person email address to receive email notifcations
	 *
	 * @var \string
	 */
	protected $contactEmail;

	/**
	 * Contains the mobile phone number to receive SMS notifications
	 *
	 * @var \string
	 */
	protected $contactMobile;

	/**
	 * Contains the serialized/encoded search query
	 *
	 * @var \string
	 */
	protected $searchQuery;

	/**
	 * Contains the object ids already sent to the user
	 *
	 * @var \string
	 */
	protected $resultsSent;

	/**
	 * Returns the contactEmail
	 *
	 * @return \string $contactEmail
	 */
	public function getContactEmail() {
		return $this->contactEmail;
	}

	/**
	 * Sets the contactEmail
	 *
	 * @param \string $contactEmail
	 * @return void
	 */
	public function setContactEmail($contactEmail) {
		$this->contactEmail = $contactEmail;
	}

	/**
	 * Returns the contactMobile
	 *
	 * @return \string $contactMobile
	 */
	public function getContactMobile() {
		return $this->contactMobile;
	}

	/**
	 * Sets the contactMobile
	 *
	 * @param \string $contactMobile
	 * @return void
	 */
	public function setContactMobile($contactMobile) {
		$this->contactMobile = $contactMobile;
	}

	/**
	 * Returns the searchQuery
	 *
	 * @return \string $searchQuery
	 */
	public function getSearchQuery() {
		return $this->searchQuery;
	}

	/**
	 * Sets the searchQuery
	 *
	 * @param \string $searchQuery
	 * @return void
	 */
	public function setSearchQuery($searchQuery) {
		$this->searchQuery = $searchQuery;
	}

	/**
	 * Returns the resultsSent
	 *
	 * @return \string $resultsSent
	 */
	public function getResultsSent() {
		return $this->resultsSent;
	}

	/**
	 * Sets the resultsSent
	 *
	 * @param \string $resultsSent
	 * @return void
	 */
	public function setResultsSent($resultsSent) {
		$this->resultsSent = $resultsSent;
	}

}
?>