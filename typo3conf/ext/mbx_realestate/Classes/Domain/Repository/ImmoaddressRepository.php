<?php
namespace TYPO3\MbxRealestate\Domain\Repository;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2013 Denis Krüger <denis.krueger@mindbox.de>, Mindbox GmbH
 *  Anke Häslich <anke.haeslich@mindbox.de>, Mindbox GmbH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/
use TYPO3\CMS\Extbase\Utility\DebuggerUtility;

/**
 *
 *
 * @package mbx_realestate
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class ImmoaddressRepository extends \TYPO3\CMS\Extbase\Persistence\Repository {

	/**
	 * initialisizeObject
	 */
	public function initializeObject() {
		/** @var $querySettings \TYPO3\CMS\Extbase\Persistence\Generic\Typo3QuerySettings */
		$querySettings = $this->objectManager->get('TYPO3\\CMS\\Extbase\\Persistence\\Generic\\Typo3QuerySettings');
		$querySettings->setRespectStoragePage(false);

		$this->setDefaultQuerySettings($querySettings);
	}


	/**
	 * Returns an immoaddress by its Unique ID represented by $unr/$wi
	 *
	 * @param string $unr
	 * @param string $wi
	 * @param string $hs
	 *
	 * @return \TYPO3\MbxRealestate\Domain\Model\Immoaddress
	 */
	public function findByImmoId($unr, $wi, $hs = null) {

		$query = $this->createQuery();

		$params = array(
			$query->equals('unr', (string)$unr),
			$query->equals('wi', (string)$wi)
		);

		if (!empty($hs)) {
			$params[] = $query->equals('hs', (string)$hs);
		}

		$comparison = $query->logicalAnd($params);
		$query->matching($comparison);

		$result = $query->execute();

		if ($result->count()) {
			return $result->getFirst();
		}

		return null;
	}

	/**
	 * Returns an immoaddress (single) by its $wi.
	 *
	 * @param string $wi
	 * @param int|array $pid
	 *
	 * @return \TYPO3\MbxRealestate\Domain\Model\Immoaddress
	 */
	public function findSingleByWi($wi, $pid = null) {

        return $this->findSingleByWiAndHs($wi, null, $pid);
    }

    /**
     * Returns an immoaddress (single) by its $wi and $hs.
     * 
     * @param string $wi
     * @param string $hs
     * @param int|array $pid
     * @return \TYPO3\MbxRealestate\Domain\Model\Immoaddress
     */
    public function findSingleByWiAndHs($wi, $hs = null, $pid = null) {

        $query = $this->createQuery();

		$params = array(
			$query->equals('wi', (string)$wi)
		);

        if(!empty($hs)) {
            $params[] = $query->equals('hs', (string)$hs);
        }
        
        if(!empty($pid)) {
            $params[] = $query->in('pid', (array)$pid);
        }

		$comparison = $query->logicalAnd($params);
		$query->matching($comparison);

		$result = $query->execute();

		if ($result->count()) {
			return $result->getFirst();
		}

		return null;
	}

	/**
	 * Returns an immoaddress by its address data
	 *
	 * @param string $street
	 * @param string $streetnr
	 * @param string $zip
	 * @param string $city
	 *
	 * @return \TYPO3\MbxRealestate\Domain\Model\Immoaddress
	 */
	public function findByAddress($street, $streetnr, $zip, $city) {

		$query = $this->createQuery();

		$params = array(
			$query->equals('street', (string)$street)
		);

		if (!is_null($streetnr)) {
			$params[] = $query->equals('streetnr', (string)$streetnr);
		}
		if (!is_null($zip)) {
			$params[] = $query->equals('zip', (string)$zip);
		}
		if (!is_null($city)) {
			$params[] = $query->equals('city', (string)$city);
		}

		$comparison = $query->logicalAnd($params);
		$query->matching($comparison);

		$result = $query->execute();

		if ($result->count()) {
			return $result->getFirst();
		}

		return null;
	}

	/**
	 *
	 * @param array $searchArray
	 */
	public function findBySearch($searchArray = array(), $searchStr = null) {
		$sql = $this->_buildQuery($searchArray, $searchStr);
		if (!is_null($sql)) {
			$query = $this->createQuery();
			$query->statement($sql);

			return $query->execute();
		}

		return null;
	}

	public function findRawBySearch($searchArray = array()) {
		$sql = $this->_buildQuery($searchArray);
		if (!is_null($sql)) {
			$rows = array();
			$res = $GLOBALS['TYPO3_DB']->sql_query($sql);
			while($row = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($res)) {
				$rows[] = $row;
			}
			return $rows;
		}

		return null;
	}

	protected function _buildQuery($searchArray = array(), $searchStr = null) {
		$where = $joins = array();

		$tblImmoaddress = 'ima';
		$tableImmoaddress = 'tx_mbxrealestate_domain_model_immoaddress';

		$tblImmocontact = 'imc';
		$tableImmocontact = 'tx_mbxrealestate_domain_model_immocontact';

		$tblMM = 'mm';
		$tableMM = 'tx_mbxrealestate_immoaddress_immocontact_mm';

		if (!empty($searchArray)) {

			if (!empty($searchArray['contactTypes'])) {
				$contactTypes = explode(',', $searchArray['contactTypes']);

				$joins[] = ' LEFT JOIN ' . $tableMM . ' AS ' . $tblMM . ' ON ' . $tblImmoaddress . '.uid = ' . $tblMM . '.uid_local '
					. ' LEFT JOIN ' . $tableImmocontact . ' AS ' . $tblImmocontact . ' ON ' . $tblMM . '.uid_foreign = ' . $tblImmocontact . '.uid ';

				$where[] = $tblImmocontact . '.contact_type IN (' . implode(',', $GLOBALS['TYPO3_DB']->fullQuoteArray($contactTypes)) . ')';


			}

			if(!is_null($searchStr)) {
				$where[] = $tblImmoaddress.".street LIKE '".$searchStr."%'";
			}

			$sql = 'SELECT DISTINCT '
				. $tblImmoaddress . '.* '

				. ' FROM ' . $tableImmoaddress . ' AS ' . $tblImmoaddress
				. implode(' ', $joins)
				. ' WHERE ' . (count($where) ? implode(' AND ', $where) : '1')
				. ' ORDER BY ima.street ' . \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING;;

		}
		return $sql;
	}

	public function findAllWithoutGeolocation() {
		$query = $this->createQuery();
		$query->getQuerySettings()->setRespectStoragePage(false);

		$query->matching(
			$query->logicalOr(
				$query->logicalNot(
					$query->logicalAnd(
						$query->greaterThan('geoX', 0),
						$query->greaterThan('geoY', 0)
					)
				),
				$query->equals('geoX', null),
				$query->equals('geoY', null)
			)
		);

		return $query->execute()->toArray();
	}
    
    /**
     * get all cities
     * @return mixed
     */
    public function findAllCities(){
        
        $query = $this->createQuery();
        $query->getQuerySettings()->setReturnRawQueryResult(TRUE);
        
        $sql = 'SELECT city FROM tx_mbxrealestate_domain_model_immoaddress GROUP BY city';
        
        return $query->statement($sql)->execute();
    }

}

?>
