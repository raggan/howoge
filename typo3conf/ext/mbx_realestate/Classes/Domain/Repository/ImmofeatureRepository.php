<?php
namespace TYPO3\MbxRealestate\Domain\Repository;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2013 Denis Krüger <denis.krueger@mindbox.de>, Mindbox GmbH
 *  Anke Häslich <anke.haeslich@mindbox.de>, Mindbox GmbH
 *  
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 *
 *
 * @package mbx_realestate
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class ImmofeatureRepository extends \TYPO3\CMS\Extbase\Persistence\Repository {

    /**
     * initialisizeObject
     */
    public function initializeObject() {
        $querySettings = $this->objectManager->get('TYPO3\\CMS\\Extbase\\Persistence\\Generic\\Typo3QuerySettings');
        $querySettings->setRespectStoragePage(FALSE);

        $this->setDefaultQuerySettings($querySettings);
    }
    
    /**
     * Searches for the unique features (e.g. for displaying in search forms)
     * @param int|null $pid
     * @return \TYPO3\CMS\Extbase\Persistence\Generic\QueryResult
     */
    public function getUniqueFeatures($pid = null) {
        
        $tblImmofeatures = 'imf';
        $tableImmofeatures = 'tx_mbxrealestate_domain_model_immofeature';
        
        $where = array();
        
        if(preg_match('/^\d+$/', $pid)) {
            $where[] = $tblImmofeatures . '.pid = ' . $pid;            
        }
        
        $where[] = $tblImmofeatures .'.hidden = 0';
        $where[] = $tblImmofeatures .'.deleted = 0';
        
        $sql = ' SELECT ' . $tblImmofeatures .'.* '
             . ' FROM ' . $tableImmofeatures . ' AS ' . $tblImmofeatures 
                . ' WHERE '
                . implode(' AND ', $where)
             . ' GROUP BY ' . $tblImmofeatures .'.feature_type'
            ;
        
        $query = $this->createQuery();
        $query->statement($sql);
        
        return $query->execute();
    }
    
    /**
     * Searches for the unique features (e.g. for displaying in search forms)
     * @param array $featureTitles
     * @param int|null $pid
     * @return \TYPO3\CMS\Extbase\Persistence\Generic\QueryResult
     */
    public function getVirtualFeatures(array $featureTitles = array(), $pid = null) {
        
        $tblImmofeatures = 'imf';
        $tableImmofeatures = 'tx_mbxrealestate_domain_model_immofeature';
        
        $where = array();
        
        if(preg_match('/^\d+$/', $pid)) {
            $where[] = $tblImmofeatures . '.pid = ' . $pid;            
        }
        
        $featureList = array();
        
        foreach($featureTitles as $title) {
            $featureList[] = sprintf('"%s"', $title);
        }
        
        $where[] = $tblImmofeatures .'.hidden = 0';
        $where[] = $tblImmofeatures .'.deleted = 0';
        
        if(!empty($featureList)) {
            $where[] = $tblImmofeatures .'.feature_title IN (' . implode(', ', $featureList) . ')';
        } else {
            $where[] = '1 = 2';            // do not find anything!
        }
        
        $sql = ' SELECT ' . $tblImmofeatures .'.* '
             . ' FROM ' . $tableImmofeatures . ' AS ' . $tblImmofeatures 
                . ' WHERE '
                . implode(' AND ', $where)
             . ' GROUP BY ' . $tblImmofeatures .'.feature_type'
            ;
        
        $query = $this->createQuery();
        $query->statement($sql);
        
        return $query->execute();
    }
    
    /**
     * Searches for the unique available features (e.g. for displaying in search forms)
     * @param int|null $pid
     * @return \TYPO3\CMS\Extbase\Persistence\Generic\QueryResult
     */
    public function getUniqueAvailableFeatures($pid = null) {
        
        $tblImmoFeatureMM = 'mm';
        $tableImmofeatureMM = 'tx_mbxrealestate_immoobject_immofeature_mm';
        
        $tblImmofeatures = 'imf';
        $tableImmofeatures = 'tx_mbxrealestate_domain_model_immofeature';
        
        $where = array();
        
        if(preg_match('/^\d+$/', $pid)) {
            $where[] = $tblImmofeatures . '.pid = ' . $pid;            
        }
        
        $where[] = $tblImmofeatures .'.hidden = 0';
        $where[] = $tblImmofeatures .'.deleted = 0';
        $where[] = $tblImmoFeatureMM . '.uid_local IS NOT NULL';
        
        $sql = ' SELECT ' . $tblImmofeatures .'.* '
             . ' FROM ' . $tableImmofeatures . ' AS ' . $tblImmofeatures 
             . ' LEFT JOIN ' . $tableImmofeatureMM . ' AS ' . $tblImmoFeatureMM 
             .      ' ON ' . $tblImmofeatures .'.uid = ' . $tblImmoFeatureMM . '.uid_foreign '
                . ' WHERE '
                . implode(' AND ', $where)
             . ' GROUP BY ' . $tblImmofeatures .'.feature_type'
            ;
        
        $query = $this->createQuery();
        $query->statement($sql);
        
        return $query->execute();
    }
    
}