<?php
namespace TYPO3\MbxRealestate\Domain\Repository;

	/***************************************************************
	 *  Copyright notice
	 *
	 *  (c) 2013 Denis Krüger <denis.krueger@mindbox.de>, Mindbox GmbH
	 *  Anke Häslich <anke.haeslich@mindbox.de>, Mindbox GmbH
	 *
	 *  All rights reserved
	 *
	 *  This script is part of the TYPO3 project. The TYPO3 project is
	 *  free software; you can redistribute it and/or modify
	 *  it under the terms of the GNU General Public License as published by
	 *  the Free Software Foundation; either version 3 of the License, or
	 *  (at your option) any later version.
	 *
	 *  The GNU General Public License can be found at
	 *  http://www.gnu.org/copyleft/gpl.html.
	 *
	 *  This script is distributed in the hope that it will be useful,
	 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
	 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	 *  GNU General Public License for more details.
	 *
	 *  This copyright notice MUST APPEAR in all copies of the script!
	 ***************************************************************/
use TYPO3\CMS\Extbase\Utility\DebuggerUtility;

/**
 *
 *
 * @package mbx_realestate
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class ImmoobjectRepository extends \TYPO3\CMS\Extbase\Persistence\Repository {

	/**
	 * initialisizeObject
	 */
	public function initializeObject() {
		/** @var $querySettings \TYPO3\CMS\Extbase\Persistence\Generic\Typo3QuerySettings */
		$querySettings = $this->objectManager->get('TYPO3\\CMS\\Extbase\\Persistence\\Generic\\Typo3QuerySettings');
		// don't add the pid constraint
		$querySettings->setRespectStoragePage(false);

		$this->setDefaultQuerySettings($querySettings);

		$this->setDefaultOrderings(
			array(
				'title' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING,
			)
		);
	}

	/**
	 * Returns an immoobject by its Unique ID represented by $unr/$wi/$me
	 *
	 * @param string $unr
	 * @param string $wi
	 * @param mixed $hs
	 * @param mixed $me
	 *
	 * @return \TYPO3\MbxRealestate\Domain\Model\Immoobject
	 */
	public function findByImmoId($unr, $wi, $hs = null, $me = null) {

		$query = $this->createQuery();

		if (!empty($unr)) {
			$params[] = $query->equals('unr', (string)$unr);
		}

		if (!empty($wi)) {
			$params[] = $query->equals('wi', (string)$wi);
		}

		if (!empty($hs)) {
			$params[] = $query->equals('hs', (string)$hs);
		}

		if (!empty($me)) {
			$params[] = $query->equals('me', (string)$me);
		}
		$comparison = $query->logicalAnd($params);
		$query->matching($comparison);

		$result = $query->execute();

		if ($result->count()) {

			return $result->getFirst();

		} else {

			return null;
		}
	}

	/**
	 * Returns an immoobject by its Unique ID represented by $unr/$wi/$me
	 *
	 * @param string $unr
	 * @param string $wi
	 * @param mixed $hs
	 * @param mixed $me
	 *
	 * @return \TYPO3\CMS\Extbase\Persistence\Generic\QueryResult
	 */
	public function findAllByImmoId($unr, $wi, $hs = null, $me = null) {

		$query = $this->createQuery();
		$query->getQuerySettings()->setRespectEnableFields(false);
		$query->matching($query->logicalAnd(
			$query->equals('unr', (string)$unr),
			$query->equals('wi', (string)$wi),
			$query->equals('hs', (string)$hs),
			$query->equals('me', (string)$me)
		));

		return $query->execute();
	}

	/**
	 * Searches for all immoobjects related to the search query parsed by $searchItem
	 *
	 * @param \TYPO3\MbxRealestate\Helper\Search\ImmoSearchItem $searchItem
	 * @param mixed $immoFindBySearchHelper
	 *
	 * @return \TYPO3\CMS\Extbase\Persistence\Generic\QueryResult
	 */
	public function findBySearch(\TYPO3\MbxRealestate\Helper\Search\ImmoSearchItem $searchItem, $immoFindBySearchHelper = NULL) {
        
        if(empty($immoFindBySearchHelper)){
        
            $immoFindBySearchHelper = new \TYPO3\MbxRealestate\Helper\Search\ImmoFindBySearchHelper();
        }
        
        $immoFindBySearchHelper->setTblImmoobject('io')
                                ->setTableImmoobject('tx_mbxrealestate_domain_model_immoobject')
                                ->setTblImmofeatures('imf')
                                ->setTableImmofeatures('tx_mbxrealestate_domain_model_immofeature')
                                ->setTblImmofeaturesMM('imf_mm')
                                ->setTableImmofeaturesMM('tx_mbxrealestate_immoobject_immofeature_mm')
                                ->setTblImmoenvironments('ime')
                                ->setTableImmoenvironments('tx_mbxrealestate_domain_model_immoenvironment')
                                ->setTblImmoaddress('ima')
                                ->setTableImmoaddress('tx_mbxrealestate_domain_model_immoaddress')
                                ->setSearchItem($searchItem)
        ;
        
        $sql = $immoFindBySearchHelper->handleStorage()
                                ->speedUpQuery()
                                ->handleIsInvestment()
                                ->handleTypeOfDisposition()
                                ->handleLivingArea()
                                ->handleRooms()
                                ->handleTypesOfUse()
                                ->handleTypesEstate()
                                ->handleCosts()
                                ->handleYearBuild()
                                ->handleFloorsMaxSet()
                                ->handleFloors()
                                ->handleOffers()
                                ->handleFeatures()
                                ->handleEnvironments()
                                ->handleEstatePreciseType()
                                ->handleRequireWbs()
                                ->handleThermalHeatingMethod()
                                ->handleProvision()
                                ->handleKeywords()
                                ->handleTypeBuilding()
                                ->handleLocation()
                                ->buildSQLStatement()
        ;

        
        // <editor-fold defaultstate="collapsed" desc="add ordering">
		if ($searchItem->hasSortSet()) {

			$sql .= ' ORDER BY ' . $searchItem->getSort() . ' ';

		} elseif ($searchItem->hasOrderBySet()) {

			$orderBy = $searchItem->getOrderBy();

//            if($orderBy === 'RAND()') {
//
//            } else {
//                unset($orderBy);
//            }

			if (isset($orderBy)) {

				$sql .= ' ORDER BY ' . $orderBy . ' ';
			}

			if ($searchItem->hasOrderSet() && isset($orderBy)) {

				$sql .= ' ' . $searchItem->getOrder() . ' ';
			}
		}
		// </editor-fold>

        // <editor-fold defaultstate="collapsed" desc="add limit">
		if ($searchItem->hasLimitSet()) {

			$sql .= ' LIMIT ' . $searchItem->getLimit();
		}
        // </editor-fold>
		// </editor-fold>

//        echo $sql;

		$query = $this->createQuery();
		$query->statement($sql);

		$result = $query->execute();
		$result->toArray();

		return $result;
	}

	/**
	 * Returns an list of immoobjects stored in Cookie
	 *
	 * @param \TYPO3\MbxRealestate\Helper\Search\ImmoSearchItem $searchItem
	 *
	 * @return \TYPO3\CMS\Extbase\Persistence\Generic\QueryResult
	 */
	public function findByNotepad(\TYPO3\MbxRealestate\Helper\Search\ImmoSearchItem $searchItem = null) {

		$cookieValue = (array)json_decode(\TYPO3\MbxRealestate\Helper\Cookie::get('notepad'));
		if(!$cookieValue) {
			$cookieValue = array(0);
		}

		$query = $this->createQuery();
		$query->matching($query->logicalAnd(
			$query->in('uid', $cookieValue)
		));

		if (!empty($searchItem) && $searchItem->hasSortSet()) {

			list($field, $type) = explode(' ', $searchItem->getSort());

            if($field != 'street') {
                $query->setOrderings(array($field => $type));
            }
		}

		if (!empty($searchItem) && $searchItem->hasLimitSet()) {

			$query->setLimit($searchItem->getLimit());
		}

		return $query->execute();
	}

	/**
	 * Called by array_walk() as callback
	 *
	 * @param string $item
	 * @param string $table (by default not necessary but TYPO3 requires this without using this param in any way -.-)
	 *   view TYPO3\CMS\Core\Database\DatabaseConnection::quoteStr()
	 *
	 * @return string
	 */
	private function quoteItems(&$item, $table = null) {

		$item = '"' . $GLOBALS['TYPO3_DB']->quoteStr($item, $table) . '"';

		return $item;
	}
}
