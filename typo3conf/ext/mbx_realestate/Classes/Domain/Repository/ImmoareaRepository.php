<?php
namespace TYPO3\MbxRealestate\Domain\Repository;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2013 Denis Krüger <denis.krueger@mindbox.de>, Mindbox GmbH
 *  Anke Häslich <anke.haeslich@mindbox.de>, Mindbox GmbH
 *  
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 *
 *
 * @package mbx_realestate
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class ImmoareaRepository extends \TYPO3\CMS\Extbase\Persistence\Repository {

    /**
     * initialisizeObject
     */
    public function initializeObject() {
        $querySettings = $this->objectManager->get('TYPO3\\CMS\\Extbase\\Persistence\\Generic\\Typo3QuerySettings');
        $querySettings->setRespectStoragePage(FALSE);

        $this->setDefaultQuerySettings($querySettings);
    }
//    
//    /**
//     * Removes all entries physically from DB which have no links to the immoobjects.
//     * @return int
//     */
//    public function removeUnlinked() {
//        
//        $GLOBALS['TYPO3_DB']->exec_DELETEquery( 'tx_mbxrealestate_domain_model_immoarea', 'immoobject=0');
//        return $GLOBALS['TYPO3_DB']->sql_affected_rows();
//
//    }
}
?>