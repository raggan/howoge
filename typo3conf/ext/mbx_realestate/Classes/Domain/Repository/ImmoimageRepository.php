<?php
namespace TYPO3\MbxRealestate\Domain\Repository;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2013 Denis Krüger <denis.krueger@mindbox.de>, Mindbox GmbH
 *  Anke Häslich <anke.haeslich@mindbox.de>, Mindbox GmbH
 *  
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 *
 *
 * @package mbx_realestate
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class ImmoimageRepository extends \TYPO3\CMS\Extbase\Persistence\Repository {

    CONST TYPE_NOT_IN = 'not_in';
    CONST TYPE_IN = 'in';
    
    /**
     * Searches for a immocontact identified by its unique fields
     * @param array $uniqueFields
     * @return \TYPO3\CMS\Extbase\Persistence\Generic\QueryResult
     */
    public function findByUniques(array $uniqueFields) {
        
        $equals = array();
        
        $query = $this->createQuery();
        
        foreach($uniqueFields as $fieldName => $fieldVal) {
            $equals[] = $query->equals($fieldName, $fieldVal);
        }
        
        $query->matching($query->logicalAnd($equals));        
        
        return $query->execute();
    }
    
    /**
     * 
     * @param array $types
     * @param \TYPO3\MbxRealestate\Domain\Model\Immoobject $immoobject
     * @param string $typeSearch
     * @param null|int $limit
     * @return \TYPO3\CMS\Extbase\Persistence\Generic\QueryResult
     */
    public function findByType(array $types, \TYPO3\MbxRealestate\Domain\Model\Immoobject $immoobject, $typeSearch = self::TYPE_IN, $limit = null) {
        
        $operand = ($typeSearch == self::TYPE_NOT_IN ? 'NOT' : '');
        
        $sql = 'SELECT * FROM tx_mbxrealestate_domain_model_immoimage
                WHERE ' . $operand . ' FIND_IN_SET( category, "' . (implode(',', $types)) . '" ) '
                . ' AND deleted=0 '
                . ' AND hidden=0 '
                . ' AND immoobject=' . (int)$immoobject->getUid()
                . ' ORDER BY FIELD(category, "' . (implode('","', $types)) . '" )';
        
        if(!is_null($limit) && preg_match('/^([0-9]+)$/', (string)$limit)) {
            $sql .= ' LIMIT ' . (string)$limit;
        }
        
        $query = $this->createQuery();
        $query->statement($sql);
        
        $result = $query->execute();        
        $result->toArray();
        
        return $result;
    }
    
    /**
     * Removes all entries physically from DB which have no links to the immoobjects.
     * @return int
     */
//    public function removeUnlinked() {
//        
//        $GLOBALS['TYPO3_DB']->exec_DELETEquery( 'tx_mbxrealestate_domain_model_immoimage', 'immoobject=0');
//        return $GLOBALS['TYPO3_DB']->sql_affected_rows();
//
//    }
}
?>