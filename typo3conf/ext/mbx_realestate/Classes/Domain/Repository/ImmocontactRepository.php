<?php
namespace TYPO3\MbxRealestate\Domain\Repository;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2013 Denis Krüger <denis.krueger@mindbox.de>, Mindbox GmbH
 *  Anke Häslich <anke.haeslich@mindbox.de>, Mindbox GmbH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 *
 *
 * @package mbx_realestate
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class ImmocontactRepository extends \TYPO3\CMS\Extbase\Persistence\Repository {


    /**
     * initialisizeObject
     */
    public function initializeObject() {
        $querySettings = $this->objectManager->get('TYPO3\\CMS\\Extbase\\Persistence\\Generic\\Typo3QuerySettings');
        $querySettings->setRespectStoragePage(FALSE);

        $this->setDefaultQuerySettings($querySettings);
    }

    /**
     * Searches for a immocontact identified by its unique fields
     * @param array $uniqueFields
     * @return \TYPO3\CMS\Extbase\Persistence\Generic\QueryResult
     */
    public function findByUniques(array $uniqueFields) {

        $equals = array();

        $query = $this->createQuery();

        foreach($uniqueFields as $fieldName => $fieldVal) {
            $equals[] = $query->equals($fieldName, $fieldVal);
        }

        $query->matching($query->logicalAnd($equals));

        return $query->execute();
    }

    /**
     * $contactType accepts: 
     * - "all" -> listing all contact persons
     * - "all,-hirer" -> listing all contact persons excepting the hirer
     * - "hirer" -> listing only the hirer contact person
     * 
     * @param \TYPO3\MbxRealestate\Domain\Model\Immoaddress $immoaddress
     * @param string|array $contactType
     *
     * @return \TYPO3\CMS\Extbase\Persistence\Generic\QueryResult
     */
    public function findByAddressAndType(\TYPO3\MbxRealestate\Domain\Model\Immoaddress $immoaddress, &$contactType = null) {

        $joinCustomerCenter = '';
        $whereContactType = '';

        if(!empty($contactType)) {

            if(!is_array($contactType)) {
                $contactTypes = explode(',', $contactType);
            } else {
                $contactTypes = $contactType;
            }
                
            // explicit list ?
            if(!in_array('all', $contactTypes)) {
                
                $tmpContactTypes = $contactTypes;
                
                array_walk($tmpContactTypes, array($this, 'escapeContactType'));
                $whereContactType .= ' AND ic.contact_type IN (' . implode(', ', $tmpContactTypes) . ')';
                
            // all listing with some disallows
            } elseif(count($contactTypes) > 1) {
                
                $tmpContactTypes = array();
                $match = null;
                
                foreach($contactTypes as $tmpContactType) {
                    if(preg_match('/^\-(?P<contactType>.*)$/', $tmpContactType, $match)) {
                        $tmpContactTypes[] = $match['contactType'];
                    }
                }
                
                if(count($tmpContactTypes) > 0) {
                    
                    array_walk($tmpContactTypes, array($this, 'escapeContactType'));
                    $whereContactType .= ' AND ic.contact_type NOT IN (' . implode(', ', $tmpContactTypes) . ')';
                }
            }
            
            // extra search for customerCenter because its no direct contact (mm-relation) but a field of the immoaddress
            if((in_array(\TYPO3\MbxRealestate\Helper\ImmoPluginHelper::getInstance()->getPluginSettings('settings.customerCenter.type'), $contactTypes) || in_array('all', $contactTypes))
            && is_object($immoaddress->getCustomerCenter())) {

                $joinCustomerCenter .= ' OR ic.uid = ' . $immoaddress->getCustomerCenter()->getUid();
            }
            
            $contactType = $contactTypes;
        }

        $sql = 'SELECT DISTINCT(ic.uid), ic.* FROM tx_mbxrealestate_domain_model_immocontact ic
                LEFT JOIN tx_mbxrealestate_immoaddress_immocontact_mm mm ON mm.uid_foreign = ic.uid ' . $joinCustomerCenter . '
                WHERE mm.uid_local = ' . $immoaddress->getUid() . ' '
                . ' AND ic.deleted = 0 '
                . ' AND ic.hidden = 0 '
                . $whereContactType;

        
        $query = $this->createQuery();
        $query->statement($sql);

        $result = $query->execute();
        $result->toArray();

        return $result;
    }


    /**
     * Find all contacts by type
     *
     * @param array $contactTypes
     * @param int $storagePage
     * @return array
     */
    public function findByTypes($contactTypes = array(), $storagePage = 0) {
        $constraints = array();

        $query = $this->createQuery();
        $query->getQuerySettings()->setRespectStoragePage(FALSE);

        if (!empty($contactTypes)) {
            $constraints[] = $query->in('contactType', $contactTypes);
        }

        if ((int)$storagePage > 0) {
            $constraints[] = $query->equals('pid', (int)$storagePage);
        }

        if ($constraints) {
            $query->matching(
                $query->logicalAnd($constraints)
            );
        }

        $query->setOrderings(array(
            'contactName' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING
        ));

		return $query->execute()->toArray();
    }

    /**
     * @param string $val
     * @return string
     */
    private function escapeContactType(&$val) {

        return $val = '"'. $val . '"';
    }

}
?>