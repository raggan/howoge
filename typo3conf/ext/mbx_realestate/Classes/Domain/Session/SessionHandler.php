<?php

namespace TYPO3\MbxRealestate\Domain\Session;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2011 Tobias Jueschke <tobias.jueschke@mindbox.de>, Rootsystem
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/


/**
 * session handling
 *
 * @package mbx_realestate
 * @license www.gnu.org/licenses/lgpl.html GNU Lesser General Public License, version 3 or later
 *
 */
class SessionHandler implements \TYPO3\CMS\Core\SingletonInterface {

    const EXTENSION_NAME = 'mbx_realestate';

    private $session = array();

    /**
     * construct
     */
    public function __construct() {
        $this->session = $this->loadSession();
    }

    /**
     * Return stored session data
     * @return Object the stored object
     */
    private function loadSession() {

        // check if fe_user is valid because in BE we cant access those information
        if(empty($GLOBALS['TSFE']->fe_user)) {
            return false;
        }

        $session = $GLOBALS['TSFE']->fe_user->getKey('ses', self::EXTENSION_NAME);
        if($session){
            return unserialize($session);
        }
    }

    /**
     * Write session data
     *
     * @return Tx_MbxRecipes_Domain_Session_SessionHandler this
     */
    public function saveSession() {

        if($this->session){
            $GLOBALS['TSFE']->fe_user->setKey('ses', self::EXTENSION_NAME, serialize($this->session));
            $GLOBALS['TSFE']->fe_user->sesData_change = 1;
            $GLOBALS['TSFE']->fe_user->storeSessionData();
        }

        return $this;
    }

    /**
     * Clean up session
     * @return Tx_MbxRecipes_Domain_Session_SessionHandler this
     */
    public function cleanUpSession() {

        // check if fe_user is valid because in BE we cant access those information
        if(!empty($GLOBALS['TSFE']->fe_user)) {
            $GLOBALS['TSFE']->fe_user->setKey('ses', self::EXTENSION_NAME, NULL);
            $GLOBALS['TSFE']->fe_user->storeSessionData();
        }

        return $this;
    }

    /**
     *
     * @return array
     */
    public function getSessionArray(){
        return $this->session;
    }

    /**
     *
     * @param string $key
     * @param mixed $value
     */
    public function __set($key, $value){

        if($value != NULL){
            $this->session[$key] = $value;
        }
        return $this;
    }

    /**
     *
     * @param string $key
     * @return mixed
     */
    public function __get($key){
        if (is_array($this->session) && array_key_exists($key, $this->session)) {
            return $this->session[$key];
        }
    }

    /**
     *
     * @param string $key
     * @return type
     */
    public function __isset($key){
        return isset($this->session[$key]);
    }

}
?>