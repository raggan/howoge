
var mbx_realestate = {
    
    init : function() {
        
    },
    
    search : {
    
        buildDistrictSelector : function(options) {

            var options = jQuery.extend({ }, {
                
                clickHandle : 'click',
                txtReplace : '%s'
                
            }, options);
                    
            var allSelectorClicked = function(inputs) {
                
                return inputs.filter(function() {
                    
                    return jQuery(this).hasClass('all-districts') && jQuery(this).is(':checked');
                    
                }).length === 1;
            },
            // deselect the single districts if all-selector exists and is selected
            handleAllSelector = function(inputs) {
                
                if(allSelectorClicked(inputs)) {

                    inputs.filter(':not(.all-districts)').attr('checked', false);
                }
            },
            selectAllSelector = function(allLabel) {
                
                allLabel.prop('checked', 'checked');
            },
            deselectAllSelector = function(allLabel) {
                
                allLabel.attr('checked', false);
            };

            jQuery('.tx-mbx-realestate #districts-list').each(function() {
                
                var container = jQuery(this),
                    allLabel = container.find('.all-districts'),
                    districtsCount = container.find('input[type="checkbox"]:not(.all-districts)').length,
                    title = container.find('.districts-title'),
                    hasAllSelector = allLabel.length > 0,
                    hasTitleSelector = title.length > 0,
                    inputs = container.find('input'),
                    str;
                
                function updateDistrictsList($input) {
                    
                    if(typeof $input !== 'undefined') {
                        
                        if(!$input.hasClass('all-districts') && hasAllSelector) {
                            deselectAllSelector(allLabel);
                        }

                        if($input.hasClass('all-districts') && hasAllSelector) {
                            handleAllSelector(inputs);
                        }
                    }
                    
                    var selectCount = (inputs.filter(function() {
                        return jQuery(this).is(':checked');
                    }).length);
                    
                    if(selectCount === 0) {
                        
                        str = 'kein Stadtteil gewählt';
                        
                    } else if(selectCount === 1 && hasAllSelector && allSelectorClicked(inputs)) {
                        
                        str = 'alle Stadtteile gewählt';
                        
                        if(hasAllSelector) {
                            selectAllSelector(allLabel);
                            handleAllSelector(inputs);
                        }
                        
                    } else if(selectCount === 1) {
                        
                        str = 'ein Stadtteil gewählt ';
                        
                    } else if(selectCount === districtsCount) {
                        
                        str = 'alle Stadtteile gewählt';
                        
                        if(hasAllSelector) {
                            selectAllSelector(allLabel);
                            handleAllSelector(inputs);
                        }
                        
                    } else {
                        
                        str = selectCount + ' Stadtteile gewählt';
                        
                    }
                    
                    if(hasTitleSelector) {
                        title.html(options.txtReplace.replace('%s', str));
                    }
                    
                    if(typeof jQuery.fn.iCheck === 'function') {
                            
                        window.setTimeout(function() {
                            inputs.iCheck('update');
                        }, 125);
                    }
                }
                
                inputs.on(options.clickHandle, function(e) {

                    updateDistrictsList(jQuery(this));
                });
                
                // call the update initial to set the label
                updateDistrictsList();
            });

        }
    },
    
    misc : {
        /**
         * 
         * @param string|jQuery object container - the selector of the container to append slider element after
         * @param jQuery object $from - the jQuery object of the input field that contains the from value
         * @param jQuery object $to - the jQuery object of the input field that contains the to value
         * @param JSON config - the configuration for this slider 
         * @param JSON fallbackConfig - the configuration if no configuration was set
         * @param JSON sliderConfiguration - the final slider configuration overwriting all other settings
         * @param boolean _return - defines if the slider should be returned or inserted after the container
         * @returns {undefined}
         */
        buildGeneralSlider : function(container, $from, $to, config, fallbackConfig, sliderConfiguration, _return) {
            
            var slider = jQuery('<div/>').addClass('slider'),
                min = parseFloat(config.min || fallbackConfig.min),     // the minimum value of the slider
                max = parseFloat(config.max || fallbackConfig.max),     // the maximum value of the slider
                valFrom = parseFloat($from.val()) || min,               // the current from value
                valTo = parseFloat($to.val()) || max,                   // the current to value
                step = config.steps || 1;                               // the steps the slider has to fix
            
            // ensure current values are in correct modulo
            if(valFrom % step !== 0) {
                valFrom -= (valFrom % step);
            }
            if(valTo % step !== 0) {
                valTo -= (valTo % step) - step;
            }
            
            $from.val(valFrom);
            $to.val(valTo);
            
            min = min < valFrom ? min : valFrom;
            max = max > valTo ? max : valTo;

            // create the slider and append our configuration
            slider.slider(jQuery.extend({ }, {
                range: true,
                min: min,
                max: max,
                values: [valFrom, valTo],
                slide: function(event, ui) {
                    $from.val(ui.values[ 0 ]);
                    $to.val(ui.values[ 1 ]);
                }
            }, sliderConfiguration || { }));
            
            $from.change(function() { slider.slider('values', 0, parseFloat(jQuery(this).val())); });
            $to.change(function() { slider.slider('values', 1, parseFloat(jQuery(this).val())); });
            
            // return the slider or append directly
            if(typeof _return === 'boolean' && _return === true) {
                return slider;
            } else {
                
                // transform selector into object
                if(typeof container === 'string') {
                    container = jQuery(container);
                }
                container.find('.options-manual').after(slider);
            }
            
        }
    },

    tools : {
        
        map : {
            
            getImmoMarkers : function(immoobjects) {
                
                var immoMarkers = [];
                
                for(var i in immoobjects) {

                    var io = immoobjects[i];

                    if(io.geo_x === 0 || io.geo_y === 0) {
                        continue;
                    }

                    immoMarkers.push({
                        latLng : [io.geo_x, io.geo_y],
                        data : io
                    });
                }
                
                return immoMarkers;
            },
            
            getBoundariesByObjects : function(objs) {
            
                var data = {
                    latMin : null, 
                    latMax : null,
                    lngMin : null,
                    lngMax : null
                };
                
                for(var a in objs) {
                    
                    var obj = objs[a];
                    
                    if(data.latMin === null || data.latMin > obj.latLng[0]) {
                        data.latMin = obj.latLng[0];
                    }
                    if(data.latMax === null || data.latMax < obj.latLng[0]) {
                        data.latMax = obj.latLng[0];
                    }
                    if(data.lngMin === null || data.lngMin > obj.latLng[1]) {
                        data.lngMin = obj.latLng[1];
                    }
                    if(data.lngMax === null || data.lngMax < obj.latLng[1]) {
                        data.lngMax = obj.latLng[1];
                    }
                }
                
                return data;
            },
            
            _toRad : function(a) { 
                return a * (Math.PI / 180); 
            },
            
            getCenterOfBoundary : function(boundary) {
              
                var data = {
                    lat : boundary.latMin - (boundary.latMin - boundary.latMax) / 2, 
                    lng : boundary.lngMin - (boundary.lngMin - boundary.lngMax) / 2
                };
                
                var lat1 = data.lat,
                    lat2 = boundary.latMin,
                    lon1 = data.lng,
                    lon2 = boundary.lngMin;
                
                var R = 6371; // km
                var dLat = this._toRad(lat2-lat1);
                var dLon = this._toRad(lon2-lon1);
                var lat1 = this._toRad(lat1);
                var lat2 = this._toRad(lat2);

                var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
                        Math.sin(dLon/2) * Math.sin(dLon/2) * Math.cos(lat1) * Math.cos(lat2); 
                var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
                var d = R * c;
                
                data.radius = d;
                
                return data;
            },
            
            immosInRange : function(mapContainer, radius) {
                
                var center = mapContainer.gmap3('get').getCenter();
                var immoobjects = mapContainer.data('immoobjects'),
                    immosInRange = [];
                
                var boundary = {
                    latMin : center.lat(),
                    lngMin : center.lng()                   
                };
                
                for(var a in immoobjects) {
                    
                    var io = immoobjects[a];
                    $.extend(boundary, {
                        latMax : io.latLng[0],
                        lngMax : io.latLng[1]
                    });
                    
                    var boundRes = this.getCenterOfBoundary(boundary);
                    
                    if(typeof boundRes.radius === 'number' && boundRes.radius * 1000 <= radius) {
                        immosInRange.push(io);
                    }
                }

                if(immosInRange.length) {
                    return immosInRange;
                } else {
                    return false;
                }
            },
            
            /**
             * This method binds the list of filter-able environmental information with the gmap. 
             * So you can filter the on the map displayed markers with the selected list.
             * 
             * @param $Object $mapContainer - the container which contains the googleMap (gmap3)
             * @param $Object $filterContainer - the container which contains the inputs related to the environmental information
             * @returns void
             */
            relateGMapPOIFilter : function($mapContainer, $filterContainerList) {

                $filterContainerList.find('input[type="checkbox"]').each(function() {
                    $(this).on('change click ifChanged', function() {
                        mbx_realestate.tools.map.updatePOIMarkers($mapContainer, $filterContainerList);
                    });
                });
            },
            
            /**
             * This method updates the markers onto the map by the currently selected values represented by inputs.
             * 
             * @param $Object $mapContainer - the container which contains the googleMap (gmap3)
             * @param $Object $filterContainerList - the list of inputs which may be checked to select the markers on the map
             * @returns void
             */
            updatePOIMarkers : function($mapContainer, $filterContainerList) {
        
                var map = $mapContainer.gmap3('get'),
                    envTypes = [];

                // push the selected types to envTypes
                $filterContainerList.each(function() {

                    var container = $(this),
                        img = container.find('.environment-icon');

                    $(this).find('input').each(function() {
                        
                        var checked = $(this).is(':checked');
                        
                        // change state of icon
                        if(img.length) { img.attr('src', img.attr('src').replace(/(\&disabled\=)\d+/, '$1' + (checked ? 0 : 1))); }
                        
                        // dont use item if not checked
                        if(!checked) { 
                            container.removeClass('selected');
                            return; 
                        }

                        container.addClass('selected');

                        $($(this).attr('class').split(/\s+/)).each(function() {

                            var res = arguments[1].match(/^marker-type-(.*)\s?$/);

                            if(res !== null) {
                                envTypes.push(res[1]);
                            }
                        });

                    });
                });

                // iterate each marker and check if marker-type is in envTypes
                $($($mapContainer).data('markers')).each(function() {

                    var visibleState = false;

                    if(typeof this.type === 'string') {
                        for(var i in envTypes) {
                            var envType = envTypes[i];
                            if(envType === this.type) {
                                visibleState = true;
                                break;
                            }
                        }
                    } else if(typeof this.type === 'object') {
                        for(var i in this.type) {
                            for(var n in envTypes) {
                                
                                var envType = envTypes[n];
                                
                                if(envType === this.type[i]) {
                                    visibleState = true;
                                    break;
                                }
                                
                                if(visibleState === true) {
                                    break;
                                }
                            }
                        }
                    }

                    this.isVisible = visibleState;
                    this.marker.setMap(visibleState ? map : null); 
                });

            },
            
            toggleMarkerVisibility : function(markers, map, mode) {
                
                $(markers).each(function() {
                    
                    if(typeof this.isVisible === 'undefined') {
                        this.isVisible = (this.marker.getMap() !== null);
                    }
                    
                    if(mode === true) {
                        if(this.isVisible) {
                            this.marker.setMap(map);
                        }
                    } else {
                        this.marker.setMap(null);
                    }
                    
                });
            }
        }
    }
};

if(typeof jQuery === 'function') {
    
    jQuery(window).load(function() {

        var RE = mbx_realestate;

        RE.init();
    });
}
