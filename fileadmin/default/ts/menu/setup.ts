/*
 * The MENU template.
 *
 * Configuration for default menu and default select menu, which can be used with small
 * modifications for other menu's, without writing the whole menu configuration all
 * over again for all kinds of menu's.
 */

# Default menu configuration
# Default menu configuration
menu.default = HMENU
menu.default {

    1 = TMENU
    1 {
        wrap = <ul class="nav inline">|</ul>

        # Always unfold all sub-levels of the menu
        expAll = 1

        # Define the normal state (not active, not selected) of menu items
        # Using NO=1 to activate normal state is not necessary, but useful when copying
        NO = 1
        NO {
            # Use the page subtitle (or title; only if subtitle is not set ) field in the title property on the A-tag
            ATagTitle.field = subtitle // title

            # Use the option-split feature to generate a different wrap for the last item on a level of the menu
            # The last item on each level gets class="last" added for CSS styling purposes.
            wrapItemAndSub = <li>|</li> |*| <li>|</li> |*| <li class="last">|</li>

            # HTML-encode special characters according to the PHP-function htmlSpecialChars
            # stdWrap.htmlSpecialChars = 1
        }

        # Copy properties of normal to active state, and then add a CSS class for styling
        ACT < .NO
        ACT.ATagParams = class="active"
    }
}

[globalVar = GP:no_mobile = 1]
menu.default.1 {
    NO.additionalParams = &no_mobile=1
    ACT.additionalParams < .NO.additionalParams
}
[global]

# Default dropdown menu configuration
menu.dropdown < menu.default
menu.dropdown {
    special = directory
    1 {
        wrap = <ul class="dropdown-menu">|</ul>
        NO.wrapItemAndSub = <li>|</li>
        ACT.wrapItemAndSub = <li class="active">|</li>
    }
}