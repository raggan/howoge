temp.showDropDown.if {
    value = 2
    isGreaterThan.data = register:count_MENUOBJ
    isTrue = 1
    isTrue.if {
        value.data = register:count_menuItems
        equals.data = register:count_MENUOBJ
    }
}

menu.breadcrumb = HMENU
menu.breadcrumb {
    special = rootline
    special.range = 1 | -1
    includeNotInMenu = 1

    1 = TMENU
    1 {
        expAll = 1
        wrap = <ul class="nav breadcrumb white inline">|</ul>

        NO = 1
        NO {
            ATagTitle.field = subtitle // title
            wrapItemAndSub =  <li>|</li>
        }

        # used to add additional navigation feature to breadcrumb
        ACT < .NO
        ACT {
            ATagParams = class="dropdown-toggle"
            ATagParams {
                if < temp.showDropDown.if
                if.isFalse = 1
                if.isFalse.if {
                    value = 0
                    equals.numRows {
                        table = pages
                        select.pidInList.field = pid
                        select.where = nav_hide = 0
                    }
                }
            }
            wrapItemAndSub = <li>|</li>|*|<li>|</li>|*|<li class="dropdown">|</li>

            allStdWrap.postCObject < menu.dropdown
            allStdWrap.postCObject.special.value.field = pid
            allStdWrap.postCObject.if < temp.showDropDown.if
        }
        CUR < .ACT
        CUR {
            doNotLinkIt = 1
            stdWrap.wrap = <span class="dropdown-toggle">|</span>
            stdWrap.wrap.if < menu.breadcrumb.1.ACT.ATagParams.if
            wrapItemAndSub = <li class="active">|</li>||<li class="active">|</li>|*|<li class="dropdown active">|</li>
        }
    }
}

# edit menu link if news detail page
[globalVar = GP:tx_news_pi1|news > 0]
menu.breadcrumb.1.CUR {
    stdWrap.override.cObject = RECORDS
    stdWrap.override.cObject {
        tables = tx_news_domain_model_news
        source.data = GP:tx_news_pi1|news
        dontCheckPid = 1
        conf.tx_news_domain_model_news = TEXT
        conf.tx_news_domain_model_news {
            field = title
            override.field = teaser_title
        }
    }
}

# edit menu link if immoobject detail page
[globalVar = GP:tx_mbxrealestate_pi1|immoobject > 0]
menu.breadcrumb.1.CUR {
    stdWrap.override.cObject = RECORDS
    stdWrap.override.cObject {
        tables = tx_mbxrealestate_domain_model_immoobject
        source.data = GP:tx_mbxrealestate_pi1|immoobject
        dontCheckPid = 1
        conf.tx_mbxrealestate_domain_model_immoobject = COA
        conf.tx_mbxrealestate_domain_model_immoobject {
            5 = TEXT
            5 {
                field = rooms
                intval = 1
                wrap = |-Zimmer-Wohnung
            }

            10 = RECORDS
            10 {
                tables = tx_mbxrealestate_domain_model_immoaddress
                source.field = immoaddress
                dontCheckPid = 1
                conf.tx_mbxrealestate_domain_model_immoaddress = TEXT
                conf.tx_mbxrealestate_domain_model_immoaddress {
                    dataWrap = &nbsp;&#124; {field:street}, {field:zip} {field:city}
                }
            }
        }
    }
}

[globalVar = GP:tx_mbxrealestate_pi1|immoobject > 0] && [globalVar = TSFE:id = 288]
menu.breadcrumb.1.CUR.stdWrap.override.cObject {
    conf.tx_mbxrealestate_domain_model_immoobject.5 >
    conf.tx_mbxrealestate_domain_model_immoobject.5 = TEXT
    conf.tx_mbxrealestate_domain_model_immoobject.5.field = type_of_use
}
[global]


# add 'Kiezmelder' categories to breadcrumb
temp.kiezmelder = CONTENT
temp.kiezmelder {
    table = tx_news_domain_model_category
    select {
        pidInList = {$plugin.tx_news.kiezStorage}
        andWhere.dataWrap = shortcut={leveluid:3}
        languageField = sys_language_uid
        max = 1
    }

    renderObj = TEXT
    renderObj {
        field = title
        typolink {
            parameter = {$contentpage.allKiezUid}#newsstream
            additionalParams.dataWrap = &tx_news_pi1[overwriteDemand][categories]={field:uid}
            ATagParams = rel="nofollow"
        }
        wrap = <li>|</li>
    }
}

[PIDinRootline = 325] && [globalString = IENV:HTTP_HOST=*howoge.de]
menu.breadcrumb.1.CUR.stdWrap.wrap >
menu.breadcrumb.1.CUR.wrapItemAndSub.preCObject < temp.kiezmelder

[PIDinRootline = 311] && [globalString = IENV:HTTP_HOST=*howoge-apps.de]
menu.breadcrumb.1.CUR.stdWrap.wrap >
menu.breadcrumb.1.CUR.wrapItemAndSub.preCObject < temp.kiezmelder

[global]