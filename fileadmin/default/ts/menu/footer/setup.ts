menu.footer < menu.default
menu.footer {
    special = directory
    special.value = {$menu.footerPid}

    1 = TMENU
    1 {
        wrap = <ul class="footer-nav nav span3">|</ul>
        expAll = 1

        NO {
            doNotLinkIt = 1
            stdWrap2.wrap = <b class="darkblue">|</b>
            wrapItemAndSub = <li>|</li>

            # Add links to immosearch page with preselection of correct district
            wrapItemAndSub.override {
                # Check for correct page ID which contains the "section" headline
                if.value.field = uid
                if.equals = 111

                cObject = COA
                cObject {
                    # render "section" headline
                    3 = TEXT
                    3.value = |

                    # generate list of immosearch links
                    5 < lib.immosearchFlyout
                    5.settings {
                        hideSearchFormWrapper = 1
                        templateLayoutImmoobjectSearchForm = Districtlist
                    }

                    wrap = <li>|</li>
                }
            }
        }

        SPC = 1
        SPC {
            doNotShowLink = 1
            allWrap = </ul><ul class="footer-nav nav span3">
        }
    }

    2 < menu.default.1
    2 {
        wrap = <ul class="nav">|</ul>
    }
}