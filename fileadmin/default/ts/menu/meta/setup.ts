menu.meta {
    # define meta navigation for header area
    header = COA
    header {

        5 = COA
        5 {
            20 = TEXT
            20.value = Mieterportal
            20.typolink.parameter = {$plugin.tx_mbxrenteraccounthowoge.pages.login}
            stdWrap.wrap = <li class="renteraccount">|</li>
        }

        # include header meta links
        10 < menu.default
        10 {
            special = directory
            special.value = {$menu.metaPid.header}

            1.wrap >
        }

        20 = TEXT
        20.value (
        <span class="fontSmall" id="jsDecreaseFont">A-</span>
        <span id="jsResetFont">A</span>
        <span class="fontLarge" id="jsIncreaseFont">A+</span>
        )
        20.wrap = <li id="jsFontSize" class="font-sizes clearfix">|</li>

        30 = TEXT
        30.typolink.parameter = {$plugin.tx_indexedsearch.searchUid}
        30.wrap = <li id="jsHeaderSearch" class="headerSearch">|<form action="/nc/suche.html" method="post" class="search"><input type="text" name="tx_indexedsearch[sword]" /></form></li>

        wrap = <ul class="nav inline text-right">|</ul>
    }

    # define meta navigation for header area
    footer < menu.meta.header.10
    footer.special.value = {$menu.metaPid.footer}
    footer.1.wrap = <ul class="span6 pull-right nav inline text-right">|</ul>
}

plugin.tx_felogin_pi1 {
    storagePid = {$plugin.tx_felogin_pi1.storagePid}
    templateFile = {$plugin.tx_felogin_pi1.templateFile}
}

[loginUser = ]
# include logout view in meta menu if user is logged in
[else]
menu.meta.header {
    
    4 = COA
    4 {
        
        5 < plugin.tx_felogin_pi1
        5 {
            storagePid = {$plugin.tx_mbxrenteraccounthowoge.persistence.renters}
            templateFile = {$plugin.tx_mbxrenteraccounthowoge.templates.login.header}
            redirectMode = logout
            redirectPageLogout = 1
            redirectFirstMethod = 1
        }

        stdWrap.wrap = <li class="felogout">|</li>
    }
    5.20.typolink.parameter = {$plugin.tx_mbxrenteraccounthowoge.pages.startpage}
}
[global]

# DOI
[usergroup = {$plugin.tx_mbxrenteraccounthowoge.feGroups.doiPending}]
menu.meta.header.5.20.typolink.parameter = {$plugin.tx_mbxrenteraccounthowoge.pages.doi}
[global]

# Postcode
[usergroup = {$plugin.tx_mbxrenteraccounthowoge.feGroups.authPending}]
menu.meta.header.5.20.typolink.parameter = {$plugin.tx_mbxrenteraccounthowoge.pages.postalcodeVerification}
[global]

# Contract canceled
[usergroup = {$plugin.tx_mbxrenteraccounthowoge.feGroups.contractTerminated}]
menu.meta.header.5.20.typolink.parameter = {$plugin.tx_mbxrenteraccounthowoge.pages.contractTerminated}
[global]