## Configuration of main menu contained in header tag

# Configuration of temporary objects used in if cases
temp.checkDivider.if {
    value.field = title
    equals = - Trenner -
}
temp.checkSubpageImmosearch.if {
    value.field = pid
    equals = 2
}

# Configuration of temporary objects used in submenu
temp.dividerImages = CONTENT
temp.dividerImages {
    table = tt_content
    select {
        pidInList.field = uid
        where = colPos=0 AND CType='image'
        languageField = sys_language_uid
        max = 1
    }
    renderObj = FILES
    renderObj {
        references {
            table = tt_content
            uid.field = uid
            fieldName = image
        }
        renderObj = IMAGE
        renderObj {
            file {
                import.data = file:current:originalUid // file:current:uid
                height = 76c
                width = 280
            }
            altText.data = file:current:alternative
            wrap = <li class="menu-image">|</li>
        }
    }
}

# Configuration for immosearch form used in flyout and on homepage
lib.immosearchFlyout = USER
lib.immosearchFlyout {
    userFunc = TYPO3\CMS\Extbase\Core\Bootstrap->run
    pluginName = Pi1
    extensionName = MbxRealestate
    vendorName = TYPO3
    switchableControllerActions.Immoobject.1 = searchForm

    mvc =< plugin.tx_mbxrealestate.mvc
    settings < plugin.tx_mbxrealestate.settings
    settings {
        templateLayoutImmoobjectSearchForm = Flyoutsearch
        idPrefix = homepage-
    }
    persistence =< plugin.tx_mbxrealestate.persistence
    view < plugin.tx_mbxrealestate.view
}

# Configuration for immosearch form used in flyout and on homepage
# Commented notepad icon due to performance issues, see HOWSPT-406
#lib.notepadMenuIcon = USER_INT
#lib.notepadMenuIcon {
#    userFunc = TYPO3\CMS\Extbase\Core\Bootstrap->run
#    pluginName = Pi1
#    extensionName = MbxRealestate
#    vendorName = TYPO3
#    switchableControllerActions.Immoobject.1 = listNotepad
#
#    mvc =< plugin.tx_mbxrealestate.mvc
#    settings < plugin.tx_mbxrealestate.settings
#    settings {
#        templateLayoutImmoobjectListNotepad = MenuIcon
#    }
#    persistence =< plugin.tx_mbxrealestate.persistence
#    view =< plugin.tx_mbxrealestate.view
#}

# Configuration of menu object
menu.main < menu.default
menu.main {
    special = directory
    special.value = {$contentpage.homeUid}

    1 {
        IFSUB = 1
        IFSUB {
            # add notepad star if menu item = 'Mieten'
            stdWrap.cObject = COA
            stdWrap.cObject {
                5 = TEXT
                5.field = nav_title // title

# Commented notepad icon due to performance issues, see HOWSPT-406
#                10 < lib.notepadMenuIcon
#                10.stdWrap.if {
#                    value.field = uid
#                    equals = 2
#                }
            }
            wrapItemAndSub = <li class="flyout">|</li>
        }
        ACTIFSUB < .IFSUB
        ACTIFSUB.ATagParams < .ACT.ATagParams
    }

    2 < menu.default.1
    2 {
        NO {
            stdWrap.cObject < menu.main.1.IFSUB.stdWrap.cObject
            stdWrap.cObject.10.stdWrap.if.equals = 121
            wrapItemAndSub = <li class="menu-item">|</li>
        }

        ACT.stdWrap.cObject < .NO.stdWrap.cObject
        ACT.wrapItemAndSub < .NO.wrapItemAndSub

        SPC = 1
        SPC {
            doNotShowLink = 1

            allWrap.cObject = COA
            allWrap.cObject {
                # Configuration for divider used as headline in default submenu
                3 = TEXT
                3 {
                    dataWrap = <div class="column-header"><div>{field:title}<hr /></div></div><ul class="nav span">|*|

                    if {
                        isFalse = 1
                        isFalse.if < temp.checkDivider.if
                        isPositive = 1
                        isPositive.if {
                            value = 1
                            isFalse = 1
                            isFalse.if < temp.checkSubpageImmosearch.if
                        }
                    }
                }

                # Configuration for SPC used as menu divider in default submenu
                5 = TEXT
                5 {
                    dataWrap = ||</ul><ul class="nav span">|*|</ul><ul class="nav span">|*|</ul><ul class="nav span">||

                    if < temp.checkDivider.if
                    if.isFalse = 1
                    if.isFalse.if < temp.checkSubpageImmosearch.if
                }

                # Configuration for SPC used as headline and menu divider in 'Mieten' submenu
                7 = TEXT
                7 {
                    dataWrap = <ul class="nav span"><li class="column-header"><div>{field:title}<hr /></div></li>|*|</ul><ul class="nav span"><li class="column-header"><div>{field:title}<hr /></div></li>

                    postCObject < temp.dividerImages

                    if < temp.checkSubpageImmosearch.if
                    if.isFalse = 1
                    if.isFalse.if < temp.checkDivider.if
                }
            }
        }

        stdWrap.orderedStdWrap {
            5.innerWrap.cObject = COA
            5.innerWrap.cObject {
                3 = CASE
                3 {
                    key.field = pid

                    default = CONTENT
                    default {
                        table = tt_content
                        select {
                            pidInList.field = pid
                            where = colPos=4
                            languageField = sys_language_uid
                            max = 1
                        }
                        renderObj < tt_content
                    }

                    2 < lib.immosearchFlyout
                    2.settings.idPrefix = flyout-

                    stdWrap.wrap = <div class="span4">|</div>
                    stdWrap.required = 1
                }

                5 = TEXT
                5.value = <div class="span8 column-box">|</ul></div>

                if {
                    value.data = field:pid
                    isInList = {$menu.invertStructureMenus}
                    negate = 1
                }
            }

            10.innerWrap.cObject = COA
            10.innerWrap.cObject {
                3 = TEXT
                3.value = <div class="span4">|</ul></div>

                5 = CASE
                5 {
                    key.field = pid

                    default = CONTENT
                    default {
                        table = tt_content
                        select {
                            pidInList.field = pid
                            where = colPos=4
                            languageField = sys_language_uid
                            max = 1
                        }
                        renderObj < tt_content
                    }

                    2 < lib.immosearchFlyout
                    2.settings.idPrefix = flyout-

                    stdWrap.wrap = <div class="span8 reversedMenu">|</div>
                    stdWrap.required = 1
                }

                if {
                    value.data = field:pid
                    isInList = {$menu.invertStructureMenus}
                }
            }

            15.wrap = <div class="row-fluid">|</div>
        }
        # Delete unneeded wrap
        wrap = <div class="flyout-bg">|</div>
    }

    3 = TMENU
    3 {
        stdWrap.outerWrap = <div class="submenu-third-level"><ul class='submenu'>|</ul></div>
        stdWrap.if {
            isInList.field = pid
            value = {$menu.thirdSubmenuList}
        }

        NO.wrapItemAndSub = <li class="menu-item">|</li>

        ACT = 1
        ACT{
            wrapItemAndSub = <li class="menu-item active">|</li>
        }

        SPC = 1
        SPC {
           doNotLinkIt = 1
           doNotShowLink = 1
           allWrap = </ul><ul class='submenu'>
        }
    }
}