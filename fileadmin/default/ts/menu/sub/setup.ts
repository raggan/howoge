# Configuration of main menu contained in header tag
menu.sub < menu.default
menu.sub {
	entryLevel = 2

	1.wrap = <ul class="nav">|</ul>
	2 < .1
}

menu.subContacts = USER
menu.subContacts {
	userFunc = TYPO3\CMS\Extbase\Core\Bootstrap->run
	pluginName = Pi1
	extensionName = MbxContacts
	vendorName = TYPO3
	switchableControllerActions.Company.1 = list

	mvc =< plugin.tx_mbxcontacts.mvc
	persistence =< plugin.tx_mbxcontacts.persistence
	view =< plugin.tx_mbxcontacts.view
	settings < plugin.tx_mbxcontacts.settings
	settings {
		templateLayout = menuList
		storage = 192
	}
}

[PIDinRootline = {$contentpage.bautUid}] && [globalVar = TSFE:id != {$contentpage.bautCatUid}]
	menu.subExpandable = HMENU
	menu.subExpandable {
		entryLevel = 2
		1 = TMENU
		1 {
			wrap = <div class="expandable-sub-wrap"><ul class="expandable-sub nav inline">|</ul></div>
			NO {
				wrapItemAndSub = <li>|</li>
			}

			ACT {
				wrapItemAndSub = <li class="active">|</li>
			}
			ACT = 1
		}
	}
[global]

[globalVar = TSFE:id = 384]
	menu.sub >
[global]
