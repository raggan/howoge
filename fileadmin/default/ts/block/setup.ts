temp.contentObj = CONTENT
temp.contentObj {
    table = tt_content
    select.languageField = sys_language_uid
    select.max = 1
}

## Configuration of content blocks used in templates
# Configuration for claim
lib.claim < temp.contentObj
lib.claim {
    select.where = colPos=2 AND CType LIKE 'header'
    slide = -1

    renderObj = TEXT
    renderObj {
        field = header
        required = 1

        override {
            if.isTrue.field = subheader
            cObject = TEXT
            cObject.dataWrap = <b>{field:header}</b> {field:subheader}
        }

        stdWrap.innerWrap = <p class="claim white">|</p>
        stdWrap.innerWrap.override = <p class="claim-preview white">|</p>
        stdWrap.innerWrap.override.if.isPositive = {$contentpage.hideNewLogo}
    }
}

# Configuration of info block for image copyrights
lib.imageCopyrights < temp.contentObj
lib.imageCopyrights {
    # customize select query
    select.where = colPos=10 AND CType LIKE 'header'
    select.max = 1

    renderObj = TEXT
    renderObj.dataWrap = <p class="span6 grey font-small">© {field:header}</p>
}

# Configuration of facebbok link on homepage
lib.facebookButton = TEXT
lib.facebookButton {
    value = Die HOWOGE auf Facebook
    typolink {
        parameter = https://www.facebook.com/howoge
        target = _blank
        title = Zur Facebook-Seite der HOWOGE
        ATagParams = class="fb-button font-bold"
    }
}

# Configuration of pinterest link on homepage
lib.pinterestButton = TEXT
lib.pinterestButton {
    value = Die HOWOGE auf Facebook
    typolink {
        parameter = https://de.pinterest.com/howoge
        target = _blank
        title = Zur Pinterest-Seite der HOWOGE
        ATagParams = class="pinterest-button font-bold"
    }
}

## Configuration for additional content areas
# Configuration for teaser areas on homepage
lib.teaserHomepageBig < temp.contentObj
lib.teaserHomepageBig {
    select.andWhere.cObject = TEXT
    select.andWhere.cObject {
        value = colPos=4
    }
}

lib.teaserHomepageSmall < lib.teaserHomepageBig
lib.teaserHomepageSmall.select.andWhere.cObject.value = colPos=5

# Configuration for teaser area on immoobject detail page
lib.immodetailTeaser = COA
lib.immodetailTeaser {
    5 < lib.teaserHomepageBig
    5.select.andWhere.cObject.noTrimWrap = || AND (CType LIKE 'header' OR (CType LIKE 'fluidcontent_content' AND tx_fed_fcefile LIKE '%TeaserDefault.html%'))|
    5.select.max = 2

    wrap = <div id="jsTeaserBottom" class="bottom-teaser">|</div>
}

# render howoge baut logo
lib.templateLogo < temp.contentObj
lib.templateLogo {
    select.where = colPos=10 AND CType LIKE 'image'
    select.max = 1
    slide = -1

    renderObj = IMAGE
    renderObj {
        file.import.field = image
        file.import.listNum = 0
        params = class="img-moving"
    }
    wrap = <div class="logo-aling-right">|</div>
}

# howoge baut zurück button
[PIDinRootline = {$contentpage.bautUid}]
lib.backToPreviousSite = COA
lib.backToPreviousSite {
    10 = TEXT
    10 {
        data = leveltitle : -2
        insertData = 1
        typolink {
            parameter.data = leveluid : -2
            ATagParams = class="back-link iconfont-carat-left"
        }
        stdWrap.cObject = COA
        stdWrap.cObject {

            #Präfix
            10 = TEXT
            10.value = Zurück zu

            20 = TEXT
            20.data = leveltitle: -2
            20.noTrimWrap = | ||
         }
    }
    wrap = <div class="spacer">|</div>
}
[global]