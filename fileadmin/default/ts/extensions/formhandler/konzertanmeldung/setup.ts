# Configuration of search flat form

plugin.Tx_Formhandler.settings.predef.konzertanmeldung {
    name = Formular | Konzertanmeldung

	# The formID setting is mandatory in combination with "enableFileRemoval=1".
    # The formID is used to submit the right form on the page when clicking on the remove link.
    formID = konzertanmeldung
    formValuesPrefix = konzertanmeldung

    # Add additional language file
    langFile.2 = {$filepaths.languageFiles}formhandler/konzertanmeldung.xml

    templateFile = TEXT
    templateFile.value = {$filepaths.extTemplates}formhandler/konzertanmeldung/tpl_form.html

    validators {
        1.class = Validator_Default
        1.config.fieldConf {

            firstname.errorCheck.1 = required
            lastname.errorCheck.1 = required
            street.errorCheck.1 = required
            zip.errorCheck {
						1 = required
						2 = pregMatch
						2.value = %^[0-9]{5}$%
					}
            city.errorCheck.1 = required
            telefon.errorCheck.1 = required
            karten.errorCheck.1 = required
            email.errorCheck {
                1 = required
                2 = email
            }
            #privacy.errorCheck.1 = required
        }
    }

    finishers {
        1.class = Finisher_Mail
        1.config {
            limitMailsToUser = 10
            mailer.class = Mailer_TYPO3Mailer
            checkBinaryCrLf = remarks
            admin {
                subject = TEXT
                subject {
                    wrap = Das Konzertanmeldung-Formular wurde ausgefüllt
                }

                templateFile = TEXT
                templateFile.value = {$filepaths.extTemplates}formhandler/konzertanmeldung/email-admin.html
                sender_email = {$emails.no-reply}
                #to_email = alexander.freitag@mindbox.de
                #to_email = sandra.schultz@mindbox.de
                #to_email = thomas.zichner@mindbox.de
                to_email = howoge.konzertanmeldung@artecom-event.de
            }
            user {
                subject = TEXT
                subject {
                    wrap = Sie haben sich für das Mieterkonzert der HOWOGE angemeldet
                }

                templateFile = TEXT
                templateFile.value = {$filepaths.extTemplates}formhandler/konzertanmeldung/email-user.html
                sender_email = {$emails.no-reply}
                to_email = email
            }
        }

        10.class = Finisher_SubmittedOK
        10.config.returns = 1
    }
}