# Configuration of job application form

plugin.Tx_Formhandler.settings.predef.job-application {
    name = Formular | Online-Bewerbung

	# The formID setting is mandatory in combination with "enableFileRemoval=1".
    # The formID is used to submit the right form on the page when clicking on the remove link.
	formID = job-application-form
    formValuesPrefix = job-application

    # Add additional language file
    langFile.2 = {$filepaths.languageFiles}formhandler/jobApplication.xml

    templateFile = TEXT
    templateFile.value = {$filepaths.extTemplates}formhandler/jobApplication/tpl_form.html

    # These wraps define how the list of uploaded files for a single upload field looks like.
    singleFileMarkerTemplate {
        totalWrap = <div class="uploadedfiles"><div class="uploadedfiles-label">###LLL:uploadedFiles###</div><ul class="fileupload-list">|</ul></div>
        singleWrap = <li>|</li>
        showThumbnails = 0
    }

    preProcessors {
        1.class = PreProcessor_LoadDefaultValues
        1.config {
            # Prefill fields in step 1
            1 {
                job.defaultValue = TEXT
                job.defaultValue.data = GP:job
            }
        }
    }

    markers {
        # Global marker to include job select in template
        jobSelection = USER
        jobSelection {
            userFunc = TYPO3\CMS\Extbase\Core\Bootstrap->run
            pluginName = Pi1
            extensionName = News
            switchableControllerActions.News.1 = list

            mvc =< plugin.tx_news.mvc
            settings < plugin.tx_news.settings
            settings {
                startingpoint = {$plugin.tx_news.jobStorage}

                orderBy = title
                orderDirection = asc
                hidePagination = 1
                disableOverrideDemand = 1

                templateLayout = jobSelection
                formValuesPrefix < plugin.Tx_Formhandler.settings.predef.job-application.formValuesPrefix
            }
            persistence =< plugin.tx_news.persistence
            view =< plugin.tx_news.view
        }
    }

	# The AJAX class takes care of inserting the needed JavaScript code for the file removal.
	ajax {
		class = Tx_Formhandler_AjaxHandler_JQuery
        config.jsPosition = footer
	}

	files {
        # Path to upload the files to (must exist!)
        uploadFolder = fileadmin/_temp_/jobApplicationFiles/

        # Allows the user to remove a previously uploaded file
        enableAjaxFileRemoval = 1

        # The default value of the link to remove a file would be "X".
        customRemovalText = TEXT
        customRemovalText {
            value = delete
            lang.de = löschen
        }
	}

    validators {
        1.class = Validator_Default
        1.config.fieldConf {
            salutation.errorCheck.1 = required
            firstname.errorCheck.1 = required
            lastname.errorCheck.1 = required
            street.errorCheck.1 = required
            streetno.errorCheck.1 = required
            zip.errorCheck.1 = required
            city.errorCheck.1 = required
            birthday.errorCheck {
                1 = required
                2 = date
                2.pattern = d.m.Y
            }
            phone.errorCheck.1 = required
            email.errorCheck {
                1 = required
                2 = email
            }

            job.errorCheck.1 = required
            education-type.errorCheck.1 = required
            starting-date.errorCheck.1 < birthday.errorCheck.2
            salary.errorCheck.1 = integer

            letter.errorCheck {
                1 = required
                2 = fileAllowedTypes
                2.allowedTypes = pdf,jpg,png
                3 = fileMaxTotalSize
                3.maxTotalSize = 5242880
                4 = fileMaxCount
                4.maxCount = 1
            }
            cv.errorCheck < .letter.errorCheck
            certificates.errorCheck < .letter.errorCheck

            #privacy.errorCheck.1 = required
            #privacy-contact.errorCheck.1 = required
        }
    }

    finishers {
        1.class = Finisher_Mail
        1.config {
            mailer.class = Mailer_TYPO3Mailer

            checkBinaryCrLf = remarks
            admin {
                subject = TEXT
                subject {
                    data = DB:tx_news_domain_model_news:{GP:job-application|job}:title
                    data.insertData = 1
                    sanitize = 1
                    wrap = Eine Bewerbung als "|" über www.howoge.de
                }

                templateFile = TEXT
                templateFile.value = {$filepaths.extTemplates}formhandler/jobApplication/email-admin.html
                sender_email = noreply@howoge.de

                # Attach the uploaded files to the email sent out
				attachment = letter,cv,certificates
            }
            user {
                subject = TEXT
                subject = Ihre Bewerbung

                templateFile = TEXT
                templateFile.value = {$filepaths.extTemplates}formhandler/jobApplication/email-user.html

                to_email = email
                sender_email = noreply@howoge.de

            }
        }

        2.class = Finisher_StoreUploadedFiles
        2.config {
            finishedUploadFolder = fileadmin/jobApplicationFiles/
            renameScheme = [time]_[filename]
        }

        5.class = Finisher_SubmittedOK
        5.config.returns = 1
    }
}



[IP = 217.18.180.50]
    #plugin.Tx_Formhandler.settings.predef.job-application.finishers.1.config.admin.disable = 1
    #plugin.Tx_Formhandler.settings.predef.job-application.debug = 1
[global]