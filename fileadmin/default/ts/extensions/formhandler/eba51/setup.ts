# Configuration of search flat form

plugin.Tx_Formhandler.settings.predef.eby51 {
    debug = 0

    name = Formular | Interessentenbogen EBA51

	# The formID setting is mandatory in combination with "enableFileRemoval=1".
    # The formID is used to submit the right form on the page when clicking on the remove link.
    formID = eba51-form
    formValuesPrefix = eba51-form

    # Add additional language file
    langFile.2 = {$filepaths.languageFiles}formhandler/eba51.xml

    templateFile = TEXT
    templateFile.value = {$filepaths.extTemplates}formhandler/eba51/tpl_form.html

    markers {
        formID = eba51-form
    }

    jsFile {
        jquery = fileadmin/default/js/lib/jquery-3.1.1.min.js
        eba51main = fileadmin/default/js/forms/eba51main.js
    }

    cssFile {
        eba = fileadmin/default/css/forms/eba51.css
    }

    validators >
    validators {
        1.class = Validator_Default
        1.config.fieldConf {

            eba_choice.errorCheck.1 = required

            first-renter_title.errorCheck.1 = required
            first-renter_firstname.errorCheck.1 = required
            first-renter_lastname.errorCheck.1 = required
            first-renter_address.errorCheck.1 = required
            first-renter_zip-city.errorCheck.1 = required
            first-renter_telephone-private.errorCheck.1 = required
            first-renter_telephone-business.errorCheck.1 = required
            first-renter_email.errorCheck.1 = required
            first-renter_email.errorCheck.2 = email
            first-renter_education.errorCheck.1 = required

            eba_entrydate.errorCheck.1 = required
            eba_exitdate.errorCheck.1 = required

            #privacy.errorCheck.1 = required
            privacy-contact.errorCheck.1 = required

        }
    }

    if {

        1   {
            conditions.OR1.AND1 = eba_choice = double-apartment
            isTrue {
                1 {
                    validators.1.config.fieldConf {
                        eba_doublewish.errorCheck.1 = required
                    }
                    markers{
                        eba_choice_double_checked = TEXT
                        eba_choice_double_checked = checked="checked"
                    }
                }
            }
        }
        2   {
            conditions.OR1.AND1 = eba_choice = single-apartment
            isTrue {
                1 {
                    markers{
                        eba_choice_single_checked = TEXT
                        eba_choice_single_checked = checked="checked"
                    }
                }
            }
        }
    }


    finishers {
        1.class = Finisher_Mail
        1.config {
            limitMailsToUser = 10
            mailer.class = Mailer_TYPO3Mailer
            checkBinaryCrLf = remarks
            admin {
                subject = TEXT
                subject {
                    value = Ein Interessentenbogen wurde ausgefüllt
                    override {
                        cObject = RECORDS
                        cObject {
                            tables = tx_mbxrealestate_domain_model_immoaddress
                            source.data = GP:search-flat|flatStreet
                            conf.tx_mbxrealestate_domain_model_immoaddress = TEXT
                            conf.tx_mbxrealestate_domain_model_immoaddress.dataWrap = Ein Interessentenbogen wurde ausgefüllt // {field:street}, WI-Nr.: {GP:search-flat|flatNo}
                        }
                        if {
                            isTrue.data = GP:search-flat|flatNo
                            isFalse = 1
                            isFalse.if {
                                value = 0
                                equals.numRows {
                                    table = tx_mbxrealestate_domain_model_immoaddress
                                    select {
                                        pidInList = 120
                                        recursive = 1
                                        andWhere {
                                            data = GP:search-flat|flatStreet
                                            intval = 1
                                            wrap = uid=|
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                templateFile = TEXT
                templateFile.value = {$filepaths.extTemplates}formhandler/eba51/email-admin.html
                sender_email = {$emails.no-reply}
                to_email = eichbuschallee@howoge.de
            }
            # Don't send e-mail to user
            user.disable = 1
        }

        2.class = Finisher_SubmittedOK
        2.config.returns = 1
    }
}

//Live System Eng Form
[globalString = IENV:HTTP_HOST = *howoge.de] && [globalVar = TSFE:id=887]
    plugin.Tx_Formhandler.settings.predef.eby51.langFile.2 = {$filepaths.languageFiles}formhandler/eba51_en.xml
    plugin.Tx_Formhandler.settings.predef.eby51.markers.privacyLink.lang.de = privacy
[global]