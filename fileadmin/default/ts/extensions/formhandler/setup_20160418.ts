/*
default setup of extension formhandler
 */

plugin.Tx_Formhandler.settings {
    debug = {$config.debug}

    disableWrapInBaseClass = 1

    formValuesPrefix = formhandler
    requiredSign = &nbsp;*

    # Define master language file
    langFile.99 = {$filepaths.languageFiles}formhandler/master.xml

    # Define master template file
    masterTemplateFile.1 = {$filepaths.extTemplates}formhandler/tpl_master.html

    isErrorMarker {
        default = error
    }

    singleErrorTemplate {
        totalWrap = |
        singleWrap = title="|"
    }

    markers {
        privacyLink = TEXT
        privacyLink {
            value = privacy policy
            lang.de = Datenschutz
            typolink.parameter = {$contentpage.privacyUid}
            typolink.additionalParams = &type=100
            typolink.ATagParams = class="cboxElementIframe iframeFlexHeight"
        }
    }

    initInterceptors {
        1.class = Tx_Formhandler_Interceptor_StdWrap
        1.config.fieldConf {
            email.replacement {
                10 {
                    search = /[ ´`'"]/
                    useRegExp = 1
                    replace =
                }
            }
        }
    }
}

<INCLUDE_TYPOSCRIPT: source="FILE:fileadmin/default/ts/extensions/formhandler/jobApplication/setup.ts">
<INCLUDE_TYPOSCRIPT: source="FILE:fileadmin/default/ts/extensions/formhandler/pressMailing/setup.ts">
<INCLUDE_TYPOSCRIPT: source="FILE:fileadmin/default/ts/extensions/formhandler/constructionSiteInterest/setup.ts">
<INCLUDE_TYPOSCRIPT: source="FILE:fileadmin/default/ts/extensions/formhandler/constructionSiteInterestNoText/setup.ts">
<INCLUDE_TYPOSCRIPT: source="FILE:fileadmin/default/ts/extensions/formhandler/searchFlat/setup.ts">
<INCLUDE_TYPOSCRIPT: source="FILE:fileadmin/default/ts/extensions/formhandler/treskowSearchFlat/setup.ts">
<INCLUDE_TYPOSCRIPT: source="FILE:fileadmin/default/ts/extensions/formhandler/eventVitallauf/setup.ts">
<INCLUDE_TYPOSCRIPT: source="FILE:fileadmin/default/ts/extensions/formhandler/serviceSurvey/setup.ts">
<INCLUDE_TYPOSCRIPT: source="FILE:fileadmin/default/ts/extensions/formhandler/contact/setup.ts">