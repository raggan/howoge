# Configuration of contact form

lib.contactForm < plugin.tx_formhandler_pi1
lib.contactForm.settings {
    formValuesPrefix = contact

    templateFile = TEXT
    templateFile.value = {$filepaths.extTemplates}formhandler/contact/tpl_form.html

    markers {
        faqFields = TEXT
        faqFields.dataWrap (
            <input type="hidden" class="hide" name="tx_mbxfaq_pi1[faqTarget]" value="{GP:tx_mbxfaq_pi1|faqTarget}" />
            <input type="hidden" class="hide" name="tx_mbxfaq_pi1[faqCategory]" value="{GP:tx_mbxfaq_pi1|faqCategory}" />
            <input type="hidden" class="hide" name="tx_mbxrealestate_pi1[immoaddress][street]" value="{GP:tx_mbxrealestate_pi1|immoaddress|street}" />
        )
    }


    preProcessors {
        1.class = PreProcessor_LoadDefaultValues
        1.config {
            # Prefill fields in step 1
            1 {
                address.defaultValue = TEXT
                address.defaultValue.data = GP:tx_mbxrealestate_pi1|immoaddress|street

                contact-by.defaultValue = TEXT
                contact-by.defaultValue.value = email
            }
        }
    }

    validators {
        1.class = Validator_Default
        1.config.fieldConf {
            firstname.errorCheck.1 = required
            lastname.errorCheck.1 = required
            address.errorCheck.1 = required
            zip.errorCheck.1 = required
            city.errorCheck.1 = required
            message.errorCheck.1 = required
            contact-by.errorCheck.1 = required
            #privacy.errorCheck.1 = required
            privacy-contact.errorCheck.1 = required
        }
    }

    if {
        1 {
            conditions {
                OR1.AND1 = contact-by = email
            }
            isTrue {
                validators.1.config.fieldConf {
                    email.errorCheck {
                        1 = required
                        2 = email
                    }
                }
            }
            else {
                validators.1.config.fieldConf {
                    phone.errorCheck.1 = required
                }
            }
        }
    }


    finishers {
        1.class = Finisher_Mail
        1.config {
            limitMailsToUser = 5
            mailer.class = Mailer_TYPO3Mailer
            checkBinaryCrLf = message
            admin {
                subject = TEXT
                subject.value = Kontaktanfrage über www.howoge.de
                templateFile = TEXT
                templateFile.value = {$filepaths.extTemplates}formhandler/email-admin.html
                sender_email = {$emails.no-reply}
                // Die E-Mail-Adresse wird im f:cobject-Tag des FAQ-Templates an die Formularkonfiguration übergeben
                to_email = TEXT
                to_email.current = 1
            }
            # Don't send e-mail to user
            user.disable = 1
        }

        10.class = Finisher_SubmittedOK
        10.config.returns = 1
    }
}