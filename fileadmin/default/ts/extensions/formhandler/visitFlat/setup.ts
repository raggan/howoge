# Configuration of search flat form

plugin.Tx_Formhandler.settings.predef.visit-flat {
    name = Formular | Besichtigung
    debug = 0

    formID = visit-flat-form
    formValuesPrefix = visit-flat

    # Add additional language file
    langFile.2 = {$filepaths.languageFiles}formhandler/visitFlat.xml

    templateFile = TEXT
    templateFile.value = {$filepaths.extTemplates}formhandler/visitFlat/tpl_form.html


    preProcessors {
        1.class = PreProcessor_LoadDefaultValues
        1.config {
            # Prefill fields in step 1
            1 {
                objectID.defaultValue = TEXT
                objectID.defaultValue.data = GP:wi
            }
        }
    }

    validators {
        1.class = Validator_Default
        1.config.fieldConf {
            salutation.errorCheck.1  = required
            first-renter_firstname.errorCheck.1  = required
            first-renter_lastname.errorCheck.1  = required
            first-renter_email.errorCheck.1  = required
            first-renter_telephone.errorCheck.1  = required
            message.errorCheck.1  = required

            #privacy.errorCheck.1 = required
            privacy-contact.errorCheck.1 = required
        }
    }

    initInterceptors {
        1 {
            class = \MBX\MbxBuildingprojects\Utility\Form\ImmoUrlInterceptor
            config {
                gpName = exposeURL
                detailPageUid = 155
            }
        }
        10 {
            class = \MBX\MbxBuildingprojects\Utility\Form\ExtendRealestateEmailInformationInterceptor
            config {
                domain = {$config.domain}
                gewerbePid = 208
                fields  {
                    titel =
                    groesse =
                    preis =
                    strasse =
                    hausnummer =
                    PLZ =
                    stadt =
                    bild =
                    ansprechpartner =
                }
            }
        }
    }

    markers {
        flatTitle = TEXT
        flatTitle.data = GP:flatTitle

        wi = TEXT
        wi.data = GP:wi
    }

    finishers {
        1.class = Finisher_Mail
        1.config {
            mailer.class = Mailer_TYPO3Mailer
            user.disable = 1

            admin {
                subject = TEXT
                subject.value = Formular | Besichtigungstermin
                templateFile = TEXT
                templateFile.value = {$filepaths.extTemplates}formhandler/visitFlat/email-admin.html
                sender_email = {$emails.no-reply}
                to_email = anfrage@howoge.de
                html.arrayValueSeparator = ,&nbsp;
            }
        }

        2.class = Finisher_SubmittedOK
        2.config.returns = 1

    }
}

[IP = 217.18.180.50]
plugin.Tx_Formhandler.settings.predef.visit-flat.finishers.1.config.admin.to_email = denis.krueger@mindbox.de
[global]
