# Configuration of search flat form

plugin.Tx_Formhandler.settings.predef.defectreport {
    name = Formular | Mangelmeldung
    debug = 0

    # The formID setting is mandatory in combination with "enableFileRemoval=1".
    # The formID is used to submit the right form on the page when clicking on the remove link.
    formID = defectreport
    formValuesPrefix = defectreport

    # Add additional language file
    langFile.2 = {$filepaths.languageFiles}formhandler/defectreport.xml

    templateFile = TEXT
    templateFile.value = {$filepaths.extTemplates}formhandler/defectreport/tpl_form.html


    validators {
        1.class = Validator_Default
        1.config.fieldConf {
            contract-nr.errorCheck.1 = required
            first-renter.errorCheck.1 = required
            adress.errorCheck.1 = required
            we.errorCheck.1 = required

            defect-description.errorCheck.1 = required
            mail.errorCheck.1 = required
        }
    }

    finishers {

        1.class = Finisher_Mail
        1.config {
            limitMailsToUser = 10
            mailer.class = Mailer_TYPO3Mailer
            checkBinaryCrLf = references,split-space,special-equipment
            admin {
                subject = TEXT
                subject.value = HOWOGE Mangelmeldung

                templateFile = TEXT
                templateFile.value = {$filepaths.extTemplates}formhandler/defectreport/email-admin.html
                sender_email = {$emails.no-reply}

                to_email = tk@howoge.de
                html.arrayValueSeparator = ,&nbsp;
            }

            user {
                subject = TEXT
                subject.value = HOWOGE Mangelmeldung

                templateFile = TEXT
                templateFile.value = {$filepaths.extTemplates}formhandler/defectreport/email-user.html
                sender_email = {$emails.no-reply}

                to_email = mail
                html.arrayValueSeparator = ,&nbsp;
            }
        }

        10.class = Finisher_SubmittedOK
        10.config.returns = 1
    }
}

[IP = 217.18.180.50]
    plugin.Tx_Formhandler.settings.predef.defectreport.debug = 0
    plugin.Tx_Formhandler.settings.predef.defectreport.finishers.1.config.admin.to_email = otto.apel@mindbox.de
[global]