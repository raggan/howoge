# Configuration of search flat form

plugin.Tx_Formhandler.settings.predef.search-flat-mini {
    name = Formular | Interessentenbogen (minimal)
    debug = 0

    # The formID setting is mandatory in combination with "enableFileRemoval=1".
    # The formID is used to submit the right form on the page when clicking on the remove link.
    formID = search-flat-form
    formValuesPrefix = search-flat-mini

    # Add additional language file
    langFile.2 = {$filepaths.languageFiles}formhandler/searchFlat.xml

    templateFile = TEXT
    templateFile.value = {$filepaths.extTemplates}formhandler/searchFlatMini/tpl_form.html

    preProcessors {
        1.class = PreProcessor_LoadGetPost
    }

    validators {
        1.class = Validator_Default
        1.config.fieldConf {

            number-of-rooms.errorCheck.1 = required
            number-of-rooms.errorCheck.2 = integer
            number-of-rooms.errorCheck.3 = betweenValue
            number-of-rooms.errorCheck.3 {
                minValue = 1
                maxValue = 10
            }
            flat-space_from.errorCheck.1 = required
            flat-space_from.errorCheck.2 = integer
            flat-space_from.errorCheck.3 = betweenValue
            flat-space_from.errorCheck.3 {
                minValue = 10
                maxValue = 200
            }
            flat-space_to.errorCheck.1 = required
            flat-space_to.errorCheck.2 = integer
            flat-space_to.errorCheck.3 = betweenValue
            flat-space_to.errorCheck.3 {
                minValue = 11
                maxValue = 200
            }
            maximum-rent.errorCheck.1 = required
            maximum-rent.errorCheck.2 = integer
            maximum-rent.errorCheck.3 = betweenValue
            maximum-rent.errorCheck.3 {
                minValue = 50
                maxValue = 4000
            }

            wbs.errorCheck.1 = required

            first-renter_firstname.errorCheck.1 = required
            first-renter_lastname.errorCheck.1 = required

            first-renter_telephone-private.errorCheck.1 = required
            first-renter_telephone-private.errorCheck.2 = pregMatch
            first-renter_telephone-private.errorCheck.2.value = /^([0-9\/\s\-]+)$/

            #privacy.errorCheck.1 = required
        }
    }

    if {

        1 {
            conditions.OR1.AND1 = first-renter_email=
            isTrue {
                validators.1.config.fieldConf.first-renter_address.errorCheck.1 = required
                validators.1.config.fieldConf.first-renter_housenum.errorCheck.1 = required
                validators.1.config.fieldConf.first-renter_zip-city.errorCheck.1 = required
            }
            else {
                validators.1.config.fieldConf.first-renter_email.errorCheck.1 = required
                validators.1.config.fieldConf.first-renter_email.errorCheck.2 = email
            }
        }
        2 {
            conditions.OR1.AND1 = first-renter_address=
            conditions.OR1.AND2 = first-renter_housenum=
            conditions.OR1.AND3 = first-renter_zip-city=
            isTrue < plugin.Tx_Formhandler.settings.predef.search-flat.if.1.else
            else < plugin.Tx_Formhandler.settings.predef.search-flat.if.1.isTrue
        }
        8 {
            conditions.OR1.AND1 = first-renter_birthday=
            else {
                validators.1.config.fieldConf.first-renter_birthday.errorCheck.2 = date
                validators.1.config.fieldConf.first-renter_birthday.errorCheck.2.pattern = d.m.Y
            }
        }
        11 {
            conditions.OR1.AND1 = wbs = Ja
            isTrue {
                validators.1.config.fieldConf.wbs-rooms.errorCheck.1 = required
                validators.1.config.fieldConf.wbs-rooms.errorCheck.2 = integer
                validators.1.config.fieldConf.wbs-rooms.errorCheck.3 = betweenValue
                validators.1.config.fieldConf.wbs-rooms.errorCheck.3 {
                    minValue = 1
                    maxValue = 10
                }
                validators.1.config.fieldConf.wbs-nr.errorCheck.1 = required
                validators.1.config.fieldConf.wbs-date.errorCheck.1 = required
                validators.1.config.fieldConf.wbs-date.errorCheck.2 = date
                validators.1.config.fieldConf.wbs-date.errorCheck.2.pattern = d.m.Y
            }
        }

        12 {
            conditions.OR1.AND1 = wbs-rooms=
            conditions.OR1.AND2 = wbs-nr=
            conditions.OR1.AND3 = wbs-date=
            else < plugin.Tx_Formhandler.settings.predef.search-flat.if.11.isTrue
        }

        13 {
            conditions.OR1.AND1 = wbs-date=
            else {
                validators.1.config.fieldConf.wbs-date.errorCheck.2 = date
                validators.1.config.fieldConf.wbs-date.errorCheck.2.pattern = d.m.Y
            }
        }

    }

    finishers {

        3.class = Finisher_Mail
        3.config {
            mailer.class = Mailer_TYPO3Mailer
            user {
                subject = TEXT
                subject.value = Interessentenbogen Lindenhof wurde ausgefüllt
                templateFile = TEXT
                templateFile.value = {$filepaths.extTemplates}formhandler/searchFlat/email-user.html
                sender_email = {$emails.no-reply}
                to_email = first-renter_email
                html.arrayValueSeparator = ,&nbsp;
            }
            admin {
                subject = TEXT
                subject.value = Interessentenbogen Lindenhof wurde ausgefüllt
                templateFile = TEXT
                templateFile.value = {$filepaths.extTemplates}formhandler/searchFlat/email-admin.html
                sender_email = {$emails.no-reply}
                to_email =  lindenhof@howoge.de
                html.arrayValueSeparator = ,&nbsp;
            }
        }

        10.class = Finisher_SubmittedOK
        10.config.returns = 1
    }
}

[IP = 217.18.180.50]
    plugin.Tx_Formhandler.settings.predef.search-flat-mini.finishers.3.config.admin.to_email = christoph.jerchel@mindbox.de
[global]


[globalVar = TSFE:id = 727,735]
    plugin.Tx_Formhandler.settings.predef.search-flat-mini.finishers.3.config.user.subject.value = Interessentenbogen Dolgenseestraße wurde ausgefüllt
    plugin.Tx_Formhandler.settings.predef.search-flat-mini.finishers.3.config.admin.subject.value = Interessentenbogen Dolgenseestraße wurde ausgefüllt
    plugin.Tx_Formhandler.settings.predef.search-flat-mini.finishers.3.config.admin.to_email =  dolgenseestraße@howoge.de
[global]