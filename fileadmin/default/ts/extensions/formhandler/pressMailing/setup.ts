# Configuration of registration for press mailing

plugin.Tx_Formhandler.settings.predef.press-mailing {
    name = Formular | Presseverteiler
    formValuesPrefix = press-mailing

    # Add additional language file
    langFile.2 = {$filepaths.languageFiles}formhandler/pressMailing.xml

    templateFile = TEXT
    templateFile.value = {$filepaths.extTemplates}formhandler/pressMailing/tpl_form.html

    validators {
        1.class = Validator_Default
        1.config.fieldConf {
            salutation.errorCheck.1 = required
            firstname.errorCheck.1 = required
            lastname.errorCheck.1 = required
            phone.errorCheck.1 = required
            email.errorCheck {
                1 = required
                2 = email
            }
            presscompany.errorCheck.1 = required
            #privacy.errorCheck.1 = required
        }
    }

    finishers {
        1.class = Finisher_Mail
        1.config {
            mailer.class = Mailer_TYPO3Mailer
            admin {
                templateFile = TEXT
                templateFile.value = {$filepaths.extTemplates}formhandler/email-admin.html
                sender_email = {$emails.no-reply}
                to_email = presse@howoge.de
				subject = TEXT
				subject.data = LLL:{$filepaths.languageFiles}formhandler/pressMailing.xml:email_admin_subject
            }
            # Don't send e-mail to user
            user.disable = 1
        }

        2.class = Finisher_SubmittedOK
        2.config.returns = 1
    }
}

[applicationContext = Development]
    plugin.Tx_Formhandler.settings.predef.press-mailing.finishers.1.config.admin.to_email = christoph.jerchel@mindbox.de
[global]