# Configuration of search flat form

plugin.Tx_Formhandler.settings.predef.eventVitallauf {
    name = Formular | Vitallauf

	# The formID setting is mandatory in combination with "enableFileRemoval=1".
    # The formID is used to submit the right form on the page when clicking on the remove link.
    formID = eventVitallauf-form
    formValuesPrefix = event

    # Add additional language file
    langFile.2 = {$filepaths.languageFiles}formhandler/eventVitallauf.xml

    templateFile = TEXT
    templateFile.value = {$filepaths.extTemplates}formhandler/eventVitallauf/tpl_form.html

    validators {
        1.class = Validator_Default
        1.config.fieldConf {

            firstname.errorCheck.1 = required
            lastname.errorCheck.1 = required
            birthday.errorCheck.1 = required
            email.errorCheck {
                1 = required
                2 = email
            }
            salutation.errorCheck.1 = required
            run.errorCheck.1 = required

            #privacy.errorCheck.1 = required
        }
    }

    finishers {
        1.class = Finisher_Mail
        1.config {
            limitMailsToUser = 10
            mailer.class = Mailer_TYPO3Mailer
            checkBinaryCrLf = remarks
            admin {
                subject = TEXT
                subject {
                    wrap = Das Vitallauf-Formular wurde ausgefüllt
                }

                templateFile = TEXT
                templateFile.value = {$filepaths.extTemplates}formhandler/eventVitallauf/email-admin.html
                sender_email = {$emails.no-reply}
                to_email = howoge@artecom-event.de
            }
            user {
                subject = TEXT
                subject {
                    wrap = Sie haben sich erfolgreich für den Vitallauf angemeldet
                }

                templateFile = TEXT
                templateFile.value = {$filepaths.extTemplates}formhandler/eventVitallauf/email-user.html
                sender_email = {$emails.no-reply}
                to_email = email
            }
        }

        10.class = Finisher_SubmittedOK
        10.config.returns = 1
    }
}