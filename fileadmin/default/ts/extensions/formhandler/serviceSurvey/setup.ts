    # Configuration of service survey form

plugin.Tx_Formhandler.settings.predef.serviceSurvey {
    name = Formular | Serviceumfrage

    formValuesPrefix = serviceSurvey

    # Add additional language file
    langFile.2 = {$filepaths.languageFiles}formhandler/serviceSurvey.xml

    # Additional master template for custom fieldsets
    masterTemplateFile.2 = {$filepaths.extTemplates}formhandler/serviceSurvey/tpl_master-additional.html

    templateFile = TEXT
    templateFile.value = {$filepaths.extTemplates}formhandler/serviceSurvey/tpl_form.html

    markers {
        # Global marker to include job select in template
        customerCenterRadioGroup = USER
        customerCenterRadioGroup {
            userFunc = TYPO3\CMS\Extbase\Core\Bootstrap->run
            pluginName = Pi1
            extensionName = MbxRealestate
            vendorName = TYPO3
            switchableControllerActions.Immocontact.1 = list

            mvc =< plugin.tx_mbxrealestate.mvc
            settings < plugin.tx_mbxrealestate.settings
            settings {
                customAddressSearch = customercenter

                templateLayout = radioGroup
                formValuesPrefix < plugin.Tx_Formhandler.settings.predef.serviceSurvey.formValuesPrefix
                radioGroupPrefix = customerCenter
            }
            persistence =< plugin.tx_mbxrealestate.persistence
            view =< plugin.tx_mbxrealestate.view
        }
    }

    validators {
        1.class = Validator_Default
        1.config.fieldConf {
            customerCenter.errorCheck.1 = required
            contactMedium.errorCheck.1 = required
            contactTopic.errorCheck.1 = required
            contactTime.errorCheck.1 = required
            contactTimeOk.errorCheck.1 = required
        }
    }

    if {
        1 {
            conditions {
                OR1.AND1 = contactMedium = E-Mail
            }
            isTrue {
                validators.1.config.fieldConf {
                    automaticEmailConfirmation.errorCheck.1 = required
                }
            }
        }
    }

	saveInterceptors.1 {

		# This Interceptor will check if the user needed at least 4 seconds to fill out the form. If not, the user gets redirected to a "SPAM detected" page.
		class = Interceptor_AntiSpamFormTime
		config {
			minTime {
				value = 10
				unit = seconds
			}
		}
	}

    finishers {
        1.class = Finisher_SubmittedOK
        1.config.returns = 1
    }
}