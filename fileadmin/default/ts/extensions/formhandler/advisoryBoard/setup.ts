# DEV
# Configuration Mieterbeirat Wahlvorschlagformular
plugin.Tx_Formhandler.settings.predef.advisoryBoard {
	name = Formular | Mieterbeirat Bewerbung
	debug = 0
	storagePid = 359
	formValuesPrefix = advisoryBoard

	# Add additional language file
	langFile.2 = {$filepaths.languageFiles}formhandler/advisoryBoard.xml

	templateFile = TEXT
	templateFile.value = {$filepaths.extTemplates}formhandler/advisoryBoard/tpl_form.html

	markers {

	}

	validators {
		1.class = Validator_Default
		1.config.fieldConf {
			firstname.errorCheck.1 = required
			lastname.errorCheck.1 = required
			street.errorCheck.1 = required
			streetno.errorCheck.1 = required
			zip.errorCheck.1 = required
			city.errorCheck.1 = required
			birthday.errorCheck{
				1 = required
				2 = date
				2.pattern = d.m.Y
			}
			profession.errorCheck.1 = required
			publicOccupations.errorCheck.1 = required
			contractId.errorCheck.1 = required
			email.errorCheck {
				#1 = required
				2 = email
			}
			phone.errorCheck.1 = required
			advisoryBoardType.errorCheck.1 = required
		}

		2.class = \MBX\MbxAdvisoryboard\Validator\ContractIdValidator
		2.config {
			param = contractId
			storagePid < plugin.Tx_Formhandler.settings.predef.advisoryBoard.storagePid
		}

		#3.class = \MBX\MbxAdvisoryboard\Validator\CheckboxValidator
		#3.config {

		#	param {
		#		10 = commission
		#		20 = runForAdvisoryBoard
		#	}
		#}
	}

	if.1 {
		conditions.OR1.AND1 = advisoryBoardType=Mieterrat
		isTrue.validators.1.config.fieldConf {
			occupationConflict.errorCheck.1 = required
		}
	}

	loggers {
		1.class = Logger_DB
		1.config {
			pid < plugin.Tx_Formhandler.settings.predef.advisoryBoard.storagePid
		}
	}

	finishers {
		10.class = Finisher_Mail
		10.config {
			limitMailsToUser = 5
			mailer.class = Mailer_TYPO3Mailer
			user {
				subject = TEXT
				subject.value = Ihre Bewerbung ist eingegangen
				templateFile = TEXT
				templateFile.value = {$filepaths.extTemplates}formhandler/advisoryBoard/email-user.html
				sender_email = {$emails.no-reply}
				to_email = email
			}
			admin {
				subject = TEXT
				subject.value = Bewerbung Mieterrat
				templateFile = TEXT
				templateFile.value = {$filepaths.extTemplates}formhandler/advisoryBoard/email-admin.html
				sender_email = {$emails.no-reply}
				to_email = mieterrat@howoge.de
			}
		}



		20.class = Finisher_SubmittedOK
		20.config.returns = 1
	}
}


# LIVE
[globalString = IENV:HTTP_HOST=*howoge.de]
	plugin.Tx_Formhandler.settings.predef.advisoryBoard.storagePid = 615
	plugin.Tx_Formhandler.settings.predef.advisoryBoard.loggers.1.config.pid = 615
	plugin.Tx_Formhandler.settings.predef.advisoryBoard.validators.2.config.storagePid = 615
[global]

