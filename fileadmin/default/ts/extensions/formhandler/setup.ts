/*
default setup of extension formhandler
 */

plugin.Tx_Formhandler.settings {
    debug = 0

    disableWrapInBaseClass = 1

    formValuesPrefix = formhandler
    requiredSign = &nbsp;*

    # Define master language file
    langFile.99 = {$filepaths.languageFiles}formhandler/master.xml

    # Define master template file
    masterTemplateFile.1 = {$filepaths.extTemplates}formhandler/tpl_master.html

    isErrorMarker {
        default = error
    }

    singleErrorTemplate {
        totalWrap = |
        singleWrap = title="|"
    }

    markers {
        privacyLink = TEXT
        privacyLink {
            value = privacy policy
            lang.de = Datenschutz
            typolink.parameter = {$contentpage.privacyUid}
            typolink.additionalParams = &type=100
            typolink.ATagParams = class="cboxElementIframe iframeFlexHeight"
        }
    }

    initInterceptors {
        1.class = Tx_Formhandler_Interceptor_StdWrap
        1.config.fieldConf {
            email.replacement {
                10 {
                    search = /[ ´`'"]/
                    useRegExp = 1
                    replace =
                }
            }
        }
    }
}

<INCLUDE_TYPOSCRIPT: source="FILE:fileadmin/default/ts/extensions/formhandler/jobApplication/setup.ts">
<INCLUDE_TYPOSCRIPT: source="FILE:fileadmin/default/ts/extensions/formhandler/pressMailing/setup.ts">
<INCLUDE_TYPOSCRIPT: source="FILE:fileadmin/default/ts/extensions/formhandler/constructionSiteInterest/setup.ts">
<INCLUDE_TYPOSCRIPT: source="FILE:fileadmin/default/ts/extensions/formhandler/constructionSiteInterestNoText/setup.ts">
<INCLUDE_TYPOSCRIPT: source="FILE:fileadmin/default/ts/extensions/formhandler/advisoryBoard/setup.ts">
<INCLUDE_TYPOSCRIPT: source="FILE:fileadmin/default/ts/extensions/formhandler/searchFlat/setup.ts">
<INCLUDE_TYPOSCRIPT: source="FILE:fileadmin/default/ts/extensions/formhandler/searchFlatMini/setup.ts">
<INCLUDE_TYPOSCRIPT: source="FILE:fileadmin/default/ts/extensions/formhandler/searchFlatBusiness/setup.ts">
<INCLUDE_TYPOSCRIPT: source="FILE:fileadmin/default/ts/extensions/formhandler/treskowSearchFlat/setup.ts">
<INCLUDE_TYPOSCRIPT: source="FILE:fileadmin/default/ts/extensions/formhandler/eba51/setup.ts">
<INCLUDE_TYPOSCRIPT: source="FILE:fileadmin/default/ts/extensions/formhandler/eventVitallauf/setup.ts">
<INCLUDE_TYPOSCRIPT: source="FILE:fileadmin/default/ts/extensions/formhandler/serviceSurvey/setup.ts">
<INCLUDE_TYPOSCRIPT: source="FILE:fileadmin/default/ts/extensions/formhandler/contact/setup.ts">
<INCLUDE_TYPOSCRIPT: source="FILE:fileadmin/default/ts/extensions/formhandler/visitFlat/setup.ts">
<INCLUDE_TYPOSCRIPT: source="FILE:fileadmin/default/ts/extensions/formhandler/visitProject/setup.ts">
<INCLUDE_TYPOSCRIPT: source="FILE:fileadmin/default/ts/extensions/formhandler/defectreport/setup.ts">
<INCLUDE_TYPOSCRIPT: source="FILE:fileadmin/default/ts/extensions/formhandler/ukmailing/setup.ts">
