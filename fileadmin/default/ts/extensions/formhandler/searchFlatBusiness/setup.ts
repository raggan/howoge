# Configuration of search flat form

plugin.Tx_Formhandler.settings.predef.search-flat-business {
    name = Formular | Interessentenbogen Gewerbe
    debug = 0

    # The formID setting is mandatory in combination with "enableFileRemoval=1".
    # The formID is used to submit the right form on the page when clicking on the remove link.
    formID = search-flat-business
    formValuesPrefix = search-flat-business

    # Add additional language file
    langFile.2 = {$filepaths.languageFiles}formhandler/searchFlatBusiness.xml

    templateFile = TEXT
    templateFile.value = {$filepaths.extTemplates}formhandler/searchFlatBusiness/tpl_form.html


    validators {
        1.class = Validator_Default
        1.config.fieldConf {
            name-and-legal.errorCheck.1 = required
            sector.errorCheck.1 = required

            contact_firstname.errorCheck.1 = required
            contact_lastname.errorCheck.1 = required
            contact_address.errorCheck.1 = required
            contact_zip-city.errorCheck.1 = required
            contact_telephone-business.errorCheck.1 = required
            contact_email.errorCheck.1 = required
            contact_email.errorCheck.2 = email
            contact_homepage.errorCheck.1 = required

            flat-space_from.errorCheck.1 = required
            flat-space_to.errorCheck.1 = required

            #special-equipment.errorCheck.1 = required
            #special-equipment.errorCheck.2 = minLength
            #special-equipment.errorCheck.2.value = 1
        }
    }


    finishers {

        1.class = Finisher_Mail
        1.config {
            limitMailsToUser = 10
            mailer.class = Mailer_TYPO3Mailer
            checkBinaryCrLf = references,split-space,special-equipment
            admin {
                subject = TEXT
                subject.value = Ein Gewerbe-Interessentenbogen wurde ausgefüllt

                templateFile = TEXT
                templateFile.value = {$filepaths.extTemplates}formhandler/searchFlatBusiness/email-admin.html
                sender_email = {$emails.no-reply}

                to_email = ralf.staniek@howoge.de
                html.arrayValueSeparator = ,&nbsp;
            }

            # Don't send e-mail to user
            user.disable = 1
        }

        3.class = Finisher_Mail
        3.config {
            mailer.class = Mailer_TYPO3Mailer
            checkBinaryCrLf = references,split-space,special-equipment
            user {
                subject = TEXT
                subject.value = Interessentenbogen wurde ausgefüllt
                templateFile = TEXT
                templateFile.value = {$filepaths.extTemplates}formhandler/searchFlatBusiness/email-user.html
                sender_email = {$emails.no-reply}
                to_email = contact_email
                html.arrayValueSeparator = ,&nbsp;
            }

            admin.disable = 1
        }

        10.class = Finisher_SubmittedOK
        10.config.returns = 1
    }
}

[IP = 217.18.180.50]
    #plugin.Tx_Formhandler.settings.predef.search-flat-business.debug = 1
[global]
