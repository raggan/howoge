# Configuration of search flat form

plugin.Tx_Formhandler.settings.predef.search-flat {
    name = Formular | Interessentenbogen
    debug = 0

    # The formID setting is mandatory in combination with "enableFileRemoval=1".
    # The formID is used to submit the right form on the page when clicking on the remove link.
    formID = search-flat-form
    formValuesPrefix = search-flat

    # Add additional language file
    langFile.2 = {$filepaths.languageFiles}formhandler/searchFlat.xml

    templateFile = TEXT
    templateFile.value = {$filepaths.extTemplates}formhandler/searchFlat/tpl_form.html

    markers {
        flatNo = RECORDS
        flatNo {
            tables = tx_mbxrealestate_domain_model_immoobject
            source.data = GP:immo
            source.intval = 1
            conf.tx_mbxrealestate_domain_model_immoobject = TEXT
            conf.tx_mbxrealestate_domain_model_immoobject.dataWrap (
                <input type="hidden" name="search-flat[flatNo]" value="{GP:WeNr}" class="hide" />
                <input type="hidden" name="search-flat[flatStreet]" value="{field:immoaddress}" class="hide" />
            )
        }

        flatContact = RECORDS
        flatContact {
            tables = tx_mbxrealestate_domain_model_immocontact
            source.data = GP:immoContact
            source.intval = 1
            conf.tx_mbxrealestate_domain_model_immocontact = TEXT
            conf.tx_mbxrealestate_domain_model_immocontact.dataWrap = <input type="hidden" name="search-flat[flatContact]" value="{GP:immoContact}" class="hide" />
        }
    }

    preProcessors {
        1.class = PreProcessor_LoadGetPost
    }

    validators {
        1.class = Validator_Default
        1.config.fieldConf {
            desired-inception-of-treaty.errorCheck.1 = required

            number-of-rooms.errorCheck.1 = required
            number-of-rooms.errorCheck.2 = integer
            number-of-rooms.errorCheck.3 = betweenValue
            number-of-rooms.errorCheck.3 {
                minValue = 1
                maxValue = 10
            }
            flat-space_from.errorCheck.1 = required
            flat-space_from.errorCheck.2 = integer
            flat-space_from.errorCheck.3 = betweenValue
            flat-space_from.errorCheck.3 {
                minValue = 10
                maxValue = 200
            }
            flat-space_to.errorCheck.1 = required
            flat-space_to.errorCheck.2 = integer
            flat-space_to.errorCheck.3 = betweenValue
            flat-space_to.errorCheck.3 {
                minValue = 11
                maxValue = 200
            }
            maximum-rent.errorCheck.1 = required
            maximum-rent.errorCheck.2 = integer
            maximum-rent.errorCheck.3 = betweenValue
            maximum-rent.errorCheck.3 {
                minValue = 50
                maxValue = 4000
            }

            wbs.errorCheck.1 = required

            first-renter_firstname.errorCheck.1 = required
            first-renter_lastname.errorCheck.1 = required

            first-renter_telephone-private.errorCheck.1 = required
            first-renter_telephone-private.errorCheck.2 = pregMatch
            first-renter_telephone-private.errorCheck.2.value = /^([0-9\/\s\-]+)$/

            #privacy.errorCheck.1 = required
        }
    }

    if {

        1 {
            conditions.OR1.AND1 = first-renter_email=
            isTrue {
                validators.1.config.fieldConf.first-renter_address.errorCheck.1 = required
                validators.1.config.fieldConf.first-renter_housenum.errorCheck.1 = required
                validators.1.config.fieldConf.first-renter_zip-city.errorCheck.1 = required
            }
            else {
                validators.1.config.fieldConf.first-renter_email.errorCheck.1 = required
                validators.1.config.fieldConf.first-renter_email.errorCheck.2 = email
            }
        }
        2 {
            conditions.OR1.AND1 = first-renter_address=
            conditions.OR1.AND2 = first-renter_housenum=
            conditions.OR1.AND3 = first-renter_zip-city=
            isTrue < plugin.Tx_Formhandler.settings.predef.search-flat.if.1.else
            else < plugin.Tx_Formhandler.settings.predef.search-flat.if.1.isTrue
        }

        3 {
            conditions.OR1.AND1 = second-renter_email=
            conditions.OR1.AND2 = check-second-renter=1
            isTrue {
                validators.1.config.fieldConf.second-renter_address.errorCheck.1 = required
                validators.1.config.fieldConf.second-renter_housenum.errorCheck.1 = required
                validators.1.config.fieldConf.second-renter_zip-city.errorCheck.1 = required
            }
        }
        4 {
            conditions.OR1.AND1 = second-renter_address=
            conditions.OR1.AND2 = second-renter_housenum=
            conditions.OR1.AND3 = second-renter_zip-city=
            conditions.OR1.AND4 = check-second-renter=1
            isTrue {
                validators.1.config.fieldConf.second-renter_email.errorCheck.1 = required
                validators.1.config.fieldConf.second-renter_email.errorCheck.2 = email
            }
        }

        6 {
            conditions.OR1.AND1 = desired-inception-of-treaty=
            else {
                validators.1.config.fieldConf.desired-inception-of-treaty.errorCheck.2 = date
                validators.1.config.fieldConf.desired-inception-of-treaty.errorCheck.2.pattern = d.m.Y
            }
        }

        7 {
            conditions.OR1.AND1 = check-second-renter=
            conditions.OR1.AND2 = second-renter_firstname=
            conditions.OR1.AND3 = second-renter_lastname=
            conditions.OR1.AND4 = second-renter_birthday=
            conditions.OR1.AND5 = second-renter_address=
            conditions.OR1.AND6 = second-renter_housenum=
            conditions.OR1.AND7 = second-renter_zip-city=
            conditions.OR1.AND8 = second-renter_telephone-private=
            conditions.OR1.AND9 = second-renter_telephone-business=
            conditions.OR1.AND10 = second-renter_email=
            else {
                validators.1.config.fieldConf.second-renter_firstname.errorCheck.1 = required
                validators.1.config.fieldConf.second-renter_lastname.errorCheck.1 = required
                validators.1.config.fieldConf.second-renter_telephone-private.errorCheck.1 = required
                validators.1.config.fieldConf.second-renter_telephone-private.errorCheck.2 = pregMatch
                validators.1.config.fieldConf.second-renter_telephone-private.errorCheck.2.value = /^([0-9\/\s\-]+)$/
            }
        }

        8 {
            conditions.OR1.AND1 = first-renter_birthday=
            else {
                validators.1.config.fieldConf.first-renter_birthday.errorCheck.2 = date
                validators.1.config.fieldConf.first-renter_birthday.errorCheck.2.pattern = d.m.Y
            }
        }

        9 {
            conditions.OR1.AND1 = second-renter_birthday=
            else {
                validators.1.config.fieldConf.second-renter_birthday.errorCheck.2 = date
                validators.1.config.fieldConf.second-renter_birthday.errorCheck.2.pattern = d.m.Y
            }
        }

        11 {
            conditions.OR1.AND1 = wbs = Ja
            isTrue {
                validators.1.config.fieldConf.wbs-rooms.errorCheck.1 = required
                validators.1.config.fieldConf.wbs-rooms.errorCheck.2 = integer
                validators.1.config.fieldConf.wbs-rooms.errorCheck.3 = betweenValue
                validators.1.config.fieldConf.wbs-rooms.errorCheck.3 {
                    minValue = 1
                    maxValue = 10
                }
                validators.1.config.fieldConf.wbs-nr.errorCheck.1 = required
                validators.1.config.fieldConf.wbs-date.errorCheck.1 = required
                validators.1.config.fieldConf.wbs-date.errorCheck.2 = date
                validators.1.config.fieldConf.wbs-date.errorCheck.2.pattern = d.m.Y
            }
        }

        12 {
            conditions.OR1.AND1 = wbs-rooms=
            conditions.OR1.AND2 = wbs-nr=
            conditions.OR1.AND3 = wbs-date=
            else < plugin.Tx_Formhandler.settings.predef.search-flat.if.11.isTrue
        }

        13 {
            conditions.OR1.AND1 = wbs-date=
            else {
                validators.1.config.fieldConf.wbs-date.errorCheck.2 = date
                validators.1.config.fieldConf.wbs-date.errorCheck.2.pattern = d.m.Y
            }
        }

        14 {
            conditions.OR1.AND1 = first-renter_telephone-business=
            else {
                validators.1.config.fieldConf.first-renter_telephone-business.errorCheck.2 = pregMatch
                validators.1.config.fieldConf.first-renter_telephone-business.errorCheck.2.value = /^([0-9\/\s\-]+)$/
            }
        }
        15 {
            conditions.OR1.AND1 = second-renter_telephone-business=
            else {
                validators.1.config.fieldConf.second-renter_telephone-business.errorCheck.2 = pregMatch
                validators.1.config.fieldConf.second-renter_telephone-business.errorCheck.2.value = /^([0-9\/\s\-]+)$/
            }
        }
        16 {
            conditions.OR1.AND1 = neighborhood_1=
            conditions.OR1.AND2 = neighborhood_2=
            conditions.OR1.AND3 = neighborhood_3=
            conditions.OR1.AND4 = neighborhood_4=
            conditions.OR1.AND5 = neighborhood_5=
            conditions.OR1.AND6 = neighborhood_6=
            conditions.OR1.AND7 = neighborhood_7=
            conditions.OR1.AND8 = neighborhood_8=
            conditions.OR1.AND9 = neighborhood_9=
            conditions.OR1.AND9 = neighborhood_10=
            conditions.OR1.AND9 = neighborhood_all=
            isTrue {
                validators.1.config.fieldConf.neighborhood_1.errorCheck.1 = required
                validators.1.config.fieldConf.neighborhood_2.errorCheck.1 = required
                validators.1.config.fieldConf.neighborhood_3.errorCheck.1 = required
                validators.1.config.fieldConf.neighborhood_4.errorCheck.1 = required
                validators.1.config.fieldConf.neighborhood_5.errorCheck.1 = required
                validators.1.config.fieldConf.neighborhood_6.errorCheck.1 = required
                validators.1.config.fieldConf.neighborhood_7.errorCheck.1 = required
                validators.1.config.fieldConf.neighborhood_8.errorCheck.1 = required
                validators.1.config.fieldConf.neighborhood_9.errorCheck.1 = required
                validators.1.config.fieldConf.neighborhood_10.errorCheck.1 = required
                validators.1.config.fieldConf.neighborhood_all.errorCheck.1 = required
            }

        }

        # Nutzer kommt von Wohnungsseite, daher geht die E-Mail an den Vermieter
        99 {
            conditions {
                OR1 {
                    AND1 = flatNo
                    AND2 = flatStreet
                    AND3 = flatContact
                }
            }

            isTrue {
                markers.flatNo.conf.tx_mbxrealestate_domain_model_immoobject.postCObject = RECORDS
                markers.flatNo.conf.tx_mbxrealestate_domain_model_immoobject.postCObject {
                    tables = tx_mbxrealestate_domain_model_immocontact
                    source.data = GP:search-flat|flatContact
                    source.intval = 1
                    conf.tx_mbxrealestate_domain_model_immocontact = TEXT
                    conf.tx_mbxrealestate_domain_model_immocontact.dataWrap = {field:contact_name}, {field:contact_mail}
                }

            }
        }
    }

    finishers {

        3.class = Finisher_Mail
        3.config {
            mailer.class = Mailer_TYPO3Mailer
            user {
                subject = TEXT
                subject.value = Interessentenbogen wurde ausgefüllt
                templateFile = TEXT
                templateFile.value = {$filepaths.extTemplates}formhandler/searchFlat/email-user.html
                sender_email = {$emails.no-reply}
                to_email = first-renter_email
                html.arrayValueSeparator = ,&nbsp;
            }
            admin.disable = 1
        }

        10.class = Finisher_SubmittedOK
        10.config.returns = 1
    }
}

[IP = 217.18.180.50]
    plugin.Tx_Formhandler.settings.predef.search-flat.debug = 0
[global]
