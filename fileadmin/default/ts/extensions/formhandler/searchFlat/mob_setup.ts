<INCLUDE_TYPOSCRIPT: source="FILE:fileadmin/default/ts/extensions/formhandler/searchFlat/setup.ts">

plugin.Tx_Formhandler.settings.predef.search-flat-mob < plugin.Tx_Formhandler.settings.predef.search-flat
plugin.Tx_Formhandler.settings.predef.search-flat-mob {
    name = Formular | Interessentenbogen (mobile)

    templateFile = TEXT
    templateFile.value = {$filepaths.extTemplates}formhandler/searchFlat/tpl_form_mob.html

    markers {
        flatNo = RECORDS
        flatNo {
            tables = tx_mbxrealestate_domain_model_immoobject
            source.data = GP:immo
            source.intval = 1
            conf.tx_mbxrealestate_domain_model_immoobject = TEXT
            conf.tx_mbxrealestate_domain_model_immoobject.dataWrap (
                <input type="hidden" name="search-flat[flatNo]" value="{GP:WeNr}" class="hide" />
                <input type="hidden" name="search-flat[flatStreet]" value="{field:immoaddress}" class="hide" />
            )
        }

        flatContact = RECORDS
        flatContact {
            tables = tx_mbxrealestate_domain_model_immocontact
            source.data = GP:immoContact
            source.intval = 1
            conf.tx_mbxrealestate_domain_model_immocontact = TEXT
            conf.tx_mbxrealestate_domain_model_immocontact.dataWrap = <input type="hidden" name="search-flat[flatContact]" value="{GP:immoContact}" class="hide" />
        }
    }


}

[IP = 217.18.180.50]
    plugin.Tx_Formhandler.settings.predef.search-flat-mob.debug = 0
[global]
