# Configuration of registration for press mailing
plugin.Tx_Formhandler.settings.predef.ukmailing {
	name = Formular | UK-Mailing
	storagePid = 1070
	debug = 0
	formValuesPrefix = ukmailing

	# Add additional language file
	langFile.2 = {$filepaths.languageFiles}formhandler/ukmailing.xml

	templateFile = TEXT
	templateFile.value = {$filepaths.extTemplates}formhandler/ukmailing/tpl_form.html


	preProcessors {
		1. class = PreProcessor_LoadGetPost

		2.class = PreProcessor_LoadDB
		2.config {

			#Fields shown in the first step of the form
			1 {
				salutation.mapping = anrede
				title.mapping = titel
				firstname.mapping = vorname
				lastname.mapping = nachname
				firm.mapping = firma
				function.mapping = funktion
				street.mapping = strasse
				zip.mapping = plz
				city.mapping = ort
				phone.mapping = telefon
				email.mapping = mail
			}

			select {
				table = tx_mbxukmailing_domain_model_contact
				where {
					data = GP:hash
					wrap = hash LIKE ' | '
				}
				limit = 1
			}
		}
		3.class = PreProcessor_LoadDefaultValues
		3.config {
				hash.defaultValue = TEXT
				hash.defaultValue.data = GP:hash
		}
	}

	markers {
		hashMarker = TEXT
		hashMarker.data = GP:hash
	}

	validators {
		1.class = Validator_Default
		1.config.fieldConf {
			firstname.errorCheck.1  = required
			lastname.errorCheck.1  = required
			firm.errorCheck.1  = required
			email.errorCheck.1  = required
			contact.errorCheck.1  = required
		}

	}

	loggers {
		1.class = Logger_DB
		1.config {
			pid = 1070
		}
	}

	finishers {
		20.class = Finisher_SubmittedOK
		20.config.returns = 1
	}
}

