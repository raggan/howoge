# Configuration of registration for press mailing
plugin.Tx_Formhandler.settings.predef.constructionSiteInterest {
	name = Formular | Zukünftiges Interesse an Wohnung (für Bauprojekte)
	storagePid = 492
	debug = 0
	formValuesPrefix = constructionSiteInterest

	# Add additional language file
	langFile.2 = {$filepaths.languageFiles}formhandler/constructionSiteInterest.xml

	templateFile = TEXT
	templateFile.value = {$filepaths.extTemplates}formhandler/constructionSiteInterest/tpl_form.html

	#    preProcessors {
	#        1.class = Tx_Formhandler_PreProcessor_LoadDefaultValues
	#        1.config {
	#            1 {
	#                constructionsite.defaultValue = TEXT
	#                constructionsite.defaultValue.data = page:title
	#            }
	#        }
	#        2.class = PreProcessor_LoadGetPost
	#    }

	markers {
		#constructionSite = TEXT
		#constructionSite.data = TSFE:page|uid
		#constructionSite.wrap = {DB:pages:|:title}
		#constructionSite.insertData = 1

		options_constructionsite = CONTENT
		options_constructionsite {
			table = tt_content
			select {
				leftjoin = pages on (pages.uid = tt_content.pid) JOIN tx_mbxbuildingprojects_domain_model_project projects on (projects.link = pages.uid)
				pidInList.cObject = USER
				pidInList.cObject {
					userFunc = user_pages->getList
					includeLibs = fileadmin/default/userfunc/user_pages.php
				}
				orderBy = pages.sorting ASC
				groupBy = tt_content.pid
				selectFields = tt_content.uid, tt_content.pid, pages.title, pages.nav_title, projects.uid as projectUid
				# possible conditions
				where = ( tt_content.CType='formhandler_pi1' AND (tt_content.pi_flexform LIKE '%constructionSiteInterest.%' OR tt_content.pi_flexform LIKE '%constructionSiteInterestNoText.%') AND tt_content.hidden = 0 AND tt_content.deleted = 0 )
			}
			renderObj = COA
			renderObj {

				9.noTrimWrap =|<li><input id="check-|" |
				9 = TEXT
				9.field = projectUid

				#value
				10.wrap =  type="checkbox" name="###formValuesPrefix###[constructionsite][]" value="|"
				10 = TEXT
				10.field = projectUid // title

				#selected
				12.noTrimWrap = | ###checked_constructionsite_|###|
				12 = TEXT
				12.field = projectUid // title

				#label
				13 = TEXT
				13.wrap = /><label for="check-|">
				13.field = projectUid

				14 = TEXT
				14.wrap = |</label></li>
				14.field = nav_title // title

			}
		}
        wbsLink = TEXT
        wbsLink {
            value = wbs
            lang.de = WBS
            typolink.parameter = {$contentpage.wbsUid}
            typolink.additionalParams = &type=100
            typolink.ATagParams = class="cboxElementIframe iframeFlexHeight"
        }
	}

	initInterceptors {
		1 {
			class = \MBX\MbxBuildingprojects\Utility\Form\ProjectNamesInterceptor
			config {
				gpName = constructionSiteInterest|constructionsite
			}
		}
	}

	validators {
		1.class = Validator_Default
		1.config.fieldConf {
			constructionsite.errorCheck.1 = required
			salutation.errorCheck.1 = required
			firstname.errorCheck.1 = required
			lastname.errorCheck.1 = required
			#privacy.errorCheck.1 = required
            #privacy-contact.errorCheck.1 = required
			rooms.errorCheck.1 = required
			dsgvo-confirmed.errorCheck.1 = required
		}
	}

	if.1 {
		conditions.OR1.AND1 = email=
		isTrue.validators.1.config.fieldConf {
			street.errorCheck.1 = required
			streetno.errorCheck.1 = required
			zip.errorCheck.1 = required
			city.errorCheck.1 = required
		}
		else {
			email.errorCheck.1 = required
			email.errorCheck.2 = email
		}
	}

	loggers {
		1.class = Logger_DB
		1.config {
			pid < plugin.Tx_Formhandler.settings.predef.constructionSiteInterest.storagePid
		}
	}

	finishers {
		10.class = Finisher_Mail
		10.config {
			limitMailsToUser = 5
			mailer.class = Mailer_TYPO3Mailer
			user {
				subject = TEXT
				subject.value = Sie sind für Ihr gewünschtes Bauprojekt eingetragen
				templateFile = TEXT
				templateFile.value = {$filepaths.extTemplates}formhandler/constructionSiteInterest/email-user.html
				sender_email = {$emails.no-reply}
				to_email = email
				html.arrayValueSeparator = ,&nbsp;
			}
			admin.disable = 1
		}

		20.class = Finisher_SubmittedOK
		20.config.returns = 1
	}
}

