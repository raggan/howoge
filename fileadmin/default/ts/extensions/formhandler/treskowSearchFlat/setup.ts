# Configuration of search flat form

plugin.Tx_Formhandler.settings.predef.treskow-search-flat {
    #debug = 1
    name = Formular | Interessentenbogen Treskow-HÃ¶fe

    # The formID setting is mandatory in combination with "enableFileRemoval=1".
    # The formID is used to submit the right form on the page when clicking on the remove link.
    formID = search-flat-form
    formValuesPrefix = search-flat

    # Add additional language file
    langFile.2 = {$filepaths.languageFiles}formhandler/treskowSearchFlat.xml

    templateFile = TEXT
    templateFile.value = {$filepaths.extTemplates}formhandler/treskowSearchFlat/tpl_form.html

    markers {
        flatNo = RECORDS
        flatNo {
            tables = tx_mbxrealestate_domain_model_immoobject
            source.data = GP:immo
            source.intval = 1
            conf.tx_mbxrealestate_domain_model_immoobject = TEXT
            conf.tx_mbxrealestate_domain_model_immoobject.dataWrap (
                <input type="hidden" name="search-flat[flatNo]" value="{GP:WeNr}" class="hide" />
                <input type="hidden" name="search-flat[flatStreet]" value="{field:immoaddress}" class="hide" />
            )
        }
        flatContact = RECORDS
        flatContact {
            tables = tx_mbxrealestate_domain_model_immocontact
            source.data = GP:immoContact
            source.intval = 1
            conf.tx_mbxrealestate_domain_model_immocontact = TEXT
            conf.tx_mbxrealestate_domain_model_immocontact.dataWrap = <input type="hidden" name="search-flat[flatContact]" value="{GP:immoContact}" class="hide" />
        }
    }

    preProcessors {
        1.class = PreProcessor_LoadGetPost
    }

    validators {
        1.class = Validator_Default
        1.config.fieldConf {

            first-renter_firstname.errorCheck.1 = required
            first-renter_lastname.errorCheck.1 = required

            email.errorCheck {
                1 = required
                2 = email
            }
            telephone-private.errorCheck.1 = required
            #address.errorCheck.1 = required
            #zip-city.errorCheck.1 = required
            #household-income.errorCheck.1 = required
            #additional-persons.errorCheck.1 = required

            #privacy.errorCheck.1 = required
            privacy-contact.errorCheck.1 = required
        }
    }

    finishers {
        1.class = Finisher_Mail
        1.config {
            limitMailsToUser = 10
            mailer.class = Mailer_TYPO3Mailer
            checkBinaryCrLf = remarks
            admin {
                to_email >
                to_email = TEXT
                to_email {
                    data = DB:tx_mbxrealestate_domain_model_immocontact:{GP:search-flat|flatContact}:contact_mail
                    data.insertData = 1
                    sanitize = 1
                    stdWrap.ifEmpty = {$emails.searchFlatDefault}
                }
                subject = TEXT
                subject {
                    value = Ein Interessentenbogen wurde ausgefÃ¼llt
                    override {
                        cObject = RECORDS
                        cObject {
                            tables = tx_mbxrealestate_domain_model_immoaddress
                            source.data = GP:search-flat|flatStreet
                            conf.tx_mbxrealestate_domain_model_immoaddress = TEXT
                            conf.tx_mbxrealestate_domain_model_immoaddress.dataWrap = Ein Interessentenbogen wurde ausgefÃ¼llt // {field:street}, WI-Nr.: {GP:search-flat|flatNo}
                        }
                        if {
                            isTrue.data = GP:search-flat|flatNo
                            isFalse = 1
                            isFalse.if {
                                value = 0
                                equals.numRows {
                                    table = tx_mbxrealestate_domain_model_immoaddress
                                    select {
                                        pidInList = 120
                                        recursive = 1
                                        andWhere {
                                            data = GP:search-flat|flatStreet
                                            intval = 1
                                            wrap = uid=|
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                templateFile = TEXT
                templateFile.value = {$filepaths.extTemplates}formhandler/treskowSearchFlat/email-admin.html
                sender_email = {$emails.no-reply}
            }
            # Don't send e-mail to user
            user.disable = 1
        }

        9.class = TYPO3\MbxRealestateHowoge\Finisher\TreskowFlatHide
        9.config {
            limitMailsToUser = 10
            mailer.class = Mailer_TYPO3Mailer
            checkBinaryCrLf = remarks
            admin {
                to_email >
                to_email = TEXT
                to_email = {$emails.treskowAlarmMailContact}
                #                to_email {
                #                    data = DB:tx_mbxrealestate_domain_model_immocontact:{GP:search-flat|flatContact}:contact_mail
                #                    data.insertData = 1
                #                    sanitize = 1
                #                    stdWrap.ifEmpty = {$emails.searchFlatDefault}
                #                }
                subject = TEXT
                subject {
                    value = Alarmmail fÃ¼r
                    override {
                        cObject = RECORDS
                        cObject {
                            tables = tx_mbxrealestate_domain_model_immoaddress
                            source.data = GP:search-flat|flatStreet
                            conf.tx_mbxrealestate_domain_model_immoaddress = TEXT
                            conf.tx_mbxrealestate_domain_model_immoaddress.dataWrap = Alarmmail fÃ¼r // {field:street}, WE-Nr.: {GP:search-flat|flatNo}
                        }
                        if {
                            isTrue.data = GP:search-flat|flatNo
                            isFalse = 1
                            isFalse.if {
                                value = 0
                                equals.numRows {
                                    table = tx_mbxrealestate_domain_model_immoaddress
                                    select {
                                        pidInList = 120
                                        recursive = 1
                                        andWhere {
                                            data = GP:search-flat|flatStreet
                                            intval = 1
                                            wrap = uid=|
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                templateFile = TEXT
                templateFile.value = {$filepaths.extTemplates}formhandler/treskowSearchFlat/alarm-email-admin.html
                sender_email = {$emails.no-reply}
            }
            # Don't send e-mail to user
            user.disable = 1
        }
        10.class = Finisher_SubmittedOK
        10.config.returns = 1
    }
}

[page|uid = 334] && [globalString = IENV:HTTP_HOST=*howoge-apps.de]
    plugin.Tx_Formhandler.settings.predef.treskow-search-flat.finishers.1.config.admin.to_email >
    plugin.Tx_Formhandler.settings.predef.treskow-search-flat.finishers.1.config.admin.to_email =
    plugin.Tx_Formhandler.settings.predef.treskow-search-flat.finishers.9.config.admin.to_email >
    plugin.Tx_Formhandler.settings.predef.treskow-search-flat.finishers.9.config.admin.to_email =
[global]