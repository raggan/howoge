plugin.Tx_Formhandler.settings.predef.visit-project {
    name = Formular | Besichtigungstermin für Neubauprojekt
    debug = 0

    formID = search-flat-form
    formValuesPrefix = visit-project

    # Add additional language file
    langFile.2 = {$filepaths.languageFiles}formhandler/visitFlat.xml

    templateFile = TEXT
    templateFile.value = {$filepaths.extTemplates}formhandler/visitProject/tpl_form.html

    markers {
        subline = TEXT
        subline.value =

        info = TEXT
        info.value = Nach Versand dieses Formulars erhalten Sie per E-Mail Terminvorschläge, solange ausreichend vorhandene Plätze vorhanden sind.
    }

    validators {
        1.class = Validator_Default
        1.config.fieldConf {
            salutation.errorCheck.1  = required
            first-renter_firstname.errorCheck.1  = required
            first-renter_lastname.errorCheck.1  = required
            first-renter_email.errorCheck.1  = required
            first-renter_telephone.errorCheck.1  = required
            privacy-contact.errorCheck.1 = required
        }
    }

    initInterceptors.10 {
        class = \MBX\MbxBuildingprojects\Utility\Form\ExtendEmailInformationInterceptor
        config {
            domain = {$config.domain}
            fields  {
                exposeURL =
                objektID =
                titel =
                groesse =
                preis =
                strasse =
                hausnummer =
                PLZ =
                stadt =
                bild =
                ansprechpartner =
            }
        }
    }

    finishers {
        1.class = Finisher_Mail
        1.config {
            mailer.class = Mailer_TYPO3Mailer
            user.disable = 1

            admin {
                subject = TEXT
                subject.value = Formular | Besichtigungstermin für Neubauprojekt
                templateFile = TEXT
                templateFile.value = {$filepaths.extTemplates}formhandler/visitProject/email-admin.html
                sender_email = {$emails.no-reply}
                to_email = anfrage@howoge.de
            }
        }

        2.class = Finisher_SubmittedOK
        2.config.returns = 1
    }
}

[IP = 217.18.180.50]
    #plugin.Tx_Formhandler.settings.predef.visit-project.finishers.1.config.admin.to_email = martin@wohnungshelden.de
    plugin.Tx_Formhandler.settings.predef.visit-project.finishers.1.config.admin.to_email = denis.krueger@mindbox.de
[global]

# Lindenhof
[globalVar = TSFE:id = 54] || [globalVar = TSFE:id = 313]
    plugin.Tx_Formhandler.settings.predef.visit-project.markers.subline = Besichtigungstermin im Lindenhof vereinbaren
    plugin.Tx_Formhandler.settings.predef.visit-project.finishers.1.config.admin.subject.value = Besichtigungstermin für Neubauprojekt "Lindenhof"

    plugin.Tx_Formhandler.settings.predef.visit-project.initInterceptors.10.config.fields  {
        objektID = LH-10
        exposeURL = https://www.howoge.de/neubau/neubauprojekte/lindenhof.html
        titel = Neubauprojekt Lindenhof - 10
        hausnummer = 2-20
        ansprechpartner = Nino Barth
        #bild = https://www.howoge.de/fileadmin/user_upload/user_upload/Lindenhof_Copyright_Max_Dudler_Architekten_AG.jpg
    }
[global]

