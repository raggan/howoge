# include basic extension configuration
<INCLUDE_TYPOSCRIPT: source="FILE:EXT:t3colorbox/Configuration/TypoScript/setup.txt">

page.jsFooterInline {
    26.value = maxHeight:"{$plugin.t3colorbox.maxHeight}"
    27 >
    28 >
    97 >
    99.value = if(window.addEventListener){window.addEventListener('orientationchange',function(){if($('#cboxOverlay').is(':visible')){$.colorbox.load(true);}},false);}
}