<INCLUDE_TYPOSCRIPT: source="FILE:EXT:rzautocomplete/pi1/static/setup.txt">

plugin.tx_rzautocomplete_pi1 {
    searchPage = 9
    minChars = 3

    # cat=rzautocomplete/option/030; type=int; label=Maximum number of results;
    maxResults = 5
    
    template = {$filepaths.extTemplates}rzautocomplete/template.html
}