<INCLUDE_TYPOSCRIPT: source="FILE:EXT:mbx_faq/Configuration/TypoScript/setup.txt">

plugin.tx_mbxfaq {
    view {
        templateRootPath = {$filepaths.extTemplates}mbx_faq/Templates/
        partialRootPath = {$filepaths.extTemplates}mbx_faq/Partials/
        layoutRootPath = {$filepaths.extTemplates}mbx_faq/Layouts/
    }
}

lib.faqAddressSearch = USER
lib.faqAddressSearch {
    userFunc = TYPO3\CMS\Extbase\Core\Bootstrap->run
    pluginName = Pi1
    extensionName = MbxRealestate
    vendorName = TYPO3
    switchableControllerActions.Immoaddress.1 = searchForm

    mvc =< plugin.tx_mbxrealestate.mvc
    settings < plugin.tx_mbxrealestate.settings
    settings {
        templateLayout = inputField
        hideWrap = 1

        customAddressSearch = allContacts
    }
    persistence =< plugin.tx_mbxrealestate.persistence
    view =< plugin.tx_mbxrealestate.view
}

lib.faqAddress = TEXT
lib.faqAddress.data = GP:tx_mbxrealestate_pi1|immoaddress|street

faqAjaxCall = PAGE
faqAjaxCall {
    config{
        disableAllHeaderCode = 1
        xhtml_cleaning = 1
        admPanel = 0
        debug = 0
        no_cache = 1
        tx_realurl_enable = 0
    }

    typeNum = 2000

    10 = USER_INT
    10 {
        userFunc = TYPO3\CMS\Extbase\Core\Bootstrap->run
        pluginName = Pi1
        extensionName = MbxFaq
        vendorName = TYPO3
        switchableControllerActions.Question.1 = list

        mvc =< plugin.tx_mbxfaq.mvc
        settings =< plugin.tx_mbxfaq.settings
        persistence =< plugin.tx_mbxfaq.persistence
        view =< plugin.tx_mbxfaq.view
    }
}