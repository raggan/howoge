## root setup
tt_content.gridelements_pi1.20.10.setup {
    
    <INCLUDE_TYPOSCRIPT: source="FILE:fileadmin/default/ts/extensions/gridelements/contentRow/contentRow.ts">
    # masonry
    <INCLUDE_TYPOSCRIPT: source="FILE:fileadmin/default/ts/extensions/gridelements/4-4-1Block/4-4-1Block.ts">
    <INCLUDE_TYPOSCRIPT: source="FILE:fileadmin/default/ts/extensions/gridelements/1-2-1Block/1-2-1Block.ts">
    <INCLUDE_TYPOSCRIPT: source="FILE:fileadmin/default/ts/extensions/gridelements/1-2-2Block/1-2-2Block.ts">
    <INCLUDE_TYPOSCRIPT: source="FILE:fileadmin/default/ts/extensions/gridelements/1-1-1Block/1-1-1Block.ts">
    <INCLUDE_TYPOSCRIPT: source="FILE:fileadmin/default/ts/extensions/gridelements/2-1-1Block/2-1-1Block.ts">
}