[PIDinRootline = 3]
plugin.tx_news.settings.stickyNavigation = 1
[global]

# DEV
plugin.tx_news.settings.event.howogeEventCategoryId = 30

# STAGING
[globalString = IENV:HTTP_HOST=*howoge-apps.de]
	plugin.tx_news.settings.event.howogeEventCategoryId = 45
[global]

# LIVE
[globalString = IENV:HTTP_HOST=*howoge.de]
	plugin.tx_news.settings.event.howogeEventCategoryId = 47
[global]
