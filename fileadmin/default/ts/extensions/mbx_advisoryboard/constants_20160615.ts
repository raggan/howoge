# DEV
plugin.tx_mbxadvisoryboard {
	persistence {
		# cat=plugin.tx_mbxadvisoryboard//a; type=string; label=Default storage PID
		storagePid = 359
	}
	settings {
		votingPid = 360
	}
}

# STAGING
[globalString = IENV:HTTP_HOST=*howoge-apps.de]
	plugin.tx_mbxadvisoryboard.persistence.storagePid = 615
	plugin.tx_mbxadvisoryboard.settings.votingPid = 614
[global]

# LIVE
[globalString = IENV:HTTP_HOST=*howoge.de]
	plugin.tx_mbxadvisoryboard.persistence.storagePid = 641
	plugin.tx_mbxadvisoryboard.settings.votingPid = 636
[global]
