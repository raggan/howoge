tt_content.tx_contentdesigner_video < plugin.tx_contentdesigner

tt_content.tx_contentdesigner_video.settings{
    title = Video
    description = Create Video Element
    
    cObjectFlexFile = fileadmin/default/flexforms/contentdesignerelements/video/video.xml
    renderObj = FLUIDTEMPLATE
    renderObj {
        file = fileadmin/default/templates/extensions/contentdesignerelements/video/video.html
        
        variables{
            videoMP = COA
            videoMP {
                10 = TEXT
                10.data = field:videoMP
                10.value = |
            }

            videoWebm = COA
            videoWebm {
                10 = TEXT
                10.data = field:videoWebm
                10.value = |
            }

            videoOGV = COA
            videoOGV {
                10 = TEXT
                10.data = field:videoOGV
                10.value = |
            }
            
            youtubeCode = COA
            youtubeCode {
                10 = TEXT
                10.data = field:youtubeCode
                10.value = |
            }

            previewImage = COA
            previewImage {
                10 = TEXT
                10.data = field:previewImage
                10.value = |
            }
            
            videoHeader = COA
            videoHeader {
                10 = TEXT
                10.data = field:videoHeader
                10.value = |
            }
           
            videoSubheader = COA
            videoSubheader {
                10 = TEXT
                10.data = field:videoSubheader
                10.value = |
            }
            
            elementFormat = COA
            elementFormat {
                10 = TEXT
                10.data = field:elementFormat
                10.value = |
            }
            
            videoWidth = COA
            videoWidth {
                10 = TEXT
                10.data = field:videoWidth
                10.value = |
            }
            
            videoHeight = COA
            videoHeight {
                10 = TEXT
                10.data = field:videoHeight
                10.value = |
            }

            elementUid = TEXT
            elementUid.data = field:uid

            
        }
    }

}