tt_content.tx_contentdesigner_tile < plugin.tx_contentdesigner

tt_content.tx_contentdesigner_tile.settings{
    title = Tile
    description = Create Tile of an website
    
    cObjectFlexFile = fileadmin/default/flexforms/contentdesignerelements/tile/tile.xml
    renderObj = FLUIDTEMPLATE
    renderObj {
        file = fileadmin/default/templates/extensions/contentdesignerelements/tile/tile.html
        
        variables{

            previewImage = COA
            previewImage {
                10 = TEXT
                10.data = field:previewImage
                10.value = |
            }
            
            title = COA
            title {
                10 = TEXT
                10.data = field:title
                10.value = |
            }
            
            link = COA
            link {
                10 = TEXT
                10.data = field:link
                10.value = |
            }
            
            category = COA
            category {
                10 = TEXT
                10.data = field:category
                10.value = |
            }
            
        }
    }

}