tt_content.tx_contentdesigner_verticalTile < plugin.tx_contentdesigner

tt_content.tx_contentdesigner_verticalTile.settings{
    title = Vertical Image Tile
    description = Create Tile of an website with a vertical image
    
    cObjectFlexFile = fileadmin/default/flexforms/contentdesignerelements/verticalTile/verticalTile.xml
    renderObj = FLUIDTEMPLATE
    renderObj {
        file = fileadmin/default/templates/extensions/contentdesignerelements/verticalTile/verticalTile.html
        
        variables{

            previewImage = COA
            previewImage {
                10 = TEXT
                10.data = field:previewImage
                10.value = |
            }
            
            title = COA
            title {
                10 = TEXT
                10.data = field:title
                10.value = |
            }
            
            link = COA
            link {
                10 = TEXT
                10.data = field:link
                10.value = |
            }
            
            category = COA
            category {
                10 = TEXT
                10.data = field:category
                10.value = |
            }
            
        }
    }

}