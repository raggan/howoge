<INCLUDE_TYPOSCRIPT: source="FILE:EXT:mbx_comments/Configuration/TypoScript/setup.txt">

plugin.tx_mbxcomments {
    view {
        templateRootPath = {$filepaths.extTemplates}mbx_comments/Templates/
        partialRootPath = {$filepaths.extTemplates}mbx_comments/Partials/
        layoutRootPath = {$filepaths.extTemplates}mbx_comments/Layouts/
    }
}

# Include comment Modul
lib.comments = USER_INT
lib.comments {
    userFunc = TYPO3\CMS\Extbase\Core\Bootstrap->run
    pluginName = Pi1
    extensionName = MbxComments
    vendorName = TYPO3
    switchableControllerActions.Comment.1 = list

    mvc =< plugin.tx_mbxcomments.mvc
    settings =< plugin.tx_mbxcomments.settings
    persistence =< plugin.tx_mbxrealestate.persistence
    view =< plugin.tx_mbxrealestate.view
}

lib.comments >

lib.mbxCommentsForm.settings {
    templateFile.value = {$filepaths.extTemplates}mbx_comments/Templates/Formhandler/commentform.html
}
