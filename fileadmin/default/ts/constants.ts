## root constants

## Read in the templates for the extensions.
<INCLUDE_TYPOSCRIPT: source="FILE:fileadmin/default/ts/extensions/t3colorbox/constants.ts">
<INCLUDE_TYPOSCRIPT: source="FILE:fileadmin/global/ts/extensions/news/constants.ts">
<INCLUDE_TYPOSCRIPT: source="FILE:fileadmin/default/ts/extensions/mbx_comments/constants.ts">
<INCLUDE_TYPOSCRIPT: source="FILE:fileadmin/global/ts/extensions/mbx_realestate/constants.ts">
<INCLUDE_TYPOSCRIPT: source="FILE:fileadmin/global/ts/extensions/mbx_renteraccount_howoge/constants.ts">
<INCLUDE_TYPOSCRIPT: source="FILE:fileadmin/default/ts/extensions/mbx_faq/constants.ts">
<INCLUDE_TYPOSCRIPT: source="FILE:fileadmin/default/ts/extensions/rzautocomplete/constants.ts">
<INCLUDE_TYPOSCRIPT: source="FILE:fileadmin/global/ts/extensions/tq_seo/constants.ts">
<INCLUDE_TYPOSCRIPT: source="FILE:fileadmin/global/ts/extensions/kiezredirect/constants.ts">
<INCLUDE_TYPOSCRIPT: source="FILE:fileadmin/global/ts/extensions/canternate/constants.ts">
<INCLUDE_TYPOSCRIPT: source="FILE:EXT:gridelements/Configuration/TypoScript/constants.txt">
<INCLUDE_TYPOSCRIPT: source="FILE:EXT:content_designer/Configuration/TypoScript/default/constants.txt">
<INCLUDE_TYPOSCRIPT: source="FILE:EXT:mbx_buildingprojects/Configuration/TypoScript/constants.txt">
<INCLUDE_TYPOSCRIPT: source="FILE:fileadmin/global/ts/extensions/mbx_buildingprojects/constants.ts">
<INCLUDE_TYPOSCRIPT: source="FILE:fileadmin/default/ts/extensions/mbx_advisoryboard/constants.ts">
<INCLUDE_TYPOSCRIPT: source="FILE:fileadmin/global/ts/extensions/mbx_secure_download/constants.ts">

config {
    # cat=config; type=boolean; label=Admin Panel: Turn on admin panel (mainly for testing purposes only)
    adminPanel = 0

    # cat=config; type=boolean; label=Debugging: Turn on debugging (testing purposes only)
    debug = 0

    # cat=config; type=boolean; label=No caching: Turn off caching (testing purposes only)
    no_cache = 0

    # cat=config; type=string; label=Absolute URI prefix: (use "/" if running on top level; use empty value to use relative URI)
    absRefPrefix = /

    # cat=config; type:string; label= Google account for current website
    googleAnalytics = UA-5444414-6

    mobileDomain = m.howoge.de
    domain = https://www.howoge.de

    #1 -> Main Site, 326 -> Mein Howoge, 327 -> Kiezgeschichten ...
    domainSitemapStartingPoint = 1, 326, 327, 328, 372, 329, 330, 678
    mobileDomainSitemapStartingPoint = 231
    sitemap = sitemap.xml
    sitemapPress = presse-sitemap.xml
    sitemapNews = news-sitemap.xml
    sitemapEvents = events-sitemap.xml
}

contentpage {
    # cat=contentpage; type=int+; label= ID of the home page: ID of the websites home (root) page.
    homeUid = 1

    # cat=contentpage; type=int+; label= ID of the page 'Alle Kieze'.
    allKiezUid = 20

    # cat=contentpage; type=int+; label= ID of the page containing the job application form.
    jobApplicationUid = 75

    # cat=contentpage; type=int+; label= ID of the privacy page: ID of the websites privacy policy page.
    privacyUid = 14

    # cat=contentpage; type=int+; label= ID of the wbs page: ID of the websites wbs page.
    wbsUid = 835

    # cat=contentpage; type=int+; label= prospectiveCustomerUid
    prospectiveCustomerUid = 286
    
    # cat=contentpage; type=int+; label= prospectiveBusinessUid
    prospectiveBusinessUid = 969

    # cat=contentpage; type=int+; label= treskowProspectiveCustomerUid Dev: 334
    treskowProspectiveCustomerUid = 386
    devTreskowProspectiveCustomerUid = 334

    # cat=contentpage; type=boolean; label= Hide new logo and claim.
    hideNewLogo = 0

    bautUid = 38,482
    bautCatUid = 38|482
#    bautUid = 619
    
    bautRealUid = 621
}

filepaths {

	# cat=filepaths; type=string; label=CSS: Location of the Cascading Style Sheets relative to site
	css = fileadmin/default/css/

	# cat=filepaths; type=string; label=Images: Location of the images relative to site
	images = fileadmin/default/images/

	# cat=filepaths; type=string; label=Scripts: Location of the Javascript files relative to site
	js = fileadmin/default/js/

	# cat=filepaths; type=string; label=HTML Templates: Location of the (X)HTML templates relative to site
	templates = fileadmin/default/templates/

	# cat=filepaths; type=string; label=HTML Templates for extensions: Location of the (X)HTML templates for extensions
	extTemplates = fileadmin/default/templates/extensions/

	# cat=filepaths; type=string; label=Language files: Location of the language files for extensions (XML or XLF)
	languageFiles = fileadmin/global/language/
}

emails {
    # cat=emails; type=string; label= Admin email
    admin = it@mindbox.de

    # cat=emails; type=string; label= Default contact email
    contact = info@howoge.de

    # cat=emails; type=string; label= Default search flat contact email
    searchFlatDefault = ulrike.melzig@howoge.de

    # cat=emails; type=string; label= No-reply email
    no-reply = noreply@howoge.de

    # cat=emails; type=string; label= Alarm contact Mail
    treskowAlarmMailContact = charlene.sydow@howoge.de
}


menu {
    metaPid {
        # cat=navigation menus; type=int+; label= metaPid header: ID of the folder containing the pages to be included in the header meta menu.
        header = 11

        # cat=navigation menus; type=int+; label= metaPid footer: ID of the folder containing the pages to be included in the footer meta menu.
        footer = 12
    }

    # cat=navigation menus; type=int+; label= footerPid: ID of the folder containing the pages to be included in footer meta menu.
    footerPid = 104

    # cat=navigation menus; type=int+; label= excludeUidList: List of page IDs which schould not be included in menu.
    excludeUidList =

    #invert structure submenus
    invertStructureMenus = 619

    #display third submenu
    thirdSubmenuList = 38,482
}

plugin.felogin {
    # cat=plugin.felogin; type:string; label= template: Name of the HTML template used in content area (without path)
    template = tpl_felogin-content.html

    # cat=plugin.felogin; type:string; label= templateHeader: Name of the HTML template used in header area (without path)
    templateHeader = tpl_felogin-header.html

    # cat=plugin.felogin; type:int+; label= Storage Page ID: UID of the page which contains the frontend user records.
    storageID =
}

plugin.tx_indexedsearch {
    # cat=plugin.indexed_search; type=int+; label= Search Page ID: UID of the page which contains the indexed search plugin.
    searchUid = 9

    # cat=plugin.indexed_search; type=boolean; label= Show advanced: Show link to advanced search.
    showAdvanced = 0
}

plugin.meta {
    # cat=plugin.meta; type=string; label= Description: Write a short abstract for your website.
    description =

    # cat=plugin.meta; type=string; label= Keywords: Enter a comma separated list of keywords.
    keywords =

    # cat=plugin.meta; type=string; label= Robots: Use for instance these codes: Index all pages: "all".  Index no pages: "none". Only this page: "index,nofollow".  Only subpages: "noindex,follow"
    robots = index, follow

    # cat=plugin.meta; type=string; label= Copyright info: Enter copyright information, eg. "Me Myself and I, 2001. All rights reserved."
    copyright = HOWOGE Wohnungsbaugesellschaft mbH

    # cat=plugin.meta; type=string; label= Reply-to email
    email = info@howoge.de

    # cat=plugin.meta; type=string; label= Author: Enter name of author.
    author = HOWOGE Wohnungsbaugesellschaft mbH

    # cat=plugin.meta; type=options[,Arabic=ar,Chinese=zh,Danish=dk,Dutch=nl,English=en,Finnish=fi,French=fr,German=de,Greek=el,Hebrew=he,Icelandic=is,Italian=it,Japanese=ja,Norwegian=no,Polish=pl,Portuguese=pt,Russian=ru,Spanish=es,Swedish=sv,Turkish=tr,Multi language=mul]; label= Language: Select language of the content.
    language = de

    # cat=plugin.meta; type=string; label= Revisit after: Time between search engine visits.
    revisit-after = 2 days
}

# Define default header type
content.defaultHeaderType = 2

styles.content {
    # This defines the maximum width of images inserted in content records of type Images or Text-with-images.
    # There are seperate settings for images floated next to text (..InText)
    imgtext {
        colSpace = 20
        rowSpace = 20
        textMargin = 20
        maxW = 940
        maxWInText = 460
        borderThick = 0
        linkWrap.newWindow = 1
    }
    media {
        defaultVideoWidth = 940
#        defaultVideoHeight = 284
    }
    uploads {
        filesizeBytesLabels = Byte| kB| MB| GB
        jumpurl_secure = 1
        jumpurl_secure_mimeTypes = pdf=application/pdf, doc=application/msword
        jumpurl = 1
    }
}

[globalString = IENV:HTTP_HOST=*howoge-apps.de]
menu {
    #invert structure submenus
    invertStructureMenus = 586

    #display third submenu
    thirdSubmenuList = 482,38
}

contentpage {
    bautUid = 586
    bautRealUid = 587
    wbsUid = 634
}
[global]
