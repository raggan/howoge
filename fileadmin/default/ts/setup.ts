## root setup

### Redirecting - START
# handle mobile redirection if necessary
# Redirect default pages (prevent 404 error)
[device = mobi] && [globalVar = GP:no_mobile != 1]
config.additionalHeaders = Location: https://m.howoge.de

# Redirecting 'Mieten'
[device = mobi] && [globalVar = GP:no_mobile != 1] && [page|uid = 444]
config.additionalHeaders = Location: https://m.howoge.de/konzertanmeldung.html
[device = mobi] && [globalVar = GP:no_mobile != 1] && [PIDinRootline = 2]
config.additionalHeaders = Location: https://m.howoge.de/objektsuche.html
# Redirect 'Merkzettel'
[device = mobi] && [globalVar = GP:no_mobile != 1] && [page|uid = 121]
config.additionalHeaders = Location: https://m.howoge.de/nc/mein-merkzettel

# clear refresh redirect for pages 'HOWOGE baut'
[device = mobi] && [globalVar = GP:no_mobile != 1] && [PIDinRootline = 38,213]
config.additionalHeaders >
# Redirect 'HOWOGE baut'
[device = mobi] && [globalVar = GP:no_mobile != 1] && [page|uid = 38,213]
config.additionalHeaders = Location: https://m.howoge.de/das-besondere.html
# Redirect 'Treskow Höfe'
[device = mobi] && [globalVar = GP:no_mobile != 1] && [PIDinRootline = 79,83,189,215]
config.additionalHeaders = Location: https://m.howoge.de/das-besondere/treskow-hoefe.html
# Redirect 'Lindenhof'
[device = mobi] && [globalVar = GP:no_mobile != 1] && [PIDinRootline = 54,88,190,216]
config.additionalHeaders = Location: https://m.howoge.de/das-besondere/lindenhof.html
# Redirect 'Konrad-Wolf-Straße'
[device = mobi] && [globalVar = GP:no_mobile != 1] && [PIDinRootline = 55,89,191,217]
config.additionalHeaders = Location: https://m.howoge.de/das-besondere/konrad-wolf-strasse.html
# Redirect 'Wohnungen in Berlin-Buch'
[device = mobi] && [globalVar = GP:no_mobile != 1] && [PIDinRootline = 80,90,212]
config.additionalHeaders = Location: https://m.howoge.de/das-besondere/berlin-buch.html
# Redirect 'Appartements in Berlin-Buch'
[device = mobi] && [globalVar = GP:no_mobile != 1] && [PIDinRootline = 307,308]
config.additionalHeaders = Location: https://m.howoge.de/das-besondere/appartements-berlin-buch.html

# Redirect 'Treskow Höfe 2015'
[device = mobi] && [globalVar = GP:no_mobile != 1] && [PIDinRootline = 383,384,385,386]
config.additionalHeaders = Location: https://m.howoge.de/treskow-hoefe-2015.html

[device = mobi] && [globalVar = GP:no_mobile != 1] && [PIDinRootline = 6]
config.additionalHeaders = Location: https://m.howoge.de/kontakt-hilfe.html

[device = mobi] && [globalVar = GP:no_mobile != 1] && [page|uid = 474]
config.additionalHeaders = Location: https://m.howoge.de/karriere-bei-der-howoge/online-bewerbungsformular.html

[global]

# Redirect 'Kiezmelder'-entries in plugin
[device = mobi] && [globalVar = GP:no_mobile != 1] && [PIDinRootline = 325]
config.additionalHeaders >

[device = mobi] && [globalVar = GP:no_mobile != 1] && [PIDinRootline = 311] && [globalString = IENV:HTTP_HOST=*howoge-apps.de]
config.additionalHeaders >
[global]

temp.urlSplitForMetaRefresh {
    data = getIndpEnv:REQUEST_URI
    split {
        token = /
        min = 2

        # rendering of content
        cObjNum = 1 || 2 || 3
        3.current = 1
        3.wrap = /|

    }
    wrap = 0; URL=https://m.howoge.de|
}

# Redirect 'Wohnungen in Berlin' & 'Gewerbeimmobilien in Berlin'
[device = mobi] && [globalVar = GP:no_mobile != 1] && [PIDinRootline = 84,287]
config.additionalHeaders >
page.meta.refresh < temp.urlSplitForMetaRefresh
page.meta.refresh.split {
    cObjNum = 1 || 2 || 3 || 4
    3.override = objektsuche
    4.current = 1
    4.wrap = /|
}
# Redirect 'Serviceangebote für Mieter' & 'Exklusivangebote'
[device = mobi] && [globalVar = GP:no_mobile != 1] && [PIDinRootline = 23,25]
config.additionalHeaders >
page.meta.refresh < temp.urlSplitForMetaRefresh

# Redirect 'Veranstaltungen' & 'Pressemitteilungen'
[device = mobi] && [globalVar = GP:no_mobile != 1] && [PIDinRootline = 58,67]
config.additionalHeaders >
page.meta.refresh < temp.urlSplitForMetaRefresh
page.meta.refresh.split {
    min = 3
    cObjNum = 1 || 2 || 3 || 4
    3 >
    4.current = 1
    4.wrap = /|
}
[global]
### Redirecting - END


# Read in the templates for changes in tt_content, lib.stdHeader etc.
<INCLUDE_TYPOSCRIPT: source="FILE:fileadmin/global/ts/system/cssStyledContent/setup.ts">

# Read in the templates for the extensions.
<INCLUDE_TYPOSCRIPT: source="FILE:fileadmin/default/ts/extensions/csvexport/setup.ts">
<INCLUDE_TYPOSCRIPT: source="FILE:fileadmin/default/ts/extensions/t3colorbox/setup.ts">
<INCLUDE_TYPOSCRIPT: source="FILE:fileadmin/default/ts/extensions/formhandler/setup.ts">
<INCLUDE_TYPOSCRIPT: source="FILE:fileadmin/default/ts/extensions/news/setup.ts">
<INCLUDE_TYPOSCRIPT: source="FILE:fileadmin/global/ts/extensions/news/setup.ts">
<INCLUDE_TYPOSCRIPT: source="FILE:fileadmin/default/ts/extensions/mbx_comments/setup.ts">
<INCLUDE_TYPOSCRIPT: source="FILE:fileadmin/global/ts/extensions/mbx_contacts/setup.ts">
<INCLUDE_TYPOSCRIPT: source="FILE:fileadmin/global/ts/extensions/mbx_cleaner/setup.ts">
<INCLUDE_TYPOSCRIPT: source="FILE:fileadmin/global/ts/extensions/mbx_realestate/setup.ts">
<INCLUDE_TYPOSCRIPT: source="FILE:fileadmin/global/ts/extensions/mbx_renteraccount_howoge/setup.ts">
<INCLUDE_TYPOSCRIPT: source="FILE:fileadmin/default/ts/extensions/mbx_faq/setup.ts">
<INCLUDE_TYPOSCRIPT: source="FILE:fileadmin/global/ts/extensions/indexed_search/setup.ts">
<INCLUDE_TYPOSCRIPT: source="FILE:fileadmin/default/ts/extensions/rzautocomplete/setup.ts">
<INCLUDE_TYPOSCRIPT: source="FILE:fileadmin/global/ts/extensions/felogin/setup.ts">
<INCLUDE_TYPOSCRIPT: source="FILE:fileadmin/global/ts/extensions/tq_seo/setup.ts">
<INCLUDE_TYPOSCRIPT: source="FILE:fileadmin/global/ts/extensions/kiezredirect/setup.ts">
<INCLUDE_TYPOSCRIPT: source="FILE:fileadmin/global/ts/extensions/canternate/setup.ts">
<INCLUDE_TYPOSCRIPT: source="FILE:fileadmin/default/ts/extensions/mbx_advisoryboard/setup.ts">
<INCLUDE_TYPOSCRIPT: source="FILE:fileadmin/global/ts/extensions/mbx_secure_download/setup.ts">
<INCLUDE_TYPOSCRIPT: source="FILE:fileadmin/global/ts/extensions/imageopt/setup.ts">

# Read setup of special extension to generate custom content elements
<INCLUDE_TYPOSCRIPT: source="FILE:EXT:fluidcontent/Configuration/TypoScript/setup.txt">
<INCLUDE_TYPOSCRIPT: source="FILE:EXT:mbx_content_howoge/Configuration/TypoScript/setup.txt">

# read setup of content designer elements and gridelements
<INCLUDE_TYPOSCRIPT: source="FILE:EXT:gridelements/Configuration/TypoScript/setup.ts">
<INCLUDE_TYPOSCRIPT: source="FILE:EXT:content_designer/Configuration/TypoScript/default/setup.ts">
<INCLUDE_TYPOSCRIPT: source="FILE:fileadmin/default/ts/extensions/gridelements/setup.ts">
<INCLUDE_TYPOSCRIPT: source="FILE:fileadmin/default/ts/extensions/contentdesigner/setup.ts">

#external extensions
<INCLUDE_TYPOSCRIPT: source="FILE:EXT:mbx_buildingprojects/Configuration/TypoScript/setup.txt">

# Read in the Lib (Blocks) templates containing TypoScript to generate the building blocks for your website, except menu's.
<INCLUDE_TYPOSCRIPT: source="FILE:fileadmin/global/ts/block/setup.ts">
<INCLUDE_TYPOSCRIPT: source="FILE:fileadmin/default/ts/block/setup.ts">

# Read in the templates for the menu parts, like the default menu setup, language menu, header- and footermenu's.
<INCLUDE_TYPOSCRIPT: source="FILE:fileadmin/default/ts/menu/setup.ts">
<INCLUDE_TYPOSCRIPT: source="FILE:fileadmin/default/ts/menu/breadcrumb/setup.ts">
<INCLUDE_TYPOSCRIPT: source="FILE:fileadmin/default/ts/menu/main/setup.ts">
<INCLUDE_TYPOSCRIPT: source="FILE:fileadmin/default/ts/menu/meta/setup.ts">
<INCLUDE_TYPOSCRIPT: source="FILE:fileadmin/default/ts/menu/sub/setup.ts">
<INCLUDE_TYPOSCRIPT: source="FILE:fileadmin/default/ts/menu/footer/setup.ts">

# Read in the templates for the page, like page setup, config and header data.
<INCLUDE_TYPOSCRIPT: source="FILE:fileadmin/default/ts/page/setup.ts">
<INCLUDE_TYPOSCRIPT: source="FILE:fileadmin/default/ts/page/config/setup.ts">
<INCLUDE_TYPOSCRIPT: source="FILE:fileadmin/default/ts/page/meta/setup.ts">
<INCLUDE_TYPOSCRIPT: source="FILE:fileadmin/default/ts/page/media/setup.ts">

# Include development configuration
<INCLUDE_TYPOSCRIPT: source="FILE:fileadmin/global/ts/development.ts">
