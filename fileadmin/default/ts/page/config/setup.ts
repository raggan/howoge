config {
    // Administrator settings
    admPanel = {$config.adminPanel}
    debug = {$config.debug}
    no_cache = {$config.no_cache}

    doctype = html5
    htmlTag_setParams = none

    // Character sets
    renderCharset = utf-8
    metaCharset = utf-8

    // Cache settings
    cache_period = 43200
    sendCacheHeaders = 1
    contentObjectExceptionHandler = 0
    // URL Settings
    tx_realurl_enable = 1
    simulateStaticDocuments = 0

    // Language Settings
    uniqueLinkVars = 1
    linkVars = L(0), no_mobile, print
    sys_language_uid = 0
    sys_language_overlay = hideNonTranslated
    #sys_language_mode = content_fallback; 1,0
    sys_language_mode = strict
    language = de
    locale_all = de_DE.UTF-8
    htmlTag_langKey = de

    // Link settings
    absRefPrefix = {$config.absRefPrefix}
    prefixLocalAnchors = all

    // Remove targets from links
    intTarget =
    extTarget = _blank

    // Page title settings
    noPageTitle = 2
    pageTitleFirst = 1
    pageTitleSeparator = |
    pageTitleSeparator.noTrimWrap = | | &#448; HOWOGE|

    // Indexed Search
    index_enable = 1
    index_externals = 0
    index_metatags = 1

    // Code cleaning
    disablePrefixComment = 1

    // Move default CSS and JS to external file
    removeDefaultJS = external
    inlineStyle2TempFile = 1
    // Merge CSS and JS files
    concatenateCss = 0
    concatenateJs = 0

    // Protect mail addresses from spamming
    spamProtectEmailAddresses = -3
    spamProtectEmailAddresses_atSubst = @<span style="display:none;">remove-this.</span>
}

# minify html source code
config.sourceopt.formatHtml = 1

[globalVar = GP:tx_mbxrealestate_pi1|immoobject > 0]
config.noPageTitle = 2
[global]

[globalVar = GP:type > 0, GP:no_mobile>0, GP:job>0] || [globalString = GP:tx_mbxrealestate_pi1|controller=Immoaddress, GP:tx_mbxrealestate_pi1|controller=Cookie, GP:tx_mbxrealestate_pi1|controller=Searchagent]
config.index_enable = 0
[global]

[applicationContext = Production]
    config.compressCss = 1
    config.concatenateCss = 1
    config.concatenateJs = 1
[end]

[IP = 217.18.180.50]
    config.admPanel = 1
[global]
