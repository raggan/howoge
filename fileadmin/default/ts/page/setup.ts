/*
 * The PAGE OBJECT template.
 * Tells the PAGE object to use the Fluidtemplate.
 */

# Make the PAGE object
page = PAGE
page {
	# Regular pages always have typeNum = 0
	typeNum = 0
    
    headerData {
      10 = TEXT
      10 {
        field = title
        noTrimWrap = |<title>| &#448; HOWOGE</title>|
      }

      500 = TEXT
      500.value = <meta name="p:domain_verify" content="b7b707987db050c7e050cdc877ff2441"/>
    }


	# Add the icon that will appear in front of the url in the browser
	# This icon will also be used for the bookmark menu in browsers
	shortcutIcon = {$filepaths.images}favicon.ico

    bodyTag = <body class="noJs">

    10 = FLUIDTEMPLATE
    10 {
        layoutRootPath = {$filepaths.templates}
        partialRootPath = {$filepaths.templates}partials/

        file = {$filepaths.templates}tpl_default.html

        variables {
            # variables used for templating
            langFile < lib.langFile
            feLayout < lib.feLayout
            # page navigation
            headerMetaMenu < menu.meta.header
            breadcrumbMenu < menu.breadcrumb
            #mainMenu < menu.main
            mainMenu = TEXT
            mainMenu.value (
            <ul class="nav inline"><li><a href="/" title="zurück zur Startseite">Startseite</a></li><li class="flyout"><a href="/mieten/wohnungssuche.html">Mieten</a><div class="flyout-bg"><div class="row-fluid"><div class="span4"> <div class="re-searchform-wrapper"> <div class="flyout-search clearfix"> </div> <div class="tab-content"> <form method="post" class="tab-pane active flyout-search clearfix" id="flyout-apartments" action="/mieten/wohnungssuche.html"> <div> <input type="hidden" name="tx_mbxrealestate_pi1[__referrer][@extension]" value="MbxRealestate"> <input type="hidden" name="tx_mbxrealestate_pi1[__referrer][@vendor]" value="TYPO3"> <input type="hidden" name="tx_mbxrealestate_pi1[__referrer][@controller]" value="Immoobject"> <input type="hidden" name="tx_mbxrealestate_pi1[__referrer][@action]" value="searchForm"> <input type="hidden" name="tx_mbxrealestate_pi1[__referrer][arguments]" value="YTowOnt911ea2914b14ba33193d2bde9b89f241dd12a484d"> <input type="hidden" name="tx_mbxrealestate_pi1[__referrer][@request]" value="a:4:{s:10:&quot;@extension&quot;;s:13:&quot;MbxRealestate&quot;;s:11:&quot;@controller&quot;;s:10:&quot;Immoobject&quot;;s:7:&quot;@action&quot;;s:10:&quot;searchForm&quot;;s:7:&quot;@vendor&quot;;s:5:&quot;TYPO3&quot;;}593570cd8dfe23ce3492b8d4fd941c741d0a28f7"> <input type="hidden" name="tx_mbxrealestate_pi1[__trustedProperties]" value="a:2:{s:10:&quot;immosearch&quot;;a:3:{s:10:&quot;rooms_from&quot;;i:1;s:9:&quot;area_from&quot;;i:1;s:13:&quot;cost_gross_to&quot;;i:1;}s:17:&quot;search_realestate&quot;;i:1;}1f58a545e55b6dbb411fa248a7c6370817bc6b31"> </div> <div class="from-to-selection clearfix"> <label for="flyout-re-search-rooms_from" class="darkblue">Zimmer ab</label> <div class="mbx-dropdown-auto absolute-dropdown mbx-dropdown-wrapper"> <select id="flyout-re-search-rooms_from" name="tx_mbxrealestate_pi1[immosearch][rooms_from]" style="display: none;"><option value="1" selected="selected">1+</option> <option value="2">2+</option> <option value="3">3+</option> <option value="4">4+</option> <option value="5">5+</option> </select><div class="mbx-dropdown"><div class="mbx-dropdown-label"><div><span class="dropdown-title"></span><span class="value-list"><span class="title-item">1+</span></span><span class="dropdown-icon"><span></span></span></div></div><div class="mbx-dropdown-list" style="display: none;"><ul><li class="selected"><input type="radio" id="mbx-dropdown-custom-item-6113484701562266" rel="1" name="mbxdropdown[tx_mbxrealestate_pi1][immosearch][rooms_from]" style="display: none;"><label for="mbx-dropdown-custom-item-6113484701562266">1+</label></li><li><input type="radio" id="mbx-dropdown-custom-item-5194174637953846" rel="2" name="mbxdropdown[tx_mbxrealestate_pi1][immosearch][rooms_from]" style="display: none;"><label for="mbx-dropdown-custom-item-5194174637953846">2+</label></li><li><input type="radio" id="mbx-dropdown-custom-item-8539444359106412" rel="3" name="mbxdropdown[tx_mbxrealestate_pi1][immosearch][rooms_from]" style="display: none;"><label for="mbx-dropdown-custom-item-8539444359106412">3+</label></li><li><input type="radio" id="mbx-dropdown-custom-item-8516237487950539" rel="4" name="mbxdropdown[tx_mbxrealestate_pi1][immosearch][rooms_from]" style="display: none;"><label for="mbx-dropdown-custom-item-8516237487950539">4+</label></li><li><input type="radio" id="mbx-dropdown-custom-item-4693172830639818" rel="5" name="mbxdropdown[tx_mbxrealestate_pi1][immosearch][rooms_from]" style="display: none;"><label for="mbx-dropdown-custom-item-4693172830639818">5+</label></li></ul></div></div> </div> </div> <div class="from-to-selection clearfix"> <label for="flyout-re-search-area_from" class="darkblue">Fläche ab</label> <span class="inset-unit" data-unit="m²"> <input maxlength="4" id="flyout-re-search-area_from" type="text" name="tx_mbxrealestate_pi1[immosearch][area_from]"> </span> </div> <div class="from-to-selection clearfix"> <label for="flyout-re-search-cost_gross_to" class="darkblue">Warmmiete bis</label> <span class="inset-unit" data-unit="€"> <input maxlength="7" id="flyout-re-search-cost_gross_to" type="text" name="tx_mbxrealestate_pi1[immosearch][cost_gross_to]"> </span> </div> <a class="advanced-search-link" href="/mieten/wohnungssuche.html"> Erweiterte Suche </a> <span class="submit-button-holder"> <input type="submit" name="tx_mbxrealestate_pi1[search_realestate]" value="Wohnung suchen"> </span> </form> </div> </div> </div><div class="span8 column-box"><ul class="nav span"><li class="column-header"><div>Suchen und Finden<hr></div></li><li class="menu-image"><img src="/fileadmin/user_upload/_processed_/e/2/csm_flyout-mieten-kaskel-kiez-12_b1c9c2dfba.jpg" width="280" height="76" alt="Wohnung in Berlin suchen und finden"></li><li class="menu-item"><a href="/mieten/wohnungssuche.html" title="Wohnungssuche Berlin – Wohnung mieten in Berlin">Wohnungssuche</a></li><li class="menu-item"><a href="/mieten/mietbedingungen.html" title="Mietbedingungen der HOWOGE">Mietbedingungen</a></li><li class="menu-item"><a href="/nc/mieten/merkzettel.html" title="Ihre gemerkten HOWOGE-Wohnungen">Merkzettel</a></li><li class="menu-item"><a href="/mieten/interessentenformular.html" title="Interessentenformular">Interessentenformular</a></li></ul><ul class="nav span"><li class="column-header"><div>Das Besondere<hr></div></li><li class="menu-image"><img src="/fileadmin/user_upload/_processed_/b/4/csm_flyout-mieten-teaser-2_65c7cd7cdd.jpg" width="280" height="76" alt="HOWOGE-Projekte"></li><li class="menu-item"><a href="/bauen/neubauprojekte/powerhouse.html" title="Powerhouse">Powerhouse</a></li><li class="menu-item"><a href="/mieten/appartements-berlin-buch.html" title="Möblierte Wohnungen der HOWOGE in Berlin-Buch">Appartements Berlin-Buch</a></li><li class="menu-item"><a href="/bauen/neubauprojekte/hauptstrasse.html" title="Hauptstraße">Hauptstraße</a></li><li class="menu-item"><a href="/bauen/neubauprojekte/eichbuschallee.html" title="Studentenwohnen">Studentenwohnen</a></li></ul></div></div></div></li><li class="flyout"><a href="/bauen/howoge-baut.html" class="active">Bauen</a><div class="flyout-bg"><div class="row-fluid"><div class="span4"><div class="column-header"><div>Bauen<hr></div></div><ul class="nav span"><li class="menu-item"><a href="/bauen/howoge-baut.html" title="Neubauprojekte der HOWOGE">HOWOGE baut</a></li><li class="menu-item"><a href="/bauen/neubauprojekte.html" title="Neubauprojekte in Berlin" class="active">Neubauprojekte</a><div class="submenu-third-level"><ul class="submenu"><li class="menu-item"><a href="/bauen/neubauprojekte/konrad-wolf-strasse.html">Konrad-Wolf-Straße</a></li><li class="menu-item"><a href="/bauen/neubauprojekte/dolgenseestrasse.html">Dolgenseestraße</a></li><li class="menu-item"><a href="/bauen/neubauprojekte/eichbuschallee.html">Eichbuschallee</a></li><li class="menu-item"><a href="/bauen/neubauprojekte/eitelstrasse.html">Eitelstraße</a></li><li class="menu-item"><a href="/bauen/neubauprojekte/flaemingstrasse.html">Flämingstraße</a></li><li class="menu-item"><a href="/bauen/neubauprojekte/frankfurter-allee-135.html">Frankfurter Allee 135</a></li><li class="menu-item"><a href="/bauen/neubauprojekte/gaertnerhof.html">Gärtnerhof</a></li><li class="menu-item"><a href="/bauen/neubauprojekte/genslerstrasse-17.html">Genslerstraße 17</a></li><li class="menu-item"><a href="/bauen/neubauprojekte/hagenower-ring.html">Hagenower Ring</a></li></ul><ul class="submenu"><li class="menu-item"><a href="/bauen/neubauprojekte/hauptstrasse.html">Hauptstraße</a></li><li class="menu-item"><a href="/bauen/neubauprojekte/kuestriner-strasse-17.html">Küstriner Straße 17</a></li><li class="menu-item"><a href="/bauen/neubauprojekte/kuestriner-strasse-18.html">Küstriner Straße 18</a></li><li class="menu-item"><a href="/bauen/neubauprojekte/lindenhof.html">Lindenhof</a></li><li class="menu-item"><a href="/bauen/neubauprojekte/muehlengrund.html">Mühlengrund</a></li><li class="menu-item"><a href="/bauen/neubauprojekte/neustrelitzer-strasse-65.html">Neustrelitzer Straße 65</a></li><li class="menu-item"><a href="/bauen/neubauprojekte/ohlauer-strasse.html">Ohlauer Straße</a></li><li class="menu-item"><a href="/bauen/neubauprojekte/paul-zobel-strasse.html">Paul-Zobel-Straße</a></li><li class="menu-item"><a href="/bauen/neubauprojekte/powerhouse.html">Powerhouse</a></li></ul><ul class="submenu"><li class="menu-item"><a href="/bauen/neubauprojekte/rathausstrasse.html">Rathausstraße</a></li><li class="menu-item"><a href="/bauen/neubauprojekte/reichenberger-strasse-4-5.html">Reichenberger Straße 4, 5</a></li><li class="menu-item"><a href="/bauen/neubauprojekte/rosenfelder-ring-13.html">Rosenfelder Ring 13</a></li><li class="menu-item"><a href="/bauen/neubauprojekte/stallschreiberstrasse.html">Stallschreiberstraße</a></li><li class="menu-item"><a href="/bauen/neubauprojekte/sewanstrasse.html">Sewanstraße</a></li><li class="menu-item"><a href="/bauen/neubauprojekte/stillerzeile.html">Stillerzeile</a></li><li class="menu-item"><a href="/bauen/neubauprojekte/treskow-hoefe.html">Treskow-Höfe</a></li><li class="menu-item"><a href="/bauen/neubauprojekte/treskowstrasse-26-28.html">Treskowstraße 26-28</a></li><li class="menu-item"><a href="/bauen/neubauprojekte/urbaner-holzbau.html">Urbaner Holzbau</a></li><li class="menu-item active"><a href="/bauen/neubauprojekte/weserstrasse.html">Weserstraße</a></li></ul></div></li><li class="menu-item"><a href="/bauen/sanierungsprojekte.html" title="Energetische Sanierung">Sanierungsprojekte</a><div class="submenu-third-level"><ul class="submenu"><li class="menu-item"><a href="/bauen/sanierungsprojekte/berlin-buch.html">Berlin-Buch</a></li><li class="menu-item"><a href="/bauen/sanierungsprojekte/casa-nova-karree.html">Casa Nova-Karree</a></li><li class="menu-item"><a href="/bauen/sanierungsprojekte/mellenseestrasse.html">Mellenseestraße</a></li><li class="menu-item"><a href="/bauen/sanierungsprojekte/prerower-platz.html">Prerower Platz</a></li></ul></div></li><li class="menu-item"><a href="/bauen/wettbewerbe.html" title="Wettbewerbe">Wettbewerbe</a></li><li class="menu-item"><a href="/bauen/serielles-modulares-bauen.html" title="Serielles modulares Bauen">Serielles modulares Bauen</a></li><li class="menu-item"><a href="/bauen/buergerdialog.html" title="Bürgerdialog">Bürgerdialog</a></li><li class="menu-item"><a href="/bauen/wohnungsbewertungssystem.html" title="Wohnungsbewertungssystem">Wohnungsbewertungssystem</a></li></ul></div><div class="span8 reversedMenu" style="visibility: visible;"> <a href="/bauen/howoge-baut.html" id="c3021" class="teaser teaser-layout-2 font-bold" style="background-image: url('/fileadmin/user_upload/HOWOGE-baut/flyout-lupe-howoge-baut.png');"> <p class="headline"> HOWOGE baut</p> <p> Alle Neuigkeiten über die<br>aktuellen Bauprojekte im <br>Überblick <span class="link font-small font-bold icon-carat-right">Jetzt stöbern</span> </p> </a> </div></div></div></li><li class="flyout"><a href="/kiezleben/alle-kieze.html">Kiezleben</a><div class="flyout-bg"><div class="row-fluid"><div class="span4"> <a href="/kiezleben.html" id="c178" class="teaser teaser-layout-2 font-bold"> <p class="headline"> Kiezleben bei <br>der HOWOGE</p> <p> von historisch und <br>traditionell bis <br>modern und angesagt <span class="link font-small font-bold icon-carat-right">Auf der Karte ansehen</span> </p> <img title="Karte Kieze Berlin" alt="Karte Kieze Berlin" src="/fileadmin/user_upload/_processed_/8/9/csm_alle-kieze-teaser_82bc97e374.png" width="99" height="120"> </a> </div><div class="span8 column-box"><div class="column-header"><div>Stadtteilportraits<hr></div></div><ul class="nav span"><li class="menu-item"><a href="/kiezleben/alle-kieze.html" title="Alle Kieze">Alle Kieze</a></li><li class="menu-item"><a href="/kiezleben/berlin-buch.html" title="Wohnen in Berlin-Buch">Berlin-Buch</a></li><li class="menu-item"><a href="/kiezleben/fennpfuhl.html" title="Wohnen in Berlin-Fennpfuhl">Fennpfuhl</a></li><li class="menu-item"><a href="/kiezleben/friedrichsfelde.html" title="Wohnen in Berlin-Friedrichsfelde">Friedrichsfelde</a></li><li class="menu-item"><a href="/kiezleben/marzahn.html" title="Wohnen in Berlin-Marzahn">Marzahn</a></li></ul><ul class="nav span"><li class="menu-item"><a href="/kiezleben/hohenschoenhausen.html" title="Wohnen in Berlin-Hohenschönhausen">Hohenschönhausen</a></li><li class="menu-item"><a href="/kiezleben/karlshorst.html" title="Wohnen in Berlin-Karlshorst">Karlshorst</a></li><li class="menu-item"><a href="/kiezleben/koepenick.html" title="Wohnen in Berlin-Köpenick">Köpenick</a></li><li class="menu-item"><a href="/kiezleben/alt-lichtenberg.html" title="Wohnen in Berlin-Lichtenberg">Alt-Lichtenberg</a></li></ul></div></div></div></li><li class="flyout"><a href="/mieterservice/unsere-kundenzentren.html">Mieterservice</a><div class="flyout-bg"><div class="row-fluid"><div class="span4"> <a href="/mieterservice/vorteile-fuer-mieter.html" title="Zu den Vorteilsangeboten" id="c143" class="teaser teaser-layout-2 font-bold"> <p class="headline"> Die Mieterkarte</p> <p> Jeder Mieter der <br>HOWOGE profitiert <br>von der kosten-<br>losen Vorteilskarte. <span class="link font-small font-bold icon-carat-right">Zu den Vorteilsangeboten</span> </p> <img title="Die aktuelle Mieterkarte der HOWOGE" alt="HOWOGE Mieterkarte" src="/fileadmin/user_upload/_processed_/8/e/csm_HOWOGE-Mieterkarte-2017_05d1fac9c5.png" width="130" height="145"> </a> </div><div class="span8 column-box"><div class="column-header"><div>mehr als gewohnt<hr></div></div><ul class="nav span"><li class="menu-item"><a href="/mieterservice/unsere-kundenzentren.html" title="Kundenzentren der HOWOGE">Unsere Kundenzentren</a></li><li class="menu-item"><a href="/mieterservice/unsere-servicestandards.html" title="Servicestandards">Unsere Servicestandards</a></li><li class="menu-item"><a href="/mieterservice/serviceangebote-fuer-mieter.html" title="Serviceangebote für Mieter">Serviceangebote für Mieter</a></li><li class="menu-item"><a href="/mieterservice/vorteile-fuer-mieter.html" title="Rabatte von Kooperationspartnern | Servicekarte">Vorteile für Mieter</a></li></ul><ul class="nav span"><li class="menu-item"><a href="/mieterservice/ratgeber-und-haeufige-fragen.html" title="Ratgeber und häufige Fragen">Ratgeber und häufige Fragen</a></li><li class="menu-item"><a href="/mieterservice/mieterrat/mieterrat.html" title="Mieterrat">Mieterrat</a></li><li class="menu-item"><a href="/mieterservice/mieterbeiraete.html" title="Mieterbeiräte">Mieterbeiräte</a></li><li class="menu-item"><a href="/mieterservice/mieterzeitung.html" title="Mieterzeitung">Mieterzeitung</a></li><li class="menu-item"><a href="/mieterservice/internet-und-telefonie.html" title="Internet und Telefonie">Internet und Telefonie</a></li></ul></div></div></div></li><li class="flyout"><a href="/unternehmen/ueberblick.html">Unternehmen</a><div class="flyout-bg"><div class="row-fluid"><div class="span4"><div id="c586" class="csc-default no-header"><div class="csc-textpic csc-textpic-center csc-textpic-above"><div class="csc-textpic-imagewrap" data-csc-images="1" data-csc-cols="2"><div class="csc-textpic-center-outer"><div class="csc-textpic-center-inner"><figure class="csc-textpic-image csc-textpic-last"><img src="/fileadmin/user_upload/_processed_/f/1/csm_SUE14038_19.kompr_c53168f169.jpg" width="319" height="230" alt="Unternehmenszentrale der HOWOGE"></figure></div></div></div></div></div></div><div class="span8 column-box"><div class="column-header"><div>Die HOWOGE im Fokus<hr></div></div><ul class="nav span"><li class="menu-item"><a href="/unternehmen/ueberblick.html" title="Zahlen und Fakten zur HOWOGE">Überblick</a></li><li class="menu-item"><a href="/nc/unternehmen/aktuelles.html" title="Aktuelles">Aktuelles</a></li><li class="menu-item"><a href="/unternehmen/nachhaltigkeit.html" title="Nachhaltigkeit bei der HOWOGE">Nachhaltigkeit</a></li><li class="menu-item"><a href="/unternehmen/netzwerk.html" title="Netzwerk">Netzwerk</a></li><li class="menu-item"><a href="/unternehmen/gesellschaftliches-engagement.html" title="Gesellschaftliches Engagement">Gesellschaftliches Engagement</a></li><li class="menu-item"><a href="/unternehmen/compliance-und-werte.html" title="Compliance- und Wertemanagement">Compliance und Werte</a></li></ul><ul class="nav span"><li class="menu-item"><a href="/unternehmen/tochterunternehmen.html" title="Tochterunternehmen der HOWOGE">Tochterunternehmen</a></li><li class="menu-item"><a href="/unternehmen/karriere-bei-der-howoge.html" title="Arbeit und Studium bei der HOWOGE">Karriere bei der HOWOGE</a></li><li class="menu-item"><a href="/unternehmen/ausschreibungen.html" title="Ausschreibungen">Ausschreibungen</a></li><li class="menu-item"><a href="/unternehmen/ankauf-von-immobilien.html" title="Ankauf von Immobilien">Ankauf von Immobilien</a></li><li class="menu-item"><a href="/unternehmen/presse/pressemitteilungen.html" title="Presse">Presse</a></li><li class="menu-item"><a href="/unternehmen/presse/download-center.html" title="Download-Center">Download-Center</a></li></ul></div></div></div></li><li class="flyout"><a href="/kontakt/meine-kontakte.html">Kontakt</a><div class="flyout-bg"><div class="row-fluid"><div class="span8 column-box"><div class="column-header"><div>Der richtige Ansprechpartner für jede Situation<hr></div></div><ul class="nav span"><li class="menu-item"><a href="/kontakt/meine-kontakte.html" title="Meine Kontakte">Meine Kontakte</a></li><li class="menu-item"><a href="/kontakt/haeufige-fragen.html" title="Häufige Fragen">Häufige Fragen</a></li></ul></div></div></div></li></ul>
            )
            subMenu < menu.sub
            footerMetaMenu < menu.meta.footer
            footerMenu < menu.footer
            # content blocks
            mainContent < styles.content.get
            sidebarContent < styles.content.getLeft
            # footer blocks
            seo < lib.seoText
            # crop seo-text
            noSeoCropping = TEXT
            noSeoCropping.value = 0
        }
    }
}

# MBX: Use custom layout template for login page
[globalVar = TSFE:id={$plugin.tx_mbxrenteraccounthowoge.pages.login}]
page.10.file = {$plugin.tx_mbxrenteraccounthowoge.templates.page.login}
[global]

# MBX: Use custom layout template for qr exposee page
[applicationContext = Production] && [globalVar = TSFE:id=914, TSFE:id=915, TSFE:id=916, TSFE:id=917 ]
    page.10.file = {$filepaths.templates}tpl_exposee.html
[global]

# On HOME,MIETEN do not crop seo text
[globalVar = TSFE:id = 1] || [globalVar = TSFE:id = 84] || [globalVar = TSFE:id = 287]
    page.10.variables.noSeoCropping.value = 1
[global]

# On Wohnungssuche, Gewerbe do not show text on form submit
[globalVar = TSFE:id = 84] && [globalVar = GP:tx_mbxrealestate_pi1 !=]
    page.10.variables.seo >
[global]
[globalVar = TSFE:id = 287] && [globalVar = GP:tx_mbxrealestate_pi1 !=]
    page.10.variables.seo >
[global]

# Add additional meta tag for mobile devices
page.headerData {
    75 = TEXT
    75.value = <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE9" >

    77 = TEXT
    77.value = <meta name="viewport" content="width=device-width, initial-scale=1" />
}

# Add a page for iframe calls
iframePage < page
iframePage {
    typeNum = 100

    includeCSS {
        bootstrap = {$filepaths.css}bootstrap.css
        fonts = //fonts.googleapis.com/css?family=Open+Sans:400italic,400,300,600,700italic,700,800
        fonts {
            external = 1
            excludeFromConcatenation = 1
        }
        layout = {$filepaths.css}layout.css
    }

    bodyTag = <body class="iframe">

    10.file = {$filepaths.templates}tpl_iframe.html
}

# Configuration of print page
print < page
print {
    typeNum = 98
    # include print stylesheet
    includeCSS.print = {$filepaths.css}print.css
    includeJSFooterlibs >
    jsFooterInline {
        5 = TEXT
        5.value (
if ($('#jsPrintExpose').length) {
window.print();
$('#jsPrintExposeTrigger').on('click', function () {
window.print();
});

if($('#jsPrintExpose').hasClass('miko')) {
$('.csc-firstHeader.h0').remove();
}
}
        )
    }

    10.file = {$filepaths.templates}tpl_print.html
}

# Ajax page to fetch map contents
kiezOverviewAjaxTeaser < page
kiezOverviewAjaxTeaser {
    config {
        disableAllHeaderCode = 1
        xhtml_cleaning = 1
        admPanel = 0
        debug = 0
        no_cache = 1
    }
    typeNum = 3000

    10 = CONTENT
    10 {
        table = tt_content
        select {
            languageField = sys_language_uid
            pidInList.data = leveluid : 1
            recursive = 4
            andWhere.dataWrap = uid = {GP:kiezOverview} AND colPos=5 AND CType LIKE 'fluidcontent_content' AND tx_fed_fcefile LIKE '%MapTeaser.html%'
            orderBy = header
        }
    }
}


# Ajax page to fetch map contents
mieterratOverviewAjaxTeaser < page
mieterratOverviewAjaxTeaser {
    config {
        disableAllHeaderCode = 1
        xhtml_cleaning = 1
        admPanel = 0
        debug = 0
        no_cache = 1
    }
    typeNum = 3001

    10 = CONTENT
    10 {
        table = tt_content
        select {
            languageField = sys_language_uid
            pidInList.data = leveluid : 1
            recursive = 4
            andWhere.dataWrap = uid = {GP:bezirk} AND colPos=0 AND CType LIKE 'fluidcontent_content' AND tx_fed_fcefile LIKE '%MapTeaser%'
            orderBy = header
        }
    }
}

[PIDupinRootline = {$plugin.tx_mbxrenteraccounthowoge.pages.register},{$plugin.tx_mbxrenteraccounthowoge.pages.doi}]
page.10.variables.breadcrumbMenu >
[global]

#kiezOverviewKml < kiezOverviewAjaxTeaser
#kiezOverviewKml {
#    typeNum = 3100
#    10 {
#        select.andWhere.dataWrap = colPos=5 AND CType LIKE 'fluidcontent_content' AND tx_fed_fcefile LIKE '%MapTeaser.html%'
#        renderObj = COA
#        renderObj {
#            10 =
#        }
#    }
#}

[globalVar = GP:tx_news_pi1|news > 0]
    config.noPageTitle = 1

    temp.newsTitle = RECORDS
    temp.newsTitle {
        source = {GP:tx_news_pi1|news}
        source.insertData = 1
        tables = tx_news_domain_model_news
        conf {
            tx_news_domain_model_news >
            tx_news_domain_model_news = TEXT
            tx_news_domain_model_news {
                field = title
                noTrimWrap = |<title>| &#124; HOWOGE</title>|
            }
        }
    }
    page.headerData {
        10 >
        10 = COA
        10 < temp.newsTitle
    }
[global]



[globalVar = TSFE:type = 9818, TSFE:type = 9819, TSFE:type = 9822]
    config {
        disableAllHeaderCode = 1
        xhtml_cleaning = none
        admPanel = 0
        metaCharset = utf-8
        additionalHeaders = Content-Type:text/xml;charset=utf-8
        disablePrefixComment = 1
        baseURL < config.baseURL
        absRefPrefix < config.baseURL
        debug = 0
        no_cache = 1
    }


    pageNewsPresse = PAGE
    pageNewsPresse {
        typeNum = 9818

        headerData {
          10 = TEXT
          10 {
            field = title
            noTrimWrap = |<title>| &#448; HOWOGE</title>|
          }
        }


        10 < tt_content.list.20.news_pi1
        10 {
            switchableControllerActions {
                News {
                    1 = list
                }
            }
            view {
                templateRootPaths {
                    250 = fileadmin/default/templates/extensions/sitemap/news/Templates/
                }

            }
            settings < plugin.tx_news.settings
            settings {
                detailPid = 100
                startingpoint = 97
                format = xml
                limit = 10000
                orderBy = crdate
                orderDirection = desc
#				categories = 7
#				categoryConjunction = or
                cropMaxCharacters = 200
                link {
                    skipControllerAndAction = 1
                }
            }
        }
	}

    pageNews < pageNewsPresse
    pageNews {
        typeNum = 9819
        10.settings.detailPid = 52
        10.settings.startingpoint = 98
    }

    pageEvens < pageNewsPresse
    pageEvens {
        typeNum = 9822
        10.settings.detailPid = 184
        10.settings.startingpoint = 142
    }
[global]

[globalVar = TSFE:type = 9820]
    config {
        disableAllHeaderCode = 1
        xhtml_cleaning = none
        admPanel = 0
        metaCharset = utf-8
        additionalHeaders = Content-Type:text/xml;charset=utf-8
        disablePrefixComment = 1
        baseURL < config.baseURL
        absRefPrefix < config.baseURL
        debug = 0
        no_cache = 1
    }

    page = PAGE
    page {
        typeNum = 9820
        wrap (
            <?xml version="1.0" encoding="UTF-8"?>
            <urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">|
            </urlset>
        )
        
        5 = TEXT
        5.char = 10

        # Home
        9 = LOAD_REGISTER
        9 {
            # 1 = ID of home page
            datum.data = DB:pages:{$config.domainSitemapStartingPoint}:SYS_LASTCHANGED
            datum.ifEmpty.field = crdate
            datum.strftime = %Y-%m-%d
        }
        10 = TEXT
        10 {
            value (
                <url>
                    <loc>{$config.domain}/</loc>
                    <lastmod>{register:datum}</lastmod>
                </url>
            )
            insertData = 1
            
        }
        11 = RESTORE_REGISTER

        # mobile Home
        12 = LOAD_REGISTER
        12 {
            # 1 = ID of home page
            datum.data = DB:pages:{$config.mobileDomainSitemapStartingPoint}:SYS_LASTCHANGED
            datum.ifEmpty.field = crdate
            datum.strftime = %Y-%m-%d
        }
        13 = TEXT
        13 {
            value (
                <url>
                    <loc>https://{$config.mobileDomain}/</loc>
                    <lastmod>{register:datum}</lastmod>
                </url>
            )
            insertData = 1
            
        }
        14 = RESTORE_REGISTER
        
        # The MENU
        30 = HMENU
        30 {
            special = directory
            special.value = {$config.domainSitemapStartingPoint}
            # 15,8,5
            # Startseite manuell!
#            excludeUidList = 2

            1 = TMENU
            1 {
                expAll = 1
                NO = 1
                NO {
                    doNotShowLink = 1

                    stdWrap2 {
                        cObject = CASE
                        cObject {
                            key.field = doktype
                            1 = COA
                            1 {
                                2 = TEXT
                                2.char = 9

                                3 = TEXT
                                3.value = <url>

                                4 = TEXT
                                4.char = 10

                                5 = TEXT
                                5 {
                                    noTrimWrap = |		<loc>{$config.domain}|</loc>|
                                    typolink {
                                        parameter.field = uid
                                        returnLast = url
                                    }
                                    htmlSpecialChars = 1
                                }

                                7 = TEXT
                                7.char = 10

                                8 = TEXT
                                8.char = 9

                                9 = TEXT
                                9.char = 9

                                10 = TEXT
                                10.value = <lastmod>

                                20 = TEXT
                                20.field = SYS_LASTCHANGED
                                20.ifEmpty.field = crdate
                                20.strftime = %Y-%m-%d

                                30 = TEXT
                                30.value = </lastmod>

                                31 = TEXT
                                31.char = 10

                                35 = TEXT
                                35.char = 9

                                40 = TEXT
                                40.value = </url>
                                41 = TEXT
                                41.char = 10
                            }
                            default = TEXT
                            # default.field = uid
                            # default.wrap = <leer>|</leer>
                        }
                    }
                }
            }
            2 < .1
            3 < .1
            4 < .1
        }

        # The MENU
        40 = HMENU
        40 {
            special = directory
            special.value = {$config.mobileDomainSitemapStartingPoint}
            # 15,8,5
            # Startseite manuell!
#            excludeUidList = 2

            1 = TMENU
            1 {
                expAll = 1
                NO = 1
                NO {
                    doNotShowLink = 1

                    stdWrap2 {
                        cObject = CASE
                        cObject {
                            key.field = doktype
                            1 = COA
                            1 {
                                2 = TEXT
                                2.char = 9

                                3 = TEXT
                                3.value = <url>

                                4 = TEXT
                                4.char = 10

                                5 = TEXT
                                5 {
                                    noTrimWrap = |		<loc>https://{$config.mobileDomain}|</loc>|
                                    typolink {
                                        parameter.field = uid
                                        returnLast = url
                                    }
                                    htmlSpecialChars = 1
                                }

                                7 = TEXT
                                7.char = 10

                                8 = TEXT
                                8.char = 9

                                9 = TEXT
                                9.char = 9

                                10 = TEXT
                                10.value = <lastmod>

                                20 = TEXT
                                20.field = SYS_LASTCHANGED
                                20.ifEmpty.field = crdate
                                20.strftime = %Y-%m-%d

                                30 = TEXT
                                30.value = </lastmod>

                                31 = TEXT
                                31.char = 10

                                35 = TEXT
                                35.char = 9

                                40 = TEXT
                                40.value = </url>
                                41 = TEXT
                                41.char = 10
                            }
                            default = TEXT
                            # default.field = uid
                            # default.wrap = <leer>|</leer>
                        }
                    }
                }
            }
            2 < .1
            3 < .1
            4 < .1
        }
    }
[global]

[globalVar = TSFE:type = 9821]
    config {
        disableAllHeaderCode = 1
        xhtml_cleaning = none
        admPanel = 0
        metaCharset = utf-8
        additionalHeaders = Content-Type:text/xml;charset=utf-8
        disablePrefixComment = 1
        baseURL < config.baseURL
        absRefPrefix < config.baseURL
        debug = 0
        no_cache = 1
    }
    page >
    page = PAGE
    page {
        typeNum = 9821
        
        wrap (
            <?xml version="1.0" encoding="UTF-8"?>
            <urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">|
            </urlset>
        )
        
        5 = TEXT
        5 {
            value (
                <url>
                    <loc>{$config.domain}/{$config.sitemap}</loc>
                </url>
                <url>
                    <loc>{$config.domain}/{$config.sitemapPress}</loc>
                </url>
                <url>
                    <loc>{$config.domain}/{$config.sitemapNews}</loc>
                </url>
                <url>
                    <loc>{$config.domain}/{$config.sitemapEvents}</loc>
                </url>
            )
        }
    }            
[global]