/*
 * The META template.
 *
 * Configures meta information for the website. It's possible this template
 * remains empty because most of the information is handled by constants, which
 * are not allowed here. Constant settings belong to the root.
 */

page.meta {
	# Use the meta tag 'description' from the constants as default value
	# If the meta field description in the page properties is filled, then this will override the default.
	description = {$plugin.meta.description}
	description.override.field = description

	author = {$plugin.meta.author}
	author.override.field = author

	keywords = {$plugin.meta.keywords}
	keywords.override.field = keywords

	robots = {$plugin.meta.robots}
    robots.override = noindex, follow
    robots.override.if {
        value.field = no_search
        equals = 1
    }
    revisit-after = {$plugin.meta.revisit-after}
}

# don't index pages, don't follow links:
# - with no_mobile flag
# - with some mbx_realestate controller in url
[globalVar = GP:no_mobile>0, GP:job>0] || [globalString = GP:tx_mbxrealestate_pi1|controller=Immoaddress, GP:tx_mbxrealestate_pi1|controller=Cookie, GP:tx_mbxrealestate_pi1|controller=Searchagent]
page.meta {
    revisit-after >
    robots.override >
    robots.override = noindex, follow
}

# - archive, category and tag pages
[globalVar = GP:tx_news_pi1|overwriteDemand|year>0, GP:tx_news_pi1|overwriteDemand|categories>0, globalVar = GP:tx_news_pi1|overwriteDemand|tags>0]
page.meta {
    revisit-after >
    robots.override >
    robots.override = noindex, follow
}
[global]

# - don't index sitemap page but follow links
[page|uid = 15]
page.meta {
    robots.override >
    robots.override = noindex, follow
}
[global]

# add noindex, follow when page is calles via https or '?' is found in url
 [globalString = IENV:REQUEST_URI = /\?/]
    page.meta.robots = noindex, follow
[global]


[globalVar = GP:tx_news_pi1|@widget_0|currentPage > 1]
    page.meta.robots >
[global]

# index pages Wohnungssuche and Gewerbe initial page
[globalVar = TSFE:id = 84] && [globalVar = GP:tx_mbxrealestate_pi1 =]
    page.meta.robots = index, follow
    page.meta.robots.override >
[global]
[globalVar = TSFE:id = 287] && [globalVar = GP:tx_mbxrealestate_pi1 =]
    page.meta.robots = index, follow
    page.meta.robots.override >
[global]

[globalString = IENV:REQUEST_URI = /\?tx_news/]
    page.meta {
        revisit-after >
        robots.override >
        robots.override = noindex, follow
    }
[global]

[globalVar = TSFE:id = 155] || [globalVar = TSFE:id = 288] || [globalVar = TSFE:id = 230]
    page.headerData > 
[global]