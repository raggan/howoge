/*
 * The PAGE OBJECT template.
 * Tells the PAGE object to use the Fluidtemplate.
 */

# Make the PAGE object
page = PAGE
page {
	# Regular pages always have typeNum = 0
	typeNum = 0

	# Add the icon that will appear in front of the url in the browser
	# This icon will also be used for the bookmark menu in browsers
	shortcutIcon = {$filepaths.images}favicon.ico

    bodyTag = <body class="noJs">

    10 = FLUIDTEMPLATE
    10 {
        layoutRootPath = {$filepaths.templates}
        partialRootPath = {$filepaths.templates}partials/

        file = {$filepaths.templates}tpl_default.html

        variables {
            # variables used for templating
            langFile < lib.langFile
            feLayout < lib.feLayout
            # page navigation
            headerMetaMenu < menu.meta.header
            breadcrumbMenu < menu.breadcrumb
            mainMenu < menu.main
            subMenu < menu.sub
            footerMetaMenu < menu.meta.footer
            footerMenu < menu.footer
            # content blocks
            mainContent < styles.content.get
            sidebarContent < styles.content.getLeft
            # footer blocks
            seo < lib.seoText
            # crop seo-text
            noSeoCropping = TEXT
            noSeoCropping.value = 0
        }
    }
}


# On HOME,MIETEN do not crop seo text
[globalVar = TSFE:id = 1] || [globalVar = TSFE:id = 84] || [globalVar = TSFE:id = 287]
page.10.variables.noSeoCropping.value = 1
[global]

# On Wohnungssuche, Gewerbe do not show text on form submit
[globalVar = TSFE:id = 84] && [globalVar = GP:tx_mbxrealestate_pi1 !=]
    page.10.variables.seo >
[global]
[globalVar = TSFE:id = 287] && [globalVar = GP:tx_mbxrealestate_pi1 !=]
    page.10.variables.seo >
[global]

# Add additional meta tag for mobile devices
page.headerData {
    10 = TEXT
    10 {
        field = title
        noTrimWrap = |<title> | &#124; HOWOGE</title>|
    }

    75 = TEXT
    75.value = <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE9" >

    77 = TEXT
    77.value = <meta name="viewport" content="width=device-width, initial-scale=1" />
}

# Add a page for iframe calls
iframePage < page
iframePage {
    typeNum = 100

    includeCSS {
        bootstrap = {$filepaths.css}bootstrap.css
        fonts = //fonts.googleapis.com/css?family=Open+Sans:400italic,400,300,600,700italic,700,800
        fonts {
            external = 1
            excludeFromConcatenation = 1
        }
        layout = {$filepaths.css}layout.css
    }

    bodyTag = <body class="iframe">

    10.file = {$filepaths.templates}tpl_iframe.html
}

# Configuration of print page
print < page
print {
    typeNum = 98
    # include print stylesheet
    includeCSS.print = {$filepaths.css}print.css
    includeJSFooterlibs >
    jsFooterInline {
        5 = TEXT
        5.value (
            if ($('#jsPrintExpose').length) {
                window.print();
                $('#jsPrintExposeTrigger').on('click', function () {
                    window.print();
                });
            }
        )
    }

    10.file = {$filepaths.templates}tpl_print.html
}

# Ajax page to fetch map contents
kiezOverviewAjaxTeaser < page
kiezOverviewAjaxTeaser {
    config {
        disableAllHeaderCode = 1
        xhtml_cleaning = 1
        admPanel = 0
        debug = 0
        no_cache = 1
    }
    typeNum = 3000

    10 = CONTENT
    10 {
        table = tt_content
        select {
            languageField = sys_language_uid
            pidInList.data = leveluid : 1
            recursive = 1
            andWhere.dataWrap = uid = {GP:kiezOverview} AND colPos=5 AND CType LIKE 'fluidcontent_content' AND tx_fed_fcefile LIKE '%MapTeaser.html%'
            orderBy = header
        }
    }
}

#kiezOverviewKml < kiezOverviewAjaxTeaser
#kiezOverviewKml {
#    typeNum = 3100
#    10 {
#        select.andWhere.dataWrap = colPos=5 AND CType LIKE 'fluidcontent_content' AND tx_fed_fcefile LIKE '%MapTeaser.html%'
#        renderObj = COA
#        renderObj {
#            10 =
#        }
#    }
#}

[globalString = ENV:HTTPS=on]
    # add canonical tag to https-version
    temp.canonical = COA
    temp.canonical {
        wrap = <link rel="canonical" href="http://|" />
        10 = TEXT
        10.data = getEnv:HTTP_HOST
        20 = TEXT
        20 {
            typolink {
                parameter.data = TSFE:id
                addQueryString = 1
                addQueryString {
                    method = GET
                    // vermeide doppelte id:
                    exclude = id
                }
                returnLast = url
            }
        }
    }
    page.headerData.2000 < temp.canonical

[else]
    temp.alternate = COA
    temp.alternate {
        wrap = <link rel="alternate" media="only screen and (max-width: 640px)" href="http://|" />
        10 = TEXT
        10.value = {$config.mobileDomain}
        20 = TEXT
        20 {
            typolink {
                parameter.data = TSFE:id
                addQueryString = 1
                addQueryString {
                    method = GET
                    // vermeide doppelte id:
                    exclude = id
                }
                returnLast = url
            }
        }

    }


    page.headerData.2000 < temp.alternate
[global]


[globalVar = GP:tx_news_pi1|news > 0]
    config.noPageTitle = 1

    temp.newsTitle = RECORDS
    temp.newsTitle {
        source = {GP:tx_news_pi1|news}
        source.insertData = 1
        tables = tx_news_domain_model_news
        conf {
            tx_news_domain_model_news >
            tx_news_domain_model_news = TEXT
            tx_news_domain_model_news {
                field = title
                noTrimWrap = |<title>| &#124; HOWOGE</title>|
            }
        }
    }
    page.headerData {
        10 >
        10 = COA
        10 < temp.newsTitle
    }
[global]