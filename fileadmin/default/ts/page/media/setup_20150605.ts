/*
 * The MEDIA template.
 *
 * Includes all necessarry Cascading Style Sheets and JavaScripts by
 * referring to the file on the server and configuring the media type.
 */

page.includeCSS {
    bootstrap = {$filepaths.css}bootstrap.css
    fonts = //fonts.googleapis.com/css?family=Open+Sans:400italic,400,300,600,700italic,700,800
    fonts {
        external = 1
        excludeFromConcatenation = 1
        forceOnTop = 1
    }
    jqueryUi = {$filepaths.css}lib/jquery-ui/custom.css
    jqueryQtip = //cdn.jsdelivr.net/qtip2/2.2.0/jquery.qtip.min.css
    jqueryQtip {
        external = 1
        excludeFromConcatenation = 1
        forceOnTop = 1
    }
    iCheck = {$filepaths.css}iCheck/minimal/blue.css
    layout = {$filepaths.css}layout.css
    kiezmelder = {$filepaths.css}kiezmelder.css
    # include css for extensions
    mbxRealestate = {$filepaths.css}extensions/mbx_realestate.css
    mbxContacts = {$filepaths.css}extensions/mbx_contacts.css
    news = {$filepaths.css}extensions/news.css
#    comments = {$filepaths.css}extensions/comments.css
    indexedsearch = {$filepaths.css}extensions/indexed_search.css
    rzautocomplete = {$filepaths.css}extensions/rzautocomplete.css

    # include print styles
    print = {$filepaths.css}print.css
    print.media = print
}

[browser = msie] && [version= <9]
page.includeCSS {
    # remove google fonts due to issues with ie8
    fonts >
    # load custom ie.css
    ieFixes = {$filepaths.css}ie.css
}

page.includeJSlibs {
    modernizer = {$filepaths.js}lib/modernizr.js
    modernizer.excludeFromConcatenation = 1
}
[browser = msie] && [version= <10]
page.includeJSlibs {
    css3multicolumn = {$filepaths.js}lib/css3-multi-column.js
    css3multicolumn.excludeFromConcatenation = 1
}
[global]

page.includeJSFooterlibs {
    gmap = //maps.googleapis.com/maps/api/js?sensor=false&libraries=places
    gmap {
        external = 1
        excludeFromConcatenation = 1
    }
    jqueryGmap3 = {$filepaths.js}lib/gmap3.min.js
    jqueryQtip = //cdn.jsdelivr.net/qtip2/2.2.0/jquery.qtip.min.js
    jqueryQtip {
        external = 1
        excludeFromConcatenation = 1
    }
    jqueryCaroufredsel = {$filepaths.js}lib/jquery.carouFredSel-6.2.1-packed.js
    jqueryTouchswipe = {$filepaths.js}lib/helper-plugins/jquery.touchSwipe.min.js
    iCheck = {$filepaths.js}lib/jquery.icheck.min.js
    mbxDropdown = {$filepaths.js}lib/jquery.mbx-dropdown.js
    readmore = {$filepaths.js}lib/readmore.min.js
    socialshareprivacy = {$filepaths.js}lib/jquery.socialshareprivacy.min.js

    scripts = {$filepaths.js}scripts.js
    scripts_dk = {$filepaths.js}scripts_dk.js
}

[treeLevel = 0]
page.includeJSFooterlibs {
    simpleWeather = {$filepaths.js}lib/jquery.simpleWeather-2.3.min.js
    homepageInit = {$filepaths.js}homepage.js
}

[global]
page.jsFooterInline {
    101 = TEXT
    101.value = document.body.className = document.body.className.replace('noJs','js');
}

[globalString = IENV:HTTP_HOST=www.howoge.de]
page.jsFooterInline {
    # Include google analytics code
    99 = TEXT
    99.value (
        var gaProperty = '{$config.googleAnalytics}';

        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', gaProperty, {'cookieDomain': 'howoge.de'});
        ga('set', 'forceSSL', true);
        ga('set', 'anonymizeIp', true);
        ga('send', 'pageview');

        // Disable tracking if the opt-out cookie exists.
        var disableStr = 'ga-disable-' + gaProperty;
        if (document.cookie.indexOf(disableStr + '=true') > -1) {
            window[disableStr] = true;
        }

        // Opt-out function
        function gaOptout() {
            document.cookie = disableStr + '=true; expires=Thu, 31 Dec 2099 23:59:59 UTC; path=/';
            window[disableStr] = true;
        }

        jQuery(document).ready(function() {
            jQuery('#GAOptOut').on('click', function(e) {
                e.preventDefault();
                gaOptout();
                GAOptOutFeedback();
            });

            // Disable tracking if the opt-out cookie exists.
            if (document.cookie.indexOf(disableStr + '=true') > -1) {
                GAOptOutFeedback();
            }

            function GAOptOutFeedback() {
                jQuery('#GAOptOutFeedback').remove();
                jQuery('#GAOptOut').after('<span id="GAOptOutFeedback" style="color:#FF0000"> (Stand: Opt-Out-Cookie ist gesetzt)</span>');
            }
        });
    )
}
[global]