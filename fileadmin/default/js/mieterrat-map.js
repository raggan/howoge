var km = km || {
    dom : {
        mapContainer : null
    },
    filename : '/fileadmin/default/kml/HOWOGE-Mieterrat.kml?v=' + Date.now(),
    mapOptions : null,
    geoXml : null,
    geoXmlDoc : null,
    googleMap : null,
    overlayHighlight : {
        fill : null,
        line : null
    }
};

km.init = function() {

    this.dom.mapContainer = jQuery('#mieterratMap');

    this.mapOptions = {
        center: new google.maps.LatLng(52.50685814034978, 13.42474337847879),
        zoom: 11,
        mapTypeControl: false,
        streetViewControl: false,
        preserveViewport: true,
        suppressInfoWindows: true
    };

    if (this.dom.mapContainer.length) {

        this.googleMap = new google.maps.Map(this.dom.mapContainer[0], this.mapOptions);

        this.geoXml = new geoXML3.parser({
            map           : this.googleMap,
            zoom          : false,
            afterParse    : this.handleKml,
            infoWindowOptions : {content : null},
            singleInfoWindow  : true,
            suppressInfoWindows : true
        });

        // load modified development KML on testing server because of different 
        // fluid content IDs in KML configuration
        if(location.host.match(/^dev/)) {
            this.filename = this.filename.replace(/\.kml/, '-dev.kml');
        }
        
        this.geoXml.parse(this.filename);

        try {
            
            this.googleMap.google.addListener("click", km.infoWindow.closeAll);
        } catch(e) {
            console.log(e);
        }
    }
};

km.handleKml = function(doc) {

    var currentBounds = km.googleMap.getBounds();
    var $km = km;

    if (!currentBounds) {
        currentBounds = new google.maps.LatLngBounds();
    }

    $km.geoXmlDoc = doc[0];

    var fill = $km.geoXmlDoc.styles['#DistrictFill2'];

    $km.overlayHighlight.fill = {fillColor: "#003c5a", strokeColor: "#003c5a", fillOpacity: 1, strokeWidth: fill.width};
    $km.overlayHighlight.line = {strokeColor: "#003c5a", strokeWidth: fill.width};

    for (var i = 0; i < $km.geoXmlDoc.placemarks.length; i++) {
        var placemark = $km.geoXmlDoc.placemarks[i];

        var objPlacemark = $km.placemarks.push(placemark);

        if (placemark.polygon) {
            var normalStyle = {
                strokeColor   : placemark.polygon.get('strokeColor'),
                strokeWeight  : placemark.polygon.get('strokeWeight'),
                strokeOpacity : placemark.polygon.get('strokeOpacity'),
                fillColor     : placemark.polygon.get('fillColor'),
                fillOpacity   : placemark.polygon.get('fillOpacity')
            };
            placemark.polygon.normalStyle = normalStyle;
        }

        if (placemark.polyline) {
            var normalStyle = {
                strokeColor   : placemark.polyline.get('strokeColor'),
                strokeWeight  : placemark.polyline.get('strokeWeight'),
                strokeOpacity : placemark.polyline.get('strokeOpacity')
            };
            placemark.polyline.normalStyle = normalStyle;
        }

        $km.poly.bindEvents(objPlacemark);
    }
};

km.placemarks = (function() {

    var storage = [];

    var push = function(placemark) {

        var uid = placemark.name.replace(/.*\|(\d+)$/gi, '$1');

        storage.push({
            placemark : placemark,
            uid : uid
        });

        return getByUid(uid);
    };

    var getByUid = function(uid) {

        if(typeof uid === 'object') {
            return uid;
        }

        var ret = false;

        jQuery(storage).each(function() {
            if(this.uid == uid) {
                ret = this;
            }
        });

        return ret;
    };

    var click = function(uid) {

        var placemarkObj = getByUid(uid);

        if(placemarkObj !== false) {
            km.poly.click(placemarkObj);
        }
    };

    return {
        push : push,
        click : click,
        getByUid : getByUid,
        getStorage : function() { return storage; }
    };
}());


km.poly = (function() {

    var $km = km;

    var click = function(placemarkObj) {

        var requestId = placemarkObj.placemark.name.replace(/.*\|(\d+)$/gi, '$1');

        if (requestId.match(/^\d+$/)) {

            $km.infoWindow.load(requestId);
            $km.infoWindow.show(requestId);

            highlight(placemarkObj);
        }
    };

    var highlight = function(placemarkObj) {

        jQuery(km.placemarks.getStorage()).each(function() {
            unhighlight(this);
        });

        if (placemarkObj.placemark.polygon) { placemarkObj.placemark.polygon.setOptions($km.overlayHighlight.fill); }
        if (placemarkObj.placemark.polyline) { placemarkObj.placemark.polyline.setOptions($km.overlayHighlight.line); }
    };

    var unhighlight = function(placemarkObj) {

        if(typeof placemarkObj === 'undefined') {

            jQuery(km.placemarks.getStorage()).each(function() {
                unhighlight(this);
            });
            return;
        }

        if(km.infoWindow.isOpen(placemarkObj.uid) === true) {
            return;
        }

        if (placemarkObj.placemark.polygon) { placemarkObj.placemark.polygon.setOptions(placemarkObj.placemark.polygon.normalStyle); }
        if (placemarkObj.placemark.polyline) { placemarkObj.placemark.polyline.setOptions(placemarkObj.placemark.polygon.normalStyle); }
    };

    var bindEvents = function(placemarkObj) {

        if(placemarkObj.placemark.styleUrl === '#CityOutline') {
            return;
        }

        if(typeof placemarkObj.placemark.polygon !== 'undefined') {

            google.maps.event.addListener(placemarkObj.placemark.polygon, "mouseover", function() { click(placemarkObj); });
            google.maps.event.addListener(placemarkObj.placemark.polygon, "mouseout", function() { unhighlight(placemarkObj); });
            google.maps.event.addListener(placemarkObj.placemark.polygon, 'click', function() { click(placemarkObj); });
        }

        if(typeof placemarkObj.placemark.polyline !== 'undefined') {

            google.maps.event.addListener(placemarkObj.placemark.polyline, "mouseover", function() { click(placemarkObj); });
            google.maps.event.addListener(placemarkObj.placemark.polyline, "mouseout", function() { unhighlight(placemarkObj); });
            google.maps.event.addListener(placemarkObj.placemark.polyline, 'click', function() { click(placemarkObj); });
        }
    };

    return {
        bindEvents : bindEvents,
        click : click,
        highlight : highlight,
        unhighlight : unhighlight
    };
}());


km.infoWindow = (function() {

    var storage = [];

    var show = function(infoId) {

        jQuery.each(storage, function() {
            if (this.id == infoId) {
                this.infoWindow.open(km.googleMap);

                google.maps.event.addListener(this.infoWindow, 'closeclick', function(){

                    window.setTimeout(function() {
                       km.poly.unhighlight();
                    }, 250);
                });

            } else {
                this.infoWindow.close();
            }
        });
    };
    
    var closeAll = function() {
        
        jQuery.each(storage, function() {
            if (isOpen(this.id)) {
                this.infoWindow.close();
            }
        });
        
        km.poly.unhighlight();
    };

    var inStorage = function(uid) {
        return jQuery.grep(storage, function(element){ return (element.id == uid); }).length;
    };

    var getByUid = function(uid) {

        var ret = false;

        jQuery(storage).each(function() {
            if(this.id == uid) {
                ret = this;
            }
        });

        return ret;
    };

    var push = function(o) {
        storage.push(o);
    };

    var load = function(uid) {

        if (!inStorage(uid)) {

            jQuery.ajax({
                url: document.location.href + '?type=3001&bezirk=' + uid,
                async: false,
                success: function(response) {
                    var responseHTML = jQuery.parseHTML(response.trim()),
                        infoPosition = jQuery(responseHTML).data('position');

                    if (typeof infoPosition != 'null') {
                        infoPosition = infoPosition.split(',',2);

                        push({
                            id: uid,
                            infoWindow: new google.maps.InfoWindow({
                                content: response,
                                position: new google.maps.LatLng(parseFloat(infoPosition[0]),parseFloat(infoPosition[1])),
                                maxWidth: 380
                            })
                        });
                    }
                }
            });
        }
    };

    var isOpen = function(uid) {

        var obj = getByUid(uid);

        if(obj !== false) {
            return (obj.infoWindow.getMap() !== null);
        }

        return false;
    };

    return {
        push : push,
        show : show,
        closeAll : closeAll,
        load : load,
        getByUid : getByUid,
        isOpen : isOpen,
        inStorage : inStorage,
        getStorage : function() { return storage; }
    };
}());

jQuery(document).ready(function() {

    km.init();
});