var mbxDropDownDefaults = {
    labelType : 'insert',
    labels : {
        noSelection : 'egal'
    }
};

var colorboxArgs = {
    current : 'Bild {current} von {total}',
    previous : 'zur&uuml;ck',
    next : 'weiter'
};

var howImmo = {

    options : {
        environmentTypes : ["school", "doctor", "food", "cafe", "bakery", "gym"],
        environmentRadius : 1000
    },

    init : function(opts) {

        this.options = $.extend(true, {}, this.options, opts || { });

        var initFuncs = {};

        initFuncs.initResultsListAccordion = function()  {

            $('#immo-results .nav-tabs a').click(function (e) {
                e.preventDefault();
                $(this).tab('show');

                // initialize google map when accessing tab
                if ($(this).hasClass('icon-result-map')) {
                    initFuncs.initResultsListMap();
                }
            });
        };

        initFuncs.initResultsListMap = function(gMap3Customs) {

            var $container = $('#immo-map > .results-list-header:first-child');

            // only init map once
            if($container.find('.gmap').length > 0) {
                return;
            }

            var mapContainer = $('<div/>').addClass('gmap'),
                immoMarkers = mbx_realestate.tools.map.getImmoMarkers(immoobjects),
                boundaries = mbx_realestate.tools.map.getBoundariesByObjects(immoMarkers),
                immoCenter = mbx_realestate.tools.map.getCenterOfBoundary(boundaries)
                ;

            $container.append(mapContainer);

            // you can override/append this default options by using gMap3Customs when calling this metod
            var gMap3Defaults = {
                map: {
                    options: {
                        center: [immoCenter.lat, immoCenter.lng],
                        zoom: 14,
                        mapTypeId: google.maps.MapTypeId.ROADMAP
                    },
                    callback : function() {

                        var map = mapContainer.gmap3('get');

                        if(typeof mapContainer.data('immoobjects') === 'undefined') {
                            mapContainer.data('immoobjects', immoMarkers);
                        }
                        if(typeof mapContainer.data('markers') === 'undefined') {
                            mapContainer.data('markers', []);
                        }

                        // display immo objects
                        mapContainer.gmap3({

                            marker : {
                                values : immoMarkers,
                                options : {
                                    icon : new google.maps.MarkerImage('https://' + document.location.host + '/fileadmin/default/images/map-icon.php?type=immo',new google.maps.Size(46, 44, "px", "px"))
                                },
                                events:{
                                  click: function(marker, event, context) {
                                        var map = $(this).gmap3("get"),
                                            infowindow = $(this).gmap3({get: {name: "infowindow"}}),
                                            immoId = parseInt(context.data.general.immo);
                                    
                                        if (infowindow) {
                                            infowindow.close();
                                            infowindow.open(map, marker);
                                            infowindow.setContent(howImmo.tools.map.parseImmoOverlayContent(context.data));
                                        } else {
                                            $(this).gmap3({
                                                infowindow: {
                                                    anchor: marker,
                                                    options: {
                                                        content: howImmo.tools.map.parseImmoOverlayContent(context.data)
                                                    }
                                                }
                                            });
                                        }
                                            
                                        window.setTimeout(function() {

                                            $('.gmap .notelist-action').click(function() {
                                            
                                                var star = $(this).find('>*'),
                                                    action = (star.hasClass('add') ? 'add' : 'drop');
                                            
                                                var updateJsImmoobjectNotepad = function(state) {
                                                    
                                                    $(immoobjects).each(function() {
                                                        if(parseInt(this.general.immo) === immoId) {
                                                            this.notepad = state;
                                                        }
                                                    });
                                                };
                                            
                                                $.ajax({
                                                    async: 'true',
                                                    url: 'index.php',
                                                    type: 'POST',
                                                    data: {
                                                        eID: "ajaxDispatcher",
                                                        request: {
                                                            pluginName : 'mbx_realestate',
                                                            controller : 'Cookie',
                                                            action : action + 'Cookie',
                                                            arguments: {
                                                                cookie : 'notepad',
                                                                value : immoId
                                                            }
                                                        }
                                                    },
                                                    dataType: "json",
                                                    success: function(result) {
                                                        try {
                                                            
                                                            var isInNotepad = false;
                                                            
                                                            $(result).each(function() {
                                                                if(parseInt(this) === immoId) {
                                                                    isInNotepad = true;
                                                                }
                                                            });
                                                            
                                                            if(isInNotepad === true) {
                                                                star.addClass('remove').removeClass('add');
                                                                updateJsImmoobjectNotepad(true);
                                                            } else {
                                                                star.addClass('add').removeClass('remove');
                                                                updateJsImmoobjectNotepad(false);
                                                            }
                                                            
                                                            $('.notepad-count').text(result.length);
                                                            
                                                        } catch(e) { }
                                                    }
                                                });
                                            });
                                        },500);
                                    }
                                }
                            }
                        });

                        var mapEvents = ['zoom_changed', 'dragend'];

                        for(var mapEventKey in mapEvents) {

                            google.maps.event.addDomListener(map,mapEvents[mapEventKey], function() {

                                var zoom = map.getZoom(),
                                    center = map.getCenter(),
                                    distance = 500;

                                if(zoom <= 14) {

                                    $('.tx-mbx-realestate #immo-poi-filter .pointer-mask').fadeIn();

                                    mbx_realestate.tools.map.toggleMarkerVisibility(mapContainer.data('markers'), map, false);

                                } else if(mbx_realestate.tools.map.immosInRange(mapContainer, distance)) {

                                    $('.tx-mbx-realestate #immo-poi-filter .pointer-mask').fadeOut();

                                    mbx_realestate.tools.map.toggleMarkerVisibility(mapContainer.data('markers'), map, true);

                                    var request = {
                                        location : center,
                                        radius : distance,
                                        types : howImmo.options.environmentTypes
                                    };

                                    var service = new google.maps.places.PlacesService(map);

                                    service.search(request, function(results, status, pagination) {

                                        if (status === google.maps.places.PlacesServiceStatus.OK) {
                                            var markers = mapContainer.data('markers');
                                            for (var i = 0; i < results.length; i++) {

                                                var place = results[i],
                                                    pTypes = place.types,
                                                    iconType = null,
                                                    skip = false;

                                                // check if marker was already fetched
                                                for(var a in markers) {
                                                    if(markers[a].id === place.id) {
                                                        skip = true;
                                                        break;
                                                    }
                                                }

                                                // skip item because already fetched
                                                if(skip === true) {
                                                    continue;
                                                }

                                                for(var a in howImmo.options.environmentTypes) {
                                                    if(pTypes.indexOf(howImmo.options.environmentTypes[a]) > -1) {
                                                        iconType = howImmo.options.environmentTypes[a];
                                                        break;
                                                    }
                                                }

                                                if(iconType === null) {
                                                    continue;
                                                }

                                                var marker = new google.maps.Marker({
                                                    map : map,
                                                    position : place.geometry.location,
                                                    icon : new google.maps.MarkerImage('https://' + document.location.host + '/fileadmin/default/images/map-icon.php?type=' + iconType + '&cluster=1',new google.maps.Size(32, 32, "px", "px"))
                                                });

                                                markers.push({
                                                    id : place.id,
                                                    type : iconType,
                                                    marker : marker
                                                });

                                                mbx_realestate.tools.map.updatePOIMarkers(mapContainer, $('#immo-poi-filter li'));
                                            }
                                            mapContainer.data('markers', markers);

                                            if (typeof pagination.hasNextPage === 'boolean' && pagination.hasNextPage === true) {
                                                window.setTimeout(function() {
                                                    pagination.nextPage();
                                                }, 2000);
                                            }

                                            var copyrightCounter = 0;

                                            $('.gmap').find('.gm-style-cc').each(function() {

                                                var href = $(this).find('a').attr('href') || '';
                                                if (href.match(/gelbeseiten/) && copyrightCounter++ > 0) {
                                                    $(this).remove();
                                                }
                                            });

                                        }
                                    }, map);

                                }
                            });

                        }



                    }
                }
            };

            var gMap3Opts = $.extend(true, {}, gMap3Defaults, gMap3Customs || {});

            mapContainer.gmap3(gMap3Opts);
            mbx_realestate.tools.map.relateGMapPOIFilter(mapContainer, $('#immo-poi-filter li'));
        };

        initFuncs.initDetailGallery = function() {

            // bind the colorbox
            jQuery('#realestate-detailview .immo-media-container a[data-rel="img2"]').colorbox(colorboxArgs);

            var $gallery = jQuery('#realestate-detailview .immo-media-container a[data-rel="img"]').colorbox(colorboxArgs);

            $('#realestate-detailview .immo-media-container .extend').click(function(e) {
                e.preventDefault();
                $gallery.eq(0).click();
            });

            var $listContainer = $('#realestate-detailview .immo-media-container .image-container ul'),
                $pager = $('#realestate-detailview .immo-media-container .image-pager'),
                pagerText = $pager.find('.image-pagination strong').html();

            // remember the original image index for each item because they will be moved while using caroufredsel
            $listContainer.find('>li').each(function(i) {
                $(this).data('imagecycle', i+1);
            });

            // bind the caroufredsel
            $listContainer.caroufredsel({
                auto : {
                    play : false
                },
                scroll: {
                    duration: 1000,
                    onBefore : function(data) {

                        var $currentItem = data.items.visible;

                        // 'Image x of y'
                        $pager.find('.image-pagination strong').html(pagerText.replace(/^([^0-9]*)\d+(.*?)$/, '$1' + $currentItem.data('imagecycle') + '$2'));
                        $pager.find('.image-title strong').text($currentItem.find('img').attr('title') || '');
                    }
                },
                prev: {
                    button: $pager.find('.previous'),
                    key: 'left'
                },
                next: {
                    button: $pager.find('.next'),
                    key: 'right'
                }
            });
        };

        initFuncs.initFloorplanOverlay = function() {

            $('#floorplan-image').closest('a').colorbox(colorboxArgs);
        };

        initFuncs.initGMapDetailview = function() {
            var PlaceId = ['0'];
            var marker = false;
            var $container = $('.immo-address-map-wrapper'),
                createdMap,
                // the callback when the google API finished the places search
                onPOISearchReady = function(results, status) {

                    if(typeof $gmap3Container.data('markers') === 'undefined') {
                        $gmap3Container.data('markers', []);
                    }

                    if (status === google.maps.places.PlacesServiceStatus.OK) {
                        for (var i = 0; i < results.length; i++) {

                            var place = results[i],
                                pTypes = place.types,
                                iconType = null;

                            if (PlaceId.indexOf(place.id) > -1) {
                                marker= false;
                            }
                            else {
                                marker = true;
                                PlaceId.push(place.id);
                            }

                            if(marker === true) {
                                for(var a in howImmo.options.environmentTypes) {
                                    for(var n in pTypes) {

                                        var pType = pTypes[n];

                                        if(pType === howImmo.options.environmentTypes[a]) {
                                            iconType = howImmo.options.environmentTypes[a];
                                            break;
                                        }
                                    }

                                }

                                if(iconType === null) {
                                    continue;
                                }

                                var marker = new google.maps.Marker({
                                    map : createdMap,
                                    position : place.geometry.location,
                                    icon : new google.maps.MarkerImage('https://' + document.location.host + '/fileadmin/default/images/map-icon.php?type=' + iconType + '&cluster=1',new google.maps.Size(32, 32, "px", "px"))
                                });

                                var markers = $gmap3Container.data('markers');
                                markers.push({
                                    type : iconType,
                                    marker : marker
                                });
                                $gmap3Container.data('markers', markers);
                            }

                        }

                        $('#immo-poi-filter li input').attr('checked', 'checked').prop('checked', true);

                        mbx_realestate.tools.map.updatePOIMarkers($gmap3Container, $('#immo-poi-filter li'));
                    }
                },
                // the callback when the gmap3 has finished loading
                onMapCreate = function(map) {

                    for(var a in howImmo.options.environmentTypes) {

                        createdMap = map;

                        var latLng = new google.maps.LatLng(map.getCenter().lat(), map.getCenter().lng());
                        var request = {
                            location: latLng,
                            radius: howImmo.options.environmentRadius,
                            type : howImmo.options.environmentTypes[a]
                        };

                        var service = new google.maps.places.PlacesService(map);

                        service.search(request, onPOISearchReady, map);

                    }
                };

            var $gmap3Container = howImmo.replaceStaticMap($container, {

                map: {
                    callback : onMapCreate
                },
                marker: {
                    options: {
                        icon: new google.maps.MarkerImage('https://' + document.location.host + '/fileadmin/default/images/map-icon.php?type=immo',new google.maps.Size(46, 44, "px", "px"))
                    }
                }
            });

            mbx_realestate.tools.map.relateGMapPOIFilter($gmap3Container, $('#immo-poi-filter li'));
        };

        initFuncs.initFurnishTool = function() {

            var iOS = ( navigator.userAgent.match(/(iPad|iPhone|iPod)/g) ? true : false );

            if (iOS) {
                $('#realestate-detailview .icon-interior').hide();
            }

            $('#realestate-detailview .icon-interior').click(function(e) {

                e.preventDefault();

                var immoIdData = $('#realestate-detailview .apartDetail').attr('id').match(/immo\-id\-(\d+)\-(\d+)/);

                if(immoIdData !== null) {

                    var wi = immoIdData[1],
                        me = immoIdData[2];

                    howImmo.tools.furnishTool.run(
                            { wi : wi, me : me},
                            $('.basedata-type-rooms').text() + ' Zimmer / ca. ' + $('.basedata-type-area').text().split(/\s+/)[0] + ' qm',
                            null,
                            $('h1').text().split(/[\|\,]/)[1],
                            $('h1').text().split(/[\|\,]/)[2],
                            $('.contacts .icon-phone').text(),
                            $('.contacts .icon-email').text()
                    );
                }

                return false;
            });
        };

        initFuncs.initStreetSearch = function() {
            if(typeof re_streets === 'undefined') {
                return;
            }

            var streets = [];
            for(var i in re_streets) {
                streets.push(re_streets[i]);
            }
            streets.sort();

            $('#realestate-address-search input').autocomplete({
                source: function (request, response) {
                    var term = $.ui.autocomplete.escapeRegex(request.term)
                        , startsWithMatcher = new RegExp("^" + term, "i")
                        , startsWith = $.grep(streets, function(value) {
                            return startsWithMatcher.test(value.label || value.value || value);
                        })
                        , containsMatcher = new RegExp(term, "i")
                        , contains = $.grep(streets, function (value) {
                            return $.inArray(value, startsWith) < 0 &&
                                containsMatcher.test(value.label || value.value || value);
                        });

                    response(startsWith.concat(contains));
                },
                minLength: 3,
                appendTo : '#realestate-address-search',
                response: function( event, ui ) {
                    if (ui.content.length > 0 || $.inArray($(this).val(), streets) > 0) {
                        $(this).closest('form').find('input[type=submit]').prop('disabled', false);
                    } else {
                        $(this).closest('form').find('input[type=submit]').prop('disabled', true);
                    }
                }
            });

        };

        initFuncs.initContactsMap = function() {
            if (typeof immocontacts === 'undefined') {
                return;
            }

            $('#contactMap').gmap3({
                marker: {
                    values: immocontacts,
                    options: {
                        draggable: false,
                        icon: new google.maps.MarkerImage('https://' + document.location.host + '/fileadmin/default/images/map-icon.php?type=immo',new google.maps.Size(46, 44, "px", "px"))
                    },
                    events: {
                        click: function(marker, event, context) {

                            var map        = $(this).gmap3('get'),
                                infowindow = $(this).gmap3({get: {name: 'infowindow'}}),
                                dataBuild  = '<div class="contactInfobox clearfix">'
                                           +    '<p class="h4 spacer-small">' + context.data.name + '</p>'
                                           +    '<a href="https://www.google.de/maps/dir//' + context.data.address + '" target="_blank">Route berechnen</a>'
                                           + '</div>';

                            if (infowindow) {
                                infowindow.close();
                                infowindow.open(map, marker);
                                infowindow.setContent(dataBuild);
                            } else {
                                $(this).gmap3({
                                    infowindow: {
                                        anchor: marker,
                                        options: {content: dataBuild}
                                    }
                                });
                            }
                        }
                    }
                },
                map: {
                    options: {
                        center: [52.5585,13.508],
                        zoom: 11,
                        mapTypeControl: false,
                        streetViewControl: false
                    }
                },
                autofit:{}
            });
        };

        if($('#realestate-list').length) {

            initFuncs.initResultsListAccordion();

            var li = $('<li/>').addClass('pointer-mask').html('<p>Karte zoomen um Informationen einzublenden</p>');

            $('#immo-poi-filter').append(li);
        }

        if($('#realestate-detailview').length) {

            initFuncs.initDetailGallery();
            initFuncs.initFloorplanOverlay();
            initFuncs.initGMapDetailview();
            initFuncs.initFurnishTool();
        }

        if ($('#realestate-address-search').length) {
            initFuncs.initStreetSearch();
        }

        initFuncs.initContactsMap();
    },
    replaceStaticMap : function($container, gMap3Customs) {

        var img     = $container.find('>img').clone(),
            div     = $('<div/>'),                                         // the new container to wrap the map in
            href    = img.attr('src') || '',                               // gain image source of static map
            parts   = href.match(/^.*?center=([^&zoom].*)&zoom=(\d+).*$/), // extract 'center' and 'zoom' level of static map
            address = parts ? parts[1] : null,                             // extract address from static map
            zoom    = parseInt(parts ? parts[2] : 0);                      // extract zoom level from static map

        if (typeof address === 'string') {

            $container.append(div).find('>img').hide();

            // you can override/append this default options by using gMap3Customs when calling this metod
            var gMap3Defaults = {
                map: {
                    address: address,
                    options: {
                        zoom: zoom,
                        mapTypeId: google.maps.MapTypeId.ROADMAP
                    }
                },
                marker: {
                    options: {
                        icon : new google.maps.MarkerImage('https://' + document.location.host + '/fileadmin/default/images/map-icon.php', new google.maps.Size(46, 44, "px", "px"))
                    }
                }
            };

            // check if we have a specific address or geodata to set the marker
            // geodata needs to be set as values-property to set the location correctly
            var latlng = address.match(/^(\d+\.\d+)[\,\s]+(\d+\.\d+)$/);

            if(latlng === null) {
                gMap3Defaults.marker.address = address;
            } else {
                gMap3Defaults.marker.values = [{
                    latLng : [latlng[1], latlng[2]]
                }];
            }

            var gMap3Opts = $.extend(true, {}, gMap3Defaults, gMap3Customs || {});

            div.gmap3(gMap3Opts);

            return div;
        }
    }
    ,
    tools : {

        map : {

            parseImmoOverlayContent : function(immoobject) {

                var address = immoobject.address,
                    title   = immoobject.general.rooms + ' Zimmer | ' + address.street,
                    img     = immoobject.img;

                return '<div class="immo-information mini clearfix">'
                        + ' <div class="immoresult-previmg">'
                        + (typeof img !== 'null'
                            ? ' <img src="' + img + '" alt="' + title + '" width="77" height="77" />'
                            : ''
                          )
                        + '    <div class="notelist-action"><div class="' + (immoobject.notepad ? 'remove' : 'add') + '"></div></div>'
                        + ' </div>'
                        + ' <div class="immo-data">'
                        + '     <a href="' + immoobject.general.url + '" title="' + title + '">'
                        + '         <div class="district">' + address.district + '</div>'
                        + '         <div class="address darkblue">'
                        + '             <strong>' + address.street + ',</strong>'
                        + '             <span class="city">' + address.zip + ' ' + address.city + '</span>'
                        + '         </div>'
                        + '         <div class="immo-basedata clearfix">'
                        + '             <ul>'
                        + '                 <li>'
                        + '                     <p class="darkblue">Warmmiete:</p>'
                        + '                     <p class="darkblue">' + immoobject.general.costGross.formatEuro() + ' &euro;</p>'
                        + '                 </li>'
                        + '                 <li>'
                        + '                     <p>Größe:</p>'
                        + '                     <p>' + immoobject.general.areaGeneral + ' m<sup>2</sup></p>'
                        + '                 </li>'
                        + '                 <li>'
                        + '                     <p>Zimmer:</p>'
                        + '                     <p>' + immoobject.general.rooms + '</p>'
                        + '                 </li>'
                        + '             </ul>'
                        + '         </div>'
                        + '         <div class="continue"></div>'
                        + '     </a>'
                        + ' </div>'
                        + '</div>';
            }
        },

        furnishTool : {

            options : {
                 tool_url : '/fileadmin/default/furnish-tool/index.php'
            },

            run : function(immoData, f1, f2, f3, f4, f5, mail) {

                var $this = howImmo.tools.furnishTool;

                var tool_url = $this.options.tool_url + '?name=' + $this.strPad(immoData.wi, 5, '0') + $this.strPad(immoData.me, 3, '0')
                             + '&f1=' + $this.escape2(f1)
                             + '&f2=' + $this.escape2(f2 || '(WE-Nr. ' + immoData.wi +'.' + $this.strPad(immoData.me, 3, '0')  + ')')
                             + '&f3=' + $this.escape2(f3)
                             + '&f4=' + $this.escape2(f4)
                             + '&f5=' + $this.escape2(f5)
                             + '&mail=' + $this.escape2(mail);

                if(typeof immoData.image === 'string') {
                    tool_url += '&image=' + immoData.image;
                }

                var windowH = $(window).height() - 80;
                var windowB = parseInt(windowH * 590 / 638);

                $.colorbox({
                    iframe      : true,
                    href        : tool_url,
                    escKey      : false,
                    overlayClose: false,
                    innerWidth  : windowB,
                    innerHeight : windowH
                });
            },
            strPad : function(input, length, string) {
                string = string || '0'; input = input + '';
                return input.length >= length ? input : new Array(length - input.length + 1).join(string) + input;
            },
            escape2: function(str) {

                if(typeof str === 'undefined') {
                    return '';
                } else {

                    var i, code, esc = [];
                    for (i = 0; i < str.length; i++) {
                        code = parseInt(str.charCodeAt(i));
                        if ((code > 41 && code < 58 && code !== 44) || (code > 63 && code < 91) || (code > 94 && code < 123 && code !== 96)) {
                            esc[i] = str.charAt(i);
                        } else {
                            if (code < 0)
                                code += 256;
                            esc[i] = (code < 16 ? "%0" : "%") + code.toString(16);
                        }
                    }
                    return esc.join("");
                }
            }
        }
    }
};

jQuery(window).ready(function() {

    if (!("ontouchstart" in document.documentElement)) {
        document.documentElement.className += " no-touch";
    }

    jQuery('.tx-mbx-realestate input[type="radio"], .tx-mbx-realestate input[type="checkbox"]').iCheck({
        checkboxClass: 'icheckbox_minimal-blue',
        radioClass: 'iradio_minimal-blue',
        increaseArea: '20%'
    });

    jQuery('.mbx-dropdown-auto').dropDownMenu($.extend({}, mbxDropDownDefaults, { }));

    jQuery('#dropdown-districts').dropDownMenu($.extend({}, mbxDropDownDefaults, { 
        labelType : 'remove'
    }));

    jQuery('#dropdown-year-build').dropDownMenu($.extend({}, mbxDropDownDefaults, {
        multiple : false
    }));

    jQuery('.dropdown-sort-by').dropDownMenu($.extend({}, mbxDropDownDefaults, {
        labelType : 'keep'
    }));

    jQuery('.dropdown-per-page').dropDownMenu($.extend({}, mbxDropDownDefaults, {
        labelType : 'remove',
        hooks : {
            onUpdateLabel : function() {

                var checkedInputs = this.find('.mbx-dropdown-list input').filter(function() {
                    return $(this).prop('checked');
                });

                return jQuery(checkedInputs[0]).closest('li').text() + ' ' + this.data('title');
            }
        }
    }));

    jQuery('.dropdown-sort-by, .dropdown-per-page').each(function() {

        var $form = $($(this).find('form').get(0) || $(this).closest('form').get(0));

        $(this).change(function() {
            $form.trigger('submit');
        });

        $form.find('input[type="submit"]').hide();
    });

    // avoid opacity on list-items which have no preview thumb images
    $('#realestate-list .realestate-list-item').each(function() {

        if($(this).find('.preview-thumbs div').length > 0) {
            $(this).find('.immoresult_previmg').addClass('has-preview-thumbs');
        }
    });

    $('.icon-expandable').click(function() {

        if($(this).find('+*').is(':hidden')) {
            $(this).find('+*').slideDown();
        } else {
            $(this).find('+*').slideUp();
        }

    }).css({
        cursor : 'pointer'
    }).trigger('click');

    howImmo.init();
});

Number.prototype.formatEuro = function(){
var n = this,
    c = 2,
    d = ',',
    t = '.',
    s = n < 0 ? "-" : "",
    i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "",
    j = (j = i.length) > 3 ? j % 3 : 0;
   return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
 };

jQuery(window).ready(function() {

    (function($form) {

        if($form.length === 0) {
            return;
        }

        var $neighborhoodAll = $form.find('#neighborhood_all');
        var $neighborhoods = $form.find('input[type="checkbox"]').filter(function() {

            return ($(this).attr('id') || '').match(/^neighborhood_\d+/);
        });

        var testAllSelection = function() {

            if($neighborhoodAll.is(':checked')) {
                $neighborhoods.prop('checked', true).attr('checked', 'checked').iCheck('update');
            } else {
                $neighborhoods.prop('checked', false).removeAttr('checked').iCheck('update');
            }
        };

        var testSingleSelection = function() {

            window.setTimeout(function() {

                var lenChecked = $neighborhoods.filter(function() {
                    return $(this).is(':checked');
                }).length;

                if(lenChecked === $neighborhoods.length) {
                    $neighborhoodAll.prop('checked', true).attr('checked', 'checked').iCheck('update');
                } else if($neighborhoodAll.is(':checked')) {
                    $neighborhoodAll.prop('checked', false).removeAttr('checked').iCheck('update');
                }

            }, 500);
        };

        $neighborhoodAll.on('change ifChanged', testAllSelection);
        $neighborhoods.on('change ifChanged', testSingleSelection);


        var $blockFirstRenter = $form.find('#block-first-renter');
        var $blockSecondRenter = $form.find('#block-second-renter');
        var $checkboxSecondRenter = $form.find('#check-second-renter');
        var $checkboxWbs = $form.find('#wbs');


        var checkboxUpdate = function(checkbox, block) {

            var updateVisibility = function() {

                var visible = checkbox.is(':checked');

                if(visible) {
                    block.show();
                } else {
                    block.hide();
                }
            };

            checkbox.on('change ifChanged', updateVisibility);

            updateVisibility();
        };

        checkboxUpdate($checkboxSecondRenter, $blockSecondRenter);
        checkboxUpdate($checkboxWbs, $form.find('#block-wbs'));

        $('#wbs-no').on('ifChecked', function () { $('#block-wbs').hide() });
        $('#wbs-yes').on('ifChecked', function () { $('#block-wbs').show() });

        // mark the mandatory fields in the block of the second renter information as set in the first renter block

        $blockFirstRenter.find('label').each(function() {

            if(!$(this).text().match(/\*$/)) {
                return;
            }

            var id = $(this).attr('for') || '';
            var idSecondRenter = id.replace(/^first/, 'second');
            var $labelSecondRenter = $form.find('label[for="' + idSecondRenter + '"]');

            // avoid double "*" if the mandatory marks are set by formhandler validation by default
            if($labelSecondRenter.text().match(/\*$/)) {
                return;
            }

            $labelSecondRenter.text($labelSecondRenter.text() + ' *');
        });

        $form.find('#block-wbs label').each(function() {

            var $label = $(this),
                txt = $label.text();

            if(txt.match(/\*$/)) {
                return;
            }

            $label.text(txt + ' *');
        });

    })($('#search-flat-form'));
});