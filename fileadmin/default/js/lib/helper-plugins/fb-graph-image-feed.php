<?php

$idAlbum = filter_input(INPUT_GET, 'albumId');

if(preg_match('/^\d+$/', $idAlbum)) {
    
    $referer = filter_input(INPUT_SERVER, 'HTTP_REFERER');
    $host = filter_input(INPUT_SERVER, 'HTTP_HOST');
    
    $replacements = array('-' => '\-', '.' => '\.');
    
    // verify referer host match
    if(preg_match('/^https?\:\/\/' . str_replace(array_keys($replacements), $replacements, $host) . '/', $referer)) {
        
        header('content-type: text/json');
        echo file_get_contents('https://graph.facebook.com/' . $idAlbum . '/photos/?fields=picture,source&limit=100&access_token=174986502555567|Mi_QSQFqpNHP38Z08T9vDyj5zKA');
    }
}
