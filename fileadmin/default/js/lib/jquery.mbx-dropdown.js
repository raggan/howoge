(function( $ ){
    var methods = {
        init : function(newoptions) {
            return this.each(function(){

                var $this = $(this),
                    isSelect = $this.find('select').length === 1,
                    multiple = (isSelect
                                 ? ($this.find('select').attr('multiple') === 'multiple')       // check state from <select>
                                 : $this.find('ul input[type="checkbox"]').length > 0);         // check state from <input>'s

                 if($this.hasClass('mbx-dropdown-wrapper')) {
                     return;
                 }

                var options = {
                            opened: false,
                            label: '>label',
                            hide: null,
                            hideEmptyValues : false,        // hides the <li> that contains options without any values
                            multiple: multiple,             // user can select multiple items
                            supportReset : false,           // reset-<li> will prepend to option list (will uncheck all options)
                            removeable: false,              // every selected item can be dropped within the dropdown-label-box
                            titleItemClass: 'title-item',
                            labelType: 'keep', // insert|keep|remove
                            templates: {
                                dropdown: '<div class="mbx-dropdown">'
                                        + '<div class="mbx-dropdown-label"><div></div></div>'
                                        + '<div class="mbx-dropdown-list">'
                                        + '</div>',
                                removeItem: '<span class="remove">x</span>', // will be partial of %titleItemTemplate->remove%
                                icon: '<span></span>',
                                noSelection: '<span class="no-selection">%title%</span>',
                                titleItem: '<span class="%titleitemclass%">%item%%remove%</span>', // will be a list in %labelTemplate->values%
                                label: '<span class="dropdown-title">%title%</span>'
                                        + '<span class="value-list"></span>'
                                        + '<span class="dropdown-icon">%dropdownicon%</span>'
                            },
                            labels: {
                                noSelection : 'no selection'
                            },
                            hooks: {
                                onUpdateLabel: null
                            }
                        };

                $.extend(options, newoptions);

                $this.addClass('mbx-dropdown-wrapper');

                if(options.removeable === true) {
                    $this.addClass('removeable');
                } else if($this.hasClass('removeable')) {
                    options.removeable = true;
                }

                if(options.supportReset === true) {
                    $this.addClass('support-reset');
                } else if($this.hasClass('support-reset')) {
                    options.supportReset = true;
                }
                
                if(options.hideEmptyValues === true) {
                    $this.addClass('hide-empty-values');
                } else if($this.hasClass('hide-empty-values')) {
                     options.hideEmptyValues = true;
                 }

                $this.data('opts', options);
                $this.data('label', $this.find(options.label));
                $this.data('title', $this.find(options.label).html() || '');
                $this.data('obj', $this);

                $this.dropDownMenu('transposeSelectToInputs');
                
                var dropDown = $(options.templates.dropdown),
                    origUl = $this.find('ul'),
                    ul = origUl.clone();
                
                ul.find('input').hide();
                
                if(isSelect) {
                
                    $this.find('select').after(dropDown);
                    
                } else {
                    
                    $this.append(dropDown);
                }
                
                dropDown.find('.mbx-dropdown-list').append(ul);
                
                origUl.remove();
                
                $this.data('dropdown', dropDown);
                
                switch(options.labelType) {
                    case 'remove' :
                    case 'insert' :
                        $this.find(options.label).remove();
                        break;
                }
                

                dropDown.find('.mbx-dropdown-label').click(function(e) {
                    
                    // dont open dropdown when clicked a remove element
                    if($(e.target).hasClass('remove')) {
                        return;
                    }
                    
                    dropDown.find('.mbx-dropdown-list').slideToggle('slow', function() { $this.dropDownMenu('changeDropDown'); });
                });

                $this.dropDownMenu('transposeSelectedStates');
                
                // embed 'reset' option
                if(options.supportReset === true) {
                    
                    var liReset = $('<li/>').addClass('reset-options');
                    
                    liReset.append($('<label/>').text(options.resetLabel ? options.resetLabel : options.labels.noSelection ));
                    liReset.click(function() {
                        ul.find('li').trigger('unselect');
                    });
                    ul.prepend(liReset);
                }
                
                // hide the inputs/options that has empty values
                if(options.hideEmptyValues === true) {
                    
                    ul.find('li').each(function() {

                        var input = $(this).find('input').filter(function(i,o) {

                            return (options.multiple === false || /\[\]$/.test($(o).attr('name')));
                        });
                        
                        if((isSelect && input.attr('rel') === '')       // is select and the related <option> has no value
                        || (input.length && input.val() === '')) {      // input found and has no value
                            $(this).hide();
                        }
                    });
                }

                
                $this.dropDownMenu('bindDropDownItems');
                $this.dropDownMenu('changeDropDown');
                $this.dropDownMenu('preSelectCheckedFields');
                $this.dropDownMenu('updateLabel');
                $this.dropDownMenu('close');
                
                
                
                // hide options on escape key
                $(document).keyup(function(e) {                    
                    if (e.keyCode === 27) { $this.dropDownMenu('close'); }   // esc
                });
                  
            });
        },
        transposeSelectToInputs : function() {
            return this.each(function() {
                
                var $this = $(this),
                    opts = $this.data('opts'),
                    select = $this.find('select'),
                    hasSelect = select.length === 1,
                    inputType = opts.multiple ? 'checkbox' : 'radio',
                    name = ($this.find('select').attr('name') || '').match(/^([^[]*)(.*)$/);
                    
                if(hasSelect) {
                    
                    $this.data('originalselect', $(select));
                    select.data('mbxdropdown', $this).on('change', function() {
                        
                        $this.dropDownMenu('transposeSelectedStates');
                        $this.dropDownMenu('updateLabel');
                    });
                    
                    var ul = $('<ul/>');
                    
                    $this.find('select option').each(function() {
                        
                        var li = $('<li/>'),
                            id = 'mbx-dropdown-custom-item-' + (Math.random(0,100) *100 + 1).toString().replace('.', '');
                        
                        li.append('<input type="' + inputType + '" \n\
                                          id="' + id + '" \n\
                                          rel="' + $(this).val() + '" \n\
                                          name="mbxdropdown[' + name[1] + ']' + name[2] + '">'
                                 +'<label for="' + id + '">' + $(this).text() + '</label>');
                         
                        li.appendTo(ul);
                    });
                    
                    ul.appendTo($this);
                    
                    $(select).hide();
                }
            });
        },
        transposeSelectedStates : function() {
            return this.each(function() {
                
                var $this = $(this),
                    list = $this.find('.mbx-dropdown-list'),
                    hasSelect = $this.find('select').length === 1,
                    select = $($this.data('originalselect'));

                // reset all checked
                list.find('input').prop('checked', false);
                    
                if(hasSelect) {
                    
                    select.find(':selected').each(function() {
                        var listItem = list.find('input[rel="' + $(this).val() + '"]');
                        listItem.prop('checked', true);
                    });
                }
                
                $this.dropDownMenu('updateSelectionsLayout');
            });  
        },
        preSelectCheckedFields : function() {
            return this.each(function() {
                
                var $this = $(this),
                    list = $this.find('.mbx-dropdown-list'),
                    checkedInputs = list.find('input').filter(function() {
                        return $(this).prop('checked') && $(this).val() !== '';
                    });
                
                checkedInputs.each(function() {
                    $(this).closest('li').addClass('selected').trigger('select');
                });
                
                if(checkedInputs.length === 0){
                    list.find('li.selected').trigger('click');
                }
            });
        },
        bindDropDownItems : function() {
            return this.each(function() {
            
                var $this = $(this),
                    opts = $this.data('opts'),
                    optionList = $this.find('.mbx-dropdown-list'),
                    lis = optionList.find('li');
                
                lis.each(function() {
                    
                    $(this).find('label').click(function(e) {
                       e.preventDefault();
                       return;
                    });
                    
                    var input = $(this).find('input').filter(function(i,o) {

                        if(opts.multiple === false) {
                            return true;
                        } else {
                            return /\[\]$/.test($(o).attr('name'));
                        }
                    });
                    
                    if(input.length === 0) {
                        return;
                    }
                    
                    if(input.attr('checked') === 'checked') {
                        input.prop('checked', true);
                    }
                    
                    var clickFunc = function(e, element, select) {

                        e.stopPropagation();

                        if($(element).hasClass('no-selection')) {
                            return;
                        }

                        if(typeof select === 'boolean' && select === true) {
                            
                            input.prop('checked', true);
                            
                        } else if(typeof select === 'boolean' && select === false) {
                            
                            input.prop('checked', false);
                            
                        } else if(opts.multiple) {
                            
                            input.prop('checked', !input.prop('checked'));
                            
                        } else {
                            
                            input.prop('checked', true);
                        }

                        // set states to select if mbx-dropdown was generated by <select>
                        if(typeof $this.data('originalselect') === 'object') {
                            
                            lis.find('input').each(function() {
                                
                                $($this.data('originalselect')).find('option[value="' + $(this).attr('rel') + '"]').attr('selected', $(this).prop('checked'));
                            });
                            
                            $($this.data('originalselect')).trigger('change');
                        }

                        if(!opts.multiple) {

                            optionList.slideUp('slow', function() { $this.dropDownMenu('close'); });                            
                        }
                            
                        $this.dropDownMenu('updateSelectionsLayout');
                        $this.dropDownMenu('updateLabel');

                        input.trigger('change');
                    };
                    
                    $(this).bind('click', function(e) {
                        
                        e.stopPropagation();
                        clickFunc(e, this, null);
                        
                    }).bind('select', function(e) {
                        
                        e.stopPropagation();
                        clickFunc(e, this, true);
                        
                    }).bind('unselect', function(e) {
                        
                        e.stopPropagation();
                        clickFunc(e, this, false);
                        
                    });
                });
            });
        },
        updateLabel : function(newValue) {
            return this.each(function() {
                var $this = $(this),
                    opts = $this.data('opts'),
                    label = $this.find('.mbx-dropdown-label > div');
            
                var captionType = $this.data('opts').labelType,
                    caption = $this.data('title');

                var labels = [];

                // set custom value if 
                if(typeof opts.hooks.onUpdateLabel === 'function') {
                    
                    var result = opts.hooks.onUpdateLabel.apply($this, [{
                        newValue : newValue
                    }]);

                    if(result !== null) {

                        newValue = result;
                    }
                }

                if(typeof newValue === 'undefined') {
                
                    var checkedInputs = $this.find('.mbx-dropdown-list input').filter(function() {
                        return ($(this).prop('checked') || $(this).is(':checked'));
                    });
              
                    checkedInputs.each(function() {

                        var item,
                            title = $this.find('label[for="' + $(this).attr('id') + '"]').text(),
                            remove = null;

                        if(title.length > 0) {
                            
                            if(opts.removeable === true) {

                                remove = $(opts.templates.removeItem).attr({
                                    rel : $(this).attr('id')
                                }).click(function(e) {

                                    e.preventDefault();

                                    $(this).closest('.' + opts.titleItemClass).remove();
                                    $this.find('#' + $(this).attr('rel')).prop('checked', false);
                                    $this.dropDownMenu('updateSelectionsLayout');
                                    $this.dropDownMenu('updateLabel');
                                });
                            }

                            item = $(opts.templates.titleItem
                                        .replace('%titleitemclass%', opts.titleItemClass)
                                        .replace('%item%', title)
                                        .replace('%remove%', '<div class="_remove_"></div>')
                                        .replace(/\%([^\%]*)\%/gi, '')
                            );

                            if(remove === null) {
                                item.find('._remove_').remove();
                            } else {
                                item.find('._remove_').replaceWith($(remove));
                            }

                            labels.push(item);
                        }

                    });
                }

                label.html(
                    $this.data('opts').templates.label
                        .replace('%title%', captionType === 'insert' ? caption : '')    // display the list caption
                        .replace('%dropdownicon%', opts.templates.icon)                   // 
                        .replace(/\%([^\%]*)\%/gi, '')                                  // remove all none replaced markers
                );

                if(typeof newValue === 'undefined') {

                    label.find('.value-list').append(labels);
                    
                    if(labels.length === 0) {

                        label.find('.value-list').append($(opts.templates.noSelection.replace('%title%', opts.labels.noSelection)));
                        $this.find('.mbx-dropdown-list .reset-options').hide();
                        
                    } else {
                        
                        $this.find('.mbx-dropdown-list .reset-options').show();
                    }
                    
                } else if(typeof newValue === 'object') {
                    
                    label.find('.value-list').append(newValue);
                    
                } else if(typeof newValue === 'string') {
                    
                    label.find('.value-list').html(newValue);
                }

            });
        },
        updateSelectionsLayout : function() {
            return this.each(function() {
                var $this = $(this),
                    optionList = $this.find('.mbx-dropdown-list');
                
                optionList.find('input').each(function() {
                    
                    if($(this).prop('checked')) {
                        $(this).closest('li').addClass('selected');
                    } else {
                        $(this).closest('li').removeClass('selected');
                    }
                });
            });
        },
        changeDropDown : function() {
            return this.each(function() {
                var $this = $(this),
                    opts = $this.data('opts'),
                    optionList = $this.find('.mbx-dropdown-list');

                if(optionList.is(':hidden')) {
                    $this.removeClass('open');                    
                    $(document).unbind('click.dropdown_' + $this.data('dropdownid'));
                } else {
                    $this.addClass('open');
                    
                    $this.data('dropdownid', $('.mbx-dropdown-wrapper').length);
                    $(document).bind('click.dropdown_' + $this.data('dropdownid'), function() {
                        $('.mbx-dropdown-wrapper').dropDownMenu('close');
                    });

                    if(typeof opts.hooks === 'object' && typeof opts.hooks.onAfterOpen === 'object') {

                        var openHook = opts.hooks.onAfterOpen;
                        
                        if((typeof openHook.once === 'boolean' && openHook.once === false) || (typeof openHook.called === 'undefined' || typeof openHook.once === 'undefined')) {
                            openHook.func.apply( $this , openHook.options || []);
                            
                            $this.data('opts', jQuery.extend(true, { }, $this.data('opts'), {
                                hooks : { onAfterOpen : { called : true } }
                            }));
                        }
                    }
                }
            });
        },
        close : function() {
            return this.each(function() {

                var $this = $(this);
                
                $this.find('.mbx-dropdown-list').hide();
                $this.dropDownMenu('changeDropDown');
            });            
        }
        
    };

    $.fn.dropDownMenu = function( method ) {
    
        if ( methods[method] ) {
            return methods[method].apply( this, Array.prototype.slice.call( arguments, 1 ));
        } else if ( typeof method === 'object' || ! method ) {
            return methods.init.apply( this, arguments );
        } else {
            $.error( 'Method ' +  method + ' does not exist on jQuery.dropDownMenu' );
        }    

    };
})( jQuery );