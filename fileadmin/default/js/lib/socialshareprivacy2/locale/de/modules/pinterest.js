/*
 * jquery.socialshareprivacy.js | 2 Klicks fuer mehr Datenschutz
 *
 * Copyright (c) 2012 Mathias Panzenböck
 *
 * is released under the MIT License http://www.opensource.org/licenses/mit-license.php
 *
 * Spread the word, link to us if you can.
 */

(function(originalFunc) {
    
    jQuery.extend(jQuery.fn.socialSharePrivacy.settings.services.pinterest, {
        'txt_info' : 'Zwei Klicks f&uuml;r mehr Datenschutz: Erst wenn Sie hier klicken, wird der Button aktiv und Sie k&ouml;nnen Ihre Empfehlung an Pinterest senden. Schon beim Aktivieren werden Daten an Dritte &uuml;bertragen &ndash; siehe <em>i</em>.',
        'txt_off'  : 'nicht mit Pinterest verbunden',
        'txt_on'   : 'mit Pinterest verbunden',
        'button'   : function (options, uri, settings) {

            var $code = originalFunc(options, uri, settings);
            var minSize = 100;
            
            var clickableImages = $('.mainColumn img, .header-image-wrap.kiezmelder img, .mainColumn.span8 + .span4 img').filter(function() {
                return ($(this).is(':visible') 
                        && $(this).width() > minSize 
                        && $(this).height() > minSize
                    );
            });

            if(clickableImages.length > 0) {

                var $unclickAble = $('img').filter(function() {
                    
                    var $img = $(this);
                    var isNotClickable = true;
                    
                    $.each(clickableImages, function() {
                        var $clickable = $(this);
                        
                        if($clickable.get(0) == $img.get(0)) {
                            isNotClickable = false;
                        }
                    });
                    
                    return isNotClickable;
                });
                
                $unclickAble.attr('data-pin-nopin', 'any value');
            }
            
            $code.filter('a').attr({
                'data-pin-do' : 'buttonBookmark',
				href          : 'https://pinterest.com/pin/create/button/'
			});
            
            // copy the background image of the 'header-image-wrap'-container 
            // as new image-tag to make it fetchable for the instagram script
            if(typeof window.copiedTeaserImage === 'undefined') {
                
                var bgImage = ($('.header-image-wrap').css('background-image') || '');
                var regEx = bgImage.match(/^url\(\"(.*)\"\)$/);

                if (regEx) {
                    $('.header-image-wrap').prepend($('<img/>').attr({
                        src: regEx[1]
                    }).css({
                        position: 'absolute',
                        zIndex: -1
                    }));
                }
                
                window.copiedTeaserImage = true;
            }

            return $code;
        }
    });
    
})(jQuery.fn.socialSharePrivacy.settings.services.pinterest.button);
