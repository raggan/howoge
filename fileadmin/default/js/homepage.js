jQuery(document).ready(function($){
    var weatherWidget = $('#weather');

    var d = new Date();
    var h = d.getHours();
    var salutation = '';
    if(h <= 4) {
        salutation = 'Hute Nacht';
    } else if(h <= 10) {
        salutation = 'Guten Morgen'
    } else if(h <= 18) {
        salutation = 'Guten Tag';
    } else if(h <= 22) {
        salutation = 'Guten Abend';
    } else if(h <= 23) {
        salutation = 'Gute Nacht';
    }



    if ($.simpleWeather && weatherWidget.length) {
        $.simpleWeather({
            zipcode: '',
            woeid: '638242',
            unit: 'c',
            success: function(weather) {
                html = '<p><span class="h4 white">' + weather.city + '</span>';
                html += weather.temp + '<span class="deg">&deg;</span></p>';
                html += '<img src="' + weather.image + '" alt="' + weather.currently + '" class="img' + weather.code + '" />';
                weatherWidget.find('.smartweather').html(html);
                weatherWidget.find('.h1.white').text(salutation);
                weatherWidget.fadeIn();
            },
            error: function(error) {
                weatherWidget.html('<p>'+error+'</p>');
            }
        });

    }
});
