jQuery(document).ready(function($){
    var weatherWidget = $('#weather');

    if ($.simpleWeather && weatherWidget.length) {
        $.simpleWeather({
            zipcode: '',
            woeid: '638242',
            unit: 'c',
            success: function(weather) {
                html = '<p><span class="h4 white">' + weather.city + '</span>';
                html += weather.temp + '<span class="deg">&deg;</span></p>';
                html += '<img src="' + weather.image + '" alt="' + weather.currently + '" class="img' + weather.code + '" />';
                weatherWidget.find('.smartweather').html(html);
                weatherWidget.fadeIn();
            },
            error: function(error) {
                weatherWidget.html('<p>'+error+'</p>');
            }
        });

    }
});