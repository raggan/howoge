$( window ).load(function() {
    var map = new google.maps.Map(document.getElementById('googleMap'), {
        center: {lat: 52.531, lng: 13.381},
        scrollwheel: false,
        zoom: 11
    });

    var oms = new OverlappingMarkerSpiderfier(map, {
        markersWontMove: true,
        markersWontHide: true,
        basicFormatEvents: true
    });

    $.each( eventsForMap, function( key, value ) {
        addMarkerWithEvent(value,map,oms);
    });
});

function addMarkerWithEvent(event,map,oms) {
    //console.log("addMarkerWithEvent " + event.title)
    //console.log("coordinates: " + event.latlng)
    var latlng = event.latlng.split(";");
    var title = event.title.replace(/&quot;/g, '"');

    var marker = new google.maps.Marker({
        position: {lat: parseFloat(latlng[0]), lng: parseFloat(latlng[1])},
        map: map,
        icon: "https://www.howoge.de/fileadmin/default/images/map-imarker.png",
        title: title
    });

    var contentString = '<div class="info-wrap">'+
        '<div class="inline-block" id="image"><img style="width:90px;height:90px" src="' + event.imagePath + '"></div>'+
        '<div class="inline-block" id="text">'+
        '<div id="title"><b>' + event.title + '</b><br/><br/></div>'+
        '<div id="date">' + event.date + '<br/><br/></div>'+
        '<div id="description"><a href=' + event.link + '>Mehr erfahren</a></div>'+
        '</div>' +
        '</div>';

    var infowindow = new google.maps.InfoWindow({
        content: contentString
    });
    marker.addListener('spider_click', function() {
        infowindow.open(map, marker);
    });
    oms.addMarker(marker);
}