$(function() {
	var $mikoSlider = $('.miko-teaser-slider');
	if($mikoSlider.length > 0) {
		$mikoSlider.slick({
			arrows: false,
			dots: true,
			autoplay: true,
			autoplaySpeed: 5000
		});
	}

	var $mikoNewsSlider = $('.miko-news-slider');
	if($mikoNewsSlider.length > 0) {
		$mikoNewsSlider.slick({
			arrows: false,
			dots: true,
			autoplay: false,
			autoplaySpeed: 5000,
			infinite: false,
			slidesToShow: 3
		});
	}

    $('#content .miko-layer').each(function() {
    
        var $layer = $(this);
        var $overlay = $layer.prev('.miko-overlay');
        
        if($overlay.length === 1) {
            $('#content').after($overlay);
            $overlay.after($layer);
            
            return;
        }
        
        $layer.find('.iconfont-close.close').remove();
    });

	var $mikoLayer = $('.miko-layer');
	if($mikoLayer.length > 0) {
		var $mikoOverlay = $('.miko-overlay');
		$mikoLayer.on('click', '.close', function(e) {
			e.preventDefault();
			$mikoLayer.hide();
			$mikoOverlay.hide();
		});
	}
    
    $('#changed-email').each(function() {
    
        var changedEmailAddress = $(this).data('email') || ''; 
    
        if(/\@/.test(changedEmailAddress)) {
            $('#miko-current-email').text(changedEmailAddress);
        }
    });
    
    $('#miko-postalverification-success .btn').each(function() {
    
        var $btn = $(this);
    
        window.setTimeout(function() {
            document.location = $btn.attr('href');
        }, 5000);        
    });
    
    
    $('#realestate-detailview .cta-furnishing-tool').click(function(e) {

        e.preventDefault();

        var immoIdData = $('#realestate-detailview .apartDetail').attr('id').match(/immo\-id\-(\d+)\-(\d+)/);

        if(immoIdData !== null) {

            var wi = immoIdData[1],
                me = immoIdData[2];

            howImmo.tools.furnishTool.run(
                    { wi : wi, me : me, image : $(this).data('image') },
                    '', //$('.basedata-type-rooms').text() + ' Zimmer / ca. ' + $('.basedata-type-area').text().split(/\s+/)[0] + ' qm',
                    null,
                    '', //$('h1').text().split(/[\|\,]/)[1],
                    '', //$('h1').text().split(/[\|\,]/)[2],
                    '', //$('.contacts .icon-phone').text(),
                    '' //$('.contacts .icon-email').text()
            );
        }

        return false;
    });
    
    $('.miko-form-layer').each(function () {

        // group error messages into one (1) error block instead a list of several error blocks
        $(this).find('.error-msg + .error-msg').each(function () {

            var $prev = $(this).prev();
            var $ul = (function(container) {
                
                if (container.hasClass('grouped')) {
                    return container.find('ul');
                }

                container.addClass('grouped');

                var tempErr = container.text();
                var $ul = $('<ul/>').append([$('<li/>').text(tempErr)]);
                
                container.find('>div').html('Folgende Fehler sind aufgetreten:');
                container.find('>div').append($ul);

                return $ul;
                
            })($prev);

            $ul.append($('<li/>').text($(this).text()));
            $(this).remove();
        });

    });
});