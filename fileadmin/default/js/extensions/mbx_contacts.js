    // gmap3 for cooperation partner detail page
    
if (typeof gmap_coop != 'undefined') {

    var myAddresses = (function prepareAddresses() {

        var addresses = [];

        $.each(gmap_coop.address, function (i, s) {
            var trimedS = $.trim(s);
            if (trimedS.length == 0) {
                return;
            }
            addresses.push({
                address : trimedS,
                data    : trimedS
            });
        });

        return addresses;
    })();

    var defaults = {
        map: {
            options: {
                zoom: 10,
            }
        }
    };

    var initializeMap = function () {
        $('#jsCooperationPartnerDetailMap').gmap3($.extend(true, defaults,{
            marker: {
                values: myAddresses,
                options: {
                    icon: new google.maps.MarkerImage('https://' + document.location.host + '/fileadmin/default/images/map-icon.php?type=immo',new google.maps.Size(46, 44, "px", "px"))
                },
                events:{
                    click: function(marker, event, context) {
                        var map = $(this).gmap3("get"),
                                infowindow = $(this).gmap3({get: {name: "infowindow"}}),
                                dataPhone  = gmap_coop.phone,
                                dataUrl    = function() {
                                    val = gmap_coop.web;
                                    if (val && !val.match(/^http([s]?):\/\/.*/)) {
                                        return ('http://' + val);
                                    } else {
                                        return val;
                                    }
                                },
                                infowindowdata = '<div class="coopPartnerInfo"><p class="coopPartnerInfoAddress">' + context.data + '</p><a target="_blank" href="' + dataUrl() + '">' + dataUrl() + '</a></div>';
                        if (infowindow) {
                            infowindow.close();
                            infowindow.open(map, marker);
                            infowindow.setContent(infowindowdata);
                        } else {
                            $(this).gmap3({
                                infowindow: {
                                    anchor: marker,
                                    options: {content: infowindowdata}
                                }
                            });
                        }
                    }
                }
            },
            map: {
                options: {
                    mapTypeControl   : false,
                    streetViewControl: false
                }
            }
        }));
    };

    if (myAddresses.length > 1) {
        defaults.map.options.center = [52.518742, 13.403482];
        initializeMap();
    } else {
        var geocoder = new google.maps.Geocoder();

        geocoder.geocode({
            'address': myAddresses[0].address
        }, function(results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                defaults.map.options.zoom = 14;
                defaults.map.options.center = results[0].geometry.location;
                initializeMap();
            }
        });

    }
}


// gmap3 for social partner page
if (typeof gmap_social != 'undefined') {

    $("#jsSocialPartnerMap").gmap3({
        marker: {
            values: gmap_social,
            options: {
                draggable: false,
                icon: new google.maps.MarkerImage('https://' + document.location.host + '/fileadmin/default/images/map-icon.php?type=social',new google.maps.Size(46, 44, "px", "px"))
            },
            events: {
                click: function(marker, event, context) {

                    var map        = $(this).gmap3("get"),
                        infowindow = $(this).gmap3({get: {name: "infowindow"}}),
                        dataName   = context.data.name,
                        dataDesc   = context.data.description,
                        dataWeb    = context.data.web,
                        dataBuild  = '<div class="socialPartnerInfobox clearfix">\n'
                                   +    (typeof context.data.logo === 'string' ? '<img class="image" src="' + context.data.logo + '" />\n' : '')
                                   +    (typeof context.data.logo === 'string' ? '<div class="content hasImage">' : '<div class="content">')
                                   +        '<div class="name">' + dataName + '</div>\n'
                                   +        '<div class="description">' + dataDesc + '</div>\n'
                                   +        '<a href="' + dataWeb + '" class="url" target="_blank">' + dataWeb + '</a>\n'
                                   +    '</div>\n'
                                   + '</div>';

                    if (infowindow) {
                        infowindow.close();
                        infowindow.open(map, marker);
                        infowindow.setContent(dataBuild);
                    } else {
                        $(this).gmap3({
                            infowindow: {
                                anchor: marker,
                                options: {content: dataBuild}
                            }
                        });
                    }
                }
            }
        },
        map: {
            options: {
                center: [52.5585,13.508],
                zoom: 11,
                mapTypeControl: false,
                streetViewControl: false
            }
        },
        infowindow: {
            address: gmap_social[0].address,
            options: {
                content: '<div class="socialPartnerInfobox clearfix"><img class="image" src="' + gmap_social[0].data.logo + '" /><div class="content hasImage"><div class="name">' + gmap_social[0].data.name + '</div><div class="description">' + gmap_social[0].data.description + '</div><a href="' + gmap_social[0].data.web + '" class="url" target="_blank">' + gmap_social[0].data.web + '</a></div></div>'
            }
        }
    });
}