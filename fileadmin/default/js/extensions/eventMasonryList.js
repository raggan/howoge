$( document ).ready(function() {

    var currentDate = new Date();
    var currentMonth = currentDate.getMonth()+1;
    var currentYear = currentDate.getFullYear();

    var displayedMonth;
    var displayedYear;

    //init masonry list
    $('.grid').masonry({
        // options
        itemSelector: '.grid-item',
        stamp: '.stamp',
        columnWidth: 306,
        gutter: 10,
    });

    $(".next.btn").hide();

    //set status: active to filter button
    var urlParams;
    (window.onpopstate = function () {
        var match,
            pl     = /\+/g,  // Regex for replacing addition symbol with a space
            search = /([^&=]+)=?([^&]*)/g,
            decode = function (s) { return decodeURIComponent(s.replace(pl, " ")); },
            query  = window.location.search.substring(1);

        urlParams = {};
        while (match = search.exec(query))
            urlParams[decode(match[1])] = decode(match[2]);
    })();

    //set status: active to filter button
    if (urlParams['tx_news_pi1[overwriteDemand][categories]']) {
        if (urlParams['tx_news_pi1[overwriteDemand][categories]'] === '47') {
            // Howoge Events (Cat. ID 47)
            $("a.iconfont-star").addClass("active");
        } else if (urlParams['tx_news_pi1[overwriteDemand][categories]'] === '52') {
            // Tag der offenen Baustelle (Cat. ID 52)
            $("a.iconfont-bricks").addClass("active");
        }
    } else {
        $("a.iconfont-news").addClass("active");
    }

    $( "a.navigation.right" ).click(function(event) {
        event.preventDefault();
        showNextMonth();
    });

    $( "a.navigation.left" ).click(function(event) {
        event.preventDefault();
        showPrevMonth();
    });

    showMonth(currentMonth,currentYear);

    $("#my-calendar").zabuto_calendar({
        language: "de",
        weekstartson: 0,
        today: true,
        show_previous: true,
        show_next: true,
        nav_icon: {
            prev: "<img src='/fileadmin/default/images/cal_prev.png' class='nav-arrow'>",
            next: "<img src='/fileadmin/default/images/cal_next.png' class='nav-arrow'>"
        },
        data: JSON.parse(JSON.stringify(eventsForCalendar)),

    });

    $("#my-calendar").zabuto_calendar( { action: function() { myDateFunction(this.id); } } );


    $('.zabuto_calendar').on('click', '.day', function () {
        var date = $(this).attr("id"); //id contains date, id has flexibel length
        var subStringStart = date.length-14;
        var subStringEnd = date.length-4;
        var date = date.substring(subStringStart,subStringEnd);


        var events = findEventsWithDate(date);
        if (events != null){


            var contentString = '<div class="eventInfo">';

            $.each(events, function( index, event ) {
                if (event.oLink.length > 0){
                    event.link = event.oLink;
                }
                contentString +=
                    '<div class="event-block" id="text">'+
                    '<div id="title"><b>' + event.title + '</b><br/></div>'+
                    '<div id="description"><a href=' + event.link + '>Mehr erfahren</a></div>'+
                    '</div>'

            });

            contentString += '<span class="close">X</span>' + '</div>';

            $(this).append(contentString);
            $(".eventInfo .close").click(function () {
                $(".eventInfo").remove();
            })
        }


 /*       var event = findEventWithDate(date);
        if (event != null){
            if (event.oLink.length > 0){
                event.link = event.oLink;
            }
            var contentString =
                '<div class="eventInfo">'+
                    '<div class="inline-block" id="text">'+
                        '<div id="title"><b>' + event.title + '</b><br/></div>'+
                        '<div id="description"><a href=' + event.link + '>Mehr erfahren</a></div>'+
                    '</div>'+
                    '<span class="close">X</span>'+
                '</div>';
            $(this).append(contentString);

            $(".eventInfo .close").click(function () {
                $(".eventInfo").remove();
            })
        }*/
    });

    function findEventWithDate(date){
        var returnEvent = null;

        eventsForCalendar.forEach (function (event) {
            if (event.date === date){
                returnEvent = event;
            }
        })
        return returnEvent;
    }

    function findEventsWithDate(date){
        var returnEvents = [];

        eventsForCalendar.forEach (function (event) {
            if (event.date === date){
                returnEvents.push(event);
            }
        })
        return returnEvents;
    }

    function showMonth(month,year){
        //console.log('table.calendar[data-month="'+ month +'"][data-year="'+ year +'"]');
        if ( $('table.calendar[data-month="' + month + '"][data-year="' + year + '"]').length ) {
            displayedMonth = month;
            displayedYear = year;
            $('table.calendar').hide();
            $('table.calendar[data-month="' + month + '"][data-year="' + year + '"]').show();
        }
    }

    function showNextMonth(){
        if(displayedMonth<12){
            showMonth(displayedMonth+1,displayedYear)
        }else{
            showMonth(1,displayedYear+1)
        }
    }

    function showPrevMonth(){
        if(displayedMonth>1){
            showMonth(displayedMonth-1,displayedYear)
        }else{
            showMonth(12,displayedYear-1)
        }
    }
});

