Application = (function() {

    var init = function() {

        Application.modules.realestate.init();
    };

    return {
        init : init
    };

})();

Application.modules = Application.modules || {};
Application.modules.realestate = (function() {

    var init = function() {

        searchFormLib.init();
    };

    var searchFormLib = (function() {

        var $form;

        var init = function() {

            if(typeof $.fn.serializeObject !== 'function') {

                $.fn.serializeObject = function()
                {
                    var o = {};
                    var a = this.serializeArray();
                    $.each(a, function() {
                        if (o[this.name] !== undefined) {
                            if (!o[this.name].push) {
                                o[this.name] = [o[this.name]];
                            }
                            o[this.name].push(this.value || '');
                        } else {
                            o[this.name] = this.value || '';
                        }
                    });
                    return o;
                };
            }

            if($('#immo-results').length) {
                form.init();
            }
        };

        var getForm = function() {
            return $form;
        };

        var form = (function() {

            var latestConfiguration;
            var iUpdateTimer;
            var xhr;
            var inputTimeout = 500;

            var init = function() {

                $form = $('#content .re-searchform-wrapper form');

                if(getForm().length) {

                    storeCurrentFormConfiguration();

                    // reset the search timeout each time the user interacts with the searchform
                    $form.on('change', intervalCheck.reset);
                    $form.find('input, select').on('change', intervalCheck.reset);

                    intervalCheck.start();
                }
            };

            var intervalCheck = {
                start : function() {

                    if(typeof iUpdateTimer === 'number') {
                        window.clearInterval(iUpdateTimer);
                    }

                    iUpdateTimer = window.setInterval(function() {

                        if(detectConfigurationChange()) {
                            search.start();
                        }

                        storeCurrentFormConfiguration();

                    }, inputTimeout);
                }
                ,reset : function() {
                    intervalCheck.start();
                    search.cancel();
                }
            };

            var searchOverlay = {
                show : function() {

                    var $overlay;

                    // get existing search overlay ...
                    if($('#immo-results .search-overlay').length) {
                        $overlay = $('#immo-results .search-overlay');

                    // ...or create new search overlay
                    } else {

                        var $txt = $('<p/>').text('Die Ergebnisliste wird aktualisiert...');
                        var $esc = $('<p/>').text('[abbrechen]').click(search.cancel);

                        $overlay = $('<div/>').addClass('search-overlay');
                        $overlay.prependTo($('#immo-results'));
                        $overlay.append($('<div/>').append([ $txt, $esc ]));
                    }

                    $overlay.hide().fadeIn(500);
                }
                ,hide : function() {
                    $('#immo-results .search-overlay').fadeOut(50, function() {
                        $(this).remove();
                    });
                }
            };

            var search = {
                start : function() {

                    searchOverlay.show();

                    if(xhr && xhr.readystate !== 4){
                        xhr.abort();
                    }

                    var postData = getCurrentFormConfiguration();

                    postData['tx_mbxrealestate_pi1[search_realestate]'] = 'Suche anpassen';

                    xhr = $.ajax({
                        method: 'POST',
                        url: getForm().attr('action'),
                        data: postData

                    }).done(function (msg) {

                        // refresh the 'immoobjects' variable to display the new immoobject items on map
                        try {

                            var resImmoobjectsJSON = msg.match(/var\s+immoobjects\s+=\s+([^\;]*)\;\s+\<\/script/);

                            if(resImmoobjectsJSON !== null) {
                                immoobjects = $.parseJSON(resImmoobjectsJSON[1]);
                            }
                        } catch(e) {

                        }

                        var $html = $(msg);
                        var $immoResults = $html.find('#immo-results');

                        $('#immo-results').replaceWith($immoResults);

                        howImmo.init();

                        // this is duplicate code and exists already in scripts_dk.js !!!
                        jQuery('.dropdown-sort-by').dropDownMenu($.extend({}, mbxDropDownDefaults, {
                            labelType : 'keep'
                        }));

                        jQuery('.dropdown-per-page').dropDownMenu($.extend({}, mbxDropDownDefaults, {
                            labelType : 'remove',
                            hooks : {
                                onUpdateLabel : function() {

                                    var checkedInputs = this.find('.mbx-dropdown-list input').filter(function() {
                                        return $(this).prop('checked');
                                    });

                                    return jQuery(checkedInputs[0]).closest('li').text() + ' ' + this.data('title');
                                }
                            }
                        }));

                        jQuery('.dropdown-sort-by, .dropdown-per-page').each(function() {

                            var $form = $($(this).find('form').get(0) || $(this).closest('form').get(0));

                            $(this).change(function() {
                                $form.trigger('submit');
                            });

                            $form.find('input[type="submit"]').hide();
                        });

                        // avoid opacity on list-items which have no preview thumb images
                        $('#realestate-list .realestate-list-item').each(function() {

                            if($(this).find('.preview-thumbs div').length > 0) {
                                $(this).find('.immoresult_previmg').addClass('has-preview-thumbs');
                            }
                        });

                        searchOverlay.hide();

                    });
                }
                ,cancel : function() {

                    searchOverlay.hide();

                    if(xhr && xhr.readystate !== 4){
                        xhr.abort();
                    }
                }
            };

            var getCurrentFormConfiguration = function() {

                return getForm().serializeObject();
            };

            var storeCurrentFormConfiguration = function() {
                latestConfiguration = getCurrentFormConfiguration();
            };

            /**
             * Checks if the latest configurations differ from the current form input selections
             * @returns {Boolean}
             */
            var detectConfigurationChange = function() {

                return (JSON.stringify(latestConfiguration) !== JSON.stringify(getCurrentFormConfiguration()));
            };

            return {
                init : init,
                search : search,
                searchOverlay : searchOverlay
            };

        })();

        return {
            init : init,
            form : form,
            getForm : getForm
        };
    })();

    return {
        init : init,
        searchForm : searchFormLib
    };

})();

jQuery(document).ready(function($) {

    Application.init();

    var mainMenu = $('#header .main-nav'),
        gaButtonTracking = $('.re-searchform-wrapper .grey-gradient-container input[type="submit"],#jsContactFormWrap .btn,#job-application-form .button-wrap .btn'),
        cBoxIframe = $('.cboxElementIframe'),
        formDatepicker = $('form input.datepicker'),
        formTooltip = $('form .error [title], form .error[title]'),
        newsArchive = $('div#archive-select'),
        contentOverlay = $('<div id="content-overlay"/>').hide(),
        hash = new String(window.location.hash);

    function smart_scroll(el, offset) {
        offset = offset || 160; // manual correction, if other elem (eg. a header above) should also be visible

        var air         = 16; // above+below space so element is not tucked to the screen edge

        var el_pos      = $(el).offset();
        var el_pos_top  = el_pos.top - air - offset;

        $('html, body').animate({ scrollTop: (el_pos_top) }, 300);
    }

    if (typeof(ga) !== 'undefined' && gaButtonTracking.length > 0) {
        gaButtonTracking.on('click', function() {
            var btnLabel = $(this).val() ? $(this).val() : $(this).html();
            ga('send', 'event', 'button', 'click', btnLabel);
        });
    }

    /* change tab according to url hash */
    if ($('.nav-tabs').length && hash.search('#tab-') === 0) {
        $('.nav-tabs a[href$="' + hash.toString() + '"]').tab('show');
        $('html, body').animate({ scrollTop: 0 });
    }

    /* enable smooth scrolling for anchor links */
    if ($('a[href*="#c"]').length) {
        $('a[href*="#c"]').click(function(e) {
            if (e.currentTarget.pathname === window.location.pathname) {
                e.preventDefault();
                smart_scroll(e.currentTarget.hash);
            }
        });
    }

    /* enable smooth scrolling if hash is in url */
    if (hash.search('#c') === 0 || hash.toString() === '#newsstream') {
        if (parentTab = $(hash.toString()).parent('.tab-pane')) {
            $('.nav-tabs a[href$="' + parentTab.attr('id') + '"]').tab('show');
        }
        smart_scroll(hash.toString());
    }

    // main menu handling
    $('body').append(contentOverlay);

    mainMenu.find('> ul > li').hover(function() {
        var subMenu = $(this).find('> a + *');

        if (subMenu.length) {
            if (contentOverlay.is(':hidden')) {
                contentOverlay.show();
            }
        }
    }, function() {
        if (contentOverlay.is(':visible')) {
            contentOverlay.hide();
        }
    });
    contentOverlay.click(function() {
        if (contentOverlay.is(':visible')) {
            contentOverlay.hide();
        }
    });

    headerSearch = function() {
        var searchContainer = $('#jsHeaderSearch'),
            searchLink      = searchContainer.find('a'),
            searchForm      = searchContainer.find('form'),
            searchField     = searchContainer.find('input');

        searchLink.on('click', function(e) {
            if (searchContainer.hasClass('showSearch')) {
                searchForm.submit();
            } else {
                searchContainer.addClass('showSearch');
                searchField.focus();
            }
            e.preventDefault();
        });
    };
    if ($('#jsHeaderSearch').length) {
        headerSearch();
    }

    // load iCheck plugin if form with checkbox/radio is on page
    iCheck = function() {
        var combined = $('input[type="radio"]:not(".mbx-dropdown-wrapper input, .absolute-dropdown input"), input[type="checkbox"]:not(".mbx-dropdown-wrapper input, .absolute-dropdown input")');

        if (combined.length !== 0) {
            combined.iCheck({
                checkboxClass: 'icheckbox_minimal-blue',
                radioClass: 'iradio_minimal-blue',
                labelHover: false,
                cursor: true,
                increaseArea: '20%'
            });
        }
    };
    iCheck();

    $('#jsCooperationPartnerDetailImage a').colorbox({
        rel: 'images',
        maxWidth: '90%',
        maxHeight: '90%'
    });

    // init colobox iframes
    if (cBoxIframe.length) {
        var colorboxSettings = {
            transition: 'fade',
            innerWidth: '860px',
            innerHeight: '540px',
            opacity: '0.6'
        };

        cBoxIframe.each(function() {
            var el = $(this),
                    additionalSettings = {};

            if (el.hasClass('cboxIframeFlexHeight')) {
                $.extend(additionalSettings, {className: 'iframeFlexHeight'});
            }

            el.colorbox($.extend({}, colorboxSettings, additionalSettings));
        });
    }
    // handle iframe resizing for colorbox overlay
    if (parent.$('#colorbox.iframeFlexHeight').length) {
        parent.$.colorbox.resize({
            innerHeight: $('#desktop-wrapper').height()
        });
    }

    if ($(".info-container:not('.no-crop')")) {
        $(".info-container:not('.no-crop') > div").readmore({
            embedCSS: false,
            maxHeight: 90,
            moreLink: '<a href="#" class="icon-more">mehr laden &hellip;</a>',
            lessLink: '<a href="#" class="icon-less">schließen</a>'
        });
    }

    if (newsArchive) {
        var newsArchiveLabel = newsArchive.find('.mbx-dropdown-label'),
                newsArchiveList = newsArchive.find('.mbx-dropdown-list');

        newsArchive.find('> *').on('click', function() {
            if (newsArchiveList.is(':hidden')) {
                newsArchiveList.slideToggle('show');
                newsArchive.closest('.mbx-dropdown-wrapper').addClass('open');
            } else {
                newsArchiveList.hide();
                newsArchive.closest('.mbx-dropdown-wrapper').removeClass('open');
            }
        });
        newsArchive.find('a').on('click', function() {
            newsArchiveLabel.text($(this).text());
        });
    }

    if ($('#jsKiezmelderHeader').length && $('#jsKiezmelderHeader').data('backlink')) {
        var backBtnText = $('#jsKiezmelderHeader').data('backlink'),
            backBtn = $('<a id="jsBackBtn" href="#" class="back-link iconfont-carat-left spacer" />').html(backBtnText);

        $('#jsKiezmelderHeader .breadcrumb').after(backBtn);
        backBtn.click(function(event) {
            event.preventDefault();
            window.history.back();
        });
    }

    jQuery('#newsstream a.icon-more').click(function(event) {
        event.preventDefault();

        var currentLink = jQuery(this),
            linkUrl = jQuery(this).attr('href');

        currentLink.addClass('cyan').text('bitte warten');
        jQuery.ajax({
            url: linkUrl,
            type: 'get',
            success: function(response) {
                if (response) {
                    if (jQuery(response).find('a.icon-more').length) {
                        currentLink.attr('href', jQuery(response).find('a.icon-more').attr('href')).removeClass('cyan').html('mehr laden &hellip;');
                    } else {
                        currentLink.hide();
                    }
                    jQuery(response).find('#newsstream .article').insertBefore(currentLink).hide().fadeIn();
                }
            }
        });

    });

    if (formDatepicker) {
        var datepickerSettings = {
            dateFormat: 'dd.mm.yy',
            showWeek: true,
            firstDay: 1
        };

        formDatepicker.each(function() {
            var el = $(this),
                    additionalSettings = {};

            // configuration of birthday selection in application form
            if (el.hasClass('birthday')) {
                $.extend(additionalSettings, {
                    changeMonth: true,
                    changeYear: true,
                    defaultDate: '-30y',
                    minDate: '-65y',
                    maxDate: '-14y'
                });
            }

            if (el.hasClass('starting-date')) {
                $.extend(additionalSettings, {
                    minDate: 0,
                    maxDate: '+1y +6m'
                });
            }

            el.datepicker($.extend({}, datepickerSettings, additionalSettings));
        });
    }
    // Include form error tooltip
    if (formTooltip) {
        formTooltip.qtip({
            style: {
                classes: 'qtip-howoge qtip-rounded'
            },
            position: {
                my: 'bottom left',
                at: 'top left',
                viewport: jQuery(window),
                adjust: {
                    method: 'shift none'
                }
            },
            show: {
                solo: true
            }
        });
    }
    jQuery('.mbx-dropdown-pleaseselect').dropDownMenu({ labels: { noSelection: 'bitte wählen' } });
    jQuery('#constructionsite').dropDownMenu({ labels: { noSelection: 'bitte wählen' } });

    // change font size in header
    function changeFontSize() {
        var initSize = parseInt($('body').css('font-size'));

        $('#jsIncreaseFont').click(function(){
            var curSize = parseInt($('body').css('font-size')) + 2;

            if (curSize <= 16) {
                $('body').css('font-size', curSize);
            }
        });
        $('#jsDecreaseFont').click(function(){
            var curSize = parseInt($('body').css('font-size')) - 2;
            if(curSize >= 12) {
	        $('body').css('font-size', curSize);
            }
        });
        $('#jsResetFont').click(function(){
            $('body').css('font-size', initSize);
        });

    };
    if ($('#jsFontSize').length) {
        changeFontSize();
    }

    // expand or shrink questions at faq page
    function faqExpand() {
        var question = $('.faqQuestion');

        question.on('click', function() {
            var that = $(this);

            if (that.hasClass('expanded')) {
                that.stop().removeClass('expanded').next().slideUp(250);
            } else {
                that.stop().addClass('expanded').next().slideDown(250);
            }
        });
    };

    // show input fields depending on target selection at faq page
    function faqToogleTarget(select, updateCategories) {
        var rowAddress = $('#realestate-address-search'),
            rowCategory = $('#jsFaqRowCategory'),
            selectedOption = select.find(":selected").text(),
            selectedValue = select.find(":selected").val();

        if (selectedValue === '') {
            rowAddress.hide();
        } else if (selectedOption == 'Mieter') {
            rowAddress.slideDown(250);
            rowCategory.slideDown(250);
        } else {
            rowAddress.slideUp(250);
            rowCategory.slideDown(250);
        }

        if ($.active && updateCategories) {
            faqAjaxRequest.abort();
        }

        if (selectedValue && !$.active && updateCategories) {
            faqAjaxRequest = $.ajax({
                url:    document.location.href + '?type=2000',
                data:   select.closest('form').serialize(),
                beforeSend: function() {
                    rowCategory.find('.mbx-dropdown').remove();
                    rowCategory.find('select').show().prop('disabled','disabled').html('<option>Auswahl wird geladen</option>');
                    rowCategory.find('.mbx-dropdown-pleaseselect').removeClass('mbx-dropdown-wrapper').dropDownMenu({});
                    rowCategory.find('.mbx-dropdown').css('color', '#8d8d8d');
                },
                success: function(response) {
                    var responseHTML = jQuery.parseHTML(response.trim());

                    if (responseHTML) {
                        rowCategory.find('.mbx-dropdown-pleaseselect').replaceWith( $(responseHTML).find('#jsFaqRowCategory .mbx-dropdown-pleaseselect') );
                        rowCategory.find('.mbx-dropdown-pleaseselect').dropDownMenu({ labels: { noSelection: 'bitte wählen' } });
                    }
                }
            });
        }
    }

    // switch email & telephone field in contact form
    showContactForm = function() {
        var contactFormWrap = $('#jsContactFormWrap'),
            showFormTrigger = $('#jsContactFormExpand');

        if (showFormTrigger.length) {

            if (contactFormWrap.find('.text-success, form .error').length) {
                showFormTrigger.hide();
                contactFormWrap.show();
            } else {
                showFormTrigger.on('click', function(){
                    $(this).slideUp();
                    contactFormWrap.slideDown();
                });
            }

        }
    };

    // switch email & telephone field in contact form
    contactSwitch = function() {
        var emailField = $('#jsContactFormEmailField'),
            phoneField = $('#jsContactFormPhoneField'),
            emailSelect = $('#contact-by-email'),
            phoneSelect = $('#contact-by-phone');

        $(emailSelect).on('ifChecked', function(){
            phoneField.hide();
            emailField.show();
        });
        $(phoneSelect).on('ifChecked', function(){
            emailField.hide();
            phoneField.show();
        });
    };

    // show email or phone field in contact form at faq page
    if ($('#jsFaqSelectTarget').length) {
        var faqTarget = $('#jsFaqSelectTarget'),
            faqTargetOldValue = faqTarget.find(':selected').val(),
            faqUpdateCategories = false,
            faqAjaxRequest = null;

        faqTarget.parent('.absolute-dropdown').dropDownMenu({
            labels: { noSelection: 'bitte wählen' },
            hooks:  {
                onUpdateLabel:  function() {
                    if (faqTargetOldValue !== faqTarget.find(':selected').val()) {
                        faqTargetOldValue = faqTarget.find(':selected').val();
                        faqUpdateCategories = true;
                    } else {
                        faqUpdateCategories = false;
                    }

                    faqToogleTarget(faqTarget, faqUpdateCategories);
                }
            }
        });

        if ($('#jsFaqExpand').length) {
            faqExpand();
        }

        showContactForm();
        if ($('#contact-form').length) {
            contactSwitch();
        }
    }

    // Facebook Image Feed
    // 'facebookImageFeedAlbumId' kommt aus news-module partial oder fluid content: FacebookImageFeed.html
    function facebookImageFeed() {
        $.ajax({
            url: '/fileadmin/default/js/lib/helper-plugins/fb-graph-image-feed.php',
            data: {
                albumId:facebookImageFeedAlbumId
            },
            dataType: 'json',
            success: function(album) {

                try {

                    $.each(album.data, function(u, photo) {
                        $('#jsFbImageFeedList').append('<li><a href="' + photo.source + '" rel="fbImageFeed"><img src="' + photo.picture + '" /></a></li>'); // photo.images[7].source
                    });

                    var loaded = 0;

                    $('#jsFbImageFeedList img').on('load', function() {
                        loaded++;
                        if (loaded === $('#jsFbImageFeedList img').length) {
                            $('#jsFbImageFeedList a').colorbox({
                                maxWidth: '90%',
                                maxHeight: '90%'
                            });
                            $('#jsFbImageFeed').css('max-height', '5000px');
                            $('#jsFbImageFeedOverlay').fadeOut(250);
                        }
                    });

                } catch(e) {

                }
            }
        });
    }
    if (typeof facebookImageFeedAlbumId !== "undefined") {
        facebookImageFeed();
    }

    // Vitallauffest submittedok displayed
    // track via google analytics!
    if($('#c1100 .success-text').length === 1) {

        var urlRes = document.location.search.match(/\origin\=facebook\-(ad|post)/);

        if(urlRes !== null) {
            var gaValue = urlRes[1];
            var params = ['send', 'pageview', document.location.pathname + '?origin=facebook-' + gaValue + '&submitted'];

            var i = window.setInterval(function() {

                if(typeof ga === 'function') {
                    window.clearInterval(i);

                    ga.apply(this, params);

                    console.log('track event via google analytics');
                    console.log(params);
                }
            },500);
        }
    }
});

jQuery(window).load(function() {
    /* dynamic height for column box */
    dynamicContent.init();

    /* Style Navigation by offset in History */
    function contactBoxFixed() {
        var contactBox       = $('#jsContactBoxFixed'),
            contactBoxHeight = contactBox.height(),
            contactBoxPosX   = contactBox.offset().left - 20,
            contactBoxPosY  = contactBox.offset().top,
            headerHeight     = $('#header').height(),
            margin           = 40,
            headerHeightCalc = headerHeight + margin,
            teaserBottom     = $('#jsTeaserBottom'),
            teaserBottomHeight = teaserBottom.height();
            teaserBottomPosY = teaserBottom.offset().top,
            teaserBottomCalc = teaserBottomPosY - margin; // reduces top offset of teaser bottom by {margin} to fade out contact;

        $(window).scroll(function(){
            var scroll = $(window).scrollTop();

            // fired when contact box is on top of viewport. toggles position fixed
            if (scroll >= (contactBoxPosY - headerHeightCalc)) {
                contactBox.css({
                    'position':'fixed',
                    'top':headerHeightCalc,
                    'left':contactBoxPosX
                });
            }

            // fired when contact box is on the edge of teaser bottom. toggles position absolute
            if (scroll >= (teaserBottomCalc - contactBoxHeight - headerHeightCalc)) {
                contactBox.removeAttr('style').css({
                    'position':'absolute',
                    'bottom':teaserBottomHeight + margin
                });
            }

            // reset to position static if contact box is not near the viewport top
            if (scroll <= (contactBoxPosY - headerHeightCalc)) {
                contactBox.removeAttr('style');
            }
        });
    };

    if ($('#jsContactBoxFixed').length) {
        contactBoxFixed();
    }

    $('.category-list.sticky').stick_in_parent({parent: $('#content')});
});

var dynamicContent = {
    init: function() {
        var resizeContainer = jQuery('.column-box-3, .column-box-4');

        jQuery(window).resize(function() {
            dynamicContent.setHeight(resizeContainer);
        });

        jQuery(window).trigger('resize');
    },
    setHeight: function(resizeContainer) {

        if (resizeContainer.length) {
            resizeContainer.each(function() {
                if (jQuery(this).find('.teaser').length) {
                    /* add element wrapper if more then one element */
                    var dynamicHeight = 0;

                    /* calculate highest element */
                    jQuery(this).children().each(function() {
                        if (parseInt(dynamicHeight) < parseInt(jQuery(this).find('.content').height())) {
                            dynamicHeight = jQuery(this).find('.content').height();
                        }
                    });

                    /* set height to value */
                    jQuery(this).children().each(function() {
                        jQuery(this).find('.content').height(dynamicHeight);
                    });
                }
            });
        }
    }
};
