jQuery(document).ready(function () {

    console.log(jQuery('.videowrapper.colorbox.cboxElement'))
    jQuery('.videowrapper.colorbox.cboxElement').colorbox({iframe: true, innerWidth: 640, innerHeight: 390});
});

jQuery(document).ready(function () {

    jQuery(document).on("scroll", function () {

        if (window.innerWidth > 1150) {
            var $image = jQuery('.img-moving');
            var position = $image.css('position');

            if (jQuery('#footer').isOnScreen()) {

                if (position !== 'absolute') {
                    $image.css('position', 'absolute');
                }
            } else {

                if (position !== 'fixed') {
                    $image.css('position', 'fixed');
                }
            }
        }
    });
});

$.fn.isOnScreen = function () {

    var win = $(window);

    var viewport = {
        top: win.scrollTop(),
        left: win.scrollLeft()
    };
    viewport.right = viewport.left + win.width();
    viewport.bottom = viewport.top + win.height();

    var bounds = this.offset();
    bounds.right = bounds.left + this.outerWidth();
    bounds.bottom = bounds.top + this.outerHeight();

    return (!(viewport.right < bounds.left || viewport.left > bounds.right || viewport.bottom < bounds.top || viewport.top > bounds.bottom));

};

$(document).ready(function() {

    var $form = $('#update-interests-form');

    if($form.length === 0) {
        return;
    }

    var $stickyFooter = $('.sticky-form-footer');
    var $txtTarget = $stickyFooter.find('.pull-left');

    var updateProjectsCount = function() {

        var count = $form.find('.project-list-item [type="checkbox"]:checked').length;
        var msg;

        if(count > 1) {
            msg = 'Sie interessieren sich für ' + count + ' Projekte';
        } else if(count === 1) {
            msg = 'Sie interessieren sich für ' + count + ' Projekt';
        } else {
            msg = 'Sie interessieren sich für kein Projekt. Mit Speichern der Auswahl werden Sie abgemeldet.';
        }

        $txtTarget.text(msg);
    };

    $form.find('[type="checkbox"]').on('change ifChanged', updateProjectsCount);


    var checkDSGVO = function() {
        if ($form.find('.dsgvo-checkbox [type="checkbox"]:checked').length==1){
            $form.find('.dsgvo-text').removeClass('error');
        }else{
            $form.find('.dsgvo-text').addClass('error');
        }
    };

    $form.find('.dsgvo-checkbox [type="checkbox"]').on('change ifChanged', checkDSGVO);

    // initial call
    updateProjectsCount();


    // scroll/sticky behaviour

    $stickyFooter.before($('<div/>').addClass('sticky-helper', true));

    var checkSticky = function() {

        var originalOffsetTop = $('.sticky-helper').offset().top;
        var scrollOffsetTop = $(window).scrollTop();
        var windowHeight = $(window).height();
        var barHeight = $stickyFooter.height();
        var bottom = scrollOffsetTop + windowHeight;

        if(bottom - barHeight < originalOffsetTop) {
            $stickyFooter.addClass('is-sticky');
        } else {
            $stickyFooter.removeClass('is-sticky');
        }
    };

    $(window).scroll(checkSticky);

    // initial call
    checkSticky();
});