$( document ).ready(function() {
    $("a.header-teaser.image-teaser.image-teaser-big[href='https://youtu.be/KEx4EKQ8wno']").colorbox({ href: 'https://www.youtube.com/embed/KEx4EKQ8wno', width: '600px', height: '400px', iframe: true });
    $("a.header-teaser.image-teaser.image-teaser-big[href='https://youtu.be/0TQLRd98s_4']").colorbox({ href: 'https://www.youtube.com/embed/0TQLRd98s_4', width: '600px', height: '400px', iframe: true });
    $("a.header-teaser.image-teaser.image-teaser-big[href='https://youtu.be/9yyYgA6JAlM']").colorbox({ href: 'https://www.youtube.com/embed/9yyYgA6JAlM', width: '600px', height: '400px', iframe: true });
});
