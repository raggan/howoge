$( document ).ready(function() {
    hideAllShowX("#c6288");
});

function hideAllShowX($cID){
    $("#c6288,#c6498,#c6499,#c6500,#c6501").hide();
    $($cID).show();
}

function switchYouToDE() {
    $(".youtube-container iframe").attr("src", "https://www.youtube.com/embed/ueMEUvubmO4?showinfo=0");
    hideAllShowX("#c6288");
}

function switchYouToGB() {
    $(".youtube-container iframe").attr("src", "https://www.youtube.com/embed/lUpOeH5mAPg?showinfo=0");
    hideAllShowX("#c6498");
}

function switchYouToRU() {
    $(".youtube-container iframe").attr("src", "https://www.youtube.com/embed/WYlzizPk6i0?showinfo=0");
    hideAllShowX("#c6499");
}

function switchYouToAR() {
    $(".youtube-container iframe").attr("src", "https://www.youtube.com/embed/bBJ25cDpaWg?showinfo=0");
    hideAllShowX("#c6500");
}

function switchYouToVIET() {
    $(".youtube-container iframe").attr("src", "https://www.youtube.com/embed/ueMEUvubmO4?showinfo=0");
    hideAllShowX("#c6501");
}