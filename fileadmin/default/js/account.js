$(function() {
	var $mikoSlider = $('.miko-teaser-slider');
	if($mikoSlider.length > 0) {
		$mikoSlider.slick({
			arrows: false,
			dots: true,
			autoplay: true,
			autoplaySpeed: 5000
		});
	}

	var $mikoNewsSlider = $('.miko-news-slider');
	if($mikoNewsSlider.length > 0) {
		$mikoNewsSlider.slick({
			arrows: false,
			dots: true,
			autoplay: false,
			autoplaySpeed: 5000,
			infinite: false,
			slidesToShow: 3
		});
	}

	var $mikoLayer = $('.miko-layer');
	if($mikoLayer.length > 0) {
		var $mikoOverlay = $('.miko-overlay');
		$mikoLayer.on('click', '.close', function(e) {
			e.preventDefault();
			$mikoLayer.hide();
			$mikoOverlay.hide();
		});
	}
});